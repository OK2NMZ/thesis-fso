Generated using http://www.spiral.net/software/viterbi.html convolutional encoder/decoder C code generator
---OUTPUT---
Please wait while your code is being generated with the following parameters:
rate = 1/2
K = 7
polynomials = 109 79
frame length = 2048
isa = v1

To get the best performance on Intel-compatible platforms, we recommend using the latest Intel compiler with the -fast -fno-alias flags.
---OUTPUT---

