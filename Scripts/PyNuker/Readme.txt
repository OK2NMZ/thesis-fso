README

PyNuker 15.11.17
Marcus Dean Adams (marcusdean.adams@gmail.com)
17 November 2015

PURPOSE
----------------------------------------------------------------
The purpose of this software is to test the efficiency of your
network attached devices (servers normally in this case) while
under a heavy load.  It is more effective if multiple computers
target the same server.  This will help give you a rough idea of
how much crap your server can deal with, and still perform its
primary function(s).

PRE-REQUISITES
----------------------------------------------------------------
Python 3 or greater is required.

HOW TO USE
----------------------------------------------------------------
1) Run the pynuker.py executable, follow on-screen instructions.
2) Port numbers must be an integer.

Note: If you are using a Linux base operating system, make sure
to run the pynuker.py file in a terminal, there is no GUI.

WHAT DOES IT DO TO MY COMPUTER?
----------------------------------------------------------------
1) It sends UDP packets containing the specified text to the
   specified target.  This is just as stressful on the source
   computer as it is the target, so your computer may slow down
   while the stress test is running.  Besides that, no data is
   actually stored or manipulated on your computer, beyond storing
   some variables in RAM for the duration of execution.

THINGS TO REMEMBER
----------------------------------------------------------------
Python 3 must be installed.  If you're using Linux or any other
Unix based system, you're probably already good to go.  If you're
using Windows, and you haven't already done so, you need to install
python.  Go to www.python.org and download the latest 3.x version
available.

Does not function properly if you're using Python 2 due to
differences in syntax.

Colored text may not work properly in Windows.  The program will still
function just fine, you just won't have colored text.

All of my software "versioning" is done according to the day I release it.
The format is YY.MM.DD, not including zeroes.  So for example, version
12.1.22 was published on January 22nd, 2012.
