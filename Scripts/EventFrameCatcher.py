import matplotlib.pyplot as plt
import numpy
import socket
import sys
from random import randint
import csv
import time

def update_line(handle, x, y):
	handle.set_xdata(numpy.append(handle.get_xdata(), x))
	handle.set_ydata(numpy.append(handle.get_ydata(), y))
	plt.draw()

def bytes2int(str):
 return int(str.encode('hex'), 16)


UDP_IP = "0.0.0.0" # listen an all available IPs
UDP_PORT = 10000 # Event Frames port

print ("Event Frame Plotting, listening at port %d" % UDP_PORT)

sock = socket.socket(socket.AF_INET, # Internet
			socket.SOCK_DGRAM) # UDP

sock.bind((UDP_IP, UDP_PORT))
sock.settimeout(0.3)

plt.ion()
fig = plt.figure(1)
plt.xlabel('t [s]')

p1, = plt.step([],[], label="LINK_IS_DOWN")
p2, = plt.step([],[], label="ERR_FRAMING")
p3, = plt.step([],[], label="ERR_RS_BAD_BLOCK")
p4, = plt.step([],[], label="ERR_8B10B")
p5, = plt.step([],[], label="ERR_ALIGN")

plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=0,
           ncol=3, mode="expand", borderaxespad=0.)

p5.axes.set_ylim(-0.1, 9.1)
p5.axes.set_yticks( numpy.arange(0))


csvfile = open('%d.csv' % int(time.time()), 'w')
csvwriter = csv.writer(csvfile)

def handle_close(evt):
	csvfile.close()
	exit(0)

fig.canvas.mpl_connect('close_event', handle_close)
fig.canvas.set_window_title('Event Frame Catcher')
t = 0
packets = 0
info_frame = [0,0,0,0,0]
t0 = 0


# Read old measurement and continue with it
if len(sys.argv) > 1:
	try:
		csvreadfile = open(sys.argv[1], 'r')
		csvreader = csv.reader(csvreadfile)
		
		while True:
			therow = csvreader.next()
		
			t = int(therow[1])
	
			framedata = int(therow[0], 16)<<3

			#Extract the signal values
			for i in xrange(5):
				info_frame[i] = (framedata >> (7-i)) & 1


			if t0 == 0:
				print ("Init!")
				t0 = t
				last_info_frame = info_frame
				last_data0 = framedata
				t = 1
			else:
				t = t-t0

			for i in xrange(5):
				last_info_frame[i] = (last_data0 >> (7-i)) & 1
			tlast = t-1
			tlast = tlast*8*(10**-9)
			update_line(p1, tlast, last_info_frame[0])
			update_line(p2, tlast, 2+last_info_frame[1])
			update_line(p3, tlast, 4+last_info_frame[2])
			update_line(p4, tlast, 6+last_info_frame[3])
			update_line(p5, tlast, 8+last_info_frame[4])
	
			t = t*8*(10**-9)
			update_line(p1, t, info_frame[0])
			update_line(p2, t, 2+info_frame[1])
			update_line(p3, t, 4+info_frame[2])
			update_line(p4, t, 6+info_frame[3])
			update_line(p5, t, 8+info_frame[4])
			p1.axes.relim()
			p1.axes.autoscale_view(tight=False, scalex=True, scaley=False)
			last_data0 = framedata
			
	except StopIteration:
		plt.pause(0.001)
		csvreadfile.close()
	except:
		print("Could not load csv file!")
		exit(-1)


# Capture loop
while True:
	try:
		received, addr = sock.recvfrom(8)
		packets+=1
		plt.title('Channel Monitoring    (Received %d information frames)' % packets)

		#Convert to byte array
		data = bytearray(received)

		#print ("Received 0x%08X, 0x%08X, 0x%08X, 0x%08X, 0x%08X, 0x%08X, 0x%08X, 0x%08X" %
		#	(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]))
	
		#Extract the tick
		#           MSB                                                                     LSB
		tickdata = [data[0]&0x07, data[1], data[2], data[3], data[4], data[5], data[6], data[7]]
		t = bytes2int(str(bytearray(tickdata)))

		#Extract the signal values
		for i in xrange(5):
			info_frame[i] = (data[0] >> (7-i)) & 1

		tocsv = ["0x%02X" % ((data[0]>>3)&0x1f), "%d" % t]
		#print("Writing to csv:")
		#print(tocsv)
		csvwriter.writerow(tocsv)

		if t0 == 0:
			print ("Init!")
			t0 = t
			last_info_frame = info_frame
			last_data0 = data[0]
			t = 1
		else:
			t = t-t0

		#print ("Transition from %X to %X at %d" % ((last_data0>>3)&0x1f, (data[0]>>3)&0x1f, t))

		for i in xrange(5):
			last_info_frame[i] = (last_data0 >> (7-i)) & 1
		tlast = t-1
		tlast = tlast*8*(10**-9)
		update_line(p1, tlast, last_info_frame[0])
		update_line(p2, tlast, 2+last_info_frame[1])
		update_line(p3, tlast, 4+last_info_frame[2])
		update_line(p4, tlast, 6+last_info_frame[3])
		update_line(p5, tlast, 8+last_info_frame[4])	
		
		t = t*8*(10**-9)
		update_line(p1, t, info_frame[0])
		update_line(p2, t, 2+info_frame[1])
		update_line(p3, t, 4+info_frame[2])
		update_line(p4, t, 6+info_frame[3])
		update_line(p5, t, 8+info_frame[4])
		last_data0 = data[0]
	except socket.timeout:
		p1.axes.relim()
		p1.axes.autoscale_view(tight=False, scalex=True, scaley=False)
		plt.pause(0.00001)
	




