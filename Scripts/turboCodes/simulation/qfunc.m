function out=qfunc(x)
%% Function to compute the Q-function that is used in lots of applications
% it is simply be calculated in Matlab using the erfc function which is a
% Matlab built-in function
% Montadar Abas Taher
% ------10 April 2012---------
out=erfc(x/sqrt(2))/2;
%end of function