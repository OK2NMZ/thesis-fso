import socket

UDP_IP = "192.168.2.255"
UDP_PORT = 5005
MESSAGE = "1234567890123456789012345678901234567890123456789012345678\
"
print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)
print("Message:", MESSAGE)
print("Length: %d" % len(MESSAGE))

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#We can broadcast, if broadcast addressed used
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

#Keep sending forever
while True:
	sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
