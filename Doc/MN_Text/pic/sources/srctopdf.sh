#!/bin/bash

if [[ "$1" == "clean" ]] && [[ "$2" != "" ]] && [[ "$3" != "" ]]; then
	pids=""
	for filename in $2/*.dia; do
		filebase="${filename##*/}"
		filebase=${filebase%.*}
		rm "$3/$filebase.pdf" &> /dev/null &
		pids="$pids $!"
	done
	for filename in $2/*.svg; do
		filebase="${filename##*/}"
		filebase=${filebase%.*}
		rm "$3/$filebase.pdf" &> /dev/null &
		pids="$pids $!"
	done
	echo "Cleaning generated *.pdf in $2/"
	wait $pids
	echo "Clean done!"
elif [[ "$1" == "conv" ]] && [[ "$2" != "" ]] && [[ "$3" != "" ]]; then
	pids=""
	for filename in $2/*.dia; do
		filebase="${filename##*/}"
		filebase=${filebase%.*}
		inkscape "$filename" "--export-pdf=$3/$filebase.pdf" &
		pids="$pids $!"
	done
	for filename in $2/*.svg; do
		filebase="${filename##*/}"
		filebase=${filebase%.*}
		inkscape "$filename" "--export-pdf=$3/$filebase.pdf" &
		pids="$pids $!"
	done
	echo "Converting *.dia and *.svg in $2 to *.pdf in $3"
	wait $pids
	echo "Conversion done!"
else
	echo "Usage:                                              "
	echo "$0 clean path out ... cleans the generated pdfs       "
	echo "$0 conv path out  ... generates .pdf to out from .dia and .svg in path"
fi

