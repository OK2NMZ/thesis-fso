
\chapter{Laser Beam Propagation}
In order to understand, how certain parameters of the communication system affect the scintillation index and consequently the bit-error ratio, let's define a commonly used laser beam model - the Gaussian beam. 
\section{Gaussian Beam}
In optics, the Gaussian beam is a particular solution of the Helmholtz equation \ref{helmholtzequ} (same as a planar wave). This model produces the best description of coherent radiation such as the laser beams, even if it does not describe completely the effect of diffraction.
\begin{align}\label{helmholtzequ}
\nabla^2 A + k^2 A = 0 \\
\intertext
{
Where $\nabla^2$ is the Laplacian, $A$ is the amplitude and $k$ is the wavenumber. 
}
\nonumber
\end{align}

More specifically, a Gaussian beam is a beam, whose transversal profile evolution of amplitude in function of the spatial propagation is proportional to a Gaussian function.

\subsubsection{Definition}
There are multiple ways to define the Gaussian beam. Historically, the Gaussian beams were used in optics as a solution to the Helmholtz equation \ref{helmholtzequ} using the paraxial approximation, which allows only a small divergence of the beam towards its axis of propagation. The maximum allowed angle is about 20 degrees.
\par
There are other approaches coming from the electromagnetism allowing to obtain a formulation of the Gaussian beam. This way, we can define monomode and multimode Gaussian beams as a particular case in the paraxial approximation of one or multiple complex sources of rays.\citep{Deschamps1971}
\section{Propagation of Gaussian Beam}
The approximation done here presumes scalar Gaussian beam, where the electric field is considered linearly polarised in a direction orthogonal to its direction of propagation. This approximation gives good results when the beam waist $w_{0}$ is superior to the wavelength. If this constraint is not met, a simple scalar description is not valid.
\par
Assuming propagation in the $z$ direction ($+z$) and polarization in the $x$ axis, the electric field in phasor notation is given by \ref{electricgauss}.
\begin{align}\label{electricgauss}
\textbf{E}(r, z) = E_{0}\hat{x}\frac{w_{0}}{w(z)}\exp(\frac{-r^2}{w(z)^2})\exp(-i(kz+k\frac{r^2}{2R(z)}-\psi(z))) \\
\intertext
{
Where: \citep{Svetlo2010}
}
\shortintertext
{
$r$ is the radial distance from the center axis of the beam 
}
\shortintertext
{
$z$ is the axial distance from the beam's waist 
}
\shortintertext
{
$i=sqrt(-1)$ 
}
\shortintertext
{
$k=2\pi/\lambda$ is the wave number 
}
\shortintertext
{
$w(z)$ is the radius at which the field amplitudes fall to $1/e$ of their axial values, at the plane $z$ along the beam 
}
\shortintertext
{
$w_{0}=w(0)$ is the waist size 
}
\shortintertext
{
$R(z)$ is the radius of curvature of the beam's wavefronts at z 
}
\shortintertext
{
$\psi(z)$ is the Gouy phase at z
}
\shortintertext
{
$\hat{x}$ is the unit vector in the x direction
}
\nonumber
\end{align}

More commonly, however, the intensity or irradiance is used to describe the beam, which is more easily measurable, since it averages the electrical field over time per \ref{intensitygauss}.
\begin{align}\label{intensitygauss}
I(r, z) = \frac{\left | \textbf{E}(r, z)) \right |^2}{2\eta } = I_{0}(\frac{w_{0}}{w(z)})^2\exp(\frac{-2r^2}{w^2(z)}) \\
\intertext
{
Where $\eta$ is the characteristic impedance of the medium ($\eta_{0}=377 \Omega$ for free space).
}
\intertext
{
Thus $I_{0}=E_{0}^2/2\eta$ is the intensity at its waist, at the centre of the beam.
}
\nonumber
\end{align}

\subsection{Evolving Parameters}
In the equations \ref{electricgauss} and \ref{intensitygauss}, several parameters are evolving along the beam following the $z$ axis.
\begin{align}\label{beamwidth}
w(z)=w_{0}\sqrt{1+(\frac{z}{z_{0}})^2} \\
\intertext
{
Where $w(z)$ is denoted as "evolving beam width" and $z_{0}$ is called the Reyleigh range \ref{rayrange}. This parameter defines the spot size at position $z$.
}
\nonumber
\end{align}

\begin{align}\label{radiuscurvature}
R(z) = z\left [ 1+(\frac{z_{0}}{z})^2 \right ] \\
\intertext
{
This parameter is called "evolving radius of curvature" and defines the curvature of the wavefronts. At the beam waist and as $z$ approaches infinity, the $R(z)$ approaches infinity as well. For $z=z_{0}$, we obtain $R(z_{0})=2z_{0}$.
}
\nonumber
\end{align}

Next parameter is the Gouy phase, which is not normally observable during experiments, but has its importance in theoretical description of the Gaussian beam. \ref{gouy}

\begin{align}\label{gouy}
\psi(z)=\arctan(\frac{z}{z_{0}})\\
\intertext
{
This parameter, called Gouy phase, changes between $-\pi/2$ and $+\pi/2$ and explains the apparition of higher-order Gaussian modes.\citep{Paschotta2008}
}
\nonumber
\end{align}


\subsection{Beam parameters}
The shape of the beam is determined by multiple parameters - wavelength $\lambda$, beam waist $w_{0}$. All other subparameters depend on these two parameters.

\subsubsection{Rayleigh range, confocal parameter and beam divergence}

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=8.5cm,height=8.5cm,keepaspectratio]{pic/gaussbeam}
\end{center}
\caption{Gaussian beam parameters \citep{Wikipediathefreeencyclopedia}}
\label{gaussbeam}
\end{figure}


\begin{align}\label{rayrange}
z_{0}=\frac{\pi w_{0}^2}{\lambda}\\
\intertext
{
This parameter is called Rayleigh distance or Rayleigh range. It defines a point, where the wavefront curvature is greatest $(1/R)$.\citep{Wilfert} Rayleigh distance is sometimes used in form of confocal parameter ($b = 2z_{0}$).
}
\nonumber
\end{align}

\begin{align}\label{beamdiv}
\theta\approx \frac{\lambda}{\pi w_{0}} [rad] \\
\intertext
{
The divergence of the beam is a parameter defined by the angle between the central axis of the beam and the lines of the cone, which forms the edge of the beam for $z \gg z_{0}$.
}
\nonumber
\end{align}


\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=8cm,height=8cm,keepaspectratio]{pic/gaussbeam2d}
\end{center}
\caption{Gaussian beam spot measured by a detector \citep{Nakamura2010}}
\label{gaussbeam2d}
\end{figure}

\section{System Parameters Influencing the BER}
In previous section, a collimated beam was assumed. Let's now generalize the relation \ref{beamwidth} to a situation, where the focusing parameter \ref{focpar} is not equal to 1 ($\hat{r} \neq 1$). This will cause the beam width to grow faster as in case of the collimated beam. Obviously, when the beam width becomes larger, a bigger receive optics are required otherwise the received optical intensity is smaller, which induces a worse SNR.
\par
Another parameter influencing the BER is the length of the link. Longer the link, greater the scintillation index \ref{scintillationindex}, as shown in figure \ref{scintillationpicture}.\citep{Majumdar2008}

\subsection{Optical Aperture Parameters}

The generalized form for the beam size is expressed by \ref{beamwidthgen}. 

\begin{align}\label{beamwidthgen}
w(z)=w_{0}\sqrt{(\hat{r}^2+\zeta \hat{z}^2)} \\
\intertext
{
Where $\hat{r}$ is the focusing parameter defined in \ref{focpar}, $\zeta$ is the global coherence parameter defined in \ref{globcoherence} and $\hat{z}$ is distance with its diffractive component ($\hat{z}_{d}$) defined in \ref{difdis}. We have $\hat{z}=\frac{z}{\hat{z}_{d}}$.
}
\nonumber
\end{align}

\begin{align}\label{focpar}
\hat{r}(z) = \frac{R_{0}-z}{R_{0}} \\
\intertext
{
Where $R_{0}$ is the radius of curvature at optimum focusing. This parameter is called focusing parameter, the hat symbol expresses it is a vector.
}
\nonumber
\end{align}

\begin{align}\label{globcoherence}
\zeta = \zeta_{S} + \frac{2w_{0}^2}{\rho_{0}^2} \\
\intertext
{
This parameter is called global coherence parameter and have two additive components - $\zeta_{S}$, which describes spatial coherence properties of the signal-carrying laser beam when leaving the transmitter ($\zeta_{S}=1$ for coherent beam, $\zeta_{S} > 1$ for partially coherent beam). The second term depends on  $\rho_{0}$, which is a coherence length of a spherical wave propagating in optical turbulence as defined in \ref{cohlen}.
}
\nonumber
\end{align}


\begin{align}\label{cohlen}
\rho_{0}=(0.55C_{n}^2 k^2 z)^{-3/5} \\
\intertext
{
This parameter is called coherence length and in case optical turbulence is present (anywhere in the Earth's atmosphere), it depends on the refractive index structure parameter $C_{n}^2$. The $C_{n}^2$ parameter depends on the atmosphere propreties, thermal turbles presence and their dynamic propreties.
}
\nonumber
\end{align}

\begin{align}\label{difdis}
\hat{z}_{d} = \frac{k w_{0}^2}{2} \\
\intertext
{
This parameter is called the diffractive distance and it depends on the beam size at the origin.
}
\nonumber
\end{align}

How the coherence propreties of the transmitted beam ($\zeta_{S}$) involve increase in the beam size is depicted in the figure \ref{beamwidthpicture}. As already mentioned, widening the beam size means losses at the receiving aperture, since it cannot grow without limitation (economic reason, feasibility etc.).

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=9.5cm,height=9.5cm,keepaspectratio]{pic/beamwidthpicture}
\end{center}
\caption{Collimated beam size as a function of range for several choices of phase diffuser ($\lambda = \unit{0.785}{\micro\meter}$, $w_{0}=\unit{2.5}{\centi\meter}$ and $C_{n}^2 = \unit{10^-14}{\meter^{-2/3}}$)\citep{JenniferC.RicklinStephenM.HammelFrankD.Eaton2006}}
\label{beamwidthpicture}
\end{figure}

\subsection{Scintillation Index}
\label{scintindx}
While the generalized formula for the beam width (\ref{beamwidthgen}) implies, that in case of bad coherence, the received intesity will diminuate faster in function of the range than in case of coherent beam. This would not be the major problem if an optical noise component were not present. This component can be measured and expressed by a parameter called scintillation index and is defined in \ref{scintillationindex}. This parameter depends on the refractive index structure parameter $C_{n}^2$, which can be seen in the figure \ref{scintillationpicture}.


\begin{align}\label{scintillationindex}
\sigma_{\ln S}^2 = \frac{\left \langle I^2(r, z) \right \rangle}{\left \langle I(r, z)) \right \rangle ^2}-1 
\\ \nonumber
\end{align} 


\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=10cm,height=10cm,keepaspectratio]{pic/scintillationpicture}
\end{center}
\caption{Scintillation index in function of the range ($ \hat{r} = 1$, $ \rho_{S} = 100$, $ \lambda = \unit{1.55}{\micro\meter} $, $ w_{0} = \unit{2.5}{\centi\meter} $)\citep{JenniferC.RicklinStephenM.HammelFrankD.Eaton2006}}
\label{scintillationpicture}
\end{figure}

The scintillation index can be reduced by increasing the receiving lens diameter. However, this solution does not work well for a higher $C_{n}^2$ parameter value, as it can be seen at the figure \ref{apertureavg}.

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=10cm,height=10cm,keepaspectratio]{pic/apertureavg}
\end{center}
\caption{Aperture averaged scintillation index as function of channel distance ($\hat{r}=1$, $\rho_{S}=100$, $\lambda=\unit{1.55}{\micro\meter}$, $w_{0}=\unit{2.5}{\centi\meter}$) \citep{JenniferC.RicklinStephenM.HammelFrankD.Eaton2006}}
\label{apertureavg}
\end{figure}

\subsection{BER in Function of Received Power}

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=10cm,height=10cm,keepaspectratio]{pic/powervsber}
\end{center}
\caption{Effect of optical turbulence on BER as a function of received optical power for collimated ($\hat{r}=1$) coherent ($\rho_{S}=1$) and partially coherent ($\rho_{S}=10$) beams ($\lambda=\unit{0.785}{\micro\meter}$, $w_{0}=\unit{2.5}{\centi\meter}$, $z=\unit{2000}{\meter}$, $D=\unit{10}{\centi\meter}$). Left to right (1) $\rho_{S} = 10, C_{n}^2=\unit{10^{-15}}{\meter^{-2/3}}$, (2) $\rho_{S} = 10, C_{n}^2=\unit{1.2 10^{-14}}{\meter^{-2/3}}$, (3) $\rho_{S} = 1, C_{n}^2=\unit{10^{-15}}{\meter^{-2/3}}$, (4) $\rho_{S} = 1, C_{n}^2=\unit{1.2 10^{-14}}{\meter^{-2/3}}$ \citep{JenniferC.RicklinStephenM.HammelFrankD.Eaton2006}}
\label{powervsber}
\end{figure}

As we can see at the figure \ref{powervsber}, the BER depends mainly on the $P_{opt}[dBm]$, $\rho_{S}$ and the $C_{n}^2$ parameter.
\par
The first one can be adjusted by increasing or decreasing the transmitting power. A system designer would want to keep the received power under the saturation level of the receiver's PIN diode, but also to keep it as high as possible to be more in the right side of the figure \ref{powervsber}. 
\par
The second parameter can be changed by selecting an appropriate transmitting aperture. When we have $\rho_{S} \approx 10$, the receiver aperture averaging effect helps to reduce the scintillations and signal fades. Also selecting a greater receiver lens diameter can help to reduce the scintillations.
\par
The last parameter is determined by current state of the atmosphere and can have nearly no influence on the transmission during certain periods of the day time (from the evening until the morning, when it is cloudy etc.), but the same parameter can on the other hand have the strongest influence on the received signal - during a hot day for example. In the following chapter, common techniques to solve this problem will be resumed, and also the techniques planned to be used in the system will be announced and their benefits will be described.