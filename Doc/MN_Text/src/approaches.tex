\chapter{Approaches to Improve BER in FSO}
Based on previous chapters, several BER improving methods will be presented here and those which will be consequently used in the implementation of the system will be announced.

\section{Multiple Input, Multiple Output (MIMO)}
MIMO is a technique, which is used mostly in RF link designs to exploit the phonomenon of multipath propagation to increase the spectral efficiency of the communication. In case of FSO, the situation is however a little bit different - the main task is to mitigate the atmospheric turbulence. \par
MIMO principle is based on using M transmitters and N detectors. The signals from the N detectors are then summed either using equal gain for each branch or weighted gains. \citep{Wilson2005}  If the number of receivers approaches infinity a gain of up to \unit{25}{\deci\bel} over a single-input, single-output configuration can be attained. \citep{Lee2004}

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=11.5cm,height=11.5cm,keepaspectratio]{pic/MIMO}
\end{center}
\caption{Block diagram of M x N MIMO FSO system over atmospheric turbulence channel \citep{DuyA.Luong;TruongC.Thang;AnhT.Pham;2013}}
\label{MIMO}
\end{figure}
\par
In \citep{Duyen2014}, more realistic figures are obtained: for a given $C_{n}^2$ and link distance, a gain of \unit{5}{\deci\bel} at ASER of $10^{-5}$ is obtained when upgrading from SISO configuration to 2x2 MIMO or from 2x2 MIMO to 4x4 MIMO. What's more, the average spectral efficiency is improved by \unit{2}{b/s/Hz} in case of the upgrade. These results were obtained using the Monte Carlo simulation of a turbulent channel, results are given in \ref{ASER}.


\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=11.5cm,height=11.5cm,keepaspectratio]{pic/ASER}
\end{center}
\caption{ASER versus the average SNR of SISO, 2x2 MIMO and 4x4 MIMO FSO systems for various $C_{n}^2$. Link distance is $L = \unit{4000}{\meter}$. \citep{Duyen2014}}
\label{ASER}
\end{figure}
\par
The subject of this thesis is however not to exploit the MIMO technique, but it can be combined with the methods, which will be used to cooperatively improve the BER.

\section{The Aperture Averaging}
For weak to moderate level of atmospheric turbulence, increasing the diameter of the receiver lens can reduce the scintillation index $\sigma_{\ln S}^2$ as seen in \ref{apertureavg} and \ref{lambdaturbulences}. This technique however does not work well for strong level of turbulence and is not cost effective. It is however one of the most obvious techniques.

\section{Adaptive Optics}
Another technique to reduce the impact of atmospheric turbulence is the adaptive optics. This method consists of compensating the distortion of the beam wave-front adaptively according to the current beam wave-front. The realization of this can be made in the optical domain as well as in the electrical domain according to the picture \ref{adaptiveoptics}. The wave-front corrector can be implemented by a 132-actuator MEMS piston-type deformable mirror controlled by a VLSI system as in \citep{Weyrauch2004}. This method is however not aimed in this thesis.

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=13cm,height=13cm,keepaspectratio]{pic/adaptiveoptics}
\end{center}
\caption{Schematics of adaptive optics approaches for FSO communication. (a) Conventional adaptive optics uses a wave-front sensor (WFS) and wave-front reconstruction for control of the wave-front corrector. (b) Wave-front distortion compensation based on blind optimization of a system performance metric may use the received signal strength (determined from the communication signal after low-pass filtering) for the feedback \citep{Weyrauch2004}}
\label{adaptiveoptics}
\end{figure}

\section{Channel Coding}
Channel coding also known as forward error correction (FEC) is computionally one of the most complex techniques control the errors in data transmission. This approach was first applied by mathematician Richard Hamming in 1940s - the implementation is the well known Hamming(7, 4) linear eror-correction code. \citep{McGillSchoolofComputerScience} The principle resides in adding a redundant information in the data stream, which can be consequently used to recover the lost data.

\subsection{Reed-Solomon Coding}
The Reed-Solomon error correction coding was first released to public in 1960 and was defined by Irving S. Reed and Gustave Solomon. This code can be classified as non-binary cyclic error-correcting code. Even if Reed and Solomon did not cooperate with creators of BCH code, RS code is a subset of BCH code. 
\par
By adding $2t$ check symbols to the data, RS code is capable of detecting $2t$ erroneous symbols or to correct up to $t$ symbols.

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=11.5cm,height=11.5cm,keepaspectratio]{pic/rsclass}
\end{center}
\caption{Venn diagram to classify the RS code \citep{Geisel1996}}
\label{rsclass}
\end{figure}

If we have for example a message consisting of $k$ payload symbols and we want it to be transferred over an error-prone channel allowing up to $t$ errors to occur (up to $t$ erroneous symbols), we would need to send $n$ symbols over the channel as in equation \ref{rsbytes}.

\begin{align}\label{rsbytes}
n = k + 2t\\
\intertext
{
Where: \citep{Haiman2003}
}
\shortintertext
{
$n$ is the number of symbols, which will be transmitted to the channel
}
\shortintertext
{
$k$ is the number of payload symbols, which we want to transfer reliably
}
\shortintertext
{
$t$ is the number of errors we can fix in the RS decoder
}
\nonumber
\end{align} 
\par
As the RS codes are systematic block codes, the payload or let's say information bits are placed in the most significant byte positions of the resulting codeword and the remaining LSBs are computed by the encoder. As the RS codes support both errors and erasures to occur, some information symbols in the codeword can be erased before the actual transmission to reduce the coding rate. This way the number of tolerable errors is however reduced accordingly. Having MSBs be sent first to the medium, the figure \ref{rsencoder} shows a conceptual schematic of a RS encoder. After all payload symbols are sent to the encoder, the switch changes its state and starts outputting the parity symbols. \citep{SubhashreeDas2011}


\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=11.5cm,height=11.5cm,keepaspectratio]{pic/rsencoder}
\end{center}
\caption{Block diagram of RS encoder \citep{SubhashreeDas2011}}
\label{rsencoder}
\end{figure}

\par
On the other side of the channel, at the receiver, the decoding occurs in five steps:

\begin{itemize}
  \item Syndrome calculation
  \item Determine error-location polynomial
  \item Solving the error locator polynomial - Chien search
  \item Calculating the error magnitude
  \item Error correction
\end{itemize}

Each step is further discussed in \citep{SubhashreeDas2011}. The figure \ref{rsdecoder} shows the flow of results in the decoder. The decoder is the most complex part of the RS code implementation and will be in detail discussed in the implementation section of this thesis.

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=12.5cm,height=12.5cm,keepaspectratio]{pic/rsdecoder}
\end{center}
\caption{Architecture and data flow in an RS decoder \citep{SubhashreeDas2011}}
\label{rsdecoder}
\end{figure}

\subsection{Convolutional Coding}
Convolutional codes have an advantage over the block codes such as the mentioned Reed Solomon codes - the payload does not have to be framed in blocks. This implies multiple practical advantages:

\begin{itemize}
  \item The data does not have to be aligned to the block boundary - random length data can be send without additional logic processing it
  \item The frame/block synchronization does not need to be present
\end{itemize}

We have the advantage to use the 8B10B coding in our application, so the data is already extracted from the channel and message boundaries are recovered. In this case, the only disadvantage of the block codes is a padding at the end of the last block forming the decoded application frame. If this disadvantage would need to be further mitigated, a frame-merging logic can be implemented so only in case of an interruption of the data stream a padding would need to be added.
\par
The realization of convolutional encoder is quite simple - the input data is fed in a shift register, whose outputs are then wired to modulo two adders (XOR gates), which creates $k$ output data bits from $n$ input data bits. The resulting coding rate is therefore $n/k$. If $n$ is desired to be different from $n$ in the coding rate, the output data stream needs to be punctured - output data stream bits are erased following an erasure patern vector.

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=11.5cm,height=11.5cm,keepaspectratio]{pic/convencoder}
\end{center}
\caption{Example of convolutional encoder with $k = 1/2$}
\label{convencoder}
\end{figure}

\par
The decoding is again the most complex process and cannot be described in a single paragraph as the encoder. The most commonly used algorithm for decoding convolutional code is the Viterbi algorithm \ref{viterbi}, which is the most efficient one. Another alternative is the Fano algorithm. The principle consists in searching for the most likely binary sequence, which is further described in following subsection.

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=9.5cm,height=9.5cm,keepaspectratio]{pic/viterbi}
\end{center}
\caption{Example of Viterbi decoder decoding process \citep{Gerstlauer2009}}
\label{viterbi}
\end{figure}

\subsection{MLSE}
MLSE stands for Maximum Likelihood Sequence Estimation and it is a method capable of reconstructing a signal with erroneous bits in it. This task can be accomplished by using the Viterbi decoder.
\par
Let's explain how Viterbi decoder works following the example from the previous subsection. (\ref{viterbi} and \ref{convencoder}) The chosen convolutional code will create the output binary stream according to table \ref{viterbitable}. As you can see, only one half of the transitions is allowed (due to 1/2 coding rate). 

\begin{table}[]
\centering
\begin{tabular}{|l|l|l|}
\hline
                                                        & \multicolumn{2}{l|}{Next State} \\ \hline
\begin{tabular}[c]{@{}l@{}}Current\\ State\end{tabular} & Input = 0      & Input = 1      \\ \hline
00                                                      & 00             & 10             \\ \hline
01                                                      & 00             & 10             \\ \hline
10                                                      & 01             & 11             \\ \hline
11                                                      & 01             & 11             \\ \hline
\end{tabular}
\caption{Transition table}
\label{viterbitable}
\end{table}

The Viterbi decoder evaluates at each tap the metrics of any possible path to chose based on the received dibit and memorizes up to $K$ previous taps' metrics. An accumulated path metrics is calculated and at the end, the Trellis diagram path with the best (smallest) accumulated metric is selected, which gives a sequence of states the decoder FSM went through. This sequence translates directly to the most probable binary sequence which was transmitted by the other communicating side. To be complete, the accumulated metrics is computed as a sum of metrics at each tap. It is obvious, that the decoder cannot memorize an infinite number of previous states' metric and so only $K$ previous taps is considered. In practice, $K$ has a value going from 5 to 7. $K$ is called the "constraint length". 



\subsection{LDPC Coding \citep{SarahJ.Johnson}}
LDPC codes are regular block parity check codes, which have a special parity check matrix $H$. \ref{syndromeblockcode}.

\begin{align}\label{syndromeblockcode}
s = H y^T\\
\intertext
{
Where:
}
\shortintertext
{
$H$ is the parity check matrix
}
\shortintertext
{
$y^T$ is a collumn vector of received message bits 
}
\shortintertext
{
$s$ is the syndrome of the received message $y$
}
\nonumber
\end{align} 

Even though $H$ defines an LDPC code, if we want to actually encode our payload, we need to first calculate a generator matrix from it, which we will do using equation \ref{GvsH}. 

\begin{align}\label{GvsH}
G = GaussJordan(H)\\
\intertext
{
Where:
}
\shortintertext
{
$H$ is the parity check matrix
}
\shortintertext
{
$G$ is the codeword generator matrix
}
\shortintertext
{
$GaussJordan()$ is the Gauss-Jordan elimination
}
\nonumber
\end{align} 
\par
Having calculated the $G$ matrix, we can then calculate our codeword using the relation \ref{ldpcencode}.

\begin{align}\label{ldpcencode}
c = u G\\
\intertext
{
Where:
}
\shortintertext
{
$c$ is the resulting codeword to be send
}
\shortintertext
{
$G$ is the codeword generator matrix
}
\shortintertext
{
$u$ is the input payload bits
}
\nonumber
\end{align} 

\par
The performance of an LDPC code is entirely defined by properties of its $H$ matrix. The required properties are:
\begin{itemize}
  \item $H$ has to be sparse - the density of ones in the matrix must be low. (It must be nearly all zeroes matrix)
  \item The number of ones on a row has to be constant.
\end{itemize}
As you can see, the requirements are not really strict and there are therefore multiple approaches of how to design the $H$ matrix. These are well discussed in \citep{SarahJ.Johnson}. Nevertheless, these requirements ensure that the decoding complexity increases only linearly with the code length as well as the minimum distance increases linearly with the code length.
\par
The major difference between LDPC codes and generic block codes is the way LDPC is decoded, which is based on the sparseness of their matrix $H$. A general name of the class of decoding algorithms used to decode LDPC codes is "message-passing", among these we can cite:
\begin{itemize}
  \item Bit-flipping decoding - hard decision algorithm
  \item Sum-product decoding - soft decision algorithm
\end{itemize}
\par
\subsubsection{Example}
To better illustrate how the  encoder of an LDPC can be realized, in figure \ref{ldpcexample} an example of the encoder circuit is given of a 108 bits length codeword, 3/4 rate LDPC code.
\par
The parity check matrix of such code is given in \ref{ldpcexampleh}.
\begin{align}\label{ldpcexampleh}
H &= \left [ A_{1}, A_{2}, A_{3}, A_{4} \right ] \\
\nonumber
\\
\nonumber
a_{1}(x) &= 1 + x^3 + x^{16} \\
\nonumber
a_{2}(x) &= x^2 + x^6 + x^8 \\
\nonumber
a_{3}(x) &= x + x^8 + x^9 \\
\nonumber
a_{4}(x) &= x + x^{13} + x^{23} \\
\nonumber
\end{align} 

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=14.5cm,height=14.5cm,keepaspectratio]{pic/ldpcexample}
\end{center}
\caption{LDPC encoder example, $k = 3/4$ and $L = 108 bits$ \citep{SarahJ.Johnson}}
\label{ldpcexample}
\end{figure}
\par
The data are fed in the D latches in parallel and in following 28 clock cycles, the parity bits are shifted out of the structure of the encoder. This way a 108 bit long codeword is generated and can be consequently sent to the channel.


\section{Leveraging Channel Reciprocity}
BER reduction method presented in the previous section used the channel coding, which will work both in simplex and full-duplex communication schemes. In our case, however, we want to implement a full-duplex FSO bridge, which can give us a certain advantage over the simplex communication system. Let's leverage this.

\subsection{Comparison with Radio-frequency Channels}
In FSO, in case of a full-duplex link, the transmitting an the receiving signals travel through exactly the same environment and effects like multipath propagation does not apply, which allows us to say, that we can apply the principle of reciprocity to it. In case of RF links, this is not possible in a real environment.
\subsection{Proposed BER Reduction Technique}
\label{newtechnique}
The proposed technique consists in monitoring the incoming optical signal from the other peer and not to forward data from the ethernet to the channel when the signal from the other peer is not present (Figure \ref{berreduction}). This approach was already mentioned in article \citep{ZdenekKolka;VieraBiolkova;OtakarWilfert2015}.


\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=11.5cm,height=11.5cm,keepaspectratio]{pic/berreduction}
\end{center}
\caption{Proposed BER/FER reduction method illustration}
\label{berreduction}
\end{figure}

\subsection{Example of Usage of Presented Technique}
In our design, we will use an SFP optical module with SGMII communication interface and MDIO management interface. The SFP has however another signaling interface - an LOS (Loss Of Signal) signal, which can be used to detect whether the other side is receiving our signal or not (by knowing whether we can see the signal from the peer or not). The question is whether the latency of the LOS signal will be small enough to implement well this technique. Another solution is to use the MDIO interface and to read out the RSSI reading from the built-in registers in the SFP module and not to transmit when the RSSI level starts to drop or when it goes under a certain threshold level. The actual implementation will be considered in the last chapter of this thesis.