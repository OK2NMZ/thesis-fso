\chapter{Atmospheric Effects in FSO}
\label{turbulences}  

This section describes how atmospheric turbulence is the major problem in free space optics and how the losses caused by this effect scale with the influence of other interactions.

\paragraph{Atmospheric Interaction Types}
\label{inttypes}
Interactions between the communication laser beam and the atmosphere can be separated in two main types - extinction due to atmospheric aerosols and molecules versus optical turbulence causing random laser beam intensity fluctuations \citep{JenniferC.RicklinStephenM.HammelFrankD.Eaton2006}.

\section{Beam Extinction}
\label{extinction}
Extinction is caused either by scattering or by absorption or by a combination of these effects. Depending on the size of the object which interacts with the beam, it is further divided in aerosol extinction and molecular extinction. 
\par
	In case of the first one, the diameter of the particles is in the range of \unit{0.002}{\micro\meter} to \unit{100}{\micro\meter} - small enough to stay in the atmosphere. The diameter also influences the lifetime of the particle in the atmosphere, where smaller particles stay longer active. For example above the sea water, the sea-spray and higher humidity causes aerosols to be larger in size, but to have a shorter lifetime.
\par
	The molecular extinction manifests itself thanks to the absorption of incident radiation energy by air-contained molecules, which appears in discrete quanta. Therefore the absorption spectrum is composed of several discrete lines appearing in certain range of wavelengths, depending on the associated transition in the molecule. Table \ref{absorpBands} sums up absorption bands. FSO is mainly concerned by vibrational spectra.\citep{Majumdar2008}

To express the attenuation caused by mentioned effects we use the transmittance, which is defined by formula \ref{transmittanceDef}.

\begin{align}\label{transmittanceDef}
T_{\nu}(s) = \frac{I_{\nu}(s)}{I_{\nu,0}} = e^{-\beta_{\nu}s}  \\
\intertext
{
Where $\beta_{\nu}s$ is the total extinction coefficient comprising all aerosol scattering, aerosol absorption, molecular scattering and molecular absorption.
Its dimension is ${\kilo\meter}^{-1}$.
}
\nonumber
\end{align}

The following is a practical example of how the beam extinction demonstrates itself.
Typical values of total extiction parameter are given in the table \ref{totalExtiction}.
In our case, let's calculate the loss for a link with optical length of \unit{3}{\kilo\meter} in hazy environment \ref{extinctionExample}.

\begin{align}\label{extinctionExample}
T &= e^{-\beta_{\nu}s} = e^{-1 \cdot 3} = e^{-3}  \nonumber \\
L_{a} &= 10\log_{10}(T) = 10\log_{10}(e^{-3}) = -13.03 \deci\bel \\
\intertext
{
Losses introduced are \unit{-13.03}{\deci\bel}. 
}
\nonumber
\end{align}

\par
The scattering can be also divided into two categories - Mie scattering and Rayleigh scattering. The first one is more important for FSO links, since its impact to the SNR (signal to noise ratio) nearly does not depend on the wavelength of the propagating laser beam. Therefore its negative effect cannot be avoided. The second form of scattering is not as important for smaller distance FSO links (with $L < \unit{3}{\kilo\meter}$), since it depends on the wavelength - careful selection of the wavelength can mitigate its impact.


\begin{table}[]
\centering
\begin{tabular}{cc}
\hline
\multicolumn{1}{|c|}{\textbf{Transition  mode}} & \multicolumn{1}{c|}{\textbf{Wavelengths affected}} \\ \hline
\multicolumn{1}{|c|}{Rotation}                  & \multicolumn{1}{c|}
{
\unit{10}{\centi\meter} to \unit{100}{\micro\meter}
}                           \\ \hline
\multicolumn{1}{|c|}{Vibration}                 & \multicolumn{1}{c|}
{
\unit{100}{\micro\meter} to \unit{1}{\micro\meter}
}
                           \\ \hline
\multicolumn{1}{|c|}{Electronic transition}     & \multicolumn{1}{c|}
{
visible and ultraviolet
}                           \\ \hline
\multicolumn{1}{l}{}                            & \multicolumn{1}{l}{}                              
\end{tabular}
\caption{Absorption bands \citep{Duncan1993}}
\label{absorpBands}
\end{table}



\begin{table}[]
\centering
\begin{tabular}{cc}
\hline
\multicolumn{1}{|c|}{\textbf{Air conditions}} & 
\multicolumn{1}{c|}{$\beta_{\nu}s$ [\unit{}{\kilo\meter^{-1}}]} \\ \hline

\multicolumn{1}{|c|}{Clear}                  & \multicolumn{1}{c|}
{
0.1
}                           \\ \hline
\multicolumn{1}{|c|}{Haze}                 & \multicolumn{1}{c|}
{
1.0
}
                           \\ \hline
\multicolumn{1}{|c|}{Fog}     & \multicolumn{1}{c|}
{
\textgreater 10
}                           \\ \hline
\multicolumn{1}{|c|}{Dense Fog}     & \multicolumn{1}{c|}
{
391
}                           \\ \hline
\multicolumn{1}{l}{}                            & \multicolumn{1}{l}{}                              
\end{tabular}


\caption{Total extinction coefficient typical values\citep{Majumdar2008}}
\label{totalExtiction}
\end{table}

	
\section{Optical Turbulence}

\subsubsection{Definition}

Optical turbulence is an atmospheric effect that consists of random variations in the refractive index of the atmosphere, which are induced by spatially changing air density and temperature. \citep{Tikhonov2005}
\par
 Per definition of the refractive index (\ref{refractiveIndexDef}), its variations cause a distortion to the propagating light wave front, since the velocity at which each of its part propagates is not constant. This produces a fluctuation of the optical intensity at the receiver's optical aperture. The level of fluctuation can be measured using the scintillation index and depends on the refractive index structure parameter $C_{n}^2$, which is further described in subsection \ref{scintindx}.
\begin{align}\label{refractiveIndexDef}
n = \frac{c}{v} \\
\intertext
{
Where $n$ is the refractive index, $c$ is the speed of light in vacuum and $v$ is the speed of light in a medium. \citep{Hecht2002}
}
\nonumber
\end{align}
 \par
	Furthermore, when the size of the  "turbles" produced by the optical turbulence is larger than the transmitted beam diameter, the beam tends to be deflected, which causes a complete signal loss. This effect is known as beam wander. \citep{ThomasWeyrauch;MikhailA.Vorontsov2008}


\subsection{Beam Wander}
When beam wander starts to appear, it may cause the incident beam to completely miss the receiver's aperture, making the signal completely disappear. Since the apparition of optical turbles is a relatively slow process, the beam wander can be well compensated by the use of the adaptive optics, which can compensate these signal losses optically. In next chapter, an alternative technique will be presented based only on a electronic-based compensation, which can be more resource saving compared to expensive adaptive optics setup.

\subsection{Omnipresence of Optical Turbulence}
Unlike the process of absorption, which can be reduced by selecting appropriate wavelengths for communication (e.g. \unit{1550}{\nano\meter}), the optical turbulence does not have a "window", where its effects would not apply. It is therefore crucial to develop a method to avoid its effects, since it cannot be removed from the system by just selecting a right wavelength. Although with decreasing wavelength, the influence of turbulence slightly decreases as well, as you can see on the figure \ref{lambdaturbulences}. According to the figure, for links with the range smaller than \unit{5}{\kilo\meter}, the wavelength does not influence much the level of the attenuation. On the same figure we can notice the influence of the diameter of the receiver's lens - smaller the diameter, bigger impact does the turbulence have.

\begin{figure}[htb]
\begin{center}
   \includegraphics*[width=13cm,height=13cm,keepaspectratio]{pic/lambdaturbulences}
\end{center}
\caption{Attenuation caused by turbulence for two values of receiver's lens diameter and for two wavelengths in function of the link range \citep{Krivak2009}}
\label{lambdaturbulences}
\end{figure}
