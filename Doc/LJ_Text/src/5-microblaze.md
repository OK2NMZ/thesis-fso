\clearpage

## MicroBlaze design

### Design flow

The MicroBlaze microcontroller is a very scalable modular system developed separately
from HDL design in Xilinx Platform Studio (XPS). The tool allows direct generation and upload of a bitstream (standalone project).
In more complex systems, the MicroBlaze design can be exported to the HDL (ISE) project as a black box described by
a top HDL source defining its interface. The discussed design uses the second wariant since the MicroBlaze project is a component of the
 Media Converter design. Following examples were created in Xilinx Platform Studio, version 14.5. - Linux,\ 64-bit.

**Creating new XPS project**

XPS provides project wizards for simple project creation. The project is characterized by peripheral
bus which connects various peripherals to MicroBlaze core - AXI or PLB can be choosen. The type of peripheral bus also determines the
endianess of whole MicroBlaze design (AXI is Little Endian, PLB is Big Endian). The wizard allows user to include floating point unit (FPU),
to choose the size of program memory (allocated in block RAMs), to enable/disable instruction and data cache and many more.
An example of composition of MicroBlaze is ilustrated in [@fig:img_xps_screen].

![XPS - Used MicroBlaze IP cores in Bus interfaces panel](img/xps_screen.png){#fig:img_xps_screen}

**Adding IP cores to the design**

The new design contains only a few IP cores enabling the basic functionality - the MicroBlaze core, a block RAM (BRAM) for storing the program code,
BRAM controllers for connecting the BRAM to the core, a clock generator, a reset controller and a debugger. All available IP cores,
which can be added to the design are listed in the IP catalog on the left side of the user interface. The catalog provides
tens of categorized IP cores, e.g. :

 * general purpose inputs/outputs (GPIO)
 * fixed interval timers and complex, configurable timers
 * bus controllers (PCI, CAN, USB, $I^2C$, ...)
 * display drivers
 * BRAM blocks
 * memory controllers (SDRAM, DDR2, BRAM, EEPROM, FLASH)

 The chosen IP core is appended to the *Bus interfaces* list; if the core communicates with the MicroBlaze core, it needs to be connected
 to the apropriate bus by expanding the IP core item in the *Bus interfaces* list and selecting the apropriate bus.

![XPS - IP core IO ports](img/xps_screen2.png){#fig:img_xps_screen2}

 The *Bus interfaces* list allows simple connection of IP cores to the MicroBlaze core however the interconnection of IP cores between each other
 as well as the definition of external ports is done in the *Ports* panel. The IP core list in *Ports* panel contains all IP cores added to the
 MicroBlaze design and their IO pins. The actual connection is done by expanding the *Connected port* menu at the desired pin or bus of the IP core
 and choosing the destination pin or port. The pin or port can also be connected to an external port under custom name.

 In the standalone design (directly uploaded to FPGA), external ports are connected to FPGA pins and should be
 defined in the *ucf* file. When the design is exported to the HDL design (ISE), external ports define the interface of the MicroBlaze entity. An example
 situation can be seen in [@fig:img_xps_screen2]. The *display_port_0* IP core's SPLB interface is connected to the MicroBlaze PLB bus,
 the 256 bit wide *DISPLAY_OUTPUT* port is connected via external ports to the HDL design. The *bram_FTU* IP core (BRAM block)
 has two interfaces,
 one of them (B) is provided by external ports to HDL design, the second (A) is connected via *controller_bram_FTU* (memory controller)
 to the MicroBlaze core. The generated MicroBlaze top HDL design would then look like the following example:

```{.vhdl caption="An example of MicroBlaze top HDL source"}
entity ublaze_top is
  port (
    sys_clk_pin : in std_logic;
    sys_rst_pin : in std_logic;
    ...
    BRAM_WR_Data_pin : in  std_logic_vector(0 to 31);
    BRAM_WR_Addr_pin : in  std_logic_vector(0 to 31);
    BRAM_WR_EN_pin   : in  std_logic;
    BRAM_WR_Rst_pin  : in  std_logic;
    BRAM_WR_Clk_pin  : in  std_logic;
    BRAM_WR_WEN_pin  : in  std_logic_vector(0 to 3);
    display_out_pin  : out std_logic_vector(0 to 255)
  );
end ublaze_top;
```

**Creating custom IP cores**

XPS allows creating custom IP cores described by HDL source. Those can be useful e.g. for creating special peripherals or bus interfaces.
Custom IP cores are created using a wizard, which prepares a VHDL or Verilog skeleton. The custom IP core is created locally
in the current project or in the global XPS repository, making it available to other XPS projects.

The IP core design typically consists of two HDL sources describing a user logic and a top level design. The user logic HDL describes the
behavior of the core itself

```{.vhdl caption="An example of IP core user logic HDL"}
...
entity user_logic is
  port
  (
    -- Custom port
    Ctrl_user_out   : out std_logic_vector(7 downto 0);
    -- Interface to MicroBlaze
    Bus2IP_Clk      : in  std_logic;
    Bus2IP_Reset    : in  std_logic;
    ...
  );
end entity user_logic;

architecture IMP of user_logic is
  -- Signal connected to software-accesible register
  signal slv_reg0   : std_logic_vector(0 to C_SLV_DWIDTH-1);
  ...
begin
  ...
  -- Connecting the custom port to software register
  Ctrl_user_out(7 downto 0) <= slv_reg0(24 to 31);
  -- fetch contents of software register to slv_reg0
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin
    ... (generated code)
  end process SLAVE_REG_WRITE_PROC;
  ...
end IMP;
```

The top HDL source describes the interface of the IP core and instantiates and connects the user logic and the processor bus driver entities.

**Adress space handling**

IP cores connected to the MicroBlaze core are mapped to a 32\ bit monolithic address space of MicroBlaze which is handled in the *Addresses*
tab. Each IP core with software registers is assigned a *Base Address* and a *High Address* from the MicroBlaze address space
based on chosen size (addresses can also be assigned manually).

![XPS - MicroBlaze address space and IP core address mapping \label{img_xps_screen3}][xps_screen3]

When exporting to SDK, the *Base Address* is exported to *xparameters.h* header file as
a preprocessor macro named by *Base Name* so any change in address space is reflected in
software design.

### Base design

The main purpose of MicroBlaze in the discussed design is to gather data from the Media converter design and the climatization
unit and to provide the web interface representing these data and allowing control over these devices. The onboard display, leds and buttons may
 be advantageously used for debugging. General input/output ports are
connected to the top level design via GPIO IP cores, custom IP cores are created for unidirectional ports without the interrupt support.
The communication with the climatization unit is established by the XPS serial port IP core.

### Network interface

Xilinx Platform Studio provides XPS LocalLink TEMAC core, the advanced Media Access Controller based on the hard silicon Ethernet
MAC in the Virtex-5. Xilinx also provides the working port for mentioned device of LwIP,
the lighweight IP stack library. The XPS LocalLink TEMAC however requires an exclusive access to the PHY chip
and would take the control over the PHY chip management which is currently handled by Media Converter. Frames which should be
bridged between Ethernet and SFP interface would also needed to be extracted from MicroBlaze-side interface of TEMAC (LLC bus).

In brief, the XPS LL TEMAC core provides fast and simple solution to connecting the MicroBlaze to the network in a standalone design
however is hard to embed in a top level design with established mainstream connection. The development of a custom network interface for MicroBlaze
will be likely more straight-forward and readable solution than creation of the wrapper for XPS LL TEMAC IP core, at the cost of
porting the IP stack.

**Block RAM**

Xilinx Virtex-5 contains large number of 36\ kb block rams (BRAM) spread in columns over the FPGA structure with total capacity of
4608\ kb . The blocks have two synchronous, symmetrical interfaces (A, B), each of them can be independently accessed.
Data can be written to either or both ports and can be read from either or both ports. Each write operation is synchronous, each port has
its own address, data in, data out, clock, clock enable, and write enable [@xil_virtex5]. Block rams contain built-in FIFO support
and can be cascaded to create bigger memory blocks.

![Block RAM signals [@xil_virtex5]](img/bram_block.pdf){#fig:img_bram_block}

**BRAM controller**

The modified Flow Controller block stores frames for MicroBlaze in ETH\ -\ UB FIFO (see chapter \ref{chap_flow_controller}), frames written
to UB\ -\ ETH FIFO will be automatically sent to Ethernet interface. In order to process the incoming frame in the software, the frame needs to be
deserialized and randomly accesible within the MicroBlaze memory space. Outgoing frames are stored as a block of data, which needs to be
serialized and loaded byte-wise to UB\ -\ ETH FIFO.

Since Virtex-5 Block RAMs have two identical interfaces, they could solve both problems. They have enough space to store the frame and can be accessed randomly,
with one interface wired to Flow Controller (via external ports) and the second one to the MicroBlaze. Moreover, XPS provides the *XPS BRAM Controller*
IP core, which can
connect the BRAM block to MicroBlaze core and map its address space into the address space of the MicroBlaze.

![Control data line - connection of MicroBlaze to Flow Controller FIFOs. XPS BC - XPS BRAM Controller IP core.
CTRL - Custom IP core for control of the communication between the MicroBlaze and the BRAM controller](img/control_data_line.pdf){#fig:img_control_data_line}

The *BRAM Controller* block, shown in [@fig:img_control_data_line] is connected directly to ETH\ -\ UB and UB\ -\ ETH FIFOs of Flow Controller. When ETH - UB FIFO becomes non-empty, BRAM Controller fetches the data (single frame) to BRAM and sets the
length of written data to the control interface, evoking MicroBlaze's Rx interrupt. The next frame is fetched as soon as MicroBlaze
acknowledges the transmission. The MicroBlaze Tx data is fetched from the second BRAM block and written to Flow Controller as soon as MicroBlaze
sets the output data length.

### Optical path statistics

The codec contained in SFP-side MAC provides eight statistical 64\ bit counters and an event FIFO. Counters are connected to
the MicroBlaze core via simple read-only interface and stored as a 64\ bit unsigned integer which is further provided to the web API.
Provided statistical outputs are [@novak]:

 * Total count of frames received at the SFP - side MAC
 * Number of corrupted frames received at the SFP - side MAC
 * Number of bytes corrected by the RS decoder
 * Total number of bytes received by the receiver
 * Total number of transmitted frames
 * Number of output frames which had to be truncated (prematurely terminated)
 * Total number of transmitted bytes
 * Number of bytes dropped because of the frame buffer in SFP - side MAC overflow

 The event FIFO stores up to 512 64-bit link event frames, whose format can be seen in [@fig:img_link_state_frame].

 ![Link State Frame format](img/link_state_frame.pdf){#fig:img_link_state_frame}

 The event FIFO is connected to MicroBlaze via the *stat_event_in* custom IP core. When the event count is greater than zero, the
 64\ bit frame is written to two 32\ bit software registers, an interrupt is requested and an interrupt flag is raised in the third provided
 software register. As soon as the data are read and the flag is cleared, an another event can be processed.

 Those events can be sent in UDP packets and visualized at host PC. Since these events are marked with timestamp,
 they can be advantageously visualized in a graph and the link reliability can be calculated.

## Software implementation

The XPS project can be exported to Xilinx SDK which is based on Eclipse IDE.
The SDK adds support for the MicroBlaze toolchain, based on GNU tools including GNU C Compiler (GCC) and GNU Project Debugger (GDB). The SDK
allows the user to load the bitstream to the FPGA, to write and compile the program code, to load the binary to the program memory and to
debug the code on the hardware via JTAG interface.

During the export to the SDK project, a *Hardware platform specification* project is created within the SDK. This project contains the
copy of the bitstream, a bmm file describing the memory space of MicroBlaze and a xml file containing used IP cores and their parameters. From the
HW platform specification project, a *Board support package* project (BSP) can be created. The BSP project is a C library providing standard C
library functions and a hardware abstraction layer (HAL) above the MicroBlaze. The HAL part of the BSP library generates the *xparameters.h* file,
defining macros with base addresses, device IDs and parameters of used IP cores. The BSP also provides functions for initialization
and access to MicroBlaze and connected IP cores (e.g. enabling interrupts, connecting interrupt handlers
 and interrupts, initialization of GPIO, access to GPIO pins, ...). The BSP project wizard also allows setting up a RTOS or Linux kernel
environment, depending on the application project complexity and available resources. Following application design uses interrupt routines for
handling network events and can run directly on the hardware, without an OS.

### Software project hierarchy

The MicroBlaze SDK application project is divided into three particular parts:

 * The LwIP library, defining network interface driver and providing IP stack
 * The HTTP server example from LwIP contrib repository
 * The main application, providing hardware abstraction, system functions and the main routine

 These parts linked together with the BSP library form the final binary, uploaded to MicroBlaze.


### LwIP

LwIP is lightweight implementation of IP stack, developed by Adam Dunkels at Swedish Institute of Computer Science [@lwip]. LwIP is written
in C and
 freely available and licensed under BSD-like license. The LwIP project is released as stable and is further maintained by the community.
 The LwIP library is designated for embeded devices with few tens of kilobytes of RAM and Flash memory. Main features are\ [@lwip]:

 * Protocols: IP, ICMP, UDP, TCP, IGMP, ARP, PPPoS, PPPoE
 * DHCP client, DNS client, AutoIP/APIPA (Zeroconf), SNMP agent (private MIB support)
 * APIs: specialized APIs for enhanced performance, optional Berkeley-like socket API
 * Extended features: IP forwarding over multiple network interfaces, TCP congestion control, RTT estimation and fast recovery/fast retransmit
 * Addon applications: HTTP server, SNTP client, SMTP client, ping, NetBIOS nameserver
 * Experimental support for IPv6

These features can be enabled or disabled by preprocessor macros defined in `lwipopts.h`.
 LwIP can run either standalone or on underlying operating system. For non-os applications, LwIP provides low-level *raw API* (callback API),
  running in single execution context (thread). High-level (sequential) APIs should run in its own
  thread and thus require an OS. High-level *Netconn API* and *Socket API* are provided. The second mentioned is very similar to POSIX
  socket API, applications using the Socket API are thus easily portable to other operating systems, e.g. Linux. This project uses
  the raw API since it provides the best performance.

**Porting LwIP to custom network interface**

  To use LwIP with a custom hardware, low-level functions of LwIP must be reimplemented. User must provide functions to initialize the
  network interface, to send and receive a frame and to get the system time. The *lwip/netif/ethernetif.c* file within the LwIP root
  is a skeleton providing templates for these functions.

```{.C caption="ethernetif.c: Prototypes of elementary LwIP functions"}
static void  low_level_init(struct netif *netif);
static err_t low_level_output(struct netif *netif, struct pbuf *p);
static struct pbuf *low_level_input(struct netif *netif);
```

Especially for TCP timeouts, LwIP requires access to the system time via `sys_now();` function. This function returns the actual value of 32\ bit unsigned integer variable
which gets incremented every 10\ ms by the fixed interval timer, the timer period must also be defined in `lwipopts.h`.

The frame transmission happens transparently within LwIP however for frame reception, the `ethernet_input()` function must be called
after fetching a new frame to the BRAM. After meeting
these requirements, the LwIP can be initialized and used.

**LwIP Initialization**

The LwIP is initialized in three steps:

```{.C caption="LwIP initialization"}
void lwip_init(void);
struct netif *netif_add(struct netif *netif, ip_addr_t *ipaddr,
                        ip_addr_t *netmask, ip_addr_t *gw,
                        void *state, netif_init_fn init,
                        netif_input_fn input);
void netif_set_up(struct netif *netif);
```

The `lwip_init()` function initializes built-in LwIP modules (memory, statistics, UDP, TCP, ...) and takes no parameters. After module initialization, the network
interface is initialized and added to the interface list by `netif_add()` function. The network interface is assigned the IP address, subnet mask, default gateway and
init and input callback functions. The network interface is enabled by calling `netif_set_up()`. The `check_timeouts()` function should be called periodically from main loop
to check LwIP timers.

Since now, the IP stack is initialized; if ARP and ICMP are not disabled in `lwipopts.h`, the application should respond to ICMP echo requests ([@fig:img_ping]).

![An example of ARP and ICMP communication between PC (192.168.1.13) and LwIP at MicroBlaze (192.168.1.16)](img/ping.png){#fig:img_ping}


### HTTP server

A HTTP server is an application, responding to HTTP requests of a client (browser). Typically, the client sends the HTTP GET request to the server
(with information
  about the client and the path to the requested page) and the server responds by sending the HTTP header (with information about server) and
  the requested page.

The LwIP repository contains a simple HTTP server example. The server has dependency on LwIP and works just
after simple initialization by calling *httpd_init()* function.
Since most lightweight applications do not provide common filesystem functionality,
the HTTP server provides a virtual filesystem stored in the program code memory. The HTTP
root folder must be converted into bit arrays, which are then included and passed to the HTTP GET handler.
The conversion is done by *makefsdata* script. The
script processes files in "fs" subdirectory and creates a *fsdata.c* file containing single array of type *unsigned char* for each file.

**SSI Module**

By default, the HTTP server allows only a static HTML presentation. The response of the server contains a
HTTP server header and contents of the requested page which was pre-generated by *makefsdata* utility and
is a part of the program code. To include data dynamically provided during the application runtime,
an additional functionality is required.
For this purpose, the HTTP server example provides a Server-Side Include (SSI) module which can be enabled
in `httpd.h` file. This module looks for special tags in the source webpage
(which is generated the same way as before) and appends the generated string right after the tags.
The tag is within the HTML comment, which remains in the code but since it is a comment, it
does not affect the visible content. The tag has following syntax:

`<!--#name-->`

The application should provide an array of tag names and the function returning strings based on
given tags.

```{.C caption="httpd.h: Prototypes of SSI functions"}
typedef u16_t (*tSSIHandler)(int iIndex, char *pcInsert, int iInsertLen);
void http_set_ssi_handler(tSSIHandler pfnSSIHandler,
                          const char **ppcTags, int iNumTags);
```

This allows web server to pass data from the main routine to the webpage, e.g. the frame/byte counters from the top level design can be now
inserted into a simple web page. In the [@fig:web_v1], the very first version of the web interface utilizing only ssi can be
seen.

![The first version of simple web interface with top level counters. Only SSI used.](img/web.png){#fig:web_v1}

**CGI Module**

To enable the backward communication from a browser to a web server, the
server provides the Common Gateway Interface (CGI) module. When the webpage with *.cgi* extension is
requested, the CGI module looks for a CGI handler which is associated to the requested filename.
If the handler is found, it is called with data from the browser as parameters.
This allows sending data from web forms or javascript variables to the main application. In this project, the
CGI engine is used e.g. for enabling or disabling the channel encoding or for setting
the climate control unit.

```{.C caption="httpd.h: Prototypes of CGI functions"}
typedef const char *(*tCGIHandler)(int iIndex, int iNumParams,in
                                   char *pcParam[], char *pcValue[]);
void http_set_cgi_handlers(const tCGI *pCGIs, int iNumHandlers);
```

The CGI engine is initialized by calling *http_set_cgi_handlers()* with pointer to *tCGI struct* and
total number of handlers as parameters. The *tCGI struct* encapsulates the CGI handler-filename string pair.

**The webdesign**

The final webdesign is inspired by Network Gateway project [@ngw]. The design is divided into tabs,
each containing single cathegory. The design is formed by CSS and simple javascript framework handling
tabs and buttons. Statistical data or current settings are passed to the page by the SSI engine.

![Current version of web interface](img/web2.png){#fig:img_web_current}

By clicking submit buttons (FSO config. or Clima config.), the data from forms are sent by HTTP GET method
to apropriate CGI handler.
