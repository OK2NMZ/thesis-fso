\clearpage

## Climate control unit

This design utilizes a simple HVAC unit [@hvac], attached to the outdoor FSO
transceiver. The unit is powered by a 48V power supply and is designated to prevent the optics
from dew or freezing, to maintain suitable temperature conditions
within the transceiver device and to provide temperature and humidity
information.

### Provided functionality

The HVAC unit provides a simple interface and is connected to the host (Media Converter design)
via serial bus. The unit is fully automatized and its operation is driven by following software
controllable limit values:

**Value**          **Unit**     **Meaning**
-----------        --------     ----------------------------------
$t_h$              °C            upper temperature limit
$t_h\_krit \quad$  °C            upper critical temperature limit
$t_d$              °C            lower tempereture limit
$t_d\_krit$        °C            lower critical temperature limit
$h_{max}$          %\ RH          humidity limit

Table: Software controllable temperature and humidity limits

The HVAC unit contains temperature and humidity sensors

**Cooling process**

When the ambient temperature reaches the upper temperature limit $t_h$, the fan starts to operate.
The fan speed is linearly increasing from the minimal to the maximal value, depending on
the ambient temperature. The fan reaches its maximal speed when the ambient temperature
reaches $t_h\_krit$.

**Heating process**

The heating process starts when the ambient temperature reaches the lower temperature limit $t_d$
or the humidity reaches the maximum humidity limit $h_{max}$. The heater and the fan are triggered
and the duty cycle of the heater is linearly regulated by the ambient temperature. The maximum
duty cycle is set when the ambient temperature reaches the lower critical temperature limit $t_d\_krit$.

### RS-485 Driver

The HVAC unit is connected to the Evaluation Platform via RS-485 bus. Since the Evaluation Platform does
not provide a RS-485 driver, a simple reduction from UART to RS-485 must be constructed.
A Texas Instruments SN65HVD10D was used as an all-in-one driver with only few external resistors,
a blocking capacitor and transient voltage suppresors. The schematics is in [@fig:img_rs485_scheme]

![Schema of the RS-485 driver board](img/485_sch.pdf){#fig:img_rs485_scheme}

The bus driver is to be mounted directly on the XUPV5
board to headers *J5* and *J6*.

![Positioning of the RS-485 driver on pinheaders J5 and J6](img/485_pos_photo.jpg)

For detailed documentation of the RS-485 driver board, see appendix \ref{driver-board}.

### Serial interface

Microblaze communicates with the HVAC unit at 9600\ b/s with no parity. In the MicroBlaze design,
a common XPS UART Lite IP core is used and wired to Tx and Rx pins
of the RS-485 driver. The TxEN pin is handled from software. The software
registers of the device are then accesed by simple get/set instructions,
e.g. `"D10"` command sets the $t_d\_krit$ to 10°C. Those commands are handled from
the Microblaze based on requests from the web GUI.

### Proposed improvements

Operation of the discussed HVAC unit is triggered when the ambient temperature or the humidity reaches
preset limits. This works fine for keeping the transceiver temperature in service limits of used parts.
The dew however could occur even if the air temperature rises much faster than the temperature of the optics,
the increased humidity also does not always cause a fogging.
This could be prevented by setting loose values of temperature and humidity limits, at the cost of increased unnecessary fan
and heater runtime.

**Decreasing the run time of a HVAC**

The condensation of the water vapour on a solid surface can be well predicted by calculating
the dew point temperature $T_{dp}$. A dew point is the temperature, at which air, as a result of isobaric
cooling, becomes saturated without supplying additional vapour [@predpoved_termicke_konvekce]. The dew point
can be calculated from air temperature $T\ [\degree C]$ and a relative humidity $V\ [\%RH]$ using following empiric equation from NOAA [@noaa]:

$$T_{dp}=\frac{243.5ln\left( \frac{V}{100} e^{\frac{17.67\cdot T}{243.5+T} } \right)}{17.67-ln\left( \frac{V}{100} e^{\frac{17.67\cdot T}{243.5+T} } \right)} \quad [\degree C]$$ {#eq:dew_point}

By periodic calculation of the dew point, based on data from the temperature and humidity sensor, the anti-fogging mechanism
could operate only when there is an actual risk of fogging.


**Air drying**

By increasing the temperature of the optics, the risk of fogging is mitigated. While the
water condensation depends either on air temperature and moisture, the anti-fogging mechanism would be even more
effective when the air was also dehumidified.

The use of peltier element could theoretically solve either dehumidification and heating of the air. By
blowing the air on the cold side of the peltier element (much colder than the optics), the excessive vapour
condenses on the surface of the peltier element. The cold, dry air could then be heated by the hot side of the
peltier element. See appendix \ref{hvac-improvements} for a theoretical example of the method.

## Summary

This chapter discusses the implementation of the support system for FSO bridge, based on the MicroBlaze microcontroller.
To connect the MicroBlaze core to the network, a custom simple ethernet switch and a custom network interface was designed.
The LwIP library, providing IP functionality, was ported to the design. An example HTTP server
from the same repository was used to provide HTTP functionality. A graphical web interface was designed to provide remote control over the whole system and
the connected HVAC unit. This chapter also briefly describes functionality of the HVAC unit and the design of a simple RS-485-UART
converter board.
