\clearpage

# Free space optical communication

A free space optical link represents an optical wireless connection, mostly with the Earth's
atmosphere as the main propagation environment. The information is modulated on single or multiple
optical carriers located on single or multiple optical beams. These links could
be employed in various situations - they allow point to point or point to multi-point communication, ground to satellite,
satellite to satellite and underwater communication, they can be either mobile or stationary. Further in this thesis, terrestrial
point to point, full-duplex, stationary link will be discussed.

## Motivation

The free space optical communication provides a considerable solution for last-mile links
where wireless technologies are commonly used, due to lower costs in comparison to wired technologies
where excavation works are needed to be done. Wireless connections also bring the possibility to connect places
where wired connections are impossible due to terrain conditions. Wireless connections can be put into service very fast and
mounting them to existing antenna masts is also possible, leading to further discounts. According to this
fact, RF and microwave links are massively used.

Modern digital modulations approaches the communication speed of microwave connections to the
theoretical Shannon limit, causing demand on the increase of carrier frequency and bandwidth.
While the theoretical communication channel transfer rate is proportional to the carrier bandwidth and frequency,
the transfer from RF or microwave band to the light spectra brings theoretical increase of the transfer rate
up to five orders higher [@majumdar]. The increase of the carrier frequency also affects
the required aperture (antenna) size and directivity which are directly proportional to the used wavelength.

Compared to wireless RF connections, an optical links bring plenty of advantages, namely:

* Non-licensed operation
* Higher transfer rates
* Higher energy and spectral efficiency
* Smaller transceivers
* Durable to sniffing or exploiting
* No electromagnetic interference
* Operation with reciprocal channel

Similarly to RF connections, the quality of a FSO connection is significantly dependent on channel
conditions, but effects of particular phenomena on FSO and RF or microwave channels differ.
This fact is employed in hybrid systems, which consist of a FSO transceiver, establishing the main communication channel and
a RF transceiver as a limited rate assistance link in cases of bad weather conditions.

## Common measures

### Signal to noise ratio

The Signal to Noise Ratio (SNR) is a common measure for estimating the channel quality. The SNR compares the level of an observed
signal relatively to a noise level. The SNR is in logarithmic nature defined as:

$$SNR = 10 \cdot \log \frac{P_{sig}}{P_{noise}} \quad [dB]$${#eq:snr}

### Bit error rate

The Bit Error Rate (BER) in digital communications is a common measure for estimating the transaction quality. The bit error rate is
defined as a fraction of erroneously transferred bits to the number of all transmitted bits, during previously defined measuring time.

$$BER = \frac{N_{err}}{N_{tot}} \quad [-]$$

The BER is proportional to the SNR.

### Channel capacity

A channel capacity, defined by Shannon-Hartley theorem is defined as the theoretical upper bound of the data rate of a communication channel
with the bandwidth $B$ and noise conditions defined by the SNR:

$$C = B \log_2 \left (1+\mathit{SNR} \right) \quad [b/s]$${#eq:shanon}

## FSO performance

To ensure high data rates, a well-calculated power balance as well as an adequate electronical front-end
 are required. As you can see in [@fig:simplified_fso_model], input electrical signal is brought to the transmitter and
 transferred to the optical spectra by modulation on an optical beam with mean radiated optical power $P_{m,TX}$.
 The beam is propagating against the receiver, total losses along the path are summarized in $\alpha_{tot}$,
 total gains along the path are summarized in $\gamma_{tot}$. A beam with the received mean optical
  power $P_{m,RX}$ illuminates the photodetector, which transfers it back to an electrical signal.

![Simplified model of FSO link ](img/fso_link.pdf){#fig:simplified_fso_model}

A simplified power balance can be in logarithmic sense expressed as:

$$P_{m,RX} = P_{m,TX} - \alpha_{tot} + \gamma_{tot}$$ {#eq:simplif_power_balance}

In a real situation, all of these power contributions (positive and negative) are much more
complex and consist of many components.

It is obvious, that to reach the required amount of the received optical power, the transmitting power
must be increased while propagation losses must be decreased. Whereas the transmitted power is dependent on
 the used light source, propagation losses are proportional to the square of distance
 and are considerably affected by conditions and composition of the propagation environment. These
 conditions will be discussed later in chapter \ref{atmosphere}, which deals with an atmosphere as
 the essential propagation environment of terrestrial FSO links.

## FSO transmitter

Thanks to its power, spectral and spatial properties, lasers are almost exclusively used as an
optical transmitter for free space optical links.

### Laser

Lasers are quantum devices whose function is based on a stimulated emission (see [@fig:stim_emission]).

![Stimulated emission [@stim_emission]](img/stim_emission.pdf){#fig:stim_emission}

Three essential laser components are: a power source, a resonator and an active medium. Electrons (carriers), present on the lower energy level, are
receiving the energy from the source which pushes them
to a higher energy layer (absorption). A single carrier, decaying to lower energy level (recombination), is accompanied by a photon emission.
The average time, which takes the carrier to spontaneously recombine, is called a lifetime $\tau$. A carrier, recombining without an external affection,
 causes a so called *spontaneous emission* (undesirable), which is participating on output intensity fluctuations. The emitted photon, moving near
 the carrier at the excited energy
level, can also initiate the recombination (wanted), causing emission of an another photon. This emission is called *stimulated emission* and causes the
coherence matter of a laser beam. Emitted photons are traveling between resonator mirrors, tearing down more excited electrons. The reflectance of one of mirrors is
intentionally decreased, which allows a small part of photons to leave the resonator at the cost of slight decrease of the resonator gain.

**Laser types**

Common laser types, distinguished by active medium are:

* solid lasers (YAG-Nd)
* gas lasers (He-Ne)
* semiconductor lasers

The light intensity of solid and gas lasers cannot be directly affected (only by external modulators),
whereas semiconductor lasers have a great advantage, because their radiant power and optical frequencies can be
directly modulated by the injection current [@bouchet]. Semiconductor lasers are also more size- and cost-
effective. However, whereas internal modulation methods can reach modulation speeds of few Gb/s, external modulators
could operate at tens of Gb/s and can also provide more sophisticated modulation methods [@caplan].


**Operating wavelength**

The wavelength of a Laser beam is one of the essential things to consider when designing an optical
link, especially due to molecular absorption of the atmosphere (see chapter \ref{atmosphere}).
The transmission of an atmosphere is dependent on the wavelength of the propagating beam; there are several
wavelength ranges, where atmospheric attenuation reaches acceptable values (less than $0.2\ dB/km$), these ranges are called
*atmospheric transmission windows*. The majority of FSO systems is designed to operate in transmission windows of $780\ nm-850\ nm$
and $1520\ nm-1600\ nm$ [@majumdar].

### Gaussian beam

 In order to fully understand the problematics of a beam propagation, the optical beam used to carry the information
 should be defined first.

 A laser beam leaving a laser diode can be successfully approximated by a gaussian beam. The optical
 power of a gaussian beam, propagating along z axis, is concentrated into a narrow cone and its transverse optical intensity
 is defined by a circularly symmetric gaussian function, with $I_0$ as the maximum on the propagation axis [@kvantovka].

 ![Gaussian beam propagating along the Z axis](img/gaussian_beam.pdf){#fig:gaussian_beam}

A beam half-width is defined as:

$$w(z) = w_0 \sqrt{ 1 + \left ( \frac{z}{z_R} \right )^2 } \quad [m]$$ {#eq:gauss_beam_half_width}

 The function $w = f(z)$ is illustrated in [@fig:gaussian_beam], here
 $w_0$ stands for a beam waist which is half-width of the beam at its focus ($z = 0$),
A Rayleigh distance $z_R$ is the distance, where the half-width is equal to:

 $$w = \sqrt{2} \cdot w_0 \quad [m]$$ {#eq:gauss_beam_half_width_rayleigh}

An angle formed by single diverging edge of the cone and the propagation axis is called an angle of divergence
and is defined as:

 $$\theta = \lim_{z \to \infty}\frac{w(z)}{z} = \frac{\lambda}{\pi w_0} \quad [rad]$$ {#eq:gauss_beam_divergence}

For distances $z \gg z_R$, $w = f(z)$ becomes linear and the beam can be taken as a spherical wave originated
from  the beam focus.

A total angular spread of a beam is then defined as:

$$\Theta = 2 \theta \quad [rad]$$ {#eq:gauss_beam_spread}

 If the non-linear influence of a propagation medium is neglected, the gaussian nature of the beam remains preserved and
 only the optical intensity and the beam width changes along the direction of propagation. An optical intensity along the propagation
 axis is equal to:

 $$I(0,0,z) = \frac{I_0}{1 + \left( \frac{z}{z_R} \right)^2} \quad [W/m^2]$$ {#eq:gauss_beam_intensity}

 Which can be for distances $z \gg z_R$ simplified to:

 $$I(0,0,z) \approx I_{0} \cdot {z_R}^2 \cdot \frac{1}{z^2} \quad [W/m^2]$$ {#eq:gauss_beam_intensity_s}

As you can see in [@eq:gauss_beam_intensity_s], the intensity is decreasing with square of the distance,
which causes great beam attenuation, contributing to propagation losses.

### Transmitting optical system

The divergence of a laser beam causes the receiver to collect only a part of the emitted energy, this attenuation is called
*geometric path loss* $\alpha_{geom}$. A geometric path loss together with atmospheric losses forms a *propagation loss* $\alpha_{pr}$.
A geometric path loss is defined as [@bouchet]:

$$\alpha_{geom} = 10 \cdot \log \frac{S_b}{S_r} = 10 \cdot \log \frac{ \frac{\pi}{4} \cdot (d \Theta)^2 }{S_r} \quad [dB]$$ {#eq:geom_loss}

Here $\Theta$ is a divergence of the beam, $d$ is distance between the transmitter and the receiver, $S_r$ is an area of the receiver aperture
and $S_b$ is an area of the beam at distance $d$.

**Divergence**      **Distance**      **$\alpha_{geom}$**
--------------      ------------      -------------------
$1\ mrad$               $5\ km$           $29\ dB$
$3\ mrad$               $2\ km$           $37\ dB$
$1\ mrad$               $8\ km$           $33\ dB$

Table: Values of geometrical path losses for various distances [@bouchet]

To decrease the divergent matter of a laser beam, convergent lenses are used. Efficiency of the coupling between a laser
diode and an optical system depends on the beam distribution and the divergence and can be evaluated as:

$$\alpha_{coup,tx} = \left | 10 \cdot \log \frac{P_{tos}}{P_{LD}} \right | \quad [dB]$$ {#eq:coupling_txer}

Where $P_{tos}$ is the optical power incident on a transmitting optical system and $P_{LD}$ is the optical power leaving a laser diode.
Practically, this efficiency equals to $1.5\ dB$ [@bouchet].

## FSO receiver

###Photodetector###

Photodetectors are mostly semiconductors, responsible for detecting a modulated beam by acting as a light-sensitive resistances
or voltage or current sources.
There are many types of photodetectors, based either on direct or indirect (e.g. coherent homodyne receiver) detection. Direct-detection
PIN diodes are most commonly used for high speed optical communications [@caplan].

![PIN diode structure [@wiki_pin]](img/pin_diode.png){#fig:pin_diode}

A PIN diode consists of a semiconductive P-N junction with an intrinsic (undoped) region between P and N areas. As a photodetector, the PIN diode is biased
reverse, not participating on the conduction. An incident light (photon flux) enters the detector volume; each photon with a sufficient energy causes an absorption,
releasing a pair of carriers. If an external electric field is present, those free carriers are separated and consequently participate on
current conduction (photocurrent) [@bouchet]. If the diode does not operate at saturation, the photocurrent is directly proportional to
the amount of incident light, however even if no incident light is present on the active surface of the diode, a small amount of current, called
*dark current* is passing through the diode. This current increases with junction temperature and together with shot noise and thermal noise
participates on decreasing the system SNR.

A mean photocurrent $\overline{i}$, caused by an incident light, is proportional to an incident optical power $P_{PD}$ and a photodetector responsivity $R_i$:

$$\overline{i} = R_i \cdot P_{PD} \quad [A]$$ {#eq:photocurrent}

The responsivity of the photodetector is dependent on the wavelength and the quantum efficiency of the detector $\eta$:

$$R_i = \frac{e \cdot \eta}{\hbar \cdot \omega} \quad [A \cdot W^{-1}]$$ {#eq:responsivity}

Where $\eta$ is defined as:

$$\eta = \frac{\text{number of generated electron-hole pairs}}{\text{number of incident photons}} \quad [-]$$ {#eq:quantum_efficiency}

The responsivity is dependent upon the material, this material should be chosen with a bandgap energy slightly less than the energy of
an incident optical wavelength. This leads to satisfiable compromise between a responsivity and a dark current.

![The responsivity of common materials [@poon]](img/responsivity.png){#fig:responsivity}

###Receiving optics###

A receiving optical system focuses incident optical beam onto active surface of the photodetector and thus increases the amount of
the incident optical power.
This contribution is called a receiving optical system gain and equals to [@optoelektronika]:

$$\gamma_{ros} = \left | 20 \cdot \log \frac{ D_{ROS} } { D_{TOS} } \right| +3 \quad [dB]$${#eq:receiving_optical_gain}

In the equation, the $+3dB$ gain is caused by different shape of the wavefront at the receiver and the transmitter.
Whereas at the transmitter the beam has a gaussian energy distribution with distinct maximum near the propagation
axis, the incident wave at the receiver can be taken as a plane wave.
 The quality of the coupling between the optics and the photodetector is described by a receiving optical system coupling. This coupling is dependent on
the fraction of the active surface of the photodiode and the incident laser spot size. This coupling is equal
to zero when $A_{PD} \leq A_{spot}$.

 <!-- ![Diffraction at receiving aperture][diffraction] -->

## FSO power balance

A FSO power balance is the first step in a FSO link design, where all particular power contributions are calculated and
the power balance equation is assembled. Thanks to a power balance equation, one gains overview about power conditions
at particular parts of the link. A power balance diagram is a graphical representation of particular contributions
and brings increased lucidity to the design.
A power balance consists of:

**Symbol**          **Name**
----------          --------------------------------------------------
$P_{LD}$            Laser diode mean optical power
$\alpha_{coup,tx}$  Transmitting optical system coupling efficiency
$\alpha_{tos}$      Transmittance of the transmitting optical system
$\alpha_{geom}$     Geometric path loss
$\alpha_{atm}$      Atmospheric losses
$\alpha_{ros}$      Transmittance of the receiving optical system
$\gamma_{ros}$      Gain of the receiving optical system
$\alpha_{coup,rx}$  Receiving optical system coupling efficiency
$\rho_{atm}$        Reserve for atmospheric phenomena
$P_0$               Receiving system sensitivity

Table: Power balance contributions\label{power_balance_contributions}

With contributions listed in table \ref{power_balance_contributions}, the power balance equation can be assembled:

$$P_{m,RX} = P_{LD} - \alpha_{coup,tx} - \alpha_{tos} - \alpha_{geom} - \alpha_{atm} - \alpha_{ros} + \gamma_{ros} - \alpha_{coup,rx} - \rho_{atm} \quad [dBm]$$ {#eq:power_balance_equation}

In this equation, the received power level $P_{m,RX}$ is commonly substituted with a receiving system sensitivity $P_0$, which is defined
as minimal received power needed to reach defined SNR:

$$P_0 = P_{min} + \mathit{SNR}_0 \quad [dBm]$$ {#eq:receiving_system_sensitivity}

Where $P_{min}$ stands for a minimal detectable power on the photodetector. A receiver saturation power $P_{max}$ must be also considered since
the received optical power must at all atmospheric conditions stay in the range from $P_{min}$ to $P_{max}$ (dynamic range $\Delta_P$).
Violation of this rule would lead to increase of BER even during good atmospheric conditions.

![An example of a power balance diagram](img/power_balance.pdf){#fig:power_balance}

## Summary

The first chapter makes a brief introduction into the problematics of a free space optical communication. Common measures and basic principles are
introduced. The first chapter also discusses key components of a FSO transceiver with emphasis on their properties. As the result,
a FSO power balance is introduced, containing contributions from all discused FSO components. A power balance as well as context of entire
chapter is of static nature, overall losses, gained from worst-case results of (mostly) statistical data are balanced by sufficient
power margin, which is not efficient.
