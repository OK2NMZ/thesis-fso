\clearpage
\setcounter{page}{1}

\chapter*{Preface}
\phantomsection
\addcontentsline{toc}{chapter}{Preface}

An optical communication presents a perspective, non-pollutive and licence-free communication technique. Since an invention of the laser and the dramatical reduction of optical fibre losses, optical connections play the main role in the field of the fastest connectivity and replaced most of metalic backbone connections.

Origins of optical communications stretch hundreds of years back, starting with primitive binary signals represented by smoke or
fire signals on hilltops. Compared to preceding days, when the only possible message delivery method was sending a messenger on a horseback,
this method provided an advantage in its low propagation time. The elementary information was transferred to distances of tens of kilometers as fast,
as the remote observer could notice it, however amount of the transferred information per time unit was very low. The great turn came many years
later - at the end of 18th century, with the invention of Claude Chappe's optical telegraph. Yet the basic principle remained the same, the amount
of transferred information was significantly increased by the use of greater symbol alphabet. Chappe's telegraph consisted of three adjustable wooden arms, built atop of stone tower. These arms could take various angles; from these positions Chappe assembled telegraph symbol alphabet consisting of 92 symbols. This invention is nowadays considered to be the beginning of telecommunications, the trend of increasing symbol count or symbol transfer speed remains.

![Claude Chappe's optical telegraph [@chappe]](img/chappe_telegraph.pdf){#fig:chappe_telegraph}
