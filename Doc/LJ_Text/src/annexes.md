
\seznampriloh
\prilohy

# MicroBlaze project

Name                                  Description
----                                  -----------
dlmb                                  Local Memory Bus controller (data)
ilmb                                  Local Memory Bus controller (instructions)
mb_plb                                Processor Local Bus controller (master)
microblaze_0                          MicroBlaze core
lmb_bram                              BRAM block for the program
bram_FTU                              BRAM block (Flowcontroller to Microblaze)
bram_UTF                              BRAM block (Microblaze to Flowcontroller)
dlmb_cntlr                            Memory controller (data)
ilmb_cntlr                            Memory controller (instructions)
controller_bram_FTU                   Memory controller (FTU block)
controller_bram_UTF                   Memory controller (UTF block)
SRAM                                  Memory controller (onboard SRAM)
mdm_0                                 MicroBlaze Debug Module
intc_1                                Interrupt controller
BRAM_IF_R                             Interface to BRAM Controller - input
BRAM_IF_W                             Interface to BRAM Controller - output
DIP_switches_8bit                     DIP switches interface
LEDS_8bit                             LED signalization interface
Push_Buttons_5bit                     Push buttons interface
serial_port                           UART interface to HVAC unit
clock_generator_0                     Clock generator
timer                                 Fixed interval timer for systick
proc_sys_reset_0                      Procesor system reset module

Table: Used IP cores (generic)

Name                                  Description
----                                  -----------
config_interface_0                    Configuration and control IF (resets, etc.)
data_len_in_0                         Data length from BRAM controller
data_len_out_0                        Data length to BRAM controller
display_port_0                        Paralel display interface
flowcontroller_mac_control_0          MAC address output for FlowController
stat_events                           Statistics - Event interface
statistics_input_0                    Statistics - Counter interface

Table: Used IP cores (custom)

# Web UI

![Web UI - Statistics](img/web_fin1.png)

![Web UI - Configuration](img/web_fin2.png)

![Web UI - HVAC configuration](img/web_fin3.png)

![Web UI - About](img/web_fin4.png)

# HVAC

## Driver board

![RS-485 Driver board - Assembly, scale 2:1](img/485_assem.pdf)

![RS-485 Driver board - Layout, scale 1:1](img/485_lay.pdf)

![Assembled RS-485 Driver board](img/485_final.jpg)

\newpage

**Caption**$\quad$    **Part**$\quad$               **Value**$\quad$         **Footprint**$\quad$
------------------    ---------------               ----------------         --------------------
R1                    Resistor                      $10\ k \Omega$             0805
R2                    Resistor                      $10\ k \Omega$             0805
R3                    Resistor                      $10\ \Omega$               0805
R4                    Resistor                      $10\ \Omega$               0805
R5                    Resistor                      $120\ \Omega$              0805
C1                    Ceramic capacitor             $100\ nF$                  0805
TR1                   Bipolar TVS                   $6V, 600\ W$               SMB
TR2                   Bipolar TVS                   $6V, 600\ W$               SMB
IC1                   RS-485 Driver                 $SN65176D$                 SOIC-8
J5                    Pinheader                     $1\times6, 2.54\ mm$       -
J6                    Pinheader                     $2\times6, 2.54\ mm$       -
X1                    Terminal bar                  $1\times2, 5\ mm$          -

Table: RS-485 Driver board partlist

\newpage

## HVAC Unit commands

**Command**          **Description**                  **Range**      **Example**
-----------          ---------------                  ---------      -----------
m                    Print the menu                   -              m
t                    Get the current temperature      -              t
h                    Get the current humidity         -              h
p                    Get EEPROM contents              -              p
n                    Reset service counters           -              n
o                    Repair done                      -              o
r                    Get current limits               -              r
Hxx                  Set the $t_h$                    0-99\ °C       H40
Kxx                  Set the $t_h\_krit$              0-99\ °C       K50
Dxx                  Set the $t_d$                    0-99\ °C       D10
Lxx                  Set the $t_d\_krit$              0-99\ °C       L5
Vxx                  Set the $h_{max}$                10-90 \%RH     V70
Zxx                  Set the log period               0-99\ min[^1]  Z10
Ixx                  Set the minimal heating current  0-99[^2]       I10

Table: HVAC command list

[^1]: When set to 0, the writing period will be 10s.

[^2]: Actual current is calculated by multiplying the register value by 10\ [mA].
      e.g. I10 sets the current to 100\ mA.

\newpage

## HVAC Improvements

![Theoretical construction of HVAC with Peltier element](img/hvac_new.pdf)
