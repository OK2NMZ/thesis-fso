\clearpage

# Atmosphere

An atmosphere (from Greek word *atmos*, meaning vapour) is a set of concentric gas layers
surrounding Earth[^2]. The atmosphere is retained
by Earth's gravity and its density is thus dependent on the distance from the surface.

Common atmospheric layers are listed in table below. They differ in the composition, the temperature
gradient and the density. We are most interested in the troposphere as the undermost layer with the
biggest particle and gas density, where most of atmospheric phenomena occur [@bouchet].

[^2]: An atmosphere can also surround any sufficiently massive object

**Layer**         **Altitude**
-------------     ---------------
Troposphere       0\ km to 12\ km
Stratosphere      12\ km to 50\ km
Mesosphere        50\ km to 80\ km
Thermosphere      80\ km to 700\ km
Exosphere         700\ km to 10 000\ km

Table: \label{tabulka_atmosfera}Atmospheric layers

An atmosphere represents a heterogeneous anisotropic lossy medium,
changing its properties in time, which significantly influences an incident optical beam.

## Atmospheric composition

### Gas components

The atmosphere consists of various atoms and molecules. Its gas components can
generally be divided into two categories [@bouchet]:

 * components with fixed density proportion or majority components, especially:
    * $N_2$ - 78%
    * $O_2$ - 21%
    * $Ar$ - 0,9%
    * $CO_2$ - 0,03%
 * components with variable density (the concentration depends on the geographical
   location), e.g. water vapor, $O_3$ - 0% - 4%

### Aerosols

Aerosols are are small solid or liquid particles suspended into an atmosphere. Their size varies
from few nanometers to tens of micrometers; they drift from the stratosphere to the surface
and their lifetime is variable, dependent on particles' weight (lighter particles
are less attracted to the ground and have longer lifetime).

Aerosols can be divided according to their origin onto:

  * natural aerosols
    * volcanic aerosols
    * dust
    * sea salts
    * various forms of condensed water
  * human-made aerosols
    * smoke
    * sulfates

While sea salts and desert dust tend to be greater in size, they have shorter lifetime (few days)
and their range is thus local. Volcanic aerosols, caused by major volcanic eruptions,
can be dragged into stratosphere and remain there for up to 2 years (converted to droplets of
sulfuric acid).

**Type**               **Radius [$\mu m$]**                          **Concentration [$cm^{-3}$]**
--------               --------------------                          -----------------------------
Air molecules          $10^{-4}$                                     $10^{19}$
Solid particles$\quad$ $10^{-2}$ to $1$                              $10$ to $10^{3}$
Fog                    $1$ to $10$                                   $10$ to $100$
Cloud                  $1$ to $10$                                   $100$ to $300$
Raindrops              $10^{2}$ to $10^4$                            $10^{-5}$ to $10^{-2}$
Snow                   $10^{3}$ to $5 \cdot 10^{3}$                  --
Hailstones             $5 \cdot 10^{3}$ to $5 \cdot 10^{4} \quad$    --

Table: Radius and concentration ranges of common particles\label{aerosol_concentration} [@alkohidi]

A **Fog** is a cloud of very small particles of water, suspended into air, floating near ground. A fog reduces visibility
to less than 1km. A fog forms when the difference between the air temperature and the dew point is generally less than 2.5 °C [@wiki_fog].

A **Mist** are small droplets of a condensed water, suspended into air. The condensation is caused by sudden cooling of warm, humid
air (winter exhaling).

A **Haze** is caused by suspending combination of dry aerosols (smoke,dust,ash) into air, wich leads to mitigation of the visibility.

## Atmospheric conditions

### Transmittance

A transmittance $\tau$ is the ability of propagation environment to pass a light through it
and is defined as a fraction of the emitted light power $P(0)$ and the power leaving examined
medium $P(d)$, at distance $d$ from the transmitter:

$$\tau(d) = \frac{P(d)}{P(0)} = e^{-\sigma d} \quad [-]$$ {#eq:transmittance}

### Visibility

A visibility was defined for meteorological needs as an estimated distance, at which an observer can just see some reference marker.
For optical purposes, the visibility is deposing about the atmosphere transparency, which is closely related to weather conditions. This method
is subjective, dependent on many factors and with no strict rules.
To eliminate the subjective matter of preceding observations, a *meteorological optical range* or a visibility $V$ with strictly
defined measuring methods was established, providing objective, repeatable measurements.

In the atmosphere, a meteorological optical range is the distance, that a parallel luminous ray beam, emanating from an incandescent lamp, at a color temperature of
2700K, must reach so that the luminous flux intensity is reduced to 0.05 if its original value [@bouchet].

A MOR can be measured by transmissometers, whose principle is based on emitting a known optical power and measuring an amount of light incident at photodetector. Transmissometers are constructed either with transmitter-receiver or transceiver-mirror topologies.

A visibility is closely related to a transmittance; this relation is further discussed later in section dealing with Mie scattering.

## Atmospheric phenomena

Essential phenomena influencing an optical beam are:            <!--- Replace link -->

  * Scintillations - a heated air rising from the ground causes variability of air refractive
    index (turbulence), leading to beam magnitude fluctuations
  * Scattering - interaction of a light and particles causing directional beam redistribution
    * Rayleigh scattering - particles are equal or smaller size than the beam wavelength
    * Mie scattering - particles are of the same order of magnitude as the beam wavelength
  * Absorption - a part of incident light is transformed to a heat or a chemical energy, decreasing the beam magnitude.

  The influence of scattering and absorption is summarized by *extinction coefficient* $\sigma$ defined as:

  $$\sigma = \alpha_m + \alpha_n + \beta_m + \beta_n \quad [km^{-1}]$$ {#eq:extinction_coefficient}

  Where $\alpha_m$ stands for a molecular absorption coefficient representing the molecular
  absorption, $\alpha_n$ is an absorption of aerosols and $\beta_m,\beta_n$ are
  Rayleigh and Mie scattering coefficients.

### Scattering

A scattering of a light deflects the ray from its previous path by interaction with particles or on different refractive index borders.
Rayleigh, Mie, Tyndall, Brillion and Raman scattering are distinguished, however only Rayleigh and Mie are important to
a FSO.

**Rayleigh scattering** occurs when an optical beam interacts with particles of equal or smaller size than the beam wavelength. One of
examples of Rayleigh scattering is the interaction between the sunlight and molecules of atmosphere gases. While a solar beam with a broad spectrum is
making a way through the atmosphere, some wavelengths, defined by the atmospheric composition, are scattered. Since shorter wavelengths visible as blue
color are more scattered, the clear sky is colored with blue. A similar effect causes the sunrise and the sunset being colored red. At this case the
solar beam is traveling significantly longer distance, causing most of the shortest wavelengths being scattered out, leaving only the longer
wavelengths.

Rayleigh scattering negatively affects a laser beam, increasing total atmospheric attenuation. Thanks to the knowledge of the atmospheric composition,
a molecular scattering coefficient $\beta_m$ can be approximately obtained [@bouchet]:

$$\beta_m(\alpha) = A \cdot \lambda^{-4} \quad [km^{-1}]$${#eq:molecular_scattering_coeff}

$$A = 1,09 \cdot 10^{-3} \frac{P}{P_0} \frac{T_0}{T} \quad [km^{-1}\mu m^{-1}]$${#eq:molecular_scattering_coeff_constant}

Where $P[mbar]$ is an atmospheric pressure, $P_0=1013.25\ mbar$ is the atmospheric pressure at sea level , $T[K]$ is an atmospheric temperature and $T_0=273.15K$
is a reference temperature. At $1600\ nm$, $20\ $°C and the sealevel pressure, this gives the result of $1.744 \cdot 10^{-5}km^{-1}$. This leads to conclusion that
the attenuation caused by Rayleigh (molecular) scattering at infrared spectrum, where all common lasers operate, is negligible.

**Mie scattering** occurs when an optical beam interacts with particles whose size is comparable to the beam wavelength.
The major contributors to Mie scattering in atmosphere are aerosols, especially particles of condensed water (mist,fog).
The essential problem in evaluating a scattering coefficient is that concentration and size of aerosols varies in time and space.

The solution is the use of visibility as the rule of proportion to aerosols concentration. The Mie (aerosol) scattering
coefficient $\beta_n$ can then be expressed as [@bouchet][@rammprasath]:

$$\beta_n = \frac{3.91}{V} \left( \frac{\lambda_{nm}}{550_{nm}} \right)^{-Q} \quad [km^{-1}]$${#eq:aerosol_scattering_c}

where $V$ stands for a visibility, $\lambda_{nm}$ is a wavelength in nanometers and $Q$ is a scattering particle size distribution parameter.
The Q parameter can be modeled either by Kim or Kruse model.

**Q parameter**        **visibility range**
---------------        -----------------
$1.6$                  $>50\ km$
$1.3$                  $6\ km$ to $50\ km$
$0.585V^{1/3}\qquad$   $0$ to $6\ km$

Table: Q parameter values by Kruse model\label{tab_kruse}

Since the Q values from table \ref{tab_kruse} were defined from haze particles (and thus from long visibilities),
this model is considered not to be accurate enough for foggy situations.

**Q parameter**        **visibility range**
---------------        -----------------
$1.6$                  $>50\ km$
$1.3$                  $6\ km$ to $50\ km$
$0.16V+0.34\qquad$     $1 \ km$ to $6\ km$
$V-0.5$                $0.5\ km$ to $1\ km$
$0$                    $<0.5\ km$

Table: Q parameter values by Kim model\label{tab_kim}

The table \ref{tab_kim} contains modified values suitable also for lower visibilities.

![Comparison of ROF and calculated fog attenuations[@ijaz]](img/mie_scattering.pdf){#fig:mie_scattering}

With the knowledge of previous facts, it is obvious that Mie scattering could cause an enormous attenuation to a laser beam and
presents crucial part of the extinction coefficient. With sufficiently low values of the molecular and the aerosol absorption and
Rayleigh scattering, those can be neglected and the extinction coefficient can be considered as equal to the Mie scattering coefficient:

$$\sigma \cong \beta_n$${#eq:extinction_coefficient_beta}

An attenuation in logarithmic domain can be calculated by:

$$\alpha_{ext,dB} = 0.23 \sigma \quad [dB]$${#eq:extinction_coefficient_db}

### Scintillations

A scintillation is a flash of light, produced in a transparent material by the passage of a particle [@alkohidi].

A **turbulence** is an invisible air mass movement caused by collision of two parts of air with
different direction and/or velocity. This phenomenon is generally caused by hot air rising above the hot surface (deserts,
roads, parkings).
Turbulences differ in temperature and size, which varies from few centimeters to hundreds of meters.

Turbulences are causing local changes to
an air refractive index by local temperature and atmospheric pressure fluctuations, leading to random beam scattering and
multipath propagation. If those inhomogenities are larger than the incident beam, the beam is deviated (beam wander). In opposite case, the beam is
widened (scattered). In practice, various sizes of inhomogenities are present at the same time and both previously mentioned effects thus apply.

![Influence of variable sized inhomogenities](img/turbulences.pdf){#fig:turbulences}

Turbulences are mostly analyzed by statistical methods. By the use of Kolmogorov model, turbulent phenomena are described by a Kolmogorov structural function $D_n$,
which is defined as:

$$D_n = {C_n}^2r^{\frac{2}{3}} \quad \text{for} \quad l_0 \ll r \ll L_0 \quad [-]$${#eq:structural_function}

${C_n}^2$ stays for a *structural parameter of the refractive index*, which describes turbulence intensity and is estimated from on meteorological data.
An atmosphere without turbulences has ${C_n}^2 = 0$.
$r$ is the distance between the transmitter and the receiver and $l_0$ and $L_0$ is the minumal and maximal size of the turbulence.

Consequently, a relative scintillation fluctuation can be expressed as [@krivak]:

$${\sigma_{I,rel}}^2 = K \cdot {C_n}^2 \cdot k^{\frac{7}{6}} \cdot {L_{12}}^{\frac{11}{6}}$${#eq:relative_scintillation_variance}

$K$ is constant of the wave (1.23 for a plane wave, 0.5 for a spherical wave), $k$ is a wave number and $L_{12}$ is the
distance between the transmitter and the receiver.

For a plane wave, a low turbulence and a specific receiver, the attenuation caused by turbulences can be evaluated as [@krivak]:

$$\alpha_{turb} = 2 \cdot \sqrt{23.17 \left ( \frac{2 \pi}{\lambda}  \right )^{\frac{7}{6}} {C_n}^2 {L_{12}}^{\frac{11}{6}}}\quad [db/km]$$ {#eq:turbulence_attenuation}

Common attenuation values per 1km for appropriate wavelengths and structural parameters are shown in the table \ref{turbulence_attenuation}

**Turbulence**                                      **Low**          **Moderated**     **High**
--------------                                      ---------------  ---------------   ---------------
${C_n}^2$ [$m^{-2/3}$]                              $10^{-16}\quad$   $10^{-14}\quad$  $10^{-13}\quad$
$\alpha_{turb}(\lambda = 980\ nm)$ [$dB/km$]\qquad  0.51              5.06             16.00
$\alpha_{turb}(\lambda = 1550\ nm)$ [$dB/km$]       0.39              3.87             12.25

Table: Turbulence attenuation for 1km and 30km distances\label{turbulence_attenuation}

Further models are discussed in [@krivak].

### Molecular absorption

A molecular absorption is caused by the interaction between an incident light and molecules of the atmosphere. The absorption coefficient depends on
character and density of molecules. Selective character of this absorption is obvious in [@fig:transmittance_molecular].

![Transmittance of the atmosphere due to molecular absorption[@minerals]](img/transmittance_molecular.pdf){#fig:transmittance_molecular}

Molecules of atmospheric gases have discrete energy levels, whose distribution (energy) is dependent on its compound. An incident photon
with energy $E = h \cdot \nu$, corresponding to energy layers difference of the molecule, gets absorbed simultaneously, together with exciting electron
at the lower layer. This phenomena is expressed as:

$$\nu = \frac{E_2 - E_1}{h} \quad [Hz]$$ {#eq:molecular_abs_frequency}

Where $\nu$ stands for a photon frequency, $h$ is Planck's constant and $E_1$ and $E_2$ are energy layers of the incident molecule.
The width of spectral windows is affected by Doppler effect caused by the movement of molecules relatively to the radiation and pressure
and temperature fluctuations.

There are few wavelength ranges where the absorption drops to values lower than $0.2dB$, these regions are called *transmission windows*.
Global transmission windows corresponding with [@fig:transmittance_molecular] are mentioned in the table \ref{transmission_windows}.

**Band**                        **Wavelength range** [$\mu m$]
--------                        ------------------------------
Visible to very near IR$\quad$  $0.4 -1.4$
Near IR or IR I.                $1.4 -1.9$ and $1.9 - 2.7$
Mean IR or IR II.               $2.7 - 4.3$ and $4.5 - 5.2$
Far IR or IR III.               $8 -14$
Extreme IR or IR IV.            $16-28$

Table: Global transmission windows\label{transmission_windows} [@bouchet]

\clearpage

### Aerosol absorption

Aerosols increase an atmosphere attenuation by the scattering, which was discussed in Mie scattering theory above. An another part of
the aerosol attenuation is caused by an aerosol absorption. The aerosol absorption is described by an aerosol absorption coefficient
$\alpha_n$ which is dependent on
an absorption cross section $Q_a$, unlike Mie scattering theory, where the scattering particle size distribution parameter $Q$ was used.

The absorption cross section presents the probability of a molecule to absorb a photon of a particular wavelength and polarization.
The absorption coefficient is given by following equation [@bouchet]:

$$\alpha_n(\lambda) = 10^5 \int_{0}^{\infty} Q_a \left ( \frac{2 \pi r}{\lambda}, n'' \right) \pi r^2 \frac{dN(r)}{dr} dr \quad [km^{-1}]$${#eq:absorption_coeff}

Here $n''$ is the imaginary part of the refractive index $n$ of an aerosol, $r [cm]$ is the radius of particles and $dN(r)/dr [cm^{-4}]$ is the
particle size distribution per unit of volume.

For optical beams in a visible and near infrared band, the imaginary part of refractive index is very low, contribution of the aerosol absorption
to overall atmospheric attenuation and extinction coefficient thus can be neglected [@bouchet].

**Cloud attenuation**

A cloud is floating mass of condensed water (and chemicals) in form of droplets and frozen crystals. The concentration of those varies at $100\ cm^{-3}-300\ cm^{-3}$
(vid. table \ref{aerosol_concentration}). Common cloud types and their heights above sea level are shown in [@fig:cloud_types].

![Common cloud types[@lazarro]](img/cloud_types.pdf){#fig:cloud_types}

The attenuation of clouds is basically caused by Mie scattering. According to Mie theory, discussed in [scattering](#scattering) section,
the cloud attenuation is calculated based on the knowledge of the scattering coefficient, calculated from the visibility and the size distribution parameter $Q$
which are obtained
from Kruse or Kim model ([@eq:aerosol_scattering_c]). For FSO links located on buildings, the acceptable height above the ground does not exceed few
hundreds of meters. Thus we are most interested in low level clouds, namely Stratus, Cumulus and Stratocumulus.

![Specific attenuation of different cloud types [@nadeem2]](img/cloud_attenuation.pdf){#fig:cloud_att}

**Fog, haze and mist attenuation**

A fog can be understood as a cloud of small water (and smoke) particles suspended into air, moving near ground.
A Haze and a mist differs from a fog by visibility longer than 1km. According to the table \ref{aerosol_concentration}, a fog have particle concentration of
about $10\ cm^{-3}-100\ cm^{-3}$. Thanks to presence of water droplets, a fog is significantly decreasing the visibility and, based on its density, causes great
attenuation to an optical beam.

Two major types of a fog are distinguished, depending on how the water condensation occured. Those differ in particle dimensions.

*Radiation (continental) fog* is formed by the cooling of a land after the sunset (caused by heat radiation) and its particle diameters varies around $4\mu m$.
*Advection (maritime) fog* is formed when moist air passes over a cool surface by advection. An advection fog particles size are
close to $20 \mu m$ [@awan].

Based on the fact, that there is a correlation between the fog attenuation and the liquid water concentration [@bouchet], the specific attenuation coefficient
(for $\lambda = 630nm$) can be expressed as:

$$\gamma = 360W^{0,64} \quad [dB/km]$${#eq:specific_att_fog}

Where $W$ stands for liquid water concentration [$kg/m^3$].

The presence of a fog varies significantly over days and months and is dependent on a locality. Based on longtime measurements, a cumulative distribution
probability function of a specific attenuation can be evaluated. In [@fig:cdf_fog], monthly variations of two month data measured at Milan at
moderate continental fog conditions are shown. In this example, for 90% of the time, the specific attenuation was smaller than $10dB/km$ and for
approximately 95% of time the value did not exceed $20dB/km$.

![Cummulative distribution function for two months[@awan]](img/fog_cdf.pdf){#fig:cdf_fog}

The reasonable value of a link margin must be chosen based on the knowledge of a CDF function or number of days per year with fog and required link availability.

### Precipitation attenuation

A **rain** are liquid water droplets condensed from an atmospheric vapor, heavy enough to fall due gravity.
A rain attenuation is mostly caused by the scattering [@bouchet]. To evaluate the rain scattering attenuation, [@eq:absorption_coeff] defined
[in aerosol absorption](#aerosol_absorption) section can be used. While the used wavelength is much smaller than the particle size, the standardized cross
section $Q_d$ becomes equal to 2, wavelength independent [@brazda]. [@eq:absorption_coeff] is now simplified to:

$$\alpha_n(\lambda) = 10^5 \int_{0}^{\infty} 2 \pi r^2 \frac{dN(r)}{dr} dr \quad [km^{-1}]$${#eq:absorption_coeff_simplf}

Where $r$ and $r+dr$ means the minimal and maximal diameter of an equivalent rounded water drop and $N(r)$ is the drop size distribution.
Two common drop size distribution functions are known; first of them, Marshall and Palmer (M-P) distribution function, defines the rain
specific attenuation as:

$$\alpha_{rain} = 1,6 \cdot R^{0,63} \quad [dB/km]$$ {#eq:mp_distribution}

The rain specific attenuation with use of Carbonneau distribution equals to [@brazda]:

$$\alpha_{rain} = 1,076 \cdot R^{0,67} \quad [dB/km]$$ {#eq:carb_distribution}

Here $R [mm/h]$ stays for the rain rate.

The graphical comparison of those models can be found in [@fig:rain_att].

![Comparison of M-P and Carbonneau distributions[@brazda]](img/rain_att.pdf){#fig:rain_att}

**Snow attenuation**

A snow are flakes of frozen water, falling from clouds. Their size can reach up to 20\ mm and thanks to this, fluctuations caused by snowfall can be
slightly stronger than those caused by rain. A snow is classified by liquid water compound as dry or wet, having different effects on an incident
beam.

A snowflake crossing laser beam can cause various attenuation, depending on the snowflake orientation, the distance from the transmitter and the cross section of the beam.
A specific attenuation caused by snowfall can be expressed as [@nadeem]:

$$\alpha_{snow} = a \cdot S^b \quad [dB/km]$${#eq:snow_att}

Here $S [mm/h]$ stands for a snow rate and $a$ and $b$ are constants, dependent on the wavelength and the snow humidity. For a wet snow:

$$a = 5,42 \cdot 10^{-5} \lambda + 5,4958776 \quad b=1,38$$ {#eq:wet_snow_const}

for a dry snow:

$$a = 1,023 \cdot 10^{-4} \lambda + 3,7855466 \quad b=0,72$$ {#eq:dry_snow_const}

From [@fig:snow_att] it is obvious, that the dependence of a specific attenuation on the snow rate is almost linear and independent on
the wavelength.

![Specific attenuation of a snow at different wavelengths[@nadeem]](img/snow_att.pdf){#fig:snow_att}

## Other phenomena \label{sec_other_pheno}

Just like the optical link, the outdoor transceiver itself is also affected by weather
conditions. The further discussed outdoor transceiver is pure photonic, problems related to
electronic parts (EMC, temperature range) will not be discussed.

Several phenomena which should be taken in account still remain, especially:

 * Console bending caused by temperature changes of the material
 * Vibrations caused by precipitation and/or wind
 * A fog or a hoarfrost on the receiving optics

The first two mentioned could cause beam diversion or beam wander and are matter of
transceiver case and console construction and toughness. Fogged or frozen optics
cause scattering of the incident beam, leading to attenuation or extinction of
the beam.

The fog on the glass is caused by adsorption of water vapour contained in air. When the air,
at specific temperature and humidity blows on a solid object colder than the air, the air
is being cooled and decreases its volume while the water vapour component remains. This leads
to increase of relative humidity (RH). When the relative humidity reaches 100% (dew point), the air becomes
saturated and additional decrease of volume (temperature) or increase of moisture causes the water to condense.
The resulting beam attenuation depends on particle number and size. At temperatures below
zero, the fog turns into the hoarfrost, forming small crystals of ice.

 <!--
 Interesting:
 http://www.dtic.mil/dtic/tr/fulltext/u2/a030871.pdf
 -->

## Summary

The second chapter deals with the atmosphere from a FSO point of view. An atmospheric composition and conditions are defined. Three main
atomospheric phenomena affecting an optical beam - scintillations, scattering and absorption are defined and the way of predicting their
presence and effect is presented. The greatest attenuation to a laser beam (hundreds of decibels) may be caused by Mie scattering (fogs),
this kind of attenuation is hard to prevent differently than by sufficient power margin. On the other hand, scintillations (turbulence)
cause energy fluctuations that can be avoided or at least mitigated. Atmospheric absorptions can be avoided by using suitable optical beam parameters.
The construction of outdoor transmitter is equally important and should be taken in account.
Next chapter presents several methods for mitigation of negative atmospheric phenomena.
