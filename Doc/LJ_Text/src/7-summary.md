\chapter*{Conclusion}
\phantomsection
\addcontentsline{toc}{chapter}{Conclusion}

In the first, theoretical, part of the thesis, a FSO link is introduced and its components are discussed with
mention of requirements. An atmosphere is explained, with focus
on atmospheric phenomena affecting the FSO link. Each phenomenon has different matter and possibility of prediction. Their effect on propagating
beam can be more or less mitigated or even avoided by suitable methods. The crucial problem is caused by Mie
scattering of aerosols (fog), the attenuation can reach even several hundreds of decibels and can be
mitigated by a sufficient power margin of the FSO link. By the use of an apropriate beam wavelength, the
atmosphere absorption can be neglected. The remaining phenomenon - scintillations, caused by turbulences and
leading to beam wandering, can be successfully mitigated by several methods which are explained in the third
chapter.

The second, practical, part of the thesis deals with the design and the realization of a support system for a FSO link.
The whole design is based on the unmanaged Media Converter design, implemented on a single FPGA chip of
Xilinx XUPV5 Evaluation Platform. A softcore microcontroller MicroBlaze is implemented
on the top level HDL design as the core of the support system. A custom network switch
and network interface was implemented to connect the MicroBlaze to the Ethernet network. An IP stack functionality is provided by custom port of LwIP library.
The MicroBlaze runs web server, providing interactive user interface, that allows user to remotely control all parameters
provided by the top level design and the connected HVAC unit. The remote user gets
full control over connected HVAC unit and provided top level design (vid. appendix \ref{web-ui}).
Based on settings from the web interface, the microcontroller sends UDP packets with link events
- e.g. when the optical beam is interrupted or when the frame synchronization is lost. Those packets contain a timestamp
and thus could be harvested and visualized in remote PC.

The support system allows user to remotely control the system, the
user also gains access to information about long-term and short-term link reliability, bit error rate and weather and link conditions. Those data can then be involved
in studies of the optical channel.

The git repository of the thesis, including the text, the ISE project, the XPS project and the software project is available
at: [https://gitlab.com/OK2NMZ/thesis-fso](https://gitlab.com/OK2NMZ/thesis-fso).
