\clearpage

# Support system for FSO

## Preface

By gathering information about link state and optical channel conditions, the atmospheric
phenomena can be understood better and appropriate actions can be performed to prevent their
negative influence. Following design enables both realtime and long term data gathering, their graphical
representation and remote FSO transmitter management. The design is implemented on a single FPGA chip together with
the Media Converter, the management is accessible via web interface.

Following chapter introduces the initial design, which the support system is designated for and presents
proposed system solution and top-level implementation.

![Simplified system scheme](img/ml505_clipart.pdf)

## Initial design

Further discussed design is based on the working prototype of the Media Converter design implemented on the
XUPV5-LX110T Evaluation Platform. The base design implements full duplex, high speed data line
connecting the Ethernet PHY chip with the SFP optical transceiver, including fast fading and slow fading safeguard via
Reed-Solomon (RS) encoding and frame caching, respectively.
The base design works as an unmanaged media converter from metallic to optical medium and is to be employed in an experimental FSO link.
The electronical and optical frontends are split into two devices, connected by optical fibres. The fibres are
linked directly to the transmitting/receiving optics.

### XUPV5-LX110T Evaluation Platform (ML505)

The XUPV5-LX110T Evaluation Platform is Xilinx University Program unified platform for teaching
and research of digital communication systems.

![XUPV5-LX110T Evaluation Platform [@xil_ml505]][xupv5_img ]

The board is identical to ML505 Evaluation Platform, with only difference in the variant of the
Virtex-5 FPGA chip - Xilinx Virtex-5 XC5VLX110T FPGA on the XUPV5 vs. XC5VLX50T on the ML505.

The Evaluation Platform contains many on-board industry standard interfaces, e.g. following :

 * RJ-45 Tri-speed Ethernet PHY Transceiver
 * SFP/SMA/SATA/PCI-E Gigabit transceivers
 * RS-232
 * USB

and memories:

 * 256MB DDR2
 * 32MB ZBT Flash
 * 1MB SRAM
 * 2x32MB Flash for device configurations
 * 8Kb $I^2C$ EEPROM

### Media Converter

All bridged data go through the Main data line ([@fig:img_main_data_line]), which connects the Ethernet PHY chip and the SFP module.

![Main data line block scheme with interfaces](img/design_main_line.pdf){#fig:img_main_data_line}

Differential data signals from the metallic medium are processed by the onboard *PHY* chip (Marvell 88E1111) which takes care of
the physical signal encoding and the conversion to serial differential SGMII interface. The PHY chip is further connected via the SGMII interface to the
Xilinx *Tri-mode Ethernet Media Access Controller* (TEMAC), which is hard-wired on the FPGA and handles frame synchronization
and deserialization to 8 bit wide
TEMAC Client Interface. *Flow controller* acts as a frame buffer between Ethernet and SFP
interfaces and passes data to the *MAC*. The MAC block handles 8b/10b conversion, RS encoding ensuring BER mitigation and a DDR FIFO for data
storage during optical signal interrupts (upto about 1s). The final encoded 10 bit wide data stream is serialized
in *Gigabit Transceiver* (GTP) and connected via differential pairs to the SFP module.

## Proposed solution

Web servers are communicating with clients (web browsers) via TCP connections, which require implementation of the whole
TCP/IP stack. A direct hardware implementation (in VHDL or verilog) of the web server and TCP/IP stack is posible
however due to limited abstraction and provided functions of hardware description languages (HDL) would be very difficult and is
not commonly used. For this purpose
Xilinx provides softcore microcontrollers which can be customized to fit the user design. Those microcontrollers
act as common single chip microcontrollers however they are implemented within the FPGA structure. Their functionality is given
by binary code compiled from assembly language or C.

*PicoBlaze* is a very small and simple 8-bit RISC softcore microcontroller, which can replace complex state machines
and simplify the design since the program code is written in assembly language.

*MicroBlaze* is an advanced, 32-bit RISC softcore microcontroller. The Microblaze program code can be written in assembly or C language
and compiled by GNU C Compiler (GCC). The MicroBlaze is highly configurable and is designed as a separate project in Xilinx
Platform Studio (XPS). XPS allows connection of plenty further configurable IP cores to the MicroBlaze core
via peripheral buses (LMB, PLB, AXI), the address space of MicroBlaze is monolithic with memory mapped I/O.
The XPS project can be then either converted to the bitstream and directly loaded to the FPGA chip, or exported to the ISE project and instantiated
 as a black box, described by the top HDL source.

Depending on available memory (and timers), Microblaze can run a simple bare-metal firmware as well as a common Real Time Operating
System (RTOS) or the Linux kernel
and is thus well suitable for advanced applications including a web server. The further discussed design describes the implementation of MicroBlaze
microcontroller, required peripherals and the connection to initial design. The web server is accesible from the local area
network by default; if the network was connected to the internet, the device could be managed from anywhere.

## Frame switching

The TCP communication between the MicroBlaze (web server) and the browser must be at some point separated from the mainstream one. Frames for
MicroBlaze should be
directed to its network interface and should not pass over the optical interface as well as mainstream frames should not get to MicroBlaze
as they would have been dropped. The suitable place for the frame routing is Flow Controller since it is in
the middle of the Main data line and disposes of the TEMAC Client interface.

### TEMAC Client interface

TEMAC Client interface is flexible 8 bit or 16 bit wide interface designed for simplified frame handling. The client interface is further
divided into two slightly different interfaces. The client receive and transmit sequences also slightly differ.

 **Client transmit interface**

 The client transmit interface defines 7 signals:

  * TxCLK - Tx Clock
  * TxD[7:0] - 8 bit Tx data
  * TxDV - Tx Data Valid
  * TxACK - Tx Acknowledge
  * TxUNDERRUN
  * TxCOLLISION
  * TxRETRANSMIT

  Last three signals will not be discussed as they are not used in the design.

![TEMAC Client interface - Client transmission sequence](img/temac_cif_tx.pdf){#fig:temac_cif_tx}

Transmitter sets first data byte to TxD bus and raises TxDV pin. The first byte remains on the bus until receiver acknowledges it by
raising the TxACK pin. Since then, the transmitter serves new byte to TxD bus on every rising edge of TxCLK. The transmission ends by
setting the TxDV signal low after the last byte. The transmission is ilustrated in [@fig:temac_cif_tx]

**Client receive interface**

The client receiver interface defines 5 signals:

 * RxCLK - Rx Clock
 * RxD[7:0] - 8 bit Rx data
 * RxDV - Rx Data valid
 * RxGF - Rx Good frame
 * RxBF - Rx Bad frame

![TEMAC Client interface - Client reception sequence](img/temac_cif_rx.pdf){#fig:img_temac_cif_rx}

The client receiver must be ready to accept the data stream as soon as the TxDV signal is raised by the transmitter. Following bytes
are loaded to the data bus with following rising edges of the RxCLK signal. The RxDV signal is set low after transmission of the last
data byte.
 Before the next frame can be sent, either RxGF or RxBF signal must be driven high to signalize the condition of just transmitted frame (the TxBF signal can signalize e.g. CRC mismatch). The reception sequence is ilustrated in [@fig:img_temac_cif_rx].

### Flow Controller \label{chap_flow_controller}

The original functionality of Flow Controller was to serve as a frame buffer between Ethernet and SFP interface. For this purpose, a single
8\ bit wide FIFO memory was implemented on both data directions ([@fig:img_flow_ctrl_with_ub]). Each FIFO is accessed by write-in (W) and
read-out (R) state machines. To enable frame routing to and from Microblaze, MAC matching unit and additional two
FIFOs were added. The state machines were slightly modified as well.

![Flow Controller read-out (R) and write-in (W) state machines and data FIFOs. Modified or added parts are highlighted in red.](img/flowcontroller.pdf){#fig:img_flow_ctrl_with_ub}

**Frame transmission from Ethernet to SFP**

Frames coming from Ethernet interface arrive to the FIFO from Ethernet to SFP interface (ETH-SFP FIFO) write-in state machine (FSM). The
FSM starts writing the data to FIFO as soon as RxDV signal becomes high. This operation is clocked from the Ethernet interface and
is performed independently of the SFP interface. Based on the RxBF or RxGF signal, the read-out state machine (clocked from SFP interface)
either drops currently received frame (if RxBF = '1') or initializes the transmission to SFP interface by raising TxDV signal and waiting
for TxACK. The interface from SFP to Ethernet is symmetric.

**Frame transmission from Ethernet to MicroBlaze**

In parallel with writing the data to ETH-SFP FIFO, the first six bytes of incoming frame (destination MAC address) are compared with the MAC
address of MicroBlaze and the
link broadcast address (FF:FF:FF:FF:FF:FF). The MAC Compare unit register is connected to the MicroBlaze core and is configurable from the software.
Based on information from MAC Compare unit the SFP read-out state machine sends the frame either to SFP, MicroBlaze FIFO or both interfaces at a time.

![Frame for MicroBlaze being processed in Flow controller](img/fc_eth_to_ub_transmission.pdf){#fig:img_flow_ctrl_eth_to_ub}

**Frame transmission from MicroBlaze to Ethernet**

Frames from MicroBlaze are stored in UB-ETH FIFO. The Ethernet read-out state machine starts its operation when the signal EMPTY of one of
the connected FIFOs goes low. The mainline communication is prioritized since the emptiness of SFP-ETH FIFO is polled first, so the
Main data line throughput is practically not affected by MicroBlaze communication. Even if there is significant traffic on mainstream, some
interframe gaps occur, so the MicroBlaze takes a turn. Theoretically, when there would be so heavy traffic that frames would folow each other,
the MicroBlaze would never take a turn. To handle this situation, fixed aggregation could be applied by slight modification of
the ethernet read-out state machine. A small counter could be added, limiting the maximum count of following transmissions from SFP to
the Ethernet interface when the MicroBlaze Tx FIFO is not empty.
