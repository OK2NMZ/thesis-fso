\clearpage

# Methods for mitigation of atmospheric influences

## Aperture averaging

An aperture averaging is a common method for decreasing optical intensity fluctuations caused by turbulences at the receiver.
The aperture averaging can be taken as a form of spatial diversity, because it
is caused by receiving lens whose size is greater than a fading correlation width $\rho_c$. A receiver with an aperture size
smaller than $\rho_c$ is called a point receiver. $\rho_c$ is
obtained from a covariance function of irradiance, which describes the correlation between irradiance fluctuations at two points
in the receiver plane [@viswanath]. The correlation width is defined as:

$$\rho_c = \sqrt{z/k} \quad [m]$${#eq:correlation_width}

Where $z$ means the distance between the transmitter and the receiver and $k$ is a wave number. As the aperture diameter increases above the $\rho_c$,
low and high intensity patches incident on receiver lens get averaged, decreasing total signal fluctuations. The fading reduction, caused
by aperture averaging, is quantified by an aperture averaging factor $A$, comparing scintillation index $\sigma_{i}(D)$ for the aperture diameter
$D$ and that of point receiver:

$$A = \frac{\sigma_i(D)}{\sigma_i(0)} \quad [-]$${#eq:aperture_averaging_factor}

Increasing the aperture size have thus positive effect on the system BER. This reduction is observed at all weather conditions and turbulence
strengths [@kaur]. An example use of the aperture averaging on 3,5km long FSO link operating at wavelength of 1550nm under
moderate turbulence can be seen in [@fig:ber_vs_aperture].
\clearpage

![BER vs. aperture diameter under various weather conditions [@kaur]](img/ber_vs_aperture.pdf){#fig:ber_vs_aperture}

## Spatial diversity

MIMO (Multiple Input Multiple Output) is a common method of increasing the channel quality and reliability by the use of multiple spatially separated
transmitters, receivers, or both mentioned.

### Basic principle

Principle lies in splitting data into several streams, each one transmitted using own spatial channel. Multiple spatially diverged
communication channels are established, those channels are thanks to variance of atmosphere parameters in space
and time, affected differently, leading to application of different fading patterns on each channel. By combination of multiple received signals at
receiver, resulting signal fluctuations are noticeable reduced. An example situation can be seen in [@fig:mimo_fluctuations] where the effect of
using 1-3 transmitters on signal fluctuations can be seen.

![Received intensity as a function of time[@korevaar]](img/mimo_fluctuations.pdf){#fig:mimo_fluctuations}

Different spatial diversity arrangements are distinguished:

* SISO - Single Input Single Output (no spatial diversity)
* SIMO - Single Input Multiple Output - Receiver diversity
* MISO - Multiple Input Single Output - Transmitter diversity
* MIMO - Multiple Input Multiple Output - Combination of above

To reach the mentioned effect, receivers must be sufficiently separated by a distance at least equal to the spatial
coherence length $L$, which is defined as:

$$L = \sqrt{\lambda z} \quad [m]$${#eq:spatial_coherence}

This assumption ensures sufficient statistical independence and uncorrelation of receiving signal [@kaur].

### Combining methods

Received signals are
combined using one of combining techniques:

* Equal gain combining (EGC)
* Maximal ratio combining (MRC)
* Selection combining (SC)
* Threshold combining (TC)

Here *Selection combining* is continually scanning the quality of each channel and selects the channel with the best SNR. *Maximal ratio combining* sums all
channels with different weights, depending on
their SNR. *Threshold combining* randomly selects a channel with SNR greater than chosen threshold, if SNR falls below the threshold value,
an another channel is chosen randomly. The threshold combining is simpler than the selection method [@johnsi].
*Equal gain combining* is the simplest method among them, just coherently summing
all received signals. Comparison of mentioned combination methods is figured in [@fig:combination_comparison].

![Comparison of combining methods [@johnsi]](img/combination_comparison.pdf){#fig:combination_comparison}

From [@fig:combination_comparison] it is obvious, that the best performance is reached by the use of the threshold combining, covered deeply in [@johnsi].
For further computations will be used EGC for its simplicity.

With the use of gamma-gamma distribution, resulting photocurrent from $M$ detectors is equal to [@kaur]:

$$i = \sum_{j=1}^M(i_{S,j}+i_{N,j}) \quad [A]$${#eq:egc_sum}

Where $i_{S,j}$ and $i_{N,j}$ are signal and noise contributions from j-th photodetector. Assuming that mean values and variance of signal and noise
are equal, mean SNR gets increased by factor of $\sqrt{M}$ from the value reached by single detector; scintillation coefficient gets also reduced [@kaur]:

$$\langle\mathit{SNR}_M \rangle = \sqrt{2} \langle \mathit{SNR}_1 \rangle \quad [dB]$$ {#eq:egc_snr_decr}

$$\sigma_{I,M}^2 = \frac{1}{M} \sigma_{I,1}^2 \quad [-]$$ {#eq:egc_scint_decr}

In [@fig:ber_vs_receivers], the dependency of the BER on the number of photodetectors under various atmospheric conditions is shown.
\clearpage

![BER vs the number of receivers [@kaur]](img/ber_vs_receivers.pdf){#fig:ber_vs_receivers}

### Results

MIMO technology provides considerable solution for improving a channel BER (SNR) and fading. The diversity gain differs with atmospheric conditions
and also depends on square root of receivers count,
which (thanks to coherence length) significantly affects the transmitter size. Diversity gain
is not dependent on number of transmitters [@johnsi].

## Adaptive optics

Principle of the adaptive optics method lies in the prevention of long-term data losses by realtime correction of incident wawefront phase distortions.
A deformable correction optical element, called *wavefront corrector* is put in front of the photodetector, compensating distortions and leading to
focal plane power distribution improvement.

A part of incident beam is directed onto wavefront sensor, which detects incident wavefront distortions. Based on WFS data, the Wavefront reconstructor
evaluates the need of corrector reshaping which is then translated by a control logic to the wavefront corrector ([@fig:ao_fig]-a). Another promising
concept (model-free), is based on controlling Wavefront corrector by feedback from the photodetector ([@fig:ao_fig]-b).
\clearpage

![Two common approaches of AO[@weyrauch]](img/adaptive_optics.pdf){#fig:ao_fig}

Functionality of this approach is limited only to signal captured by the receiving aperture. In order to maximize the irradiance at the receiving aperture,
an adaptive transmitter system at the opposite end of the link is required [@weyrauch].

An example of adaptive optics effect on the focal plane power
distribution for complete wavefront correction, partial wavefront correction and no correction is shown in [@fig:ao_example].

![Effect of AO on focal plane power distribution [@hashmi]](img/ao_results.pdf){#fig:ao_example}

<!-- ## Beam shaping -->

<!-- ## Modulations -->

<!-- ## Hybrid systems -->

## Channel coding and interleaving

A channel coding, also called *forward error correction* is a common method of securing a data stream against errors caused by a communication channel.
The channel coding adds extra redundancy to the data stream, allowing recovery of certain amount of erroneous bits.
This principle is used in many common wireless systems such as GSM and DVB-T for years.

Channel coding allows  BER improvement at constant transmitter power or transmitter power decrease at constant BER. The margin between
SNR of a uncoded and a coded signal is called a coding gain and beneficially contributes to a FSO power balance. In logarithmic scale is
defined as [@prokes]:

$$G_c = 10 \cdot \log \left( \frac{E_b}{E_0} \right ) - 10 \cdot \log \left ( \frac{E_b}{E_0} \right )_{encoded} \quad [dB]$${#eq:code_gain}

Channel codes are separated to two essential groups:

* Block codes
* Convolutional codes


### Block codes

Block coders separate incoming data into blocks of $k$ bits. The coding process is performed on whole block at a time. The source block of
data is called a symbol, the set of all possible input symbols (bit combinations) is called a *source alphabet*.

The coding is process of assigning symbol from a source alphabet to corresponding symbol from a *code alphabet*, whose length is increased by $r$
(redundancy). The assigning process is based on mathematical operations, whose complexity varies with used code types. On the receiver, the decoding
process consists of assigning a received symbol to a symbol from code alphabet and a corresponding symbol from source alphabet. A code rate $R$ is a measure
of block code redundancy and is evaluated as a fraction of source symbol length $k$ and an encoded symbol length $n$:

$$R = \frac{k}{n} \quad [-]$${#eq:code_rate}

Coded symbols are independent of each other and can be transmitted and decoded separately. Common block code representatives are Hamming code,
Reed-Solomon code and low-density parity check (LDPC). Block codes are qualified as $(n,k)$ according to source and code symbol length, e.g. RS(255,239).

### Convolutional codes

Unlike the block coding, a convolutional coding is applied continually on a data stream. The incoming data stream is being shifted bitwise into a shift
register (sliding window) of a constant length. Concurentlly, at each single clock, certain logical operation is performed on a whole register,
resulting with $n$ output bits. Each encoded bit is thus dependent on other bits, whose number is determined by a shift register length.
A convolutional code rate is a fraction of number of input and output bits (output logic width) [@prokes]:

$$R = \frac{k_0}{n_0} \quad [-]$${#eq:code_rate}

Common convolutional code rates are 1/2 and 2/3.

![An example of convolutional coder [@prokes]](img/convolutional_coding.pdf){#fig:convolutional_coder}

A convolutionally coded sequence can be expressed as an output sequence of a state machine with finite number of states. A graphical visualization
of these states is called a *Trellis diagram*. A convolutionally coded sequence is decoded using a *Viterbi algorithm*, which determines the maximum
likely path through a Trellis diagram.

*Turbo codes* are convolutional codes, constructed by chaining two or more particular coders together, reaching high coding gain.

Currently, the highest coding gain is reached using turbo and LDPC codes since both mentioned are almost reaching the theoretical Shannon limit.
With similar coding gain, LDPC codes require noticeably lower computing power [@khan].

### Interleaving

An interleaving is commonly used method of data scrambling in purpose of eliminating aggregated consecutive error bits (burst) by scattering them onto
wider perspective, so that they can be better handled by channel decoder.

An interleaving is done by writing into matrix of $m$ rows and columns while writing is performed row by row and reading by columns. An interleaver is
often positioned after a channel coder. At the opposite side of the channel, a deinterleaver with similar function is present, positioned before the channel
decoder. A spreading factor is directly proportional to matrix dimensions.

An example of deinterleaving is shown in [@fig:deinterleaving].

![Deinterleaving principle](img/deinterleaving.pdf){#fig:deinterleaving}

## HVAC Unit

Section \ref{sec_other_pheno} describes problems related to a water condensation on the receiving optics. The condensation can be
reduced or even eliminated by reducing the relative humidity near the optics. This can be reached either by decreasing the
water vapor component in air (drying the air), by increasing the volume (temperature) of air or by the combination of both methods.

The lens can either be directly heated by the electric current, flowing through resistive grid within the lens, or indirectly by the
hot air from Heating, Ventillating and air Conditioning (HVAC) unit. The first mentioned method requires a special lens and
provides only heating of the lens. The HVAC unit (based on its type) can maintain constant, suitable conditions either by heating
during cold days or cooling during sunny days when the device is heated by the sun.

## Summary

This chapter presents several methods for mitigation of negative atmospheric phenomena. The first three methods can decrease beam
intensity fluctuations caused by turbulence however require intervention in the transmitter construction. Channel coding and interleaving
are methods implemented in software and can increase the power margin without any hardware modifications. The last mentioned method
is addressed to effects on the FSO transceiver device itself rather than on the laser beam.
