%--------------------------------------------------------------------------------
%
%  EEICT
%
% Author:  Marek Novak
% Data:    3/3/2016
% E-mail:  xnovak0m@vutbr.cz
%
%--------------------------------------------------------------------------------
%
\documentclass{eeict}
\inputencoding{cp1250}
\usepackage[bf]{caption2}
\usepackage[]{units}
\usepackage[hidelinks]{hyperref}
%--------------------------------------------------------------------------------

\title{Improvement of Bit Error Rate in Free Space Optical~Link}
\author{Marek Nov\'{a}k}
\programme{Master Degree Programme (2016), DREL FEEC BUT}
\emails{xnovak0m@vutbr.cz}

\supervisor{Lucie Hudcov\'{a}}
\emailv{hudcova@feec.vutbr.cz}

\abstract{The article describes an inovative bit error rate reduction technique principle and its practical implementation. The design is implemented in an FPGA and can be combined with other more conventional BER reduction techniques. The presented approach benefits from properties of an optical channel which a general RF channel does not have.}
\keywords{BER, FSO, FIFO, FPGA, Xilinx MIG}

\begin{document}

\maketitle

%-------------------------------------------------------------------------------
\selectlanguage{english}
\section{Introduction}
One of the most important factors deteriorating the bit error rate in free space optics (FSO) is atmospheric turbulence. If the size of the "turbles" (turbulent cells) producing this turbulence is larger than the transmitted beam diameter, it can be completely deflected \cite{weyrauch}. This effect called beam wander causes this way a complete signal loss. The duration of this effect is in the order of milliseconds. If a practical communication speed of \unit[1.25]{Gbps} and more is considered, such a channel unavailability cannot be simply covered by interleaving and channel coding. Herein proposed technique solves this problem.

\section{Proposed BER reduction technique}
As shown for example in \cite{mansuripur}, the principle of reciprocity applies in the classical optics. This fact is the root idea of our technique. In the considered communication setup, a full duplex fibre-less free space optical link is used. This allows each side of the link to analyse the incoming signal from the other side to estimate instantly whether the channel is available for transmission or not. This cannot be easily applied in radio frequency links, because of the multipath propagation which does not apply in FSO.

\begin{figure}[bht]
\begin{center}
  \includegraphics[width=14.0cm,keepaspectratio]{operation.png}
  \caption{
      \rm{
      \hspace{0.1cm} High level operation description}}
  \label{operation}
\end{center}
\end{figure}

\subsection{Principle of operation}

When an obstacle in form of a turbulent cell or a flying bird or other object appears in between the two communication terminals, it makes the channel temporarily unavailable. This can be detected either by a drop in receiving optical power and/or by observing integrity of data being received from the other side of the link. The last mentioned detection method can be used mainly when only one side of the link transmits data and the other one is idling and sending so-called comma symbols (which are also used to realize the symbol boundary bit alignment). When an unavailability is detected, the transmission is stopped and further data to be transmitted is put in a FIFO-based buffer. The latency depicted in Figure \ref{operation} is introduced to avoid intermittent drops in the communication. Multiple consequent errors must occur for link to be down.
\par
Thanks to this approach, the optical communication terminal can safe some of the payload from being lost. There is however always a portion of the payload lost because of the detection latency as shown in Figure \ref{operation}. This losses can be partly recovered by interleaving and channel coding or by forward error correction (FEC) mechanisms used at the application level by actual communicating endpoints. Furthermore, when the loss occurs in the middle of a frame, the whole frame is likely lost.

\subsection{Requirements}
\label{Requirements}

The proposed approach requires the communication speed to be fast enough, when the actual received data (comma symbols) is to be used in the channel unavailability detection. Another requirement is placed on the temporary payload storage buffer. It needs to be large enough so that it does not overflow, when the communication is not possible for a longer period of time (e.g. \unit[200]{ms}). Of course, the required storage capacity grows with the communication speed.
\par
For example a single comma symbol which consists of \unit[10]{bits} is transmitted in \unit[8]{ns} on a \unit[1.25]{Gbps} link. On the same link, a \unit[200]{ms} drop-out produces about \unit[25]{MB} of data. In a real design, a DDR memory of \unit[256]{MB} should be therefore sufficient.


\section{Practical implementation}

Figure \ref{technique} shows an overall architecture of the proposed technique. The Loss-of-Signal (LOS) information is used both to stop reading out of the data from the FIFO and to switch the transmitter from transmission of payload to transmission of comma symbols.

\begin{figure}[bht]
\begin{center}
  \includegraphics[width=6.5cm,keepaspectratio]{technique.png}
  \caption{
      \rm{
      \hspace{0.1cm} BER reduction technique block diagram}}
  \label{technique}
\end{center}
\end{figure}

\subsection*{DDR FIFO}

In order to overcome the problem with memory requirements described in \ref{Requirements}, a DDR-based FIFO memory was designed. The implementation itself uses the memory interface generator \cite{UG86} (MIG) IP core from Xilinx and interfaces this with "DDR Reader and Writer". This module uses the half-duplex communication interface with DDR2 SODIMM module and implements a circular buffer in it. Thanks to the bus width of \unit[128]{bits}, the module has enough time to keep the "Input FIFO" empty and the "Output FIFO" full during non-interrupted data transmissions. \unit[128]{bit} wide communication bus was selected, since it is the maximum bus width supported by the ML505 Evaluation Platform used in testing of the design and it gives enough time margin for the processing of input and output FIFOs.
\par 
Data frames are stored in the DDR FIFO in a microframe memory scheme. The scheme was developed to separate individual data frames. It enables the output module to replicate the Start-of-Frame (SOF) flag at the beginning and the End-of-Frame (EOF) flag at the end of each data frame. The scheme uses \unit[128]{bit} data units called microframes. The first fifteen bytes of microframe contain data payload and the last byte is a control character. It indicates whether the SOF or EOF flags should be generated and the size of data payload in each microframe. The indication of the data size in the last microframe is crucial as the incoming frames are not \unit[15]{B} aligned and \unit[15]{B} alignment is needed for the microframe memory scheme.
\begin{figure}[bht]
\begin{center}
  \includegraphics[width=15.5cm,keepaspectratio]{DDR_FIFO.png}
  \caption{
      \rm{
      \hspace{0.1cm} DDR FIFO internal data flow}}
  \label{DDR_FIFO}
\end{center}
\end{figure}

\section{Conclusion}
This article briefly describes a simple, yet efficient technique to reduce the BER of an FSO link. Its performance is however to be evaluated in real environment, nevertheless the theory and current on-the-table testing shows, that this approach can cooperate with existing techniques to reduce the overall bit error rate. The source codes are available at a GIT repository: \url{https://gitlab.com/OK2NMZ/thesis-fso.git}

\section*{Acknowledgement}
The described research was performed in laboratories supported by the SIX project - the registration number CZ.1.05/2.1.00/03.0072.

\begin{thebibliography}{9}
  \bibitem{weyrauch} 
  WEYRAUCH, Thomas; VORONTSOV, Mikhail A. Free-space laser communications
with adaptive optics: Atmospheric compensation experiments, p. 26, 2008.
  \bibitem{mansuripur}
  MANSURIPUR, MASUD. Reciprocity in Classical Linear Optics. Optics and Photonics News. 1998. DOI: 10.1364/OPN.9.7.000053. ISSN 10476938. 
  \bibitem{UG86}
XILINX. Memory Interface Solutions: UG86. 2010. Available from: \url{http://www.xilinx.com/support/documentation/ip_documentation/ug086.pdf}
\end{thebibliography}

\end{document}
