git clone https://github.com/hakimel/reveal.js.git
rm lib -rf
rm js -rf
rm plugin -rf
rm css -rf
mv reveal.js/lib lib -f
mv reveal.js/js js -f
mv reveal.js/plugin plugin -f
mv reveal.js/css css -f
rm reveal.js -rf

