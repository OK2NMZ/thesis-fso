%--------------------------------------------------------------------------------
%
% Text příspěvku do sborníku EEICT
%
% Vytvořil:  Martin Drahanský
% Datum:     26.02.2007
% E-mail:    drahan@fit.vutbr.cz
%
%--------------------------------------------------------------------------------
%
% Přeložení: pdflatex prispevek.tex
%
% Optimální způsob použití = přepište jen vlastní text
%
\documentclass{eeict}
%\inputencoding{utf}
\usepackage[bf,justification=centering]{caption}
\usepackage[hyphens]{url}
\usepackage{titlesec}
\titlelabel{\thetitle.\quad}


%--------------------------------------------------------------------------------

\title{Network interface for MicroBlaze}
\author{Lukáš Janík}
\programme{Master Degree Programme , FEEC BUT}
\emails{xjanik12@vutbr.cz}

\supervisor{Lucie Hudcová}
\emailv{hudcova@feec.vutbr.cz}

\abstract{This article deals with interfacing of Xilinx MicroBlaze (UB) softcore microprocessor  to 1\,Gb/s Ethernet data bus via custom simple Ethernet switch. This interface is implemented on an FPGA board and serves as the lower communication layer for administration of gigabit Ethernet-to-optical bridge.}
\keywords{Free space optics, MicroBlaze, Ethernet, Block RAM}

\begin{document}
% -- Hlavička práce --

\maketitle

%-------------------------------------------------------------------------------
\selectlanguage{english}
\section{Introduction}

Free space optical communication (FSO) brings many advantages compared to common radio frequency (RF) wireless links, however its reliability and availability is considerably dependent on atmospheric conditions \cite{bouchet}. Information about current atmospheric conditions and link state (bit error rate, loss of signal) should be gathered and monitored. 
\begin{figure}[bht]
\begin{center}
  \includegraphics[width=13cm,keepaspectratio]{obrazky/block}
  \caption{
      \rm{
      \hspace{0.1cm} Discussed design block scheme. Green area contains user-logic  written in VHDL, blue area contains Microblaze design blocks, designed in Xilinx platform studio (XPS).}}
  \label{design_overall}
\end{center}
\end{figure}
The implementation of softcore microprocessor on single chip together with Ethernet-to-optical bridge (main data line) presents effective solution for statistical processing and data gathering without the need for another external device or software. 

There are many example implementations of Xilinx MicroBlaze Processor connected to Ethernet however they use own implementation of Media Access Controller (MAC) and use PHY chip exclusively, disallowing further data bridging to optical interface (additional MAC and gigabit Ethernet switch would be needed for frame routing). Further presented design is based on unmanaged media bridge design and utilizes current MAC. 


\section{Design overview}

An example of Ethernet-to-optical bridge allowing full duplex 1\,Gb/s data rate can be seen in Figure\,\ref{design_overall}. This design is implemented on Xilinx ML505 Evaluation Platform on single FPGA chip (\mbox{Xilinx Virtex-5}) and interfaced with onboard PHYs on metallic (1000Base-T) and optical (single-mode 2.5\,Gb/s SFP optical transciever) side.

Main data line signals from metallic interface are processed by PHY chip and passed via SGMII interface to Xilinx Tri-Mode Ethernet Media Access Controller (TEMAC). TEMAC is connected to user logic via simple client interface \cite{ug_temac} (signals DV, DIN, DOUT, GF, BF, ACK). Flow controller serves as a frame buffer between different clock regions, it drops corrupted frames and routes frames to and from MicroBlaze (Control data line). On SFP side, the MAC is significantly simplified and ensures frame synchronization, 8b/10b conversion and conversion between client interface and SGMII interface; none of other advanced MAC functions are needed.

\subsection{Frame routing}
Frame routing is being done in flow controller, Figure\,\ref{flowcontroller}. Data from Ethernet interface is written to ETH-SFP FIFO; first six bytes of each frame containing destination MAC address are compared with MAC address register. If both addresses are equal, signal is sent to ETH-SFP read-out state machine, signalizing that current frame should be written to Microblaze FIFO (ETH-UB FIFO). The MAC address register is controlled from the Microblaze, allowing the change of MAC address from the software.

\begin{figure}[bht]
\begin{center}
  \includegraphics[width=14cm,keepaspectratio]{obrazky/flowcontroller}
  \caption{
      \rm{
      \hspace{0.1cm} Flow controller FIFOs with MAC compare unit and read-out (R) and write-in (W) finite state machines}}
  \label{flowcontroller}
\end{center}
\end{figure}

In the Microblaze uplink, the UB-ETH FIFO is emptied by SFP-ETH read-out state machine. The priority of main data line is ensured by first polling the SFP-ETH FIFO, data from Microblaze is sent only when SFP-ETH FIFO is empty. This behaviour is sufficient for common situations where interframe gaps occur, however, in the case when very long data stream comes from SFP interface, frames from MicroBlaze would not be processed until the stream ends. For this purpose, simple 1:1 aggregation can simply be applied.

\subsection{BRAM controller}
Since Ethernet frames vary in size, they cannot be read directly from FIFO memory and additional RAM must be added, allowing random access to the frame (single frame storage is enough). With maximum ethernet frame size of 1530\,B, the 2\,KB RAM for each direction is sufficient. On \mbox{Virtex-5} devices, there are many small block RAM (BRAM) cells spread over the FPGA structure, those BRAM cells can be joined to greater ones (up to 1024\,kB) and have two identical interfaces allowing read and write operations on both sides (even simultaneously).

The BRAM controller creates the interface between Flow controller FIFOs and BRAM block. This block is a part of Microblaze design, with its interface\,A connected to VHDL design  and interface\,B connected via internal MicroBlaze memory controller and peripheral bus (PLB) to MicroBlaze core. First data is loaded to BRAM as soon as it is present in the ETH-UB FIFO. Data ready signal is sent to MicroBlaze, invoking RX interrupt. Next data is loaded to BRAM right after ACK from MicroBlaze. Opposite direction works similarly - uplink data are loaded to BRAM and data ready signal is sent to BRAM controller. Another frame can be loaded as soon as ACK from BRAM controller arrives.

Address space of Microblaze memory controller and thus address space of BRAM block is a part of monolithic address space of Microblaze processor, allowing direct software access to the frame.

\section{Conclusion}
Discussed design allows simple Ethernet frame switching between the Ethernet interface and the MicroBlaze microprocessor at layer of TEMAC client interface. The new control data line was added to the design without the need for additional MAC controller and without affecting the main data line throughput. At MicroBlaze, frames are read or written by accessing memory (e.g. by pointer dereference in C), which could be further interfaced to LwIP (lightweight IP stack implementation) library providing common socket API and few socket-based applications (HTTP server). With MicroBlaze directly connected to network, internal data (coding scheme, signal loss, BER, ...) can be sent directly via socket to user client application running on remote PC or viewed as a static web page using web browser.

This design is a part of opensource project available at:  \\*\url{https://gitlab.com/OK2NMZ/thesis-fso} 

\section*{Acknowledgement}
The described research was performed in laboratories supported by the SIX project - the registration number CZ.1.05/2.1.00/03.0072. 

\begin{thebibliography}{9}

  \bibitem{bouchet}BOUCHET, Olivier. \textit{Free-space optics: propagation and communication}. New-
port Beach, CA: ISTE, 2006, 219 p. ISBN 9781905209026.

 
    \bibitem{ug_temac}XILINX. \textit{Virtex-5 FPGA Embedded Tri-Mode Ethernet MAC User Guide} [online]. [cit. 2016-03-03]. Available at: \url{http://www.xilinx.com/support/documentation/user_guides/ug194.pdf}

\end{thebibliography}

\end{document}
