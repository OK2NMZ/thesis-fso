Czech version
=============
## Název: ##
  Podpůrný systém pro správu a řízení FSO transceiveru
## Požadavky: ##
  Seznamte se s metodou a modelováním optických bezkabelových spojů. Optická bezkabelová komunikace nabízí řadu výhod oproti radiové komunikaci, avšak kvalita přenosu do značné míry závisí na stavu počasí. Prostudujte základní standardní i nestandardní atmosférické jevy, které zhoršují kvalitu přenosu a vypracujte přehled metod, které se používají na snížení vlivu atmosféry na spoj.
  Navrhněte a realizujte funkční vzorek (prototyp) podpůrného systému pro správu modulu FSO přijímače-vysílače. Podpůrný systém je určen k odmlžování a odmrazování optiky a napájení elektrické části transceiveru. (((Vliv myší, pavouků, much, blesků, … - všechno nás už postihlo a zlikvidovalo na měsíc komunikaci ))) Součástí realizace je implementace jednoúčelového dvouportového Ethernetového přepínače, který umožní posílat data pomocí optického bezkabelového mostu a zároveň přistupovat k webovému rozhraní celého zařízení za účelem vizualizace statistik a nastavení parametrů hlavního modulu. 

## Literatura: ##
  [1]

English version
===============
## Title: ##
  Support system for administration and control of FSO transceiver
  
## Goals: ##
Make a study about methods and modeling techniques of optical wireless connections. Optical wireless communication offers plenty of advantages in comparison with radio communication however, the quality of transaction greatly depends on weather conditions. Focus on standard and non-standard atmospherical phenomenons, which decreases transaction quality and prepare a set of methods that are used to decrease influence of atmosphere on the connection.
  Design and realize working prototype of support system for administration of FSO receiver-transmitter module. Support system is intended to defrost and demist the optics and to supply the power to electronic part of the device. Another part of the realization is also implementation of single-purpose, two port Ethernet switch, which allows sending data through optical bridge as well as accessing the web server, which provides visualized statistics and allows modifying main module parameters.

## References: ##
[1] YOUNG, Matt. Optics and lasers: an engineering physics approach. Berlin: Springer Verlag, 1977, xii, 207 s. Springer series in optical sciences, vol. 5. ISBN 3540081267.

??? [2] Optical devices & fibers. Tokyo: OHMSHA, 1984, 338 s. ISBN 4-274-03020-2.
