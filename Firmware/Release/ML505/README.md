- Use Impact to load design_with_srec_loader_for_platform_flash.mcs to the first platform flash in   the JTAG chain.
- Use XSDK->Xilinx Tools-> Program Flash to program the fso_microblaze.elf to the on board Strata flash
- Now, on reset, the srec loader contained in the bitstream loads the fso_microblaze firmware to the on board SRAM and starts executing it.
- Microblaze should respond at 192.168.2.15 and LED0 should be blinking if Microblaze application lives.

- You can load cpld.jed to the on board CPLD, if you have overwritten the original configuration for CPLD by a custom one. (not necessary, if original CPLD configuration not altered)
