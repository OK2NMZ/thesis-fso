/*!
* This file is used to set configuration of bus via IPv4 interface.
*/

var reloading;
var lastpage;


/*!
* @brief This function is called when the page loads
* and sets variables.
*/
function onpageload()
{
    window.onbeforeunload = null;
    onNavigationClick('stats.ssi');
}

/*!
* @brief This function is called when user press button 'Status'.
* This enable user to see current status of communication.
*/
function onNavigationClick(url)
{
    if (window.XMLHttpRequest)
    { /* Non-IE browsers */
          req = new XMLHttpRequest();
          try
          {
            req.open("GET", url, false);
          }
          catch (e)
          {
            alert(e);
          }
          req.send(null);
    }
    else if (window.ActiveXObject)
    { /* IE */
          req = new ActiveXObject("Microsoft.XMLHTTP");
          if (req)
          {
            req.open("GET", url, false);
            req.send();
          }
    }

    if (req.readyState == 4)
    { /* Complete */
          if (req.status == 200)
          { /* OK response */
              document.getElementById("main").innerHTML = req.responseText;
          }
          else
          {
            alert("Problem: " + req.statusText);
          }
    }
    else
    {
        alert("Problem: " + req.statusText);
    }
}

function sendViaGetRequest(url)
{
	if (window.XMLHttpRequest)
	{ /* Non-IE browsers */
		req = new XMLHttpRequest();
		try
		{
			req.open("GET", url, false);
		}
		catch (e)
		{
			alert(e);
		}
		req.send(null);
	}
	else if (window.ActiveXObject)
	{ /* IE */
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req)
		{
			req.open("GET", url, false);
			req.send();
		}
	}

	if (req.readyState == 4)
	{ /* Complete */
		if (req.status == 200)
		{ /* OK response */
			return req.responseText;
		}
		else
		{
			return "";
		}
	}
	else
	{
		return "";
	}
}

function onSendSettings()
{
	var form = document.forms.cfg;
	var cfgUrl = "cfg.cgi?";
	var ret;

	cfgUrl += "CHC="+form.CHC.value;
	cfgUrl += "&BS="+form.BS.value;
	cfgUrl += "&BSV="+form.BSV.value;
	cfgUrl += "&EFB="+form.EFB.value;
	if(sendViaGetRequest(cfgUrl).trim() == "SUCCESS")
	{
		alert("Successfully uploaded!");
	}
	else
	{
		alert("Error while sending configuration!");
	}
}

function onRefreshSettings()
{
	var form = document.forms.cfg;
	var ret = sendViaGetRequest("cfg.ssi");

	if(ret != "")
	{
		var trimmed = ret.replace(/<!--.*-->/, "")
		var values = JSON.parse(trimmed);
		form.CHC.value = values.CHC;
		form.BS.value = values.BS;
		form.BSV.value = values.BSV;
		form.EFB.value = values.EFB;
	}
	else
	{
		alert("Error in communication!");
	}

}

function onSendClima()
{
	var form = document.forms.clima;
	var cfgUrl = "clima.cgi?";
	var ret;

	cfgUrl += "UTL="+form.utl.value;
	cfgUrl += "&UTCL="+form.utcl.value;
	cfgUrl += "&LTL="+form.ltl.value;
	cfgUrl += "&LTCL="+form.ltcl.value;
  cfgUrl += "&MH="+form.mh.value;
  cfgUrl += "&LP="+form.lp.value;
	if(sendViaGetRequest(cfgUrl).trim() == "SUCCESS")
	{
		alert("Successfully uploaded!");
	}
	else
	{
		alert("Error while sending configuration!");
	}
}

function onRefreshClima()
{
	var form = document.forms.clima;
	var ret = sendViaGetRequest("cfg_clima.ssi");

	if(ret != "")
	{
		var trimmed = ret.replace(/<!--.*-->/, "");
		var values = JSON.parse(trimmed);
		form.utl.value  = values.UTL;
    document.getElementById('lbl_utl').innerHTML = values.UTL;
		form.utcl.value = values.UCTL;
    document.getElementById('lbl_utcl').innerHTML = values.UCTL;
		form.ltl.value  = values.LTL;
    document.getElementById('lbl_ltl').innerHTML = values.LTL;
		form.ltcl.value = values.LCTL;
    document.getElementById('lbl_ltcl').innerHTML = values.LCTL;
    form.mh.value   = values.MH;
    document.getElementById('lbl_mh').innerHTML = values.MH;
    form.lp.value   = values.LP;
    document.getElementById('lbl_lp').innerHTML = values.LP;
	}
	else
	{
		alert("Error in communication!");
	}

}

function CalcFER()
{
	var num_err = document.getElementById('NUM_ERR').innerHTML.replace(/<!--.*-->/, "");
	var num_frames = document.getElementById('NUM_FRAMES').innerHTML.replace(/<!--.*-->/, "");

	if(Number(num_err) == 0)
		document.getElementById('FER').innerHTML = "< " + (1/parseFloat(num_frames)).toExponential(1);
	else	
		document.getElementById('FER').innerHTML = (parseFloat(num_err)/parseFloat(num_frames)).toExponential(1);
}
