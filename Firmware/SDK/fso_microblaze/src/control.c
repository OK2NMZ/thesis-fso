/*
 * control.c
 *
 *  Created on: 21 Mar 2016
 *      Author: lookin
 */


#include "control.h"

#include "stdint.h"
#include "xgpio.h"
#include "xparameters.h"
#include "string.h"
#include "assert.h"
#include "xstatus.h"
#include "lwip/udp.h"
#include "lwip/pbuf.h"


void write_display(const char* text)
{
	if(strlen(text) > 32) {
		write_display("Too long text!");
		return;
	}


	char buffer[32];
	memset(buffer, ' ', 32);
	memcpy(buffer, text, strlen(text));
	memcpy((uint8_t *)DISPLAY_ADDR, buffer, 32);
}

u32  sys_now()
{
	return system_time;
}

int gpio_init(struct sys_gpio *gpio)
{
	int status;
	/* OUTPUTS */
	status |= XGpio_Initialize(&gpio->leds, XPAR_LEDS_8BIT_DEVICE_ID);
	/* INPUTS */
	status |= XGpio_Initialize(&gpio->dip_switches, XPAR_DIP_SWITCHES_8BIT_DEVICE_ID);
	status |= XGpio_Initialize(&gpio->buttons,      XPAR_PUSH_BUTTONS_5BIT_DEVICE_ID);
	/* SET DIRECTION */
	XGpio_SetDataDirection(&gpio->leds,         1, 0x0);
	XGpio_SetDataDirection(&gpio->dip_switches, 1, 0xFFFFFFFF);
	XGpio_SetDataDirection(&gpio->buttons,      1, 0xFFFFFFFF);

	if(status != 0)
		return XST_FAILURE;

	return XST_SUCCESS;
}

int send_udp_stat(struct udp_pcb *pcb, u64_t *event)
{
	struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT,sizeof(u64_t),PBUF_RAM); assert(p != NULL);
	memcpy(p->payload, event,sizeof(u64_t));
	p->len = p->tot_len = sizeof(u64_t);

	udp_send(pcb,p)	;
	pbuf_free(p);
}

void sram_test()
{
	/* SRAM TEST */
	u32_t *test = (u32_t *)XPAR_SRAM_MEM0_BASEADDR;
	u32_t i;
	for(i=0;i<0x3FFFF;i++){
		test[i] = i;
	}

	test= (u32_t *)XPAR_SRAM_MEM0_BASEADDR;
	for(i=0;i<0x3FFFF;i++){
		if(test[i] != i){
					xil_printf("Orig: %l08X R: %l08X\n", i, test[i]);
		}
	}
}
