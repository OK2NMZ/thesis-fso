/*
 * irpts.h
 *
 *  Created on: 9 Apr 2016
 *      Author: lookin
 */

#ifndef IRPTS_H_
#define IRPTS_H_

#include "irpts.h"
#include "stdint.h"

#include "xgpio.h"
#include "xintc.h"
#include "xil_exception.h"
#include "hw_net.h"

#define INTC_DEVICE_ID     XPAR_INTC_1_DEVICE_ID
#define TIMER_INT_ID       XPAR_INTC_1_TIMER_INTERRUPT_INTR
#define BRAM_INT_ID        XPAR_INTC_1_BRAM_IF_R_IP2INTC_IRPT_INTR
#define SP_RX_INT_ID       XPAR_INTC_1_SERIAL_PORT_INTERRUPT_INTR
#define STAT_EVENT_INT_ID  XPAR_INTC_1_STAT_EVENTS_INTERRUPT_INTR

#define STAT_EVENT_REG0    XPAR_STAT_EVENTS_BASEADDR
#define STAT_EVENT_REG1    (XPAR_STAT_EVENTS_BASEADDR+4)
#define STAT_EVENT_REG2    (XPAR_STAT_EVENTS_BASEADDR+8)
#define STAT_EVENT_FLAG    0x00000001

extern volatile u32 system_time;
extern XGpio leds_g;
extern volatile XIntc interrupt_controller;
extern volatile struct netif my_netif;
extern struct sys_gpio gpio;

void irpts_init();


#endif /* IRPTS_H_ */
