/*
 * Copyright (c) 2016, Lukas Janik <lucaso.janik@gmail.com>
 * Copyright (c) 2016, Marek Novak <ok2nmz@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xbram_hw.h"
#include "xgpio.h"
#include "xbasic_types.h"
#include "xstatus.h"
#include "xintc.h"

#include "mb_interface.h"

#include "ipv4/lwip/ip_addr.h"
#include "lwip/init.h"
#include "lwip/netif.h"
#include "lwip/udp.h"
#include "lwip/ip.h"
#include "netif/ethernetif.h"
#include "netif/etharp.h"
#include "string.h"

#include "httpd.h"
#include "configs.h"
#include "irpts.h"
#include "control.h"
#include "hw_net.h"
#include "assert.h"
#include "lwip/timers.h"
#include "xuartlite.h"
#include "clima.h"

volatile   u32      system_time = 0;
struct     sys_gpio gpio;
volatile   XIntc    interrupt_controller;
volatile   struct netif    my_netif;

struct unitConfig
{
	int EventFrameBroadcastEnabled;
	u32 BERvalue;
	int BERstate;
	int ChannelCodingSel;
};

volatile struct unitConfig cfg = {
		.EventFrameBroadcastEnabled = 0,
		.BERvalue = 0,
		.BERstate = 0,
		.ChannelCodingSel = 0,
};

volatile struct climaConfig cfgClima = {
  .upperTempLimit     = 50,
  .upperCritTempLimit = 60,
  .lowerTempLimit     = 2,
  .lowerCritTempLimit = -10,
  .maxHumidity        = 70,
  .loggingPeriod      = 10
};

const char *tags[] = { "RxFrRec", "RxFRBad", "RxByCor", "RxByRec",
		               "TxFrSen", "TxFrTru", "TxBySen", "TxByDro",
                       "UBTxByt", "UBRxByt", "config", "clConf"};

const char *configCGI(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]);
const char *configClimaCGI(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]);
const tCGI cgi_handlers[] = {{"/cfg.cgi", configCGI}, };
struct CGIentry
{
	const char* match;
	int(*callback)(char* value);
};

int EventFrameBroadcast_cb(char* value);
int BERsimulatorValue_cb(char* value);
int BERsimulatorControl_cb(char* value);
int ChannelCodingControl_cb(char* value);

struct CGIentry CGIentries[] = {
		{"EFB", EventFrameBroadcast_cb},
		{"BSV", BERsimulatorValue_cb},
		{"BS", BERsimulatorControl_cb},
		{"CHC", ChannelCodingControl_cb},
		{NULL, NULL},
};

u16_t ssi_handler(int iIndex, char *pcInsert, int iInsertLen)
{
  // TODO: sprintf() directly into pcInsert - what is its length?
	char buffer[128];
	struct ethernetif *p_ethernetif = my_netif.state;
	u64_t *stat_register = (u64_t *)XPAR_STATISTICS_INPUT_0_BASEADDR;

	if(iIndex < 8){
		sprintf(buffer, "%lld", stat_register[iIndex]); // MAC Stat regs.
	}else{
		switch(iIndex){
			case 8:  sprintf(buffer, "%ld", p_ethernetif->rx_count); break; // UB rx count
			case 9:  sprintf(buffer, "%ld", p_ethernetif->tx_count); break; // UB tx count
			case 10: sprintf(buffer, "{\"EFB\":\"%u\", \"BSV\":\"%u\", \"BS\":\"%u\", \"CHC\":\"%u\"}",
					cfg.EventFrameBroadcastEnabled,
					cfg.BERvalue,
					cfg.BERstate,
					cfg.ChannelCodingSel);
					break;
      case 11: sprintf(buffer, "{\"UTL\":\"%d\", \"UCTL\":\"%d\", \"LTL\":\"%d\", \
                                 \"LCTL\":\"%d\", \"MH\":\"%d\", \"LP\":\"%d\"}",
          cfgClima.upperTempLimit,
          cfgClima.upperCritTempLimit,
          cfgClima.lowerTempLimit,
          cfgClima.lowerCritTempLimit,
          cfgClima.maxHumidity,
          cfgClima.loggingPeriod);
          break;

			default: return 0;
		}
	}

	strcpy(pcInsert,buffer);
	return (strlen(buffer));
}

const char *configCGI(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
	int i, j, k = 0;
	for(i = 0; i < iNumParams; i++)
	{
		/*FOR DEBUG xil_printf("CGI: %s IS %s\n", pcParam[i], pcValue[i]);*/
		for(j = 0; CGIentries[j].match != NULL; j++)
		{
			if(!strcmp(CGIentries[j].match, pcParam[i]))
			{
				k += CGIentries[j].callback(pcValue[i]);
				break;
			}
		}
	}

	if(k == 0)
		return "/ok.html";
	else
		return "/err.html";
}

void setFlagRegister(u32 value);
void setBERRegister(u32 value);
u32 getFlagRegister(void);

int EventFrameBroadcast_cb(char* value)
{
	u32 val;
	sscanf(value, "%u", &val);
	if(val == 0)
	{
		cfg.EventFrameBroadcastEnabled = 0;
	}
	else if(val == 1)
	{
		cfg.EventFrameBroadcastEnabled = 1;
	}
	else
	{
		return -1;
	}
	return 0;
}

int BERsimulatorValue_cb(char* value)
{
	u32 val;

	sscanf(value, "%u", &val);
	cfg.BERvalue = val;
	setBERRegister(val);
	return 0;
}

int BERsimulatorControl_cb(char* value)
{
	u32 val;
	sscanf(value, "%u", &val);
	if(val == 0)
	{
		setFlagRegister(getFlagRegister()&(~(1<<20)));
	}
	else if(val == 1)
	{
		setFlagRegister(getFlagRegister()|(1<<20));
	}
	else
	{
		return -1;
	}

	cfg.BERstate = val;
	return 0;
}

int ChannelCodingControl_cb(char* value)
{
	u32 val;
	sscanf(value, "%u", &val);
	if(val == 0)
	{
		setFlagRegister(getFlagRegister()|(1<<17));
		setFlagRegister(getFlagRegister()&(~(1<<21)));
		setFlagRegister(getFlagRegister()&(~(1<<17)));
	}
	else if(val == 1)
	{
		setFlagRegister(getFlagRegister()|(1<<17));
		setFlagRegister(getFlagRegister()|(1<<21));
		setFlagRegister(getFlagRegister()&(~(1<<17)));
	}
	else
	{
		return -1;
	}

	cfg.ChannelCodingSel = val;
	return 0;
}

void resetFlowController(void);

int main()
{
	err_t err;
	struct ip_addr ipaddr, pc_ipaddr, netmask, gw;
	int status = 0;

	XUartLite serial_port;
	XUartLite_Initialize(&serial_port, XPAR_UARTLITE_1_DEVICE_ID);
	uint8_t databuff[4] = {0xAA, 0x11, 0xF0, 0x0F};


	xil_printf("GPIO init...  \n");
	status = gpio_init(&gpio); assert(status == XST_SUCCESS);

	XGpio_DiscreteSet(&gpio.leds, 1, (u32)0xAA);
	setFlagRegister(0); //unresets the HDL
	setBERRegister(0);
	cfg.BERstate = 0;
	cfg.BERvalue = 0;
	cfg.ChannelCodingSel = 0;
	cfg.EventFrameBroadcastEnabled = 0;

	/* LWIP BEG */
	xil_printf("LwIP init... \n");
	IP4_ADDR(&ipaddr,    192, 168,   2, 15);
	IP4_ADDR(&pc_ipaddr, 192, 168,   2, 13);
	IP4_ADDR(&netmask,   255, 255, 255,  0);
	IP4_ADDR(&gw,        192, 168,   2, 13);

	lwip_init();
	netif_add(&my_netif, &ipaddr, &netmask, &gw, NULL, ethernetif_init, ethernet_input);
	irpts_init();
	Xil_ExceptionEnable();
	netif_set_default(&my_netif);
	netif_set_up(&my_netif);

	/* HTTPD */
	xil_printf("HTTP server test ... \n");
	http_set_ssi_handler(ssi_handler, tags, sizeof(tags)/sizeof(char *));
	http_set_cgi_handlers(cgi_handlers, sizeof(cgi_handlers)/sizeof(tCGI));
	httpd_init();

	/* UDP */
	xil_printf("UDP server test ... \n");
	struct udp_pcb *pcb;
	unsigned pc_port = 10000;

	pcb = udp_new();                        assert(pcb != NULL);
	err = udp_bind(pcb, IP_ADDR_ANY, 0);
	err = udp_connect(pcb, IP_ADDR_BROADCAST, pc_port);
	u32_t *reg = (u32_t *)STAT_EVENT_REG2;
	u64_t *reg_data = STAT_EVENT_REG0;


	struct ethernetif *p_ethernetif = my_netif.state;
	XUartLite_Send(&serial_port, databuff, 4);
	xil_printf("While()...\n");


	while(1) {
		sys_check_timeouts();


		if(cfg.EventFrameBroadcastEnabled)
		{
			if(*reg != (u32)0){
				send_udp_stat(pcb,reg_data);
				// Clear the stat_event Ip core int flag
				*reg &= ~((u32)1);
			}
		}

		if(p_ethernetif->flags & (1<<0)){
			struct pbuf *p, *q;
			/* in_len is address of last BRAM word => must be multiplied by 4 for byte count,
			 * also this count is rounded up
			 * */
			uint16_t len_in = hw_net_get_in_len() * 4;
			p = pbuf_alloc(PBUF_RAW, len_in, PBUF_POOL); assert(p != NULL);

			int ptr = 0;
			for(q = p; q != NULL; q = q->next) {
				memcpy(q->payload, (uint8_t *)DATA_IN_ADDR + ptr, q->len);
				ptr += q->len;
			}

			ethernet_input(p, &my_netif);
			p_ethernetif->flags &= ~(1<<0);
			hw_net_read_ack(p_ethernetif);
		}

	}

    return 0;
}

void setFlagRegister(u32 value)
{
	u32 volatile* reg = (u32 volatile*)XPAR_CONFIG_INTERFACE_0_BASEADDR;
	*reg = value;
}

u32 getFlagRegister(void)
{
	u32 volatile* reg = (u32 volatile*)XPAR_CONFIG_INTERFACE_0_BASEADDR;
	return *reg;
}

void setBERRegister(u32 value)
{
	u32 volatile* reg = (u32 volatile*)(XPAR_CONFIG_INTERFACE_0_BASEADDR+4);
	*reg = value;
}

void resetFlowController(void)
{
	u32 old = getFlagRegister();
	setFlagRegister(old|(1<<16));
	setFlagRegister(old&(~(1<<16)));
}
