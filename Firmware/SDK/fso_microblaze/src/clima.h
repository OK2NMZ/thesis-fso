#ifndef CLIMA_H
#define CLIMA_H

#include "xbasic_types.h"

struct climaConfig
{
  int upperTempLimit;
  int upperCritTempLimit;
  int lowerTempLimit;
  int lowerCritTempLimit;
  int maxHumidity;
  int loggingPeriod;
};

struct climaStats  {
  int temperature;
  u8  humidity;
  u32 clima_runtime;
  u32 fan_runtime;
  u32 heater_runtime;
};

int clima_get_config(struct climaConfig *cc);
int clima_set_config(struct climaConfig cc);
int clima_get_stats(struct climaStats *cs);
int clima_set_stats(struct climaStats cs);

#endif /* end of include guard: CLIMA_H */
