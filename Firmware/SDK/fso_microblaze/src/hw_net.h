/*
 * hw_net.h
 *
 *  Created on: 22 Apr 2016
 *      Author: lookin
 */

#ifndef HW_NET_H_
#define HW_NET_H_

#include "netif/ethernetif.h"
#include "stdint.h"

#define MAC_REG_ADDR   XPAR_FLOWCONTROLLER_MAC_CONTROL_0_BASEADDR
#define LEN_OUT_ADDR   XPAR_DATA_LEN_OUT_0_BASEADDR
#define LEN_IN_ADDR    XPAR_DATA_LEN_IN_0_BASEADDR
#define DATA_IN_ADDR   XPAR_BRAM_0_BASEADDR
#define DATA_OUT_ADDR  XPAR_BRAM_1_BASEADDR

#define DATA_TO_UB_READY_PIN         0
#define DATA_TO_UB_READY_ACK_PIN     0
#define DATA_FROM_UB_READED_PIN      1
#define DATA_FROM_UB_READED_ACK_PIN  1

uint8_t  hw_net_is_pending_read(struct ethernetif *hw_net);
void     hw_net_read_ack(struct ethernetif *hw_net);
uint8_t  hw_net_are_data_readed(struct ethernetif *hw_net);
void     hw_net_data_readed_ack(struct ethernetif *hw_net);
void     hw_net_set_mac(uint8_t *mac);
void     hw_net_set_out_len(uint16_t len);
void     hw_net_append(struct ethernetif *hw_net, uint8_t *data, uint16_t len);
void     hw_net_send(struct ethernetif *hw_net, uint16_t length);
void     hw_net_write(struct ethernetif *hw_net, uint8_t *data, uint16_t len);
uint16_t hw_net_get_in_len();

#endif /* HW_NET_H_ */
