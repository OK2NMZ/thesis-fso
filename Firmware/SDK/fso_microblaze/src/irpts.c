/*
 * irpts.c
 *
 *  Created on: 9 Apr 2016
 *      Author: lookin
 */

#include "irpts.h"

#include "xintc.h"
#include "xil_exception.h"
#include "xgpio.h"
#include "assert.h"
#include "string.h"
#include "control.h"

#include "lwip/err.h"
#include "lwip/netif.h"
#include "netif/ethernetif.h"
#include "lwip/timers.h"
#include "lwip/tcp.h"
#include "netif/etharp.h"

static void timer_ISR(void *data)
{
	static u32 time2 = 0;
	static u8_t led_status;

	if(system_time - time2 > 50){
		if(led_status == 0){
			XGpio_DiscreteSet(&gpio.leds, 1, (u32)1);
			led_status = 1;
		}else{
			XGpio_DiscreteClear(&gpio.leds, 1, (u32)1);
			led_status = 0;
		}


		time2 = system_time;
	}

	// tcp_tmr();

	system_time++;
	XIntc_Acknowledge(&interrupt_controller, TIMER_INT_ID);
}

static void rx_interrupt_ISR(void *data)
{
	struct netif *my_netif = (struct netif *)data;
	struct ethernetif *p_ethernetif = my_netif->state;

	/* If there are data to read*/
	if(XGpio_DiscreteRead(p_ethernetif->control_if_in,1) & (1 << DATA_TO_UB_READY_PIN)){
		p_ethernetif->rx_count++;
		p_ethernetif->flags |= (1<<0);
	}

	/* If data from UB were successfully read*/
	if(XGpio_DiscreteRead(p_ethernetif->control_if_in,1) & (1 << DATA_FROM_UB_READED_PIN)){
		p_ethernetif->tx_count++;
		hw_net_set_out_len(0); /* Clear the transmit  frame lenth */
		p_ethernetif->flags |= (1<<1);
	}

	XGpio_InterruptClear(p_ethernetif->control_if_in, XGpio_InterruptGetStatus(p_ethernetif->control_if_in));
	XIntc_Acknowledge(&interrupt_controller, BRAM_INT_ID);
}

static void sp_rx_interrupt_ISR(void)
{
	xil_printf("Serial port Rx Interrupt!");
	XIntc_Acknowledge(&interrupt_controller, SP_RX_INT_ID);
}

static void stat_event_interrupt_ISR(void)
{
	u32_t *reg = (u32_t *)STAT_EVENT_REG2;
	// ACK interrupt to INTC
	XIntc_Acknowledge(&interrupt_controller, STAT_EVENT_INT_ID);
	// Clear the stat_event Ip core int flag

	//*reg &= ~(STAT_EVENT_FLAG);

}

void irpts_init()
{
	err_t status;
	xil_printf("Interrupt initialization... \n");
	status = XIntc_Initialize(&interrupt_controller, INTC_DEVICE_ID);   assert(status == XST_SUCCESS);

	status = XIntc_Connect(&interrupt_controller, TIMER_INT_ID,     (XInterruptHandler)timer_ISR, (void*)0);                assert(status == 0);
	status = XIntc_Connect(&interrupt_controller, BRAM_INT_ID,      (XInterruptHandler)rx_interrupt_ISR, (void*)&my_netif); assert(status == 0);
	status = XIntc_Connect(&interrupt_controller, SP_RX_INT_ID,     (XInterruptHandler)sp_rx_interrupt_ISR, (void*)0);      assert(status == 0);
	status = XIntc_Connect(&interrupt_controller, STAT_EVENT_INT_ID,(XInterruptHandler)stat_event_interrupt_ISR, (void*)0); assert(status == 0);

	status = XIntc_Start(&interrupt_controller, XIN_REAL_MODE);     assert(status == 0);

	XIntc_Enable(&interrupt_controller, TIMER_INT_ID);
	XIntc_Enable(&interrupt_controller, BRAM_INT_ID);
	// XIntc_Enable(&interrupt_controller, STAT_EVENT_INT_ID);

	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler)XIntc_InterruptHandler, &interrupt_controller);

	u32_t *reg = (u32_t *)STAT_EVENT_REG2;
	*reg &= ~(STAT_EVENT_FLAG);
}
