/*
 * control.h
 *
 *  Created on: 21 Mar 2016
 *      Author: lookin
 */

#ifndef CONTROL_H_
#define CONTROL_H_

#include "xgpio.h"
#include "stdint.h"
#include "xparameters.h"
#include "lwip/udp.h"


#define DISPLAY_ADDR   XPAR_DISPLAY_PORT_0_BASEADDR

extern volatile u32 system_time;

typedef struct sys_gpio{
	XGpio leds;
	XGpio dip_switches;
	XGpio buttons;
};

/* MISCELLANOUS */
u32   sys_now();
void  write_display(const char* text);
int   gpio_init(struct sys_gpio *gpio);
int   send_udp_stat(struct udp_pcb *pcb, u64_t *event);
void  sram_test();


#endif /* CONTROL_H_ */
