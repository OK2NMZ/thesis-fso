/*
 * hw_net.c
 *
 *  Created on: 22 Apr 2016
 *      Author: lookin
 */

#include "stdint.h"
#include "xgpio.h"
#include "xparameters.h"
#include "string.h"
#include "assert.h"

#include "hw_net.h"

uint8_t hw_net_is_pending_read(struct ethernetif *hw_net)
{
	if(XGpio_DiscreteRead(hw_net->control_if_in,1) & (1 << DATA_TO_UB_READY_PIN))
		return 1;
	else
		return 0;
}

void hw_net_read_ack(struct ethernetif *hw_net)
{
	XGpio_DiscreteSet(hw_net->control_if_out,   1, (u32)(1 << DATA_TO_UB_READY_ACK_PIN));
	XGpio_DiscreteClear(hw_net->control_if_out, 1, (u32)(1 << DATA_TO_UB_READY_ACK_PIN));
	}

uint8_t hw_net_are_data_readed(struct ethernetif *hw_net)
{
	if(XGpio_DiscreteRead(hw_net->control_if_in,1) & (1 << DATA_FROM_UB_READED_PIN))
		return 1;
	else
		return 0;
}

void hw_net_data_readed_ack(struct ethernetif *hw_net)
{
	XGpio_DiscreteSet(hw_net->control_if_out, 1,   (u32)(1 << DATA_FROM_UB_READED_ACK_PIN));
	XGpio_DiscreteClear(hw_net->control_if_out, 1, (u32)(1 << DATA_FROM_UB_READED_ACK_PIN));
}

void hw_net_set_mac(uint8_t *mac)
{
  uint8_t *hw_mac = (uint8_t *)MAC_REG_ADDR;
  memcpy(hw_mac, mac, 6);
}

void hw_net_set_out_len(uint16_t len)
{
	uint32_t *hw_len_out = (uint32_t *)LEN_OUT_ADDR;
	*hw_len_out = len;
}

void hw_net_append(struct ethernetif *hw_net, uint8_t *data, uint16_t len)
{
	memcpy((uint8_t *)DATA_OUT_ADDR + hw_net->data_top, data, len);
	hw_net->data_top += len;

}

void hw_net_send(struct ethernetif *hw_net, uint16_t length)
{
	hw_net_set_out_len(length+1);
	while(!(hw_net->flags & (1<<1)));
	hw_net->data_top = 0;
	hw_net_data_readed_ack(hw_net);
	hw_net->flags &= ~(1<<1);
}

void hw_net_write(struct ethernetif *hw_net, uint8_t *data, uint16_t len)
{
	memcpy((uint8_t *)DATA_OUT_ADDR, data, len);
	hw_net_set_out_len(len);
	while(!hw_net_are_data_readed(hw_net));
	hw_net_set_out_len(0);
	hw_net_data_readed_ack(hw_net);
}

uint16_t hw_net_get_in_len()
{
	uint32_t *hw_len_in = (uint32_t *)LEN_IN_ADDR;
	return (*hw_len_in)+1;
}

