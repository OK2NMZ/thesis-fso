/*
 * ethernetif.h
 *
 *  Created on: 7 Apr 2016
 *      Author: lookin
 */

#ifndef ETHERNETIF_H_
#define ETHERNETIF_H_

#include "../../src/control.h"
#include "lwip/err.h"
#include "lwip/netif.h"

/**
 * Helper struct to hold private data used to operate your ethernet interface.
 * Keeping the ethernet address of the MAC in this struct is not necessary
 * as it is already kept in the struct netif.
 * But this is only an example, anyway...
 */
struct ethernetif {
  struct eth_addr *ethaddr;
  XGpio *control_if_out;
  XGpio *control_if_in;
  uint32_t rx_count;
  uint32_t tx_count;

  uint16_t data_top;
  volatile uint8_t flags;
};

err_t ethernetif_init(struct netif *netif);

#endif /* ETHERNETIF_H_ */
