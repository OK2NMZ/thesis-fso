//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//     
//     (c) Copyright 2005 Xilinx, Inc.
//     All rights reserved.
//

module system(
   usb_reset_b,
   sysace_cfgmodepin,
   sram_flash_d0_en,
   plat_flash_ce_b,
   lcd_fpga_rw,
   lcd_fpga_rs,
   lcd_fpga_e,
   lcd_fpga_db7,
   lcd_fpga_db6,
   lcd_fpga_db5,
   lcd_fpga_db4,
   lcd_cpld_rw,
   lcd_cpld_rs,
   lcd_cpld_e,
   lcd_cpld_db7,
   lcd_cpld_db6,
   lcd_cpld_db5,
   lcd_cpld_db4,
   gpio_led_w,
   gpio_led_s,
   gpio_led_n,
   gpio_led_e,
   gpio_led_c,
   gpio_led_3,
   gpio_led_2,
   gpio_led_1,
   gpio_led_0,
   fpga_m2,
   fpga_m1,
   fpga_m0,
   fpga_done,
   audio_spdif,
   cpld_spdif_3,
   cpld_spdif_2,
   cpld_spdif_1,
   cpld_led_w,
   cpld_led_s,
   cpld_led_n,
   cpld_led_e,
   cpld_led_c,
   cpld_led_3,
   cpld_led_2,
   cpld_led_1,
   cpld_led_0,
   cpld_fpga_init,
   cpld_fpga_done,
   clk_33mhz_fpga,
   cfg_addr_out2,
   cfg_addr_out1,
   cfg_addr_out0,
   cfg_addr_in2,
   cfg_addr_in1,
   cfg_addr_in0,
   fpga_fallback_en,
   sysace_cfg_en,
   plat_flash_cf_b,
   fpga_init,
   fpga_prog_b,
   sram_flash_we_b,
   sysace_rst_n
//   fpga_cpu_reset_b,
//   bus_error_2,
//   bus_error_1,
//   fpga_din,
//   fpga_dout_busy,
//   fpga_cs_b,
//   fpga_cs0_b,
//   cpld_tp2,
//   cpld_tp10,
//   cpld_tp1,
//   cpld_tp0,
//   cpld_io_1,
//   fpga_rdwr_b,
   );

   output usb_reset_b;
   output sysace_cfgmodepin;
   output sram_flash_d0_en;
   output plat_flash_ce_b;
   input  lcd_fpga_rw;
   input  lcd_fpga_rs;
   input  lcd_fpga_e;
   inout  lcd_fpga_db7;
   inout  lcd_fpga_db6;
   inout  lcd_fpga_db5;
   inout  lcd_fpga_db4;
   output lcd_cpld_rw;
   output lcd_cpld_rs;
   output lcd_cpld_e;
   inout  lcd_cpld_db7;
   inout  lcd_cpld_db6;
   inout  lcd_cpld_db5;
   inout  lcd_cpld_db4;
   input  gpio_led_w;
   input  gpio_led_s;
   input  gpio_led_n;
   input  gpio_led_e;
   input  gpio_led_c;
   input  gpio_led_3;
   input  gpio_led_2;
   input  gpio_led_1;
   input  gpio_led_0;
   input  fpga_m2;
   input  fpga_m1;
   input  fpga_m0;
   input  fpga_done;
   input  audio_spdif;
   output cpld_spdif_3;
   output cpld_spdif_2;
   output cpld_spdif_1;
   output cpld_led_w;
   output cpld_led_s;
   output cpld_led_n;
   output cpld_led_e;
   output cpld_led_c;
   output cpld_led_3;
   output cpld_led_2;
   output cpld_led_1;
   output cpld_led_0;
   output cpld_fpga_init;
   output cpld_fpga_done;
   input  clk_33mhz_fpga;
   output cfg_addr_out2;
   output cfg_addr_out1;
   output cfg_addr_out0;
   input  cfg_addr_in2;
   input  cfg_addr_in1;
   input  cfg_addr_in0;
   input  fpga_fallback_en;
   input  sysace_cfg_en;
   output plat_flash_cf_b;
   input  fpga_init;
   inout  fpga_prog_b;
   input  sram_flash_we_b;
//   output bus_error_2;
//   output bus_error_1;
//   output fpga_cpu_reset_b;
//   input  fpga_din;
//   input  fpga_dout_busy;
//   input  fpga_cs_b;
//   input  fpga_cs0_b;
//   output cpld_tp2;
//   output cpld_tp10;
//   output cpld_tp1;
//   output cpld_tp0;
//   input  fpga_rdwr_b;
   input sysace_rst_n;

   wire lcd_read_dir;
   reg  usb_reset;
   reg  [5:0] por_counter;
   wire plat_flash_cf_b_i;
   wire [2:0] mode;


   // USED TO RESET PLATFORM FLASH AFTER LOADING THE CONFIG
   reg fpga_done_last;
   reg [3:0] countdown;
   reg counting;
   reg plat_flash_after_config_strobe; 
   reg cf_request;
   // USED TO RESET PLATFORM FLASH AFTER LOADING THE CONFIG

// Start CPLD Code - Begin //////////////////////////////////////////

   // Enable Sysace to configure FPGA on reset or startup with enable
   // switch is ON and mode pins are set to JTAG.
   assign sysace_cfgmodepin = sysace_cfg_en & fpga_m2 & ~fpga_m1 & fpga_m0;

   // LCD signal. LCD signals are buffered through CPLD because LCD has
   // a 5V logic interface. CPLD is 5V tolerant, FPGA is not 5V tolerant.
   assign lcd_cpld_rw = lcd_fpga_rw;
   assign lcd_cpld_rs = lcd_fpga_rs;
   assign lcd_cpld_e  = lcd_fpga_e;
   assign lcd_read_dir = lcd_fpga_rw & lcd_fpga_e;
   assign lcd_fpga_db7 = lcd_read_dir? lcd_cpld_db7 : 1'bz;
   assign lcd_fpga_db6 = lcd_read_dir? lcd_cpld_db6 : 1'bz;
   assign lcd_fpga_db5 = lcd_read_dir? lcd_cpld_db5 : 1'bz;
   assign lcd_fpga_db4 = lcd_read_dir? lcd_cpld_db4 : 1'bz;
   assign lcd_cpld_db7 = lcd_read_dir? 1'bz : lcd_fpga_db7;
   assign lcd_cpld_db6 = lcd_read_dir? 1'bz : lcd_fpga_db6;
   assign lcd_cpld_db5 = lcd_read_dir? 1'bz : lcd_fpga_db5;
   assign lcd_cpld_db4 = lcd_read_dir? 1'bz : lcd_fpga_db4;

   // Power-On Reset to usb_reset_b (open drain signal). 
   always @(posedge clk_33mhz_fpga) begin
     if (por_counter!= 6'd50) por_counter <= por_counter+1;
     usb_reset  = (por_counter == 6'd50);

     // USED TO RESET PLATFORM FLASH AFTER LOADING THE CONFIG
     fpga_done_last <= fpga_done;
     if ((!fpga_done_last) && (fpga_done))
	begin
	counting <= 1'b1;
     end

     if (counting)
	begin
	countdown <= countdown+1;
	if (countdown == 4'd2)
	begin
		plat_flash_after_config_strobe <= 1'b1;
	end
	if(countdown == 4'd4)
	begin
		plat_flash_after_config_strobe <= 1'b0;
	end
	else if (countdown == 4'd8) // ~ 0.16us
	begin
		cf_request <= 1'b0; //make a rising edge
	end
	else if (countdown == 4'd15) // give some time to reset... ~ 0.33us
	begin
		cf_request <= 1'b1;
		counting <= 1'b0;
		countdown <= 4'd0;
	end
     end
     // USED TO RESET PLATFORM FLASH AFTER LOADING THE CONFIG

   end

   assign usb_reset_b = (usb_reset)? 1'bz : 1'b0;

   // Enable Plat Flash to drive D0 when in select map mode (parallel download). When FPGA
   // signals DONE, Plat Flash is disabled so it stops driving sram_flash_d[7:0].
   assign sram_flash_d0_en = ~fpga_done &
                            ((fpga_m2 & ~fpga_m1 & ~fpga_m0)     // Master Select Map
                             | (fpga_m2 & fpga_m1 & ~fpga_m0)); // Slave  Select Map
   assign mode = {fpga_m2, fpga_m1, fpga_m0};
   assign plat_flash_ce_b = (mode == 3'b001) | (mode == 3'b010) | (mode == 3'b011) | (mode == 3'b101) | plat_flash_after_config_strobe;

   // Buffer Signals (Drive LEDs, Drive SPDIF 75 ohm load, Buffer DIP Switchi input
   assign cpld_led_w = plat_flash_ce_b;//gpio_led_w;
   assign cpld_led_s = plat_flash_cf_b_i; //gpio_led_s;
   assign cpld_led_n = gpio_led_n;
   assign cpld_led_e = gpio_led_e;
   assign cpld_led_c = gpio_led_c;
   assign cpld_led_3 = gpio_led_3;
   assign cpld_led_2 = gpio_led_2;
   assign cpld_led_1 = gpio_led_1;
   assign cpld_led_0 = gpio_led_0;
   assign cpld_fpga_init = fpga_init;
   assign cpld_fpga_done = fpga_done;
   assign cpld_spdif_3 = audio_spdif;
   assign cpld_spdif_2 = audio_spdif;
   assign cpld_spdif_1 = audio_spdif;
   assign cfg_addr_out2 = cfg_addr_in2;
   assign cfg_addr_out1 = cfg_addr_in1;
   assign cfg_addr_out0 = cfg_addr_in0;

   // Note sram_flash_we_b is also used as FPGA cf_b for fallback configuration mode
   //assign plat_flash_cf_b_i = (fpga_fallback_en? (fpga_done | (sram_flash_we_b & fpga_prog_b)) : fpga_prog_b)
   //                         | (mode == 3'b001) | (mode == 3'b010) | (mode == 3'b011) | (mode == 3'b101) | cf_request;
   assign plat_flash_cf_b_i = !fpga_done | (mode == 3'b001) | (mode == 3'b010) | (mode == 3'b011) | (mode == 3'b101) | cf_request;
   // plat_flash_cf_b is an open drain signal (has external pullup)
   // assign plat_flash_cf_b = (plat_flash_cf_b_i)? 1'bz : 1'b0;

   // plat_flash_cf_b is a driven signal
   assign plat_flash_cf_b = plat_flash_cf_b_i;

   assign fpga_prog_b = (sysace_rst_n || (!fpga_done))? 1'bz: 1'b0;

endmodule
