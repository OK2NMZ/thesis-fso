##############################################################################
#                                                                            #
# ML50x CPLD Reference Design  Release 1.1                                   #
#                                                                            #
# July 7, 2006                                                              #
#                                                                            #
##############################################################################
#
#     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
#     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
#     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
#     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
#     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
#     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
#     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
#     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
#     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
#     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
#     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
#     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#     FOR A PARTICULAR PURPOSE.
#     
#     (c) Copyright 2005 Xilinx, Inc.
#     All rights reserved.
#
##############################################################################

Introduction
------------

The ML50x contains a XC95144XL CPLD whose function is to implement basic
glue logic. This includes simple sets of logic gates, buffers, and level
translators.

Rebuilding the CPLD design
--------------------------
The ML50x Evaluation Platform ships with this CPLD design already
programmed onto the CPLD.  If you wish to rebuild the CPLD design,
follow the instructions below.

Requirements: Xilinx ISE software

(1) Re-synthesize design.  Run the script runxst within a bash or bourne
shell (on the PC, you can use an EDK shell)
Example: runxst
Results are stored in the 'synthesis' sub-directory.

(2) Re-implement design.  Run the batch file runxflow.bat.
Results are stored in the 'implementation' sub-directory.
The system.jed file can be written to the ML50x CPLD by iMPACT
through a download cable.

------------------------------
Marek Novak:
Or just open the shell and type: make
You will get your file named CPLD_PROGRAMMING_FILE.jed
