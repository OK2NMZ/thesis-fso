--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:28:14 02/08/2016
-- Design Name:   
-- Module Name:   /home/ok2nmz/Desktop/DP/thesis-fso/Firmware/MediaConverter_ML505/SFP_Simplified_MAC_tb.vhd
-- Project Name:  MediaConverter_ML505_v5
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: SFP_Simplified_MAC
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.math_real.all;

ENTITY SFP_Simplified_MAC_tb IS
END SFP_Simplified_MAC_tb;
 
ARCHITECTURE behavior OF SFP_Simplified_MAC_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT SFP_Simplified_MAC
    PORT(
         RAW_RX : IN  std_logic_vector(9 downto 0);
         CLK_RX : IN  std_logic;
         RAW_TX : OUT  std_logic_vector(9 downto 0);
         CLK_TX : IN  std_logic;
			
         SYS_CLK : IN  std_logic;
         SYS_RST : IN  std_logic;
         ALIGNED : OUT  std_logic;
         
			DV_RX : OUT  std_logic;
         GF_RX : OUT  std_logic;
         BF_RX : OUT  std_logic;
         DIN_RX : OUT  std_logic_vector(7 downto 0);
         
			DV_TX : IN  std_logic;
         ACK_TX : OUT  std_logic;
         DOUT_TX : IN  std_logic_vector(7 downto 0);
			
         wr_clk : OUT  std_logic;
         rd_clk : OUT  std_logic;
         din : OUT  std_logic_vector(8 downto 0);
         wr_en : OUT  std_logic;
         rd_en : OUT  std_logic;
         dout : IN  std_logic_vector(8 downto 0);
         full : IN  std_logic;
         empty : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal RAW_RX : std_logic_vector(9 downto 0) := (others => '0');
   signal CLK_RX : std_logic := '0';
   signal CLK_TX : std_logic := '0';
   signal SYS_CLK : std_logic := '0';
   signal SYS_RST : std_logic := '0';
   signal DV_TX : std_logic := '0';
   signal DOUT_TX : std_logic_vector(7 downto 0) := (others => '0');
   signal dout : std_logic_vector(8 downto 0) := (others => '0');
   signal full : std_logic := '0';
   signal empty : std_logic := '0';

 	--Outputs
   signal RAW_TX : std_logic_vector(9 downto 0);
   signal ALIGNED : std_logic;
   signal DV_RX : std_logic;
   signal GF_RX : std_logic;
   signal BF_RX : std_logic;
   signal DIN_RX : std_logic_vector(7 downto 0);
   signal ACK_TX : std_logic;
   signal wr_clk : std_logic;
   signal rd_clk : std_logic;
   signal din : std_logic_vector(8 downto 0);
   signal wr_en : std_logic;
   signal rd_en : std_logic;

   -- Clock period definitions
   constant CLK_RX_period : time := 10 ns;
   constant CLK_TX_period : time := 10 ns;
   constant SYS_CLK_period : time := 10 ns;
   constant wr_clk_period : time := 10 ns;
   constant rd_clk_period : time := 10 ns;
	
	signal counter : integer := 0;
	signal stop_in : integer := 0;
	
	signal rand_ber : integer := 10000;
	signal rand_num : integer := 0;
	signal rand_len : integer := 60;
	signal this_len : integer := 60;
BEGIN


process(SYS_CLK)
    variable seed1, seed2: positive;               -- seed values for random generator
    variable rand: real;   -- random real-number value in range 0 to 1.0  
    variable range_of_rand : real := 80.0;    -- the range of random values created will be 0 to +1000.
begin
    uniform(seed1, seed2, rand);   -- generate random number
    rand_num <= 1 + integer(rand*range_of_rand);  -- rescale to 0..1000, convert integer part 
end process;


process(SYS_CLK)
    variable seed1, seed2: positive;               -- seed values for random generator
    variable rand: real;   -- random real-number value in range 0 to 1.0  
    variable range_of_rand : real := 1500.0;    -- the range of random values created will be 0 to +1000.
begin
    uniform(seed1, seed2, rand);   -- generate random number
    rand_len <= 42 + integer(rand*range_of_rand);  -- rescale to 0..1000, convert integer part 
end process;

process(SYS_CLK)
    variable seed1, seed2: positive;               -- seed values for random generator
    variable rand: real;   -- random real-number value in range 0 to 1.0  
    variable range_of_rand : real := 10000.0;    -- the range of random values created will be 0 to +1000.
begin
    uniform(seed1, seed2, rand);   -- generate random number
    rand_ber <= 0 + integer(rand*range_of_rand);  -- rescale to 0..1000, convert integer part 
	 IF rand_ber < -1 THEN
      RAW_RX <= "0000000000";
	 ELSE
		RAW_RX <= RAW_TX;
	 END IF;
end process;
 
	-- Instantiate the Unit Under Test (UUT)
   uut: SFP_Simplified_MAC PORT MAP (
          RAW_RX => RAW_RX,
          CLK_RX => CLK_RX,
          RAW_TX => RAW_TX,
          CLK_TX => CLK_TX,
          SYS_CLK => SYS_CLK,
          SYS_RST => SYS_RST,
          ALIGNED => ALIGNED,
          DV_RX => DV_RX,
          GF_RX => GF_RX,
          BF_RX => BF_RX,
          DIN_RX => DIN_RX,
          DV_TX => DV_TX,
          ACK_TX => ACK_TX,
          DOUT_TX => DOUT_TX,
          wr_clk => wr_clk,
          rd_clk => rd_clk,
          din => din,
          wr_en => wr_en,
          rd_en => rd_en,
          dout => dout,
          full => full,
          empty => empty
        );

   -- Clock process definitions
   CLK_RX_process :process
   begin
		CLK_RX <= '0';
		wait for CLK_RX_period/2;
		CLK_RX <= '1';
		wait for CLK_RX_period/2;
   end process;
 
   CLK_TX_process :process
   begin
		CLK_TX <= '0';
		wait for CLK_TX_period/2;
		CLK_TX <= '1';
		wait for CLK_TX_period/2;
   end process;
 
   SYS_CLK_process :process
   begin
		SYS_CLK <= '0';
		wait for SYS_CLK_period/2;
		SYS_CLK <= '1';
		wait for SYS_CLK_period/2;
   end process;
 
   wr_clk_process :process
   begin
		wr_clk <= '0';
		wait for wr_clk_period/2;
		wr_clk <= '1';
		wait for wr_clk_period/2;
   end process;
 
   rd_clk_process :process
   begin
		rd_clk <= '0';
		wait for rd_clk_period/2;
		rd_clk <= '1';
		wait for rd_clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		SYS_RST <= '1';
      wait for 100 ns;
		SYS_RST <= '0';
		wait until rising_edge(SYS_CLK);
		
		DV_TX <= '1';
		DOUT_TX <= X"FF";
		wait for CLK_TX_period;
		for w in 0 to 5 loop
			IF ACK_TX = '1' THEN
				wait for CLK_TX_period;
				exit;
			END IF;
		end loop;
		for I in 0 to 80 loop
			IF I < 10 THEN
				DOUT_TX <= 	X"FF";	
			ELSE
				DOUT_TX <= DOUT_TX + X"01";
			END IF;	
			wait for CLK_TX_period;
		end loop;
		DV_TX <= '0';
		wait for 10*CLK_TX_period;
		
		
		for MASTERLOOP in 0 to 100000 loop
				DV_TX <= '1';
				DOUT_TX <= X"FF";
				wait for CLK_TX_period;
				for w in 0 to 5 loop
					IF ACK_TX = '1' THEN
						wait for CLK_TX_period;
						exit;
					END IF;
				end loop;
				
				this_len <= rand_len;
				for I in 0 to this_len loop
					IF I < 10 THEN
						DOUT_TX <= 	X"FF";	
					ELSE
						DOUT_TX <= DOUT_TX + X"01";
					END IF;
					wait for CLK_TX_period;
				end loop;
				
				DV_TX <= '0';
				wait for rand_num*CLK_TX_period;
		end loop;
		
		DV_TX <= '1';
		DOUT_TX <= X"FF";
		wait for CLK_TX_period;
		for w in 0 to 5 loop
			IF ACK_TX = '1' THEN
				wait for CLK_TX_period;
				exit;
			END IF;
		end loop;
		for I in 0 to 80 loop
			IF I < 10 THEN
				DOUT_TX <= 	X"FF";	
			ELSE
				DOUT_TX <= DOUT_TX + X"01";
			END IF;	
			wait for CLK_TX_period;
		end loop;
		DV_TX <= '0';
		wait for 10*CLK_TX_period;
		
		assert false report "Simulation Finished" severity failure;
		
      wait;
   end process;
	
	process(SYS_CLK)
	begin
		IF rising_edge(SYS_CLK) THEN
			IF DV_RX = '1' THEN
				counter <= counter + 1;
				IF counter < 9 THEN
					IF DIN_RX /= X"FF" THEN
						assert DIN_RX = X"FF" report " !!! Found error !!!" severity warning;
						stop_in <= 1;
					END IF;
				END IF;
			ELSE
				counter <= 0;
			END IF;
			
			IF stop_in /= 0 THEN
				stop_in <= stop_in + 1;
				IF stop_in = 10000 THEN
					assert false report " !!! STOP, ERROR!!!" severity failure;
				END IF;
			END IF;
			
		END IF;
	end process;

END;
