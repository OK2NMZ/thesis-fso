--------------------------------------------------------------------------------
--     (c) Copyright 1995 - 2010 Xilinx, Inc. All rights reserved.            --
--                                                                            --
--     This file contains confidential and proprietary information            --
--     of Xilinx, Inc. and is protected under U.S. and                        --
--     international copyright and other intellectual property                --
--     laws.                                                                  --
--                                                                            --
--     DISCLAIMER                                                             --
--     This disclaimer is not a license and does not grant any                --
--     rights to the materials distributed herewith. Except as                --
--     otherwise provided in a valid license issued to you by                 --
--     Xilinx, and to the maximum extent permitted by applicable              --
--     law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND                --
--     WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES            --
--     AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING              --
--     BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-                 --
--     INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and               --
--     (2) Xilinx shall not be liable (whether in contract or tort,           --
--     including negligence, or under any other theory of                     --
--     liability) for any loss or damage of any kind or nature                --
--     related to, arising under or in connection with these                  --
--     materials, including for any direct, or any indirect,                  --
--     special, incidental, or consequential loss or damage                   --
--     (including loss of data, profits, goodwill, or any type of             --
--     loss or damage suffered as a result of any action brought              --
--     by a third party) even if such damage or loss was                      --
--     reasonably foreseeable or Xilinx had been advised of the               --
--     possibility of the same.                                               --
--                                                                            --
--     CRITICAL APPLICATIONS                                                  --
--     Xilinx products are not designed or intended to be fail-               --
--     safe, or for use in any application requiring fail-safe                --
--     performance, such as life-support or safety devices or                 --
--     systems, Class III medical devices, nuclear facilities,                --
--     applications related to the deployment of airbags, or any              --
--     other applications that could lead to death, personal                  --
--     injury, or severe property or environmental damage                     --
--     (individually and collectively, "Critical                              --
--     Applications"). Customer assumes the sole risk and                     --
--     liability of any use of Xilinx products in Critical                    --
--     Applications, subject only to applicable laws and                      --
--     regulations governing limitations on product liability.                --
--                                                                            --
--     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS               --
--     PART OF THIS FILE AT ALL TIMES.                                        --
--------------------------------------------------------------------------------

--  Generated from component ID: xilinx.com:ip:sid:6.0


-- You must compile the wrapper file interleaver_ipcore.vhd when simulating
-- the core, interleaver_ipcore. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib VHDL simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

-- The synthesis directives "translate_off/translate_on" specified
-- below are supported by Xilinx, Mentor Graphics and Synplicity
-- synthesis tools. Ensure they are correct for your synthesis tool(s).

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
-- synthesis translate_off
Library XilinxCoreLib;
-- synthesis translate_on
ENTITY interleaver_ipcore IS
	port (
	clk: in std_logic;
	fd: in std_logic;
	din: in std_logic_vector(4 downto 0);
	nd: in std_logic;
	dout: out std_logic_vector(4 downto 0);
	rdy: out std_logic);
END interleaver_ipcore;

ARCHITECTURE interleaver_ipcore_a OF interleaver_ipcore IS
-- synthesis translate_off
component wrapped_interleaver_ipcore
	port (
	clk: in std_logic;
	fd: in std_logic;
	din: in std_logic_vector(4 downto 0);
	nd: in std_logic;
	dout: out std_logic_vector(4 downto 0);
	rdy: out std_logic);
end component;

-- Configuration specification 
	for all : wrapped_interleaver_ipcore use entity XilinxCoreLib.sid_v6_0(behavioral)
		generic map(
			c_num_selectable_cols => 4,
			c_architecture => 0,
			c_has_sclr => 0,
			c_type => 0,
			c_col_constant => 16,
			c_use_col_permute_file => 0,
			c_col_width => 4,
			c_has_col_sel => 0,
			c_has_row_sel => 0,
			c_col_permute_file => "interleaver_ipcore_CP.mif",
			c_min_num_cols => 15,
			c_has_block_size_valid => 0,
			c_has_block_size => 0,
			c_has_row_valid => 0,
			c_row_select_file => "interleaver_ipcore_RS.mif",
			c_block_size_type => 3,
			c_mode => 0,
			c_has_rdy => 1,
			c_col_select_file => "interleaver_ipcore_CS.mif",
			c_has_nd => 1,
			c_branch_length_type => 0,
			c_block_size_width => 8,
			c_has_rffd => 0,
			c_external_ram => 0,
			c_row_constant => 16,
			c_num_configurations => 1,
			c_has_block_start => 0,
			c_use_row_permute_file => 0,
			c_row_width => 4,
			c_has_col_sel_valid => 0,
			c_num_selectable_rows => 4,
			c_has_block_end => 0,
			c_num_branches => 16,
			c_row_permute_file => "interleaver_ipcore_RP.mif",
			c_has_col => 0,
			c_branch_length_constant => 16,
			c_row_type => 0,
			c_block_size_constant => 225,
			c_mem_init_prefix => "interleaver_ipcore",
			c_throughput_mode => 0,
			c_min_num_rows => 15,
			c_col_type => 0,
			c_has_row => 0,
			c_branch_length_file => "interleaver_ipcore.mif",
			c_has_col_valid => 0,
			c_has_fdo => 0,
			c_has_row_sel_valid => 0,
			c_memstyle => 2,
			c_ext_addr_width => 8,
			c_has_ndo => 0,
			c_has_rfd => 0,
			c_has_ce => 0,
			c_symbol_width => 5,
			c_pipe_level => 2);
-- synthesis translate_on
BEGIN
-- synthesis translate_off
U0 : wrapped_interleaver_ipcore
		port map (
			clk => clk,
			fd => fd,
			din => din,
			nd => nd,
			dout => dout,
			rdy => rdy);
-- synthesis translate_on

END interleaver_ipcore_a;

