library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library WORK;
use WORK.modules_pkg.ALL;
use WORK.parameters_pkg.ALL;
use WORK.types_pkg.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FlowController is
    Port ( ETH_DATA_IN            : in STD_LOGIC_VECTOR (7 downto 0)  := (OTHERS => '0');
           ETH_DV_IN              : in STD_LOGIC                      := '0';
           ETH_GF_IN              : in STD_LOGIC                      := '0';
           ETH_BF_IN              : in STD_LOGIC                      := '0';
           ETH_CLK_IN             : in STD_LOGIC                      := '0';

           ETH_DATA_OUT           : out STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
           ETH_DV_OUT             : out STD_LOGIC                     := '0';
           ETH_ACK_OUT            : in STD_LOGIC                      := '0';
           ETH_CLK_OUT            : in STD_LOGIC                      := '0';

           SFP_DATA_IN            : in STD_LOGIC_VECTOR (7 downto 0)  := (OTHERS => '0');
           SFP_DV_IN              : in STD_LOGIC                      := '0';
           SFP_GF_IN              : in STD_LOGIC                      := '0';
           SFP_BF_IN              : in STD_LOGIC                      := '0';
           SFP_CLK_IN             : in STD_LOGIC                      := '0';

           SFP_DATA_OUT           : out STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
           SFP_DV_OUT             : out STD_LOGIC                     := '0';
           SFP_ACK_OUT            : in STD_LOGIC                      := '0';
           SFP_CLK_OUT            : in STD_LOGIC                      := '0';

           FIFO_TO_UB_DATA_OUT    : out STD_LOGIC_VECTOR (8 downto 0) := (OTHERS => '0');
           FIFO_TO_UB_RD_EN_IN    : in STD_LOGIC                      := '0';
           FIFO_TO_UB_EMPTY       : out STD_LOGIC                     := '0';

           FIFO_FROM_UB_DATA_IN   : in STD_LOGIC_VECTOR (8 downto 0)  := (OTHERS => '0');
           FIFO_FROM_UB_WR_EN_IN  : in STD_LOGIC                      := '0';
           FIFO_FROM_UB_PROG_FULL : out STD_LOGIC                     := '0';

           UBLAZE_MAC_IN          : in STD_LOGIC_VECTOR(47 downto 0);

           CLK_UBLAZE_IN          : in STD_LOGIC;

           SYS_CLK                : in STD_LOGIC                      := '0';
           SYS_RST                : in STD_LOGIC                      := '0');

end FlowController;

architecture Behavioral of FlowController is

SIGNAL UBLAZE_MAC_ADDRESS : STD_LOGIC_VECTOR(47 downto 0) := X"000DB92B94E0";

--- STATE MACHINE SIGNALS AND TYPES
TYPE   MAC_MATCHER_STATE_T   IS (MM_IDLE, MM_M1, MM_M2, MM_M3, MM_M4, MM_M5, MM_M6,
                                 MM_BC2, MM_BC3, MM_BC4, MM_BC5, MM_BC6,
                                 MM_SET_MATCH, MM_NO_MATCH, MM_NEXT_FRAME);
TYPE   ETH_RX_STATE_T        IS (ERX_IDLE, ERX_DROP, ERX_RECEIVE, ERX_GOOD_BAD, ERX_END);
TYPE   UBLAZE_TO_ETH_STATE_T IS (UTE_IDLE, UTE_DROP, UTE_TX, UTE_END);
TYPE   UBLAZE_OUT_STATE_T    IS (UO_IDLE, UO_GOOD_BAD, UO_ACK, UO_TRANSMIT);

SIGNAL ETH_RX_STATE                 : ETH_RX_STATE_T               := ERX_IDLE;
SIGNAL ETH_TX_STATE                 : STD_LOGIC_VECTOR(3 downto 0) := X"0";
SIGNAL SFP_RX_STATE                 : STD_LOGIC_VECTOR(3 downto 0) := X"0";
SIGNAL SFP_TX_STATE                 : STD_LOGIC_VECTOR(3 downto 0) := X"0";
SIGNAL MAC_MATCHER_STATE            : MAC_MATCHER_STATE_T          := MM_IDLE;
SIGNAL MAC_MATCHER_SFP_STATE        : MAC_MATCHER_STATE_T          := MM_IDLE;
SIGNAL UBLAZE_TO_ETH_STATE          : UBLAZE_TO_ETH_STATE_T        := UTE_IDLE;
SIGNAL UBLAZE_OUT_STATE             : UBLAZE_OUT_STATE_T           := UO_IDLE;

SIGNAL ETH_DV_OUT_i                 : STD_LOGIC                    := '0';

--- ETH FIFO SIGNALS ---
SIGNAL FIFO_FROM_ETH_DIN            : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_ETH_DIN_i          : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_ETH_DOUT           : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_ETH_RDEN           : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_RDEN_i         : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_WREN           : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_WREN_i         : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_FULL           : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_EMPTY          : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_DROP           : STD_LOGIC                    := '0';

SIGNAL FIFO_FROM_ETH_SIG_DIN        : STD_LOGIC_VECTOR(1 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_ETH_SIG_MATCH      : STD_LOGIC_VECTOR(1 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_ETH_SIG_DOUT       : STD_LOGIC_VECTOR(1 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_ETH_SIG_RDEN       : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_SIG_WREN       : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_SIG_FULL       : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_ETH_SIG_EMPTY      : STD_LOGIC                    := '0';

--- SFP FIFO SIGNALS ---
SIGNAL FIFO_FROM_SFP_DIN            : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_SFP_DIN_i          : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_SFP_DOUT           : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_SFP_RDEN           : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_RDEN_i         : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_WREN           : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_WREN_i         : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_FULL           : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_EMPTY          : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_DROP           : STD_LOGIC                    := '0';

SIGNAL FIFO_FROM_SFP_SIG_DIN        : STD_LOGIC_VECTOR(1 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_SFP_SIG_MATCH      : STD_LOGIC_VECTOR(1 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_SFP_SIG_DOUT       : STD_LOGIC_VECTOR(1 downto 0) := (OTHERS => '0');
SIGNAL FIFO_FROM_SFP_SIG_RDEN       : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_SIG_WREN       : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_SIG_FULL       : STD_LOGIC                    := '0';
SIGNAL FIFO_FROM_SFP_SIG_EMPTY      : STD_LOGIC                    := '0';

-- ETH -> UBLAZE SIGNALS
SIGNAL UBLAZE_MAC_MATCH             : STD_LOGIC                    := '0';

SIGNAL FIFO_ETH_TO_UBLAZE_DIN       : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_ETH_TO_UBLAZE_DIN_i     : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_ETH_TO_UBLAZE_DOUT      : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_ETH_TO_UBLAZE_RDEN      : STD_LOGIC                    := '0';
SIGNAL FIFO_ETH_TO_UBLAZE_RDEN_i    : STD_LOGIC                    := '0';
SIGNAL FIFO_ETH_TO_UBLAZE_WREN      : STD_LOGIC                    := '0';
SIGNAL FIFO_ETH_TO_UBLAZE_WREN_i    : STD_LOGIC                    := '0';
SIGNAL FIFO_ETH_TO_UBLAZE_FULL      : STD_LOGIC                    := '0';
SIGNAL FIFO_ETH_TO_UBLAZE_PROG_FULL : STD_LOGIC                    := '0';
SIGNAL FIFO_ETH_TO_UBLAZE_EMPTY     : STD_LOGIC                    := '0';

-- UBLAZE -> ETH SIGNALS
SIGNAL FIFO_UBLAZE_TO_ETH_DIN       : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_UBLAZE_TO_ETH_DIN_i     : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_UBLAZE_TO_ETH_DOUT      : STD_LOGIC_VECTOR(8 downto 0) := (OTHERS => '0');
SIGNAL FIFO_UBLAZE_TO_ETH_RDEN      : STD_LOGIC                    := '0';
SIGNAL FIFO_UBLAZE_TO_ETH_RDEN_i    : STD_LOGIC                    := '0';
SIGNAL FIFO_UBLAZE_TO_ETH_WREN      : STD_LOGIC                    := '0';
SIGNAL FIFO_UBLAZE_TO_ETH_WREN_i    : STD_LOGIC                    := '0';
SIGNAL FIFO_UBLAZE_TO_ETH_FULL      : STD_LOGIC                    := '0';
SIGNAL FIFO_UBLAZE_TO_ETH_EMPTY     : STD_LOGIC                    := '0';


begin

FIFO_FROM_ETH : entity WORK.frame_fifo
  PORT MAP (
    rst        => SYS_RST,
    wr_clk     => ETH_CLK_IN,
    wr_en      => FIFO_FROM_ETH_WREN,
    din        => FIFO_FROM_ETH_DIN,
    full       => FIFO_FROM_ETH_FULL,
    prog_full  => FIFO_FROM_ETH_DROP,

    rd_clk     => SFP_CLK_OUT, -- ___IMPORTANT___: ETH OUT CLOCK HERE!
    rd_en      => FIFO_FROM_ETH_RDEN,
    dout       => FIFO_FROM_ETH_DOUT,
    empty      => FIFO_FROM_ETH_EMPTY,
    prog_empty => OPEN
  );

FIFO_FROM_ETH_SIG : entity WORK.signal_fifo_2b_read
  PORT MAP (
    rst    => SYS_RST,
    wr_clk => ETH_CLK_IN,
    wr_en  => FIFO_FROM_ETH_SIG_WREN,
    din    => FIFO_FROM_ETH_SIG_DIN,
    full   => FIFO_FROM_ETH_SIG_FULL,

    rd_clk => SFP_CLK_OUT, -- ___IMPORTANT___: SFP OUT CLOCK HERE!
    rd_en  => FIFO_FROM_ETH_SIG_RDEN,
    dout   => FIFO_FROM_ETH_SIG_DOUT,
    empty  => FIFO_FROM_ETH_SIG_EMPTY
  );


FIFO_FROM_SFP : entity WORK.frame_fifo
  PORT MAP (
    rst        => SYS_RST,
    wr_clk     => SFP_CLK_IN,
    wr_en      => FIFO_FROM_SFP_WREN,
    din        => FIFO_FROM_SFP_DIN,
    full       => FIFO_FROM_SFP_FULL,
    prog_full  => FIFO_FROM_SFP_DROP,

    rd_clk     => ETH_CLK_OUT, -- ___IMPORTANT___: ETH OUT CLOCK HERE!
    rd_en      => FIFO_FROM_SFP_RDEN,
    dout       => FIFO_FROM_SFP_DOUT,
    empty      => FIFO_FROM_SFP_EMPTY,
    prog_empty => OPEN
  );

FIFO_FROM_SFP_SIG : entity WORK.signal_fifo_2b_read
  PORT MAP (
    rst    => SYS_RST,
    wr_clk => SFP_CLK_IN,
    wr_en  => FIFO_FROM_SFP_SIG_WREN,
    din    => FIFO_FROM_SFP_SIG_DIN,
    full   => FIFO_FROM_SFP_SIG_FULL,

    rd_clk => ETH_CLK_OUT, -- ___IMPORTANT___: ETH OUT CLOCK HERE!
    rd_en  => FIFO_FROM_SFP_SIG_RDEN,
    dout   => FIFO_FROM_SFP_SIG_DOUT,
    empty  => FIFO_FROM_SFP_SIG_EMPTY
  );

FIFO_ETH_TO_UBLAZE : entity WORK.frame_fifo
  PORT MAP (
    rst      => SYS_RST,
    full     => FIFO_ETH_TO_UBLAZE_FULL,
    empty    => FIFO_TO_UB_EMPTY,
	 prog_full => FIFO_ETH_TO_UBLAZE_PROG_FULL,

    wr_clk   => SFP_CLK_OUT,
    rd_clk   => CLK_UBLAZE_IN,

    din      => FIFO_ETH_TO_UBLAZE_DIN,
    dout     => FIFO_TO_UB_DATA_OUT,
    wr_en    => FIFO_ETH_TO_UBLAZE_WREN,
    rd_en    => FIFO_TO_UB_RD_EN_IN
  );

 FIFO_UBLAZE_TO_ETH : entity WORK.frame_fifo
    PORT MAP (
      rst       => SYS_RST,
      full      => open,
      empty     => FIFO_UBLAZE_TO_ETH_EMPTY,
      prog_full => FIFO_FROM_UB_PROG_FULL,

      wr_clk    => CLK_UBLAZE_IN,
      rd_clk    => ETH_CLK_OUT,

      din       => FIFO_FROM_UB_DATA_IN,
      dout      => FIFO_UBLAZE_TO_ETH_DOUT,
      wr_en     => FIFO_FROM_UB_WR_EN_IN,
      rd_en     => FIFO_UBLAZE_TO_ETH_RDEN
  );

-- ####### ####### #     #    ####### #     # #######
-- #          #    #     #    #     # #     #    #
-- #          #    #     #    #     # #     #    #
-- #####      #    #######    #     # #     #    #
-- #          #    #     #    #     # #     #    #
-- #          #    #     #    #     # #     #    #
-- #######    #    #     #    #######  #####     #

FIFO_FROM_SFP_RDEN      <= FIFO_FROM_SFP_RDEN_i OR ETH_ACK_OUT when ETH_TX_STATE = X"2" else
									FIFO_FROM_SFP_RDEN_i;

FIFO_UBLAZE_TO_ETH_RDEN <= FIFO_UBLAZE_TO_ETH_RDEN_i OR ETH_ACK_OUT when ETH_TX_STATE = X"7" else
									FIFO_UBLAZE_TO_ETH_RDEN_i;



ETH_DV_OUT <= ETH_DV_OUT_i;
FIFO_ETH_TO_UBLAZE_DIN <= FIFO_FROM_ETH_DOUT;

-- MUX, ublaze vs. mainstream
ETH_DATA_OUT <= FIFO_FROM_SFP_DOUT(7 downto 0) when ETH_TX_STATE = X"0" else
					 FIFO_FROM_SFP_DOUT(7 downto 0) when ETH_TX_STATE = X"1" else
					 FIFO_FROM_SFP_DOUT(7 downto 0) when ETH_TX_STATE = X"2" else
					 FIFO_FROM_SFP_DOUT(7 downto 0) when ETH_TX_STATE = X"3" else
					 FIFO_UBLAZE_TO_ETH_DOUT(7 downto 0);

process (ETH_CLK_OUT) begin

  if rising_edge(ETH_CLK_OUT) then

	  if SYS_RST = '1' then
			ETH_TX_STATE <= X"0";
			FIFO_FROM_SFP_SIG_RDEN <= '0';
			FIFO_FROM_SFP_RDEN_i <= '0';
			FIFO_UBLAZE_TO_ETH_RDEN_i <= '0';
			ETH_DV_OUT_i <= '0';
	  else

		 FIFO_FROM_SFP_SIG_RDEN <= '0';
		 FIFO_FROM_SFP_RDEN_i <= '0';
		 FIFO_UBLAZE_TO_ETH_RDEN_i <= '0';
		 ETH_DV_OUT_i <= '0';

		 case ETH_TX_STATE is
		 when X"0" => --IDLE
			if FIFO_FROM_SFP_SIG_EMPTY = '0' then
				FIFO_FROM_SFP_SIG_RDEN <= '1';
				ETH_TX_STATE <= X"1";
			elsif FIFO_UBLAZE_TO_ETH_EMPTY = '0' then
			  ETH_DV_OUT_i <= '1';
			  ETH_TX_STATE <= X"7";
			end if;

		 when X"1" => --IS BAD?
			FIFO_FROM_SFP_RDEN_i <= '1';

			if FIFO_FROM_SFP_SIG_DOUT(0) = '1' then
			  ETH_TX_STATE <= X"6";
			else
			  ETH_TX_STATE <= X"2";
			end if;

		 when X"2" => -- WAIT FOR ACK
			  ETH_DV_OUT_i <= '1';

			if ETH_ACK_OUT = '1' then
			  FIFO_FROM_SFP_RDEN_i <= '1';
			  ETH_TX_STATE <= X"3";
			else
			  ETH_TX_STATE <= X"2";
			end if;

		 when X"7" => -- WAIT FOR ACK (UBLAZE)
			ETH_DV_OUT_i <= '1';

			if ETH_ACK_OUT = '1' then
			  FIFO_UBLAZE_TO_ETH_RDEN_i <= '1';
			  ETH_TX_STATE <= X"5";
			else
			  ETH_TX_STATE <= X"7";
			end if;

		 when X"3" => --TRANSMIT
			FIFO_FROM_SFP_RDEN_i <= '1';
			ETH_DV_OUT_i <= '1';

			if FIFO_FROM_SFP_DOUT(8) = '1' OR  FIFO_FROM_SFP_EMPTY = '1' then
			  ETH_TX_STATE <= X"0";
			  FIFO_FROM_SFP_RDEN_i <= '0';
			  ETH_DV_OUT_i <= '0';
			end if;

		 when X"5" => -- READ FROM UBLAZE
			FIFO_UBLAZE_TO_ETH_RDEN_i <= '1';
			ETH_DV_OUT_i <= '1';

			if FIFO_UBLAZE_TO_ETH_DOUT(8) = '1' OR  FIFO_UBLAZE_TO_ETH_EMPTY = '1' then
			  ETH_TX_STATE <= X"0";
			  FIFO_UBLAZE_TO_ETH_RDEN_i <= '0';
			  ETH_DV_OUT_i <= '0';
			end if;

		 when X"6" => -- DROP
			FIFO_FROM_SFP_RDEN_i <= '1';
			if FIFO_FROM_SFP_DOUT(8) = '1' OR  FIFO_FROM_SFP_EMPTY = '1' then
			  FIFO_FROM_SFP_RDEN_i <= '0';
			  ETH_TX_STATE <= X"0";
			end if;

		 when others =>
			ETH_TX_STATE <= X"0";
		 end case;

	  end if;

  end if;

end process;

-- ####### ####### #     #    ### #     #
-- #          #    #     #     #  ##    #
-- #          #    #     #     #  # #   #
-- #####      #    #######     #  #  #  #
-- #          #    #     #     #  #   # #
-- #          #    #     #     #  #    ##
-- #######    #    #     #    ### #     #

process (ETH_CLK_IN) begin

  if rising_edge(ETH_CLK_IN) then

	  if SYS_RST = '1' then
			ETH_RX_STATE <= ERX_IDLE;
			MAC_MATCHER_STATE <= MM_IDLE;

			UBLAZE_MAC_ADDRESS     <= UBLAZE_MAC_IN;
			FIFO_FROM_ETH_DIN <= FIFO_FROM_ETH_DIN_i;
			FIFO_FROM_ETH_DIN_i <= '0' & ETH_DATA_IN;
			FIFO_FROM_ETH_SIG_DIN <= "00";
			FIFO_FROM_ETH_SIG_WREN <= '0';
			FIFO_FROM_ETH_WREN <= FIFO_FROM_ETH_WREN_i;
			FIFO_FROM_ETH_WREN_i <= '0';
	  else

			UBLAZE_MAC_ADDRESS     <= UBLAZE_MAC_IN;
			FIFO_FROM_ETH_DIN <= FIFO_FROM_ETH_DIN_i;
			FIFO_FROM_ETH_DIN_i <= '0' & ETH_DATA_IN;
			FIFO_FROM_ETH_SIG_DIN <= "00";
			FIFO_FROM_ETH_SIG_WREN <= '0';
			FIFO_FROM_ETH_WREN <= FIFO_FROM_ETH_WREN_i;
			FIFO_FROM_ETH_WREN_i <= '0';

			 case ETH_RX_STATE is
			 when ERX_IDLE => --IDLE
				if ETH_DV_IN = '1' then
				  if FIFO_FROM_ETH_SIG_FULL = '0' and FIFO_FROM_ETH_DROP = '0' then
					 ETH_RX_STATE <= ERX_RECEIVE;
					 FIFO_FROM_ETH_WREN_i <= '1';
				  else
					 ETH_RX_STATE <= ERX_DROP;
				  end if;
				end if;

			 when ERX_DROP => --DROP
				if ETH_GF_IN = '1' then
				  ETH_RX_STATE <= ERX_IDLE;
				elsif ETH_BF_IN = '1' then
				  ETH_RX_STATE <= ERX_IDLE;
				end if;

			 when ERX_RECEIVE => --RECEIVE
				if ETH_DV_IN = '0' then
				  FIFO_FROM_ETH_WREN <= '1';
				  FIFO_FROM_ETH_DIN <= '1' & FIFO_FROM_ETH_DIN_i(7 downto 0); -- mark last byte
				  ETH_RX_STATE <= ERX_GOOD_BAD;
				else
				  FIFO_FROM_ETH_WREN_i <= '1';
				end if;

			 when ERX_GOOD_BAD => --GOOD/BAD
				if ETH_GF_IN = '1' then
				  FIFO_FROM_ETH_SIG_DIN <= FIFO_FROM_ETH_SIG_MATCH;
				  ETH_RX_STATE <= ERX_IDLE;
				  FIFO_FROM_ETH_SIG_WREN <= '1';
				elsif ETH_BF_IN = '1' then
				  FIFO_FROM_ETH_SIG_DIN <= "10"; --drop
				  ETH_RX_STATE <= ERX_IDLE;
				  FIFO_FROM_ETH_SIG_WREN <= '1';
				end if;

			 when others =>
				ETH_RX_STATE <= ERX_IDLE;
			 end case;

		-- #     #    #     #####     #     #    #    #######  #####  #     #
		-- ##   ##   # #   #     #    ##   ##   # #      #    #     # #     #
		-- # # # #  #   #  #          # # # #  #   #     #    #       #     #
		-- #  #  # #     # #          #  #  # #     #    #    #       #######
		-- #     # ####### #          #     # #######    #    #       #     #
		-- #     # #     # #     #    #     # #     #    #    #     # #     #
		-- #     # #     #  #####     #     # #     #    #     #####  #     #

			 case MAC_MATCHER_STATE is
			 when MM_IDLE =>
				if ETH_DV_IN = '1' then
				  MAC_MATCHER_STATE <= MM_M1;  -- go to first match state
				else
				  MAC_MATCHER_STATE <= MM_IDLE;
				end if;

			 when MM_M1 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(47 downto 40) then
				  MAC_MATCHER_STATE <= MM_M2;  -- go to next match
				elsif FIFO_FROM_ETH_DIN_i(7 downto 0) = X"FF" then
				  MAC_MATCHER_STATE <= MM_BC2;  -- no match
				end if;

			 when MM_M2 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(39 downto 32) then
				  MAC_MATCHER_STATE <= MM_M3;  -- go to next match
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- no match
				end if;

			 when MM_M3 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(31 downto 24) then
				  MAC_MATCHER_STATE <= MM_M4;  -- go to next match
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- no match
				end if;

			 when MM_M4 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(23 downto 16) then
				  MAC_MATCHER_STATE <= MM_M5;  -- go to next match
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- no match
				end if;

			 when MM_M5 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(15 downto 8) then
				  MAC_MATCHER_STATE <= MM_M6;  -- go to next match
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- no match
				end if;

			 when MM_M6 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(7 downto 0) then
				  UBLAZE_MAC_MATCH        <= '1';
				  FIFO_FROM_ETH_SIG_MATCH <= "01";
				  MAC_MATCHER_STATE       <= MM_NEXT_FRAME;  -- wait for next frame
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
				end if;
       --
       -- BROADCAST
       --
			 when MM_BC2 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = X"FF" then
				  MAC_MATCHER_STATE <= MM_BC3;
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
				end if;

			 when MM_BC3 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = X"FF" then
				  MAC_MATCHER_STATE <= MM_BC4;
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
				end if;

			 when MM_BC4 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = X"FF" then
				  MAC_MATCHER_STATE <= MM_BC5;
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
				end if;

			 when MM_BC5 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = X"FF" then
				  MAC_MATCHER_STATE <= MM_BC6;
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
				end if;

			 when MM_BC6 =>
				if FIFO_FROM_ETH_DIN_i(7 downto 0) = X"FF" then
				  UBLAZE_MAC_MATCH        <= '1';
				  FIFO_FROM_ETH_SIG_MATCH <= "11";
				  MAC_MATCHER_STATE       <= MM_NEXT_FRAME;  -- wait for next frame
				else
				  MAC_MATCHER_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
				end if;

			 when MM_NO_MATCH =>                -- no match, write '0' to signal FIFO
				UBLAZE_MAC_MATCH         <= '0';   -- and wait for next frame
				FIFO_FROM_ETH_SIG_MATCH  <= "00";
				MAC_MATCHER_STATE        <= MM_NEXT_FRAME;

			 when MM_NEXT_FRAME =>                   -- Wait for end of frame
				UBLAZE_MAC_MATCH       <= '0';

				if ETH_DV_IN = '0' then
				  MAC_MATCHER_STATE <= MM_IDLE;
				else
				  MAC_MATCHER_STATE <= MM_NEXT_FRAME;
				end if;

			 when others =>
				MAC_MATCHER_STATE <= MM_IDLE;
				UBLAZE_MAC_MATCH  <= '0';
			 end case;
		end if;
  end if;

end process;


--  #####  ####### ######     ####### #     # #######
-- #     # #       #     #    #     # #     #    #
-- #       #       #     #    #     # #     #    #
--  #####  #####   ######     #     # #     #    #
--       # #       #          #     # #     #    #
-- #     # #       #          #     # #     #    #
--  #####  #       #          #######  #####     #

SFP_DATA_OUT           <= FIFO_FROM_ETH_DOUT(7 downto 0);

FIFO_FROM_ETH_RDEN <= SFP_ACK_OUT OR FIFO_FROM_ETH_RDEN_i;
process (SFP_CLK_OUT) begin

  if rising_edge(SFP_CLK_OUT) then

    if SYS_RST = '1' then
		 SFP_TX_STATE <= X"0";
		 FIFO_FROM_ETH_SIG_RDEN <= '0';
		 FIFO_FROM_ETH_RDEN_i <= '0';
		 SFP_DV_OUT <= '0';
	 else
		 FIFO_FROM_ETH_SIG_RDEN <= '0';
		 FIFO_FROM_ETH_RDEN_i <= '0';
		 SFP_DV_OUT <= '0';

		 case SFP_TX_STATE is
		 when X"0" => --IDLE
			if FIFO_FROM_ETH_SIG_EMPTY = '0' then
				FIFO_FROM_ETH_SIG_RDEN <= '1';
				SFP_TX_STATE <= X"1";
			end if;

		 when X"1" => --IS BAD? SFP/UBLAZE?
			if FIFO_FROM_ETH_SIG_DOUT = "10" then --drop
				 FIFO_FROM_ETH_RDEN_i <= '1';
				 SFP_TX_STATE <= X"6";
			elsif FIFO_FROM_ETH_SIG_DOUT = "01" then --ublaze
				 if FIFO_ETH_TO_UBLAZE_PROG_FULL = '0' then
					SFP_TX_STATE <= X"7";
				 else
					 FIFO_FROM_ETH_RDEN_i <= '1';
					 SFP_TX_STATE <= X"6";
				 end if;
			elsif FIFO_FROM_ETH_SIG_DOUT = "11" then --broadcast
				 if FIFO_ETH_TO_UBLAZE_PROG_FULL = '0' then
					SFP_DV_OUT <= '1';
					SFP_TX_STATE <= X"8";
				 else
					SFP_DV_OUT <= '1';
					SFP_TX_STATE <= X"2";
				 end if;
			else
				 SFP_DV_OUT <= '1';
				 SFP_TX_STATE <= X"2";
			end if;

		 when X"2" => --WAIT FOR ACK
			SFP_DV_OUT <= '1';
			if SFP_ACK_OUT = '1' then
			  FIFO_FROM_ETH_RDEN_i <= '1';
			  SFP_TX_STATE <= X"3";
			else
			  SFP_TX_STATE <= X"2";
			end if;

		 when X"3" => --TRANSMIT to SFP
			FIFO_FROM_ETH_RDEN_i <= '1';
			if FIFO_FROM_ETH_DOUT(8) = '1' OR  FIFO_FROM_ETH_EMPTY = '1' then
			  SFP_DV_OUT <= '0';
				 FIFO_FROM_ETH_RDEN_i <= '0';
			  SFP_TX_STATE <= X"4";
			else
			  SFP_DV_OUT <= '1';
			end if;

		 when X"4" => --PARSE GF/BF

			  FIFO_ETH_TO_UBLAZE_WREN <= '0'; -------------------------------------------------------
			  SFP_TX_STATE <= X"0";

		 when X"6" => -- DROP
			FIFO_FROM_ETH_RDEN_i <= '1';
			if FIFO_FROM_ETH_DOUT(8) = '1' OR  FIFO_FROM_ETH_EMPTY = '1' then
			  FIFO_FROM_ETH_RDEN_i <= '0'; -------------------------------------------------------
			  SFP_TX_STATE <= X"4";
			end if;

		 when X"7" => -- Write to Ublaze FIFO
			FIFO_FROM_ETH_RDEN_i    <= '1';
			FIFO_ETH_TO_UBLAZE_WREN <= '1';

			if FIFO_FROM_ETH_DOUT(8) = '1' OR  FIFO_FROM_ETH_EMPTY = '1' then
			 SFP_TX_STATE <= X"4";
			end if;

		 when X"8" => --WAIT FOR ACK, broadcast
			SFP_DV_OUT <= '1';
			if SFP_ACK_OUT = '1' then
			  FIFO_FROM_ETH_RDEN_i <= '1';
			  FIFO_ETH_TO_UBLAZE_WREN <= '1';
			  SFP_TX_STATE <= X"9";
			else
			  SFP_TX_STATE <= X"8";
			end if;

		 when X"9" => -- Broadcast, wait for ack
			 FIFO_FROM_ETH_RDEN_i <= '1';
			 FIFO_ETH_TO_UBLAZE_WREN <= '1';

			if FIFO_FROM_ETH_DOUT(8) = '1' OR  FIFO_FROM_ETH_EMPTY = '1' then
			  SFP_DV_OUT <= '0';
			  FIFO_FROM_ETH_RDEN_i <= '0';
			  SFP_TX_STATE <= X"4";
			else
			  SFP_DV_OUT <= '1';
			end if;

		 when others =>
			SFP_TX_STATE <= X"0";
		 end case;
	 end if;
  end if;

end process;

--  #####  ####### ######     ### #     #
-- #     # #       #     #     #  ##    #
-- #       #       #     #     #  # #   #
--  #####  #####   ######      #  #  #  #
--       # #       #           #  #   # #
-- #     # #       #           #  #    ##
--  #####  #       #          ### #     #

process (SFP_CLK_IN) begin

  if rising_edge(SFP_CLK_IN) then

	if SYS_RST = '1' then
		SFP_RX_STATE <= X"0";

		FIFO_FROM_SFP_DIN      <= FIFO_FROM_SFP_DIN_i;
		FIFO_FROM_SFP_DIN_i    <= '0' & SFP_DATA_IN;
		FIFO_FROM_SFP_SIG_DIN  <= "00";
		FIFO_FROM_SFP_SIG_WREN <= '0';
		FIFO_FROM_SFP_WREN     <= FIFO_FROM_SFP_WREN_i;
		FIFO_FROM_SFP_WREN_i   <= '0';
	else

		FIFO_FROM_SFP_DIN       <= FIFO_FROM_SFP_DIN_i;
		FIFO_FROM_SFP_DIN_i     <= '0' & SFP_DATA_IN;
		FIFO_FROM_SFP_SIG_DIN   <= "00";
		FIFO_FROM_SFP_SIG_WREN  <= '0';
		FIFO_FROM_SFP_WREN      <= FIFO_FROM_SFP_WREN_i;
		FIFO_FROM_SFP_WREN_i    <= '0';

		 case SFP_RX_STATE is
		 when X"0" => --IDLE
			if SFP_DV_IN = '1' then
			  if FIFO_FROM_SFP_SIG_FULL = '0' and FIFO_FROM_SFP_DROP = '0' then
				 SFP_RX_STATE <= X"3";
				 FIFO_FROM_SFP_WREN <= '1';
				 FIFO_FROM_SFP_WREN_i <= '1';
			  else
				 SFP_RX_STATE <= X"1";
			  end if;
			end if;

		 when X"1" => --DROP
			if SFP_GF_IN = '1' then
			  SFP_RX_STATE <= X"0";
			elsif SFP_BF_IN = '1' then
			  SFP_RX_STATE <= X"0";
			end if;

		 when X"3" => --RECEIVE
			if SFP_DV_IN = '0' then
			  FIFO_FROM_SFP_WREN_i <= '0';
			  FIFO_FROM_SFP_DIN <= '1' & FIFO_FROM_SFP_DIN_i(7 downto 0); -- mark last byte
			  SFP_RX_STATE <= X"4";
			  if SFP_GF_IN = '1' then
					FIFO_FROM_SFP_SIG_DIN <= FIFO_FROM_SFP_SIG_MATCH;
					SFP_RX_STATE <= X"0";
					FIFO_FROM_SFP_SIG_WREN <= '1';
				elsif SFP_BF_IN = '1' then
					FIFO_FROM_SFP_SIG_DIN <= "10";
					SFP_RX_STATE <= X"0";
					FIFO_FROM_SFP_SIG_WREN <= '1';
				end if;
			else
			  FIFO_FROM_SFP_WREN_i <= '1';
			end if;

		 when X"4" => --GOOD/BAD
			if SFP_GF_IN = '1' then
				FIFO_FROM_SFP_SIG_DIN <= FIFO_FROM_SFP_SIG_MATCH;
				SFP_RX_STATE <= X"0";
				FIFO_FROM_SFP_SIG_WREN <= '1';
			elsif SFP_BF_IN = '1' then
				FIFO_FROM_SFP_SIG_DIN <= "10";
				SFP_RX_STATE <= X"0";
				FIFO_FROM_SFP_SIG_WREN <= '1';
			end if;

		 when others =>
			SFP_RX_STATE <= X"0";
		 end case;

     -- #     #    #     #####     #     #    #    #######  #####  #     #
     -- ##   ##   # #   #     #    ##   ##   # #      #    #     # #     #
     -- # # # #  #   #  #          # # # #  #   #     #    #       #     #
     -- #  #  # #     # #          #  #  # #     #    #    #       #######
     -- #     # ####### #          #     # #######    #    #       #     #
     -- #     # #     # #     #    #     # #     #    #    #     # #     #
     -- #     # #     #  #####     #     # #     #    #     #####  #     #
     --
     -- FROM SFP
     --

        case MAC_MATCHER_SFP_STATE is
        when MM_IDLE =>
         if SFP_DV_IN = '1' then
           MAC_MATCHER_SFP_STATE <= MM_M1;  -- go to first match state
         else
           MAC_MATCHER_SFP_STATE <= MM_IDLE;
         end if;

        when MM_M1 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(47 downto 40) then
           MAC_MATCHER_SFP_STATE <= MM_M2;  -- go to next match
         elsif FIFO_FROM_ETH_DIN_i(7 downto 0) = X"FF" then
           MAC_MATCHER_SFP_STATE <= MM_BC2;  -- no match
         end if;

        when MM_M2 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(39 downto 32) then
           MAC_MATCHER_SFP_STATE <= MM_M3;  -- go to next match
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- no match
         end if;

        when MM_M3 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(31 downto 24) then
           MAC_MATCHER_SFP_STATE <= MM_M4;  -- go to next match
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- no match
         end if;

        when MM_M4 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(23 downto 16) then
           MAC_MATCHER_SFP_STATE <= MM_M5;  -- go to next match
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- no match
         end if;

        when MM_M5 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(15 downto 8) then
           MAC_MATCHER_SFP_STATE <= MM_M6;  -- go to next match
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- no match
         end if;

        when MM_M6 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = UBLAZE_MAC_ADDRESS(7 downto 0) then
           UBLAZE_MAC_MATCH        <= '1';
           FIFO_FROM_SFP_SIG_MATCH <= "01";
           MAC_MATCHER_SFP_STATE   <= MM_NEXT_FRAME;  -- wait for next frame
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
         end if;

         --
         -- BROADCAST
         --
        when MM_BC2 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = X"FF" then
           MAC_MATCHER_SFP_STATE <= MM_BC3;
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
         end if;

        when MM_BC3 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = X"FF" then
           MAC_MATCHER_SFP_STATE <= MM_BC4;
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
         end if;

        when MM_BC4 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = X"FF" then
           MAC_MATCHER_SFP_STATE <= MM_BC5;
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
         end if;

        when MM_BC5 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = X"FF" then
           MAC_MATCHER_SFP_STATE <= MM_BC6;
         else
           MAC_MATCHER_SFP_STATE <= MM_NO_MATCH;  -- does not match, wait for next frame
         end if;

        when MM_BC6 =>
         if FIFO_FROM_SFP_DIN_i(7 downto 0) = X"FF" then
           UBLAZE_MAC_MATCH        <= '1';
           FIFO_FROM_SFP_SIG_MATCH <= "11";
           MAC_MATCHER_SFP_STATE   <= MM_NEXT_FRAME;  -- wait for next frame
         else
           MAC_MATCHER_SFP_STATE   <= MM_NO_MATCH;    -- does not match, wait for next frame
         end if;

        when MM_NO_MATCH =>                           -- no match, write '0' to signal FIFO
         UBLAZE_MAC_MATCH         <= '0';             -- and wait for next frame
         FIFO_FROM_SFP_SIG_MATCH  <= "00";
         MAC_MATCHER_SFP_STATE    <= MM_NEXT_FRAME;

        when MM_NEXT_FRAME =>                         -- Wait for end of frame
         UBLAZE_MAC_MATCH       <= '0';

         if SFP_DV_IN = '0' then
           MAC_MATCHER_SFP_STATE <= MM_IDLE;
         else
           MAC_MATCHER_SFP_STATE <= MM_NEXT_FRAME;
         end if;

        when others =>
         MAC_MATCHER_SFP_STATE <= MM_IDLE;
         UBLAZE_MAC_MATCH  <= '0';
        end case;

	 end if;
  end if;

end process;

end Behavioral;
