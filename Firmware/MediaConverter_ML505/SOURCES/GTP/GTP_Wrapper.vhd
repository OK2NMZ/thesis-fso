------------------------------------------------------------------------------
-- 2x GTP wrapper (4 channels)
--
--   Tile_0_0 = CH_0   SFP
--   Tile_0_1 = CH_1   SMA
--   Tile_1_0 = CH_2   PHY SGMII
--   Tile_1_1 = CH_3   Loopback
--
------------------------------------------------------------------------------
-- Using the TX Phase-Alignment Circuit to Bypass the TX Buffer
-- 
--	  1. Wait for all clocks to stabilize, then drive TXENPMAPHASEALIGN High. Keep
--	  TXENPMAPHASEALIGN High unless the phase-alignment procedure must be
--	  repeated. Driving TXENPMAPHASEALIGN Low causes phase alignment to be lost.
--	  
--	  2. Wait 512 TXUSRCLK2 clock cycles, and then drive TXPMASETPHASE High.
--	  
--	  3. Wait for 8192 TXUSRCLK2 clock cycles and then drive TXPMASETPHASE Low.
--	  The phase of the PMACLK is now aligned with TXUSRCLK.
--
--
-- The phase-alignment procedure must be redone if any of the following conditions occur:
--	  - GTPRESET is asserted
--	  - PLLPOWERDWNB is deasserted
--	  - The clocking source changed
--
------------------------------------------------------------------------------
--
-- Recommended clocking scheme:
--	  TXUSRCLK <= BUFG <= REFCLKOUT
--
--
------------------------------------------------------------------------------
-- Using the RX Phase-Alignment Circuit to Bypass the RX Buffer
--
--	  1. Reset the RX datapath using GTPRESET or one of the CDR resets.
--
--	  2. Wait for the shared PMA PLL and any DCM or PLL used for RXUSRCLK2 to lock.
--
--	  3. Wait for the CDR to lock and provide a stable RXRECCLK.
--
--	  4. Drive RXPMASETPHASE High for 32 RXUSRCLK2 cycles and then deassert it.
--
--
--
--
------------------------------------------------------------------------------
-- SFP_Rx_EqMix
--
--	  RX equalization circuit
--		  00:	 50.0% wideband, 50.0% high-pass
--		  01:	 62.5% wideband, 37.5% high-pass
--		  10:	 75.0% wideband, 25.0% high-pass
--		  11:	 37.5% wideband, 62.5% high-pass
--
------------------------------------------------------------------------------
-- SFP_Rx_EqPole
--
--	  RX equalizer high-pass filter
--		  0xxx:   Filter pole depends on resistor calibration
--		  1000:   0% nominal pole
--		  1001:   -12.5%
--		  1010:   -25.0%
--		  1011:   -37.5%
--		  1100:   +12.5%
--		  1101:   +25.0%
--		  1110:   +37.5%
--		  1111:   +50.0%
--
------------------------------------------------------------------------------
-- SFP_Tx_Swing
--
--	  transmitter differential output swing
--		  000:	1100 mV
--		  001:	1050 mV
--		  010:	1000 mV
--		  011:	 900 mV
--		  100:	 800 mV
--		  101:	 600 mV
--		  110:	 400 mV
--		  111:	   0 mV
--
------------------------------------------------------------------------------
-- SFP_Tx_Preemphasis
--
--	  relative strength of the main drive and pre-emphasis
--
--		  Attribute "TX_DIFF_BOOST" is set in gtp_phy_tile.vhd
--		  Current seting: TX_DIFF_BOOST TRUE
--
--		  -----------------------------------------------------------
--		  |		|			Transmitter Pre-emphasis (%)		|
--		  |		|------------------------|-----------------------|
--		  | Value  | Pre-emphasis Boost Off | Pre-emphasis Boost On |
--		  |		| TX_DIFF_BOOST = FALSE  | TX_DIFF_BOOST = TRUE  |
--		  |--------|------------------------|-----------------------|
--		  |  000   |		   2			|		   3		   |
--		  |  001   |		   2			|		   3		   |
--		  |  010   |		   2.5		  |		   4		   |
--		  |  011   |		   4.5		  |		  10.5		 |
--		  |  100   |		   9.5		  |		  18.5		 |
--		  |  101   |		  16			|		  28		   |
--		  |  110   |		  23			|		  39		   |
--		  |  111   |		  31			|		  52		   |
--		  -----------------------------------------------------------
--
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
------------------------------------------------------------------------------
ENTITY GTP_Wrapper IS
  PORT(
	-- clocking and reset
	REFCLK_SFP_P_IN						 : IN	STD_LOGIC;			  -- onboard differential reference clock (125 MHz)
	REFCLK_SFP_N_IN						 : IN	STD_LOGIC;

	clk_REF_out							 : OUT   STD_LOGIC;			  -- onboard reference clock output (125 MHz; always available)
	GTP_reset_in							: IN	STD_LOGIC:= '0';		-- full asynchronous reset of both GTP transceiver

	-- SGMII interface signals (high speed GTP Rx/Tx)
	RXP_IN								  : IN	STD_LOGIC_VECTOR(1 DOWNTO 0);
	RXN_IN								  : IN	STD_LOGIC_VECTOR(1 DOWNTO 0);
	TXP_OUT								 : OUT   STD_LOGIC_VECTOR(1 DOWNTO 0);
	TXN_OUT								 : OUT   STD_LOGIC_VECTOR(1 DOWNTO 0);

	-- SFP application interface
	clk_Rx_SFP_out						  : OUT   STD_LOGIC;
	Rx_SFP_Data_10b						 : OUT   STD_LOGIC_VECTOR(9 DOWNTO 0);
	clk_Tx_SFP_out						  : OUT   STD_LOGIC;
	Tx_SFP_Data_10b						 : IN	STD_LOGIC_VECTOR(9 DOWNTO 0);

	-- GTP control
	rst_GTP_SFP_SMA						 : IN	STD_LOGIC;
	rst_Rx_CDR_SFP						  : IN	STD_LOGIC;
	rst_Rx_CDR_SMA						  : IN	STD_LOGIC;
	PLL_locked_SFP_SMA					  : OUT   STD_LOGIC;
  CDR_aligned                 : IN STD_LOGIC;

	-- SFP control interface
	SFP_Tx_Inhibit						  : IN	STD_LOGIC;
	SFP_Tx_Polarity						 : IN	STD_LOGIC;
	SFP_Tx_Swing							: IN	STD_LOGIC_VECTOR(2 DOWNTO 0);
	SFP_Tx_Preemphasis					  : IN	STD_LOGIC_VECTOR(2 DOWNTO 0);
	SFP_Rx_EqMix							: IN	STD_LOGIC_VECTOR(1 DOWNTO 0);
	SFP_Rx_EqPole						   : IN	STD_LOGIC_VECTOR(3 DOWNTO 0));

END GTP_Wrapper;
------------------------------------------------------------------------------
ARCHITECTURE Structural of GTP_Wrapper is
------------------------------------------------------------------------------

  COMPONENT GTP_SFP_tile
  GENERIC(
	-- Simulation attributes
	TILE_SIM_MODE				: string	:= "FAST"; -- Set to Fast Functional Simulation Model	
	TILE_SIM_GTPRESET_SPEEDUP	: integer   := 0; -- Set to 1 to speed up sim reset
	TILE_SIM_PLL_PERDIV2		 : bit_vector:= x"190"; -- Set to the VCO Unit Interval time 

	-- Channel bonding attributes
	TILE_CHAN_BOND_MODE_0		: string	:= "OFF";  -- "MASTER", "SLAVE", or "OFF"
	TILE_CHAN_BOND_LEVEL_0	   : integer   := 0;	 -- 0 to 7. See UG for details
	
	TILE_CHAN_BOND_MODE_1		: string	:= "OFF";  -- "MASTER", "SLAVE", or "OFF"
	TILE_CHAN_BOND_LEVEL_1	   : integer   := 0);	-- 0 to 7. See UG for details

  PORT (
	------------------------ Loopback and Powerdown Ports ----------------------
	LOOPBACK0_IN							: in   std_logic_vector(2 downto 0);
	LOOPBACK1_IN							: in   std_logic_vector(2 downto 0);
	------------------- Receive Ports - RX Data Path interface -----------------
	RXDATA0_OUT							 : out  std_logic_vector(9 downto 0);
	RXDATA1_OUT							 : out  std_logic_vector(9 downto 0);
	RXRECCLK0_OUT						   : out  std_logic;
	RXRECCLK1_OUT						   : out  std_logic;
	RXRESET0_IN							 : in   std_logic;
	RXRESET1_IN							 : in   std_logic;
	RXUSRCLK0_IN							: in   std_logic;
	RXUSRCLK1_IN							: in   std_logic;
	RXUSRCLK20_IN						   : in   std_logic;
	RXUSRCLK21_IN						   : in   std_logic;
	------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
	RXCDRRESET0							 : in   std_logic;
	RXCDRRESET1							 : in   std_logic;
	RXEQMIX0								: in   std_logic_vector(1 downto 0);
	RXEQMIX1								: in   std_logic_vector(1 downto 0);
	RXEQPOLE0							   : in   std_logic_vector(3 downto 0);
	RXEQPOLE1							   : in   std_logic_vector(3 downto 0);
	RXN0_IN								 : in   std_logic;
	RXN1_IN								 : in   std_logic;
	RXP0_IN								 : in   std_logic;
	RXP1_IN								 : in   std_logic;
	-------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
	RXPMASETPHASE0_IN					   : in   std_logic;
	RXPMASETPHASE1_IN					   : in   std_logic;
	--------------------- Shared Ports - Tile and PLL Ports --------------------
	CLKIN_IN								: in   std_logic;
	GTPRESET_IN							 : in   std_logic;
	PLLLKDET_OUT							: out  std_logic;
	REFCLKOUT_OUT						   : out  std_logic;
	RESETDONE0_OUT						  : out  std_logic;
	RESETDONE1_OUT						  : out  std_logic;
	TXENPMAPHASEALIGN_IN					: in   std_logic;
	TXPMASETPHASE_IN						: in   std_logic;
	------------------ Transmit Ports - TX Data Path interface -----------------
	TXDATA0_IN							  : in   std_logic_vector(9 downto 0);
	TXDATA1_IN							  : in   std_logic_vector(9 downto 0);
	TXRESET0_IN							 : in   std_logic;
	TXRESET1_IN							 : in   std_logic;
	TXUSRCLK0_IN							: in   std_logic;
	TXUSRCLK1_IN							: in   std_logic;
	TXUSRCLK20_IN						   : in   std_logic;
	TXUSRCLK21_IN						   : in   std_logic;
	--------------- Transmit Ports - TX Driver and OOB signalling --------------
	TXDIFFCTRL0_IN						  : in   std_logic_vector(2 downto 0);
	TXDIFFCTRL1_IN						  : in   std_logic_vector(2 downto 0);
	TXINHIBIT0_IN						   : in   std_logic;
	TXINHIBIT1_IN						   : in   std_logic;
	TXN0_OUT								: out  std_logic;
	TXN1_OUT								: out  std_logic;
	TXP0_OUT								: out  std_logic;
	TXP1_OUT								: out  std_logic;
	TXPREEMPHASIS0_IN					   : in   std_logic_vector(2 downto 0);
	TXPREEMPHASIS1_IN					   : in   std_logic_vector(2 downto 0);
	-------------------- Transmit Ports - TX Polarity Control ------------------
	TXPOLARITY0_IN						  : in   std_logic;
	TXPOLARITY1_IN						  : in   std_logic);

  END COMPONENT;

  ----------------------------------------------------------------------------

  
  ----------------------------------------------------------------------------

  COMPONENT sig_sync
  PORT(
	sig_async			   : IN	STD_LOGIC;		  -- signal in original clock domain
	clk					 : IN	STD_LOGIC;		  -- target clock domain
	sig_clk				 : OUT   STD_LOGIC);		 -- signal synchronized to target clock domain
  END COMPONENT;

  ----------------------------------------------------------------------------

  CONSTANT TILE_SIM_MODE				: string	:= "FAST";  -- Set to Fast Functional Simulation Model	
  CONSTANT TILE_SIM_GTPRESET_SPEEDUP	: integer   := 0;	   -- Set to 1 to speed up sim reset
  CONSTANT TILE_SIM_PLL_PERDIV2		 : bit_vector:= x"190";  -- Set to the VCO Unit Interval time

  CONSTANT C_Comma					  : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"BC";	-- K_28_5; /C/ Comma

  ----------------------------------------------------------------------------
  -- clock signals

  SIGNAL clk_REF_GTP_SFP_in			 : STD_LOGIC;			-- output of IBUFDS, input to GTP

  SIGNAL REFCLKOUT_SFP				  : STD_LOGIC;			-- output from GTP (reference clock)

  SIGNAL clk_Rx_SFP_GTP_out			 : STD_LOGIC;
  SIGNAL clk_Rx_SMA_GTP_out			 : STD_LOGIC;
  
  SIGNAL clk_Tx_SFP					 : STD_LOGIC;
  SIGNAL clk_Rx_SFP					 : STD_LOGIC;

  SIGNAL clk_Tx_SMA					 : STD_LOGIC;
  SIGNAL clk_Rx_SMA					 : STD_LOGIC;

  ----------------------------------------------------------------------------
  -- GTP control signals

  SIGNAL PLL_locked_SFP_SMA_i		   : STD_LOGIC;			-- async
  SIGNAL rst_done_SFP				   : STD_LOGIC;			-- async
  SIGNAL rst_done_SMA				   : STD_LOGIC;			-- async

--SIGNAL rst_GTP_SFP_SMA				: STD_LOGIC := '0';	 -- async

  SIGNAL Phase_Align_Tx_SFP_SMA		 : STD_LOGIC := '0';	 -- async
  SIGNAL Phase_Set_Tx_SFP_SMA		   : STD_LOGIC := '0';	 -- async
  SIGNAL rst_Rx_CDR_SFP_i				 : STD_LOGIC := '0';	 -- RXUSRCLK2 = clk_Rx_SFP
  SIGNAL rst_Rx_CDR_SFP_self			: STD_LOGIC := '0';
--SIGNAL rst_Rx_CDR_SMA				 : STD_LOGIC := '0';	 -- RXUSRCLK2 = clk_Rx_SMA
  SIGNAL Set_Phase_Rx_SFP			   : STD_LOGIC := '0';	 -- RXUSRCLK2 = clk_Rx_SFP
  SIGNAL Set_Phase_Rx_SMA			   : STD_LOGIC := '0';	 -- RXUSRCLK2 = clk_Rx_SMA

  SIGNAL rst_Tx_SFP					 : STD_LOGIC := '0';	 -- async
  SIGNAL rst_Rx_SFP					 : STD_LOGIC := '0';	 -- async
  SIGNAL rst_Rx_SMA					 : STD_LOGIC := '0';	 -- async
  SIGNAL rst_Tx_SMA					 : STD_LOGIC := '0';	 -- async

  ----------------------------------------------------------------------------

  TYPE t_st_Tx_phase_Align IS (st_init, st_wait_CLK_stab, st_Phase_Align, st_Phase_Set, st_done);
  SIGNAL state_Tx_align				 : t_st_Tx_Phase_Align := st_init;
  SIGNAL cnt_Tx_SFP_align			   : UNSIGNED(19 DOWNTO 0) := (OTHERS => '0');
  CONSTANT cnt_Tx_SFP_align_ONES		: UNSIGNED(19 DOWNTO 0) := (OTHERS => '1');

  TYPE t_st_Rx_phase_Align IS (st_init, st_wait_CDR_lock, st_PMA_set_phase, st_PMA_set_phase_2, st_done);
  SIGNAL state_Rx_align				 : t_st_Rx_Phase_Align := st_init;
  SIGNAL cnt_Rx_SFP_align			   : UNSIGNED(24 DOWNTO 0) := (OTHERS => '0'); --!!! was 19
  CONSTANT cnt_Rx_SFP_align_ONES		: UNSIGNED(24 DOWNTO 0) := (OTHERS => '1'); --!!! was 19

------------------------------------------------------------------------------
BEGIN					  
------------------------------------------------------------------------------

  ----------------------------------------------------------------------------
  -- GTP reference clock input and output
  ----------------------------------------------------------------------------

  -- GTP reference clock inputs
  GTP_SFP_refclk_ibufds_i : IBUFDS
  PORT MAP(
	O	   => clk_REF_GTP_SFP_in,
	I	   => REFCLK_SFP_P_IN,
	IB	  => REFCLK_SFP_N_IN);

  -- direct oscillator clock for FPGA fabric
--clk_REF_out <= REFCLKOUT;
  clk_REF_out <= clk_Tx_SFP;		-- output of a BUFG

  ----------------------------------------------------------------------------
  -- SFP clocking
  ----------------------------------------------------------------------------

  -- SFP Tx clock for GTP
  clk_Tx_SFP_BUG_i : BUFG
  PORT MAP(
	I	   => REFCLKOUT_SFP,
	O	   => clk_Tx_SFP);

  -- SFP Tx clock for FPGA fabric
  clk_Tx_SFP_out <= clk_Tx_SFP;

  -- SFP Rx clock for GTP
  clk_Rx_SFP_BUFG_i : BUFG
  PORT MAP(
	I	   => clk_Rx_SFP_GTP_out,
	O	   => clk_Rx_SFP);

  -- SFP Rx clock for FPGA fabric
  clk_Rx_SFP_out <= clk_Rx_SFP;

  ----------------------------------------------------------------------------
  -- SMA clocking
  ----------------------------------------------------------------------------

  -- SMA Tx clock for GTP
--  clk_Tx_SMA_BUG_i : BUFG
--  PORT MAP(
--	I	   => REFCLKOUT,
--	O	   => clk_Tx_SMA);

  clk_Tx_SMA <= clk_Tx_SFP;

  -- SFP Tx clock for FPGA fabric
--clk_Tx_SMA_out <= clk_Tx_SMA;

  -- SMA Rx clock for GTP
  clk_Rx_SMA_BUFG_i : BUFG
  PORT MAP(
	I	   => clk_Rx_SMA_GTP_out,
	O	   => clk_Rx_SMA);

  -- SFP Rx clock for FPGA fabric
--clk_Rx_SMA_out <= clk_Rx_SMA;

  ----------------------------------------------------------------------------
  -- reset synchronization
  ----------------------------------------------------------------------------

--  sig_sync_i : sig_sync PORT MAP(GTP_reset_in, clk, GTP_rst)


  ----------------------------------------------------------------------------
  -- TX Phase-Alignment
  ----------------------------------------------------------------------------
  --	  1. Wait for all clocks to stabilize, then drive TXENPMAPHASEALIGN High. Keep
  --	  TXENPMAPHASEALIGN High unless the phase-alignment procedure must be
  --	  repeated. Driving TXENPMAPHASEALIGN Low causes phase alignment to be lost.
  --	  
  --	  2. Wait 512 TXUSRCLK2 clock cycles, and then drive TXPMASETPHASE High.
  --	  
  --	  3. Wait for 8192 TXUSRCLK2 clock cycles and then drive TXPMASETPHASE Low.
  --	  The phase of the PMACLK is now aligned with TXUSRCLK.
  --
  --
  -- The phase-alignment procedure must be redone if any of the following conditions occur:
  --	  - GTPRESET is asserted
  --	  - PLLPOWERDWNB is deasserted
  --	  - The clocking source changed
  ----------------------------------------------------------------------------

  -- Tx phase alignment
  PROCESS(clk_Tx_SFP) BEGIN
	IF rising_edge(clk_Tx_SFP) THEN

	  -- default assignment
	  Phase_Align_Tx_SFP_SMA <= '1';
	  Phase_Set_Tx_SFP_SMA   <= '0';

	  IF PLL_locked_SFP_SMA_i = '0' OR rst_done_SFP = '0' THEN
		state_Tx_align <= st_init;
		Phase_Align_Tx_SFP_SMA <= '0';
	  ELSE
		
		CASE state_Tx_align IS
		  WHEN st_init			  =>  cnt_Tx_SFP_align <= (OTHERS => '0');
										state_Tx_align <= st_wait_CLK_stab;
										Phase_Align_Tx_SFP_SMA <= '0';

		  WHEN st_wait_CLK_stab	 =>  Phase_Align_Tx_SFP_SMA <= '0';
										cnt_Tx_SFP_align <= cnt_Tx_SFP_align + 1;
										IF cnt_Tx_SFP_align = cnt_Tx_SFP_align_ONES THEN
										  state_Tx_align <= st_Phase_Align;
										  cnt_Tx_SFP_align <= (OTHERS => '0');
										END IF;

		  WHEN st_Phase_Align	   =>  Phase_Align_Tx_SFP_SMA <= '0';
										cnt_Tx_SFP_align <= cnt_Tx_SFP_align + 1;
										IF cnt_Tx_SFP_align = TO_UNSIGNED(8191,cnt_Tx_SFP_align'length) THEN
										  state_Tx_align <= st_Phase_Set;
										  cnt_Tx_SFP_align <= (OTHERS => '0');
										END IF;

		  WHEN st_Phase_Set		 =>  cnt_Tx_SFP_align <= cnt_Tx_SFP_align + 1;
										Phase_Set_Tx_SFP_SMA <= '1';
										IF cnt_Tx_SFP_align = TO_UNSIGNED(511,cnt_Tx_SFP_align'length) THEN
										  state_Tx_align <= st_done;
										END IF;

		  WHEN st_done			  =>  NULL;






		END CASE;
	  END IF;
	END IF;
  END PROCESS;


  PLL_locked_SFP_SMA <= PLL_locked_SFP_SMA_i;


-- OUTPUTS
--	  rst_GTP_SFP_SMA			 -- asycn
--
--	  Phase_Align_Tx_SFP_SMA	  -- asycn
--	  Phase_Set_Tx_SFP_SMA		-- async
--
--	  Set_Phase_Rx_SFP			-- RXUSRCLK2 = clk_Rx_SFP
--	  rst_Rx_CDR_SFP			  -- RXUSRCLK2 = clk_Rx_SFP


-- INPUTS
--	  PLL_locked_SFP_SMA_i		-- async
--	  rst_done_SFP				-- async
--	  rst_done_SMA				-- async


  ----------------------------------------------------------------------------
  -- SFP Rx phase alignment
  ----------------------------------------------------------------------------
  --	  1. Reset the RX datapath using GTPRESET or one of the CDR resets.
  --	  2. Wait for the shared PMA PLL and any DCM or PLL used for RXUSRCLK2 to lock.
  --	  3. Wait for the CDR to lock and provide a stable RXRECCLK.
  --	  4. Drive RXPMASETPHASE High for 32 RXUSRCLK2 cycles and then deassert it.
  ----------------------------------------------------------------------------
  PROCESS(clk_Rx_SFP) BEGIN
	IF rising_edge(clk_Rx_SFP) THEN

	  -- default assignment
	  Set_Phase_Rx_SFP <= '0';
	  rst_Rx_CDR_SFP_self <= '0';

	  -- wait for PLL lock and reset done, reset by PicoBlaze
	  IF PLL_locked_SFP_SMA_i = '0' OR rst_done_SFP = '0' OR rst_Rx_CDR_SFP_i = '1' THEN
	   cnt_Rx_SFP_align <= (OTHERS => '0');
		state_Rx_align <= st_init;
	  ELSE

		CASE state_Rx_align IS
		  WHEN st_init			  =>  
						cnt_Rx_SFP_align <= cnt_Rx_SFP_align + 1;
						IF cnt_Rx_SFP_align = TO_UNSIGNED(1024,cnt_Rx_SFP_align'length) THEN
						  state_Rx_align <= st_PMA_set_phase;
						  cnt_Rx_SFP_align <= (OTHERS => '0');
						END IF;
		  WHEN st_PMA_set_phase	 =>  
						Set_Phase_Rx_SFP <= '1';
						cnt_Rx_SFP_align <= cnt_Rx_SFP_align + 1;
						IF cnt_Rx_SFP_align = TO_UNSIGNED(31,cnt_Rx_SFP_align'length) THEN
						  state_Rx_align <= st_wait_CDR_lock;
						  cnt_Rx_SFP_align <= (OTHERS => '0');
						END IF;
						
		  WHEN st_wait_CDR_lock	 =>  
						IF CDR_aligned = '1' THEN
						  state_Rx_align <= st_PMA_set_phase_2; 
						ELSE
						  state_Rx_align <= st_PMA_set_phase; 
						END IF;
						cnt_Rx_SFP_align <= (OTHERS => '0');
						
		  WHEN st_PMA_set_phase_2	 =>  
						Set_Phase_Rx_SFP <= '1';
						cnt_Rx_SFP_align <= cnt_Rx_SFP_align + 1;
						IF cnt_Rx_SFP_align = TO_UNSIGNED(31,cnt_Rx_SFP_align'length) THEN
						  state_Rx_align <= st_done;
						END IF;

		  WHEN st_done	  =>
					  IF CDR_aligned = '0' THEN
						 cnt_Rx_SFP_align <= (OTHERS => '0');
						 state_Rx_align <= st_init;
						 rst_Rx_CDR_SFP_self <= '1';
					  END IF;

		END CASE;

	  END IF;
	END IF;
  END PROCESS;

rst_Rx_CDR_SFP_i <= rst_Rx_CDR_SFP OR rst_Rx_CDR_SFP_self;


  -- SMA Rx phase alignment
  -- INPUTS:
  --	PLL_locked_SFP_SMA_i		-- async
  --	rst_done_SMA				-- async
  --	rst_Rx_CDR_SMA			  -- RXUSRCLK2 = clk_Rx_SMA
  Set_Phase_Rx_SMA <= '0';		  -- RXUSRCLK2 = clk_Rx_SMA

  ----------------------------------------------------------------------------

  rst_Tx_SFP	<= '0';			 -- asycn
  rst_Tx_SMA	<= '0';			 -- asycn
  rst_Rx_SFP	<= '0';			 -- async
  rst_Rx_SMA	<= '0';			 -- async

  ----------------------------------------------------------------------------

  GTP_SFP_i : GTP_SFP_tile
  GENERIC MAP(
	TILE_SIM_MODE				   =>	  TILE_SIM_MODE,   
	TILE_SIM_GTPRESET_SPEEDUP	   =>	  TILE_SIM_GTPRESET_SPEEDUP,
	TILE_SIM_PLL_PERDIV2			=>	  TILE_SIM_PLL_PERDIV2,
	TILE_CHAN_BOND_MODE_0		   =>	 "OFF",
	TILE_CHAN_BOND_LEVEL_0		  =>	 0,
	TILE_CHAN_BOND_MODE_1		   =>	 "OFF",
	TILE_CHAN_BOND_LEVEL_1		  =>	 0)

  PORT MAP(
	--_____________________________________________________________________
	--_____________________________________________________________________
	--TILE0  (X0Y5)

	------------------------ Loopback and Powerdown Ports ----------------------
	LOOPBACK0_IN					=>	  "000",
	LOOPBACK1_IN					=>	  "000",
	------------------- Receive Ports - RX Data Path interface -----------------
	RXDATA0_OUT					 =>	  Rx_SFP_Data_10b,
	RXDATA1_OUT					 =>	  OPEN,
	RXRECCLK0_OUT				   =>	  clk_Rx_SFP_GTP_out,
	RXRECCLK1_OUT				   =>	  clk_Rx_SMA_GTP_out,
	RXRESET0_IN					 =>	  rst_Rx_SFP,
	RXRESET1_IN					 =>	  rst_Rx_SMA,
	RXUSRCLK0_IN					=>	  clk_Rx_SFP,
	RXUSRCLK1_IN					=>	  clk_Rx_SMA,
	RXUSRCLK20_IN				   =>	  clk_Rx_SFP,
	RXUSRCLK21_IN				   =>	  clk_Rx_SMA,
	------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
	RXCDRRESET0					 =>	  rst_Rx_CDR_SFP_i,
	RXCDRRESET1					 =>	  rst_Rx_CDR_SMA,
	RXEQMIX0						=>	  SFP_Rx_EqMix,
	RXEQMIX1						=>	  "00",
	RXEQPOLE0					   =>	  SFP_Rx_EqPole,
	RXEQPOLE1					   =>	  "1000",
	RXN0_IN						 =>	  RXN_IN(0),
	RXN1_IN						 =>	  RXN_IN(1),
	RXP0_IN						 =>	  RXP_IN(0),
	RXP1_IN						 =>	  RXP_IN(1),
	-------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
	RXPMASETPHASE0_IN			   =>	  Set_Phase_Rx_SFP,
	RXPMASETPHASE1_IN			   =>	  Set_Phase_Rx_SMA,
	--------------------- Shared Ports - Tile and PLL Ports --------------------
	CLKIN_IN						=>	  clk_REF_GTP_SFP_in,
	GTPRESET_IN					 =>	  rst_GTP_SFP_SMA,
	PLLLKDET_OUT					=>	  PLL_locked_SFP_SMA_i,
	REFCLKOUT_OUT				   =>	  REFCLKOUT_SFP,
	RESETDONE0_OUT				  =>	  rst_done_SFP,
	RESETDONE1_OUT				  =>	  rst_done_SMA,
	TXENPMAPHASEALIGN_IN			=>	  Phase_Align_Tx_SFP_SMA,
	TXPMASETPHASE_IN				=>	  Phase_Set_Tx_SFP_SMA,
	------------------ Transmit Ports - TX Data Path interface -----------------
  --TXDATA0_IN					  =>	  "0000000000",
	TXDATA0_IN					  =>	  Tx_SFP_Data_10b,
	TXDATA1_IN					  =>	  "0000000000",
	TXRESET0_IN					 =>	  rst_Tx_SFP,
	TXRESET1_IN					 =>	  rst_Tx_SMA,
	TXUSRCLK0_IN					=>	  clk_Tx_SFP,
	TXUSRCLK1_IN					=>	  clk_Tx_SMA,
	TXUSRCLK20_IN				   =>	  clk_Tx_SFP,
	TXUSRCLK21_IN				   =>	  clk_Tx_SMA,
	--------------- Transmit Ports - TX Driver and OOB signalling --------------
	TXDIFFCTRL0_IN				  =>	  SFP_Tx_Swing,
	TXDIFFCTRL1_IN				  =>	  "111",			  --   0 mV swing
	TXINHIBIT0_IN				   =>	  SFP_Tx_Inhibit,
	TXINHIBIT1_IN				   =>	  '1',
	TXN0_OUT						=>	  TXN_OUT(0),
	TXN1_OUT						=>	  TXN_OUT(1),
	TXP0_OUT						=>	  TXP_OUT(0),
	TXP1_OUT						=>	  TXP_OUT(1),
	TXPREEMPHASIS0_IN			   =>	  SFP_Tx_Preemphasis,
	TXPREEMPHASIS1_IN			   =>	  "000",			  -- no preemphasis
	-------------------- Transmit Ports - TX Polarity Control ------------------
	TXPOLARITY0_IN				  =>	  SFP_Tx_Polarity,
	TXPOLARITY1_IN				  =>	  '0');

  ----------------------------------------------------------------------------

------------------------------------------------------------------------------
END Structural;
------------------------------------------------------------------------------
