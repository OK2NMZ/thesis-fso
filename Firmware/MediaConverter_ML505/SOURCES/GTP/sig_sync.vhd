----------------------------------------------------------------------------------
-- simple synchronization module (forward status over clock domain boundary)
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.vcomponents.ALL;
----------------------------------------------------------------------------------
ENTITY sig_sync IS
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_clk                 : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
END sig_sync;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF sig_sync IS
----------------------------------------------------------------------------------

  SIGNAL reset_sync_reg     : STD_LOGIC;
  SIGNAL reset_sync_reg2    : STD_LOGIC;

  ATTRIBUTE ASYNC_REG                           : STRING;
  ATTRIBUTE ASYNC_REG OF reset_sync_reg         : SIGNAL IS "TRUE";
  ATTRIBUTE ASYNC_REG OF reset_sync_reg2        : SIGNAL IS "TRUE";

  -- These attributes will stop XST translating the desired flip-flops into an
  -- SRL based shift register.
  ATTRIBUTE SHREG_EXTRACT                       : STRING;
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg     : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg2    : SIGNAL IS "NO";

----------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------------

  reset_sync1 : FDPE
  GENERIC MAP(
    INIT => '1')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => sig_async,
    D    => '0',
    Q    => reset_sync_reg);

  reset_sync2 : FDPE
  GENERIC MAP(
    INIT => '1')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => sig_async,
    D    => reset_sync_reg,
    Q    => reset_sync_reg2);

  sig_clk <= reset_sync_reg2;

----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------
