------------------------------------------------------------------------
-- Title      : Demo testbench
-- Project    : Virtex-5 Embedded Tri-Mode Ethernet MAC Wrapper
-- File       : demo_tb.vhd
-- Version    : 1.8
-------------------------------------------------------------------------------
--
-- (c) Copyright 2004-2010 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
--
------------------------------------------------------------------------
-- Description: This testbench will exercise the PHY ports of the EMAC
--              to demonstrate the functionality.
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity testbench is
end testbench;


architecture behavioral of testbench is


  ----------------------------------------------------------------------
  -- Component Declaration for TEMAC_SGMII_example_design
  --                           (the top level EMAC example deisgn)
  ----------------------------------------------------------------------
  component TEMAC_SGMII_example_design
    port(
        -- Client Receiver Interface - EMAC0
        EMAC0CLIENTRXDVLD               : out std_logic;
        EMAC0CLIENTRXFRAMEDROP          : out std_logic;
        EMAC0CLIENTRXSTATS              : out std_logic_vector(6 downto 0);
        EMAC0CLIENTRXSTATSVLD           : out std_logic;
        EMAC0CLIENTRXSTATSBYTEVLD       : out std_logic;

        -- Client Transmitter Interface - EMAC0
        CLIENTEMAC0TXIFGDELAY           : in  std_logic_vector(7 downto 0);
        EMAC0CLIENTTXSTATS              : out std_logic;
        EMAC0CLIENTTXSTATSVLD           : out std_logic;
        EMAC0CLIENTTXSTATSBYTEVLD       : out std_logic;

        -- MAC Control Interface - EMAC0
        CLIENTEMAC0PAUSEREQ             : in  std_logic;
        CLIENTEMAC0PAUSEVAL             : in  std_logic_vector(15 downto 0);

        -- Clock Signal - EMAC0

        -- SGMII Interface - EMAC0
        TXP_0                           : out std_logic;
        TXN_0                           : out std_logic;
        RXP_0                           : in  std_logic;
        RXN_0                           : in  std_logic;
        PHYAD_0                         : in  std_logic_vector(4 downto 0); 

        -- unused transceiver
        TXN_1_UNUSED                    : out std_logic;
        TXP_1_UNUSED                    : out std_logic;
        RXN_1_UNUSED                    : in  std_logic;
        RXP_1_UNUSED                    : in  std_logic;

        EMAC0CLIENTSYNCACQSTATUS        : out std_logic;              
        EMAC0ANINTERRUPT                : out std_logic;

        -- MDIO Interface - EMAC0
        MDC_0                           : out std_logic;
        MDIO_0_I                        : in  std_logic;
        MDIO_0_O                        : out std_logic;
        MDIO_0_T                        : out std_logic;

        MGTCLK_P                        : in  std_logic;
        MGTCLK_N                        : in  std_logic;        
        -- Host Interface
        HOSTCLK                         : in  std_logic;
        HOSTOPCODE                      : in  std_logic_vector(1 downto 0);
        HOSTREQ                         : in  std_logic;
        HOSTMIIMSEL                     : in  std_logic;
        HOSTADDR                        : in  std_logic_vector(9 downto 0);
        HOSTWRDATA                      : in  std_logic_vector(31 downto 0);
        HOSTMIIMRDY                     : out std_logic;
        HOSTRDDATA                      : out std_logic_vector(31 downto 0);
        HOSTEMAC1SEL                    : in  std_logic;




        -- Asynchronous Reset
        RESET                           : in  std_logic
        );
  end component;


  component configuration_tb is
    port(
      reset                       : out std_logic;
      ------------------------------------------------------------------
      -- Host Interface
      ------------------------------------------------------------------
      host_clk                    : out std_logic;
      host_opcode                 : out std_logic_vector(1 downto 0);
      host_req                    : out std_logic;
      host_miim_sel               : out std_logic;
      host_addr                   : out std_logic_vector(9 downto 0);
      host_wr_data                : out std_logic_vector(31 downto 0);
      host_miim_rdy               : in  std_logic;
      host_rd_data                : in  std_logic_vector(31 downto 0);
      host_emac1_sel              : out std_logic;

      ------------------------------------------------------------------
      -- Test Bench Semaphores
      ------------------------------------------------------------------
      sync_acq_status_0           : in  std_logic;

      emac0_configuration_busy    : out boolean;
      emac0_monitor_finished_1g   : in  boolean;
      emac0_monitor_finished_100m : in  boolean;
      emac0_monitor_finished_10m  : in  boolean;

      emac1_configuration_busy    : out boolean;
      emac1_monitor_finished_1g   : in  boolean;
      emac1_monitor_finished_100m : in  boolean;
      emac1_monitor_finished_10m  : in  boolean

      );
  end component;


  ----------------------------------------------------------------------
  -- Component Declaration for the EMAC0 PHY stimulus and monitor
  ----------------------------------------------------------------------

  component emac0_phy_tb is
    port(
      clk125m                 : in std_logic;

      ------------------------------------------------------------------
      -- GMII Interface
      ------------------------------------------------------------------
      txp                     : in  std_logic;
      txn                     : in  std_logic;
      rxp                     : out std_logic;
      rxn                     : out std_logic; 

      ------------------------------------------------------------------
      -- Test Bench Semaphores
      ------------------------------------------------------------------
      configuration_busy      : in  boolean;
      monitor_finished_1g     : out boolean;
      monitor_finished_100m   : out boolean;
      monitor_finished_10m    : out boolean
      );
  end component;





  ----------------------------------------------------------------------
  -- testbench signals
  ----------------------------------------------------------------------
    signal reset                : std_logic                     := '1';

    -- EMAC0
    signal tx_client_clk_0      : std_logic;
    signal tx_ifg_delay_0       : std_logic_vector(7 downto 0)  := (others => '0'); -- IFG stretching not used in demo.
    signal rx_client_clk_0      : std_logic;
    signal pause_val_0          : std_logic_vector(15 downto 0) := (others => '0');
    signal pause_req_0          : std_logic                     := '0';

    -- SGMII Signals
    signal txp_0                : std_logic;
    signal txn_0                : std_logic;
    signal rxp_0                : std_logic;
    signal rxn_0                : std_logic;
    signal sync_acq_status_0    : std_logic;
    signal phyad_0              : std_logic_vector(4 downto 0);

 
    -- MDIO Signals
    signal mdc_0                : std_logic;
    signal mdc_in_0             : std_logic                     := '1';
    signal mdio_in_0            : std_logic;
    signal mdio_out_0           : std_logic;
    signal mdio_tri_0           : std_logic;

    -- Host Signals
    signal host_opcode          : std_logic_vector(1 downto 0)  := (others => '1');
    signal host_addr            : std_logic_vector(9 downto 0)  := (others => '1');
    signal host_wr_data         : std_logic_vector(31 downto 0) := (others => '0');
    signal host_rd_data         : std_logic_vector(31 downto 0);
    signal host_miim_sel        : std_logic                     := '0';
    signal host_req             : std_logic                     := '0';
    signal host_miim_rdy        : std_logic;
    signal host_emac1_sel       : std_logic                     := '0';


    -- Clock signals
    signal host_clk             : std_logic                     := '0';
    signal gtx_clk              : std_logic;

    signal mgtclk_p             : std_logic := '0';   
    signal mgtclk_n             : std_logic := '1';

    ------------------------------------------------------------------
    -- Test Bench Semaphores
    ------------------------------------------------------------------
    signal emac0_configuration_busy    : boolean := false;
    signal emac0_monitor_finished_1g   : boolean := false;
    signal emac0_monitor_finished_100m : boolean := false;
    signal emac0_monitor_finished_10m  : boolean := false;

    signal emac1_configuration_busy    : boolean := false;
    signal emac1_monitor_finished_1g   : boolean := false;
    signal emac1_monitor_finished_100m : boolean := false;
    signal emac1_monitor_finished_10m  : boolean := false;


begin


  ----------------------------------------------------------------------
  -- Wire up Device Under Test
  ----------------------------------------------------------------------
  dut : TEMAC_SGMII_example_design
  port map (
    -- Client Receiver Interface - EMAC0
    EMAC0CLIENTRXDVLD               => open,
    EMAC0CLIENTRXFRAMEDROP          => open,
    EMAC0CLIENTRXSTATS              => open,
    EMAC0CLIENTRXSTATSVLD           => open,
    EMAC0CLIENTRXSTATSBYTEVLD       => open,

    -- Client Transmitter Interface - EMAC0
    CLIENTEMAC0TXIFGDELAY           => tx_ifg_delay_0,
    EMAC0CLIENTTXSTATS              => open,
    EMAC0CLIENTTXSTATSVLD           => open,
    EMAC0CLIENTTXSTATSBYTEVLD       => open,

    -- MAC Control Interface - EMAC0
    CLIENTEMAC0PAUSEREQ             => pause_req_0,
    CLIENTEMAC0PAUSEVAL             => pause_val_0,

    EMAC0CLIENTSYNCACQSTATUS        => sync_acq_status_0,
    EMAC0ANINTERRUPT                => open,

    -- SGMII Interface - EMAC0
    TXP_0                           => txp_0,
    TXN_0                           => txn_0,
    RXP_0                           => rxp_0,
    RXN_0                           => rxn_0,
    PHYAD_0                         => phyad_0, 

    -- unused transceiver
    TXN_1_UNUSED                    => open,
    TXP_1_UNUSED                    => open,
    RXN_1_UNUSED                    => '1',
    RXP_1_UNUSED                    => '0',
           
    -- MDIO Interface - EMAC0
    MDC_0                           => mdc_0,
    MDIO_0_I                        => mdio_in_0,
    MDIO_0_O                        => mdio_out_0,
    MDIO_0_T                        => mdio_tri_0,

    MGTCLK_P                        =>  mgtclk_p,   
    MGTCLK_N                        =>  mgtclk_n,
    
    -- Host Interface
    HOSTCLK                         => host_clk,
    HOSTOPCODE                      => host_opcode,
    HOSTREQ                         => host_req,
    HOSTMIIMSEL                     => host_miim_sel,
    HOSTADDR                        => host_addr,
    HOSTWRDATA                      => host_wr_data,
    HOSTMIIMRDY                     => host_miim_rdy,
    HOSTRDDATA                      => host_rd_data,
    HOSTEMAC1SEL                    => host_emac1_sel,



        
    -- Asynchronous Reset
    RESET                           => reset
    );


    ----------------------------------------------------------------------------
    -- Flow Control is unused in this demonstration
    ----------------------------------------------------------------------------
    pause_req_0 <= '0';
    pause_val_0 <= "0000000000000000";



    ----------------------------------------------------------------------------
    -- Simulate the MDIO_IN port floating high
    ----------------------------------------------------------------------------
    mdio_in_0 <= 'H';

    -- Tie-off EMAC0 PHY address to a default value
    phyad_0 <= "00001";

    ----------------------------------------------------------------------------
    -- Clock drivers
    ----------------------------------------------------------------------------

    -- Drive GTX_CLK at 125 MHz
    p_gtx_clk : process 
    begin
        gtx_clk <= '0';
        wait for 10 ns;
        loop
            wait for 4 ns;
            gtx_clk <= '1';
            wait for 4 ns;
            gtx_clk <= '0';
        end loop;
    end process p_gtx_clk;



    -- Drive Gigabit Transceiver differential clock with 125MHz
    mgtclk_p <= gtx_clk;
    mgtclk_n <= not gtx_clk;

  ----------------------------------------------------------------------
  -- Instantiate the EMAC0 PHY stimulus and monitor
  ----------------------------------------------------------------------

  phy0_test: emac0_phy_tb
    port map (
      clk125m                 => gtx_clk,

      ------------------------------------------------------------------
      -- GMII Interface
      ------------------------------------------------------------------
      txp                     => txp_0,
      txn                     => txn_0,
      rxp                     => rxp_0,
      rxn                     => rxn_0, 

      ------------------------------------------------------------------
      -- Test Bench Semaphores
      ------------------------------------------------------------------
      configuration_busy      => emac0_configuration_busy,
      monitor_finished_1g     => emac0_monitor_finished_1g,
      monitor_finished_100m   => emac0_monitor_finished_100m,
      monitor_finished_10m    => emac0_monitor_finished_10m
      );






  ----------------------------------------------------------------------
  -- Instantiate the Host Configuration Stimulus
  ----------------------------------------------------------------------

  config_test: configuration_tb
    port map (
      reset                       => reset,
      ------------------------------------------------------------------
      -- Host Interface
      ------------------------------------------------------------------
      host_clk                    => host_clk,
      host_opcode                 => host_opcode,
      host_req                    => host_req,
      host_miim_sel               => host_miim_sel,
      host_addr                   => host_addr,
      host_wr_data                => host_wr_data,
      host_miim_rdy               => host_miim_rdy,
      host_rd_data                => host_rd_data,
      host_emac1_sel              => host_emac1_sel,

      ------------------------------------------------------------------
      -- Test Bench Semaphores
      ------------------------------------------------------------------
      sync_acq_status_0           => sync_acq_status_0,

      emac0_configuration_busy    => emac0_configuration_busy,
      emac0_monitor_finished_1g   => emac0_monitor_finished_1g,
      emac0_monitor_finished_100m => emac0_monitor_finished_100m,
      emac0_monitor_finished_10m  => emac0_monitor_finished_10m,

      emac1_configuration_busy    => emac1_configuration_busy,
      emac1_monitor_finished_1g   => emac1_monitor_finished_1g,
      emac1_monitor_finished_100m => emac1_monitor_finished_100m,
      emac1_monitor_finished_10m  => emac1_monitor_finished_10m 

      );


  

end behavioral;

