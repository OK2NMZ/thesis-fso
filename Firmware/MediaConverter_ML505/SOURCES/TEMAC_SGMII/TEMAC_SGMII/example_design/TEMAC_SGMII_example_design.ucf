# The xc5vlx50tff1136-1 part is chosen for this example design.
# This value should be modified to match your device.
CONFIG PART = xc5vlx50tff1136-1;
 
##################################
# BLOCK Level constraints
##################################

# EMAC0 Clocking
# 125MHz clock input from BUFG
NET "CLK125" TNM_NET          = "clk_gtp";
TIMEGRP  "TEMAC_SGMII_gtp_clk"            = "clk_gtp";
TIMESPEC "TS_TEMAC_SGMII_gtp_clk"         = PERIOD "TEMAC_SGMII_gtp_clk" 8 ns HIGH 50 %;
# EMAC0 Tri-speed clock input from BUFG
NET "CLIENT_CLK_0" TNM_NET    = "clk_client0";
TIMEGRP  "TEMAC_SGMII_gtp_clk_client0"    = "clk_client0";
TIMESPEC "TS_TEMAC_SGMII_gtp_clk_client0" = PERIOD "TEMAC_SGMII_gtp_clk_client0" 8 ns HIGH 50 %;







##################################
# LocalLink Level constraints
##################################


# EMAC0 LocalLink client FIFO constraints.

INST "*client_side_FIFO_emac0?tx_fifo_i?rd_tran_frame_tog"    TNM = "tx_fifo_rd_to_wr_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?rd_retran_frame_tog"  TNM = "tx_fifo_rd_to_wr_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?rd_col_window_pipe_1" TNM = "tx_fifo_rd_to_wr_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?rd_addr_txfer*"       TNM = "tx_fifo_rd_to_wr_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?rd_txfer_tog"         TNM = "tx_fifo_rd_to_wr_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?wr_frame_in_fifo"     TNM = "tx_fifo_wr_to_rd_0";

TIMESPEC "TS_tx_fifo_rd_to_wr_0" = FROM "tx_fifo_rd_to_wr_0" TO "TEMAC_SGMII_gtp_clk_client0" 8 ns DATAPATHONLY;
TIMESPEC "TS_tx_fifo_wr_to_rd_0" = FROM "tx_fifo_wr_to_rd_0" TO "TEMAC_SGMII_gtp_clk_client0" 8 ns DATAPATHONLY;


# Reduce clock period to allow 3 ns for metastability settling time
INST "*client_side_FIFO_emac0?tx_fifo_i?wr_tran_frame_tog"    TNM = "tx_metastable_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?wr_rd_addr*"          TNM = "tx_metastable_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?wr_txfer_tog"         TNM = "tx_metastable_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?frame_in_fifo"        TNM = "tx_metastable_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?wr_retran_frame_tog*" TNM = "tx_metastable_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?wr_col_window_pipe_0" TNM = "tx_metastable_0";

TIMESPEC "ts_tx_meta_protect_0" = FROM "tx_metastable_0" 5 ns DATAPATHONLY;

INST "*client_side_FIFO_emac0?tx_fifo_i?rd_addr_txfer*"       TNM = "tx_addr_rd_0";
INST "*client_side_FIFO_emac0?tx_fifo_i?wr_rd_addr*"          TNM = "tx_addr_wr_0";
TIMESPEC "TS_tx_fifo_addr_0" = FROM "tx_addr_rd_0" TO "tx_addr_wr_0" 10ns;

## RX Client FIFO
# Group the clock crossing signals into timing groups
INST "*client_side_FIFO_emac0?rx_fifo_i?wr_store_frame_tog"   TNM = "rx_fifo_wr_to_rd_0";
INST "*client_side_FIFO_emac0?rx_fifo_i?rd_addr_gray*"        TNM = "rx_fifo_rd_to_wr_0";

TIMESPEC "TS_rx_fifo_wr_to_rd_0" = FROM "rx_fifo_wr_to_rd_0" TO "TEMAC_SGMII_gtp_clk_client0" 8 ns DATAPATHONLY;
TIMESPEC "TS_rx_fifo_rd_to_wr_0" = FROM "rx_fifo_rd_to_wr_0" TO "TEMAC_SGMII_gtp_clk_client0" 8 ns DATAPATHONLY;


# Reduce clock period to allow for metastability settling time
INST "*client_side_FIFO_emac0?rx_fifo_i?wr_rd_addr_gray_sync*" TNM = "rx_metastable_0";
INST "*client_side_FIFO_emac0?rx_fifo_i?rd_store_frame_tog"    TNM = "rx_metastable_0";

TIMESPEC "ts_rx_meta_protect_0" = FROM "rx_metastable_0" 5 ns;


# Area constaint to place example design near embedded TEMAC. Constraint is 
# optional and not necessary for a successful implementation of the design.
INST v5_emac_ll/* AREA_GROUP = AG_v5_emac ;
AREA_GROUP "AG_v5_emac" RANGE = CLOCKREGION_X1Y2,CLOCKREGION_X1Y3 ;
 
##################################
# EXAMPLE DESIGN Level constraints
##################################

NET "*host_clk_i" TNM_NET = "host_clock";
TIMEGRP "clk_host"                   = "host_clock";
TIMESPEC "TS_clk_host"               = PERIOD "clk_host" 10 ns HIGH 50 %;   

# Place the transceiver components. Please alter to your chosen transceiver.
INST "*GTP_DUAL_1000X_inst?GTP_1000X?tile0_rocketio_wrapper_i?gtp_dual_i" LOC = "GTP_DUAL_X0Y2";
INST "MGTCLK_N" LOC = "Y3";
INST "MGTCLK_P" LOC = "Y4";




