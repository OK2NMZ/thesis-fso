----------------------------------------------------------------------------------
-- read data from MII
-- realign 4b => 8b, strip preamble
-- insert into FIFO, mark end of a frame using special character (8b => 9b words) = preparation for 8b/10b
-- each frame separated by at least 4 comma characters (K28.5)
-- FIFO write in MII_RxCLK_Rx clock domain (25 MHz), 8(+1) bits every two clock cycles
-- EOF: valid frame K29.7
-- invalid frame K30.7
--
-- FIFO read in TxC clock domain (125 MHz; derived using DCM or PLL from MII_RxCLK)
-- read FIFO, whole frame at once, read one byte every 10 clock cycles
-- the 9th bit = CHAR_IS_K
-- no data in FIFO => forward comma (K28.5) to 8b10b encoder
-- encode 8b/10b
-- transmit; LSB first
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
LIBRARY unisim;
USE unisim.vcomponents.ALL;
USE WORK.modules_pkg.ALL;
USE WORK.parameters_pkg.ALL;
----------------------------------------------------------------------------------
ENTITY SM_Tx_no_DCM IS
	PORT (
		-- Interface Tx
		TxC : IN STD_LOGIC := '0';
		TxD : OUT STD_LOGIC_VECTOR (9 DOWNTO 0) := (OTHERS => '0');
		-- PHY MII Rx interface
		MII_RxCLK : IN STD_LOGIC := '0';
		MII_RxD : IN STD_LOGIC_VECTOR (7 DOWNTO 0) := (OTHERS => '0');
		MII_RxDV : IN STD_LOGIC := '0';
		MII_RxER : IN STD_LOGIC := '0';
		MII_ACK : OUT STD_LOGIC := '0';
		LINK_IS_DOWN : IN STD_LOGIC := '0';
		CHANNEL_ENCODER_USED : IN STD_LOGIC := '0';
		TX_FRAMES_SENT : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		TX_FRAMES_TRUNCATED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		TX_BYTES_SENT : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		TX_BYTES_DROPPED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		-- DDR FIFO interface
		wr_clk : OUT STD_LOGIC := '0';
		rd_clk : OUT STD_LOGIC := '0';
		din : OUT STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		wr_en : OUT STD_LOGIC := '0';
		rd_en : OUT STD_LOGIC := '0';
		dout : IN STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		full : IN STD_LOGIC := '0';
		empty : IN STD_LOGIC := '0';
		empty_prog : IN STD_LOGIC := '0';
		-- FPGA system interface
		clk : IN STD_LOGIC := '0';
		rst : IN STD_LOGIC := '0');
	END SM_Tx_no_DCM;
	----------------------------------------------------------------------------------
	ARCHITECTURE Behavioral OF SM_Tx_no_DCM IS
		--------------------------------------------------------------------------------
		-- clocking and reset
		SIGNAL TxCC_OK : STD_LOGIC;
		SIGNAL rst_i : STD_LOGIC;
		SIGNAL rst_TxC : STD_LOGIC;
		SIGNAL rst_MII : STD_LOGIC := '1';
		-- FIFO input
		TYPE t_st_MII_Rx IS (st_idle, st_wait, st_data, st_wait_fr_end, st_wait_fr_end_err);
		SIGNAL st_MII_Rx : t_st_MII_Rx := st_idle;
		SIGNAL FIFO_Rx_MII_wr_en : STD_LOGIC := '0';
		SIGNAL FIFO_Rx_MII_wr_ack : STD_LOGIC := '0';
		SIGNAL FIFO_Rx_MII_din : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		SIGNAL FIFO_Rx_MII_full : STD_LOGIC;
		SIGNAL FIFO_Rx_MII_prog_full : STD_LOGIC;
		SIGNAL FIFO_Rx_MII_prog_empty : STD_LOGIC;
		-- FIFO output
		SIGNAL FIFO_Rx_MII_empty : STD_LOGIC;
		SIGNAL FIFO_Rx_MII_rd_en : STD_LOGIC := '0';
		SIGNAL FIFO_Rx_MII_dout : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		-- 8b10b encoder
		SIGNAL encode_Tx_Din : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"00";
		SIGNAL encode_Tx_DVin : STD_LOGIC := '0';
		SIGNAL encode_TX_CharIsK : STD_LOGIC := '0';
		SIGNAL encode_Tx_RunDisp : STD_LOGIC;
		SIGNAL encode_Tx_Dout : STD_LOGIC_VECTOR(9 DOWNTO 0);
		-- RS encoder
		SIGNAL RS_DIN : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
		SIGNAL RS_DIN_TEMP : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
		SIGNAL RS_DOUT : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
		SIGNAL RS_START : STD_LOGIC := '0';
		SIGNAL RS_DV_OUT : STD_LOGIC := '0';
		SIGNAL RS_DV_TIMEOUT : STD_LOGIC_VECTOR(1 DOWNTO 0) := "10";
		SIGNAL RS_READY_FOR_START : STD_LOGIC := '0';
		SIGNAL RS_READY_FOR_START_i : STD_LOGIC := '0'; --when writing EOF and SOF, we need extra latency on the output
		SIGNAL RS_READY_FOR_NEXT_DATA : STD_LOGIC := '0';
		SIGNAL FIFO_RS_DIN : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
		SIGNAL FIFO_RS_DOUT : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
		SIGNAL FIFO_SIG_RS_DIN : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
		SIGNAL FIFO_SIG_RS_DOUT : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
		CONSTANT FIFO_SIG_LEN_ZERO : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
		SIGNAL SUM_SENT_BYTES : STD_LOGIC_VECTOR(15 DOWNTO 0) := (OTHERS => '0');
		CONSTANT ENCODED_MAX_LEN : STD_LOGIC_VECTOR(15 DOWNTO 0) := X"0640"; -- 1600 bytes
		SIGNAL FIFO_RS_wr_en : STD_LOGIC := '0';
		SIGNAL FIFO_RS_almost_full : STD_LOGIC := '0';
		SIGNAL FIFO_RS_prog_full : STD_LOGIC := '0';
		SIGNAL FIFO_RS_count : STD_LOGIC_VECTOR(12 DOWNTO 0) := (OTHERS => '0');
		SIGNAL FIFO_RS_rd_en : STD_LOGIC := '0';
		SIGNAL FIFO_RS_empty : STD_LOGIC := '0';
		SIGNAL FIFO_RS_rst : STD_LOGIC := '0';
		SIGNAL FIFO_RS_rst_i : STD_LOGIC := '0';
		SIGNAL FIFO_SIG_RS_wr_en : STD_LOGIC := '0';
		SIGNAL FIFO_SIG_RS_almost_empty : STD_LOGIC := '0';
		SIGNAL FIFO_SIG_RS_empty : STD_LOGIC := '0';
		SIGNAL FIFO_SIG_RS_valid : STD_LOGIC := '0';
		SIGNAL FIFO_SIG_RS_full : STD_LOGIC := '0';
		SIGNAL FIFO_SIG_RS_rd_en : STD_LOGIC := '0';
		SIGNAL FIFO_SIG_RS_count : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
		ATTRIBUTE keep : BOOLEAN;
		ATTRIBUTE keep OF FIFO_RS_empty : SIGNAL IS true;
		-- Output process
		SIGNAL PROC_TX_dout : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		SIGNAL PROC_TX_rd_en : STD_LOGIC := '0';
		SIGNAL PROC_TX_empty : STD_LOGIC := '0';
		SIGNAL PROC_TX_prog_empty : STD_LOGIC := '0';
		-- RS IN process
		SIGNAL RS_IN_dout : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		SIGNAL RS_IN_rd_en : STD_LOGIC := '0';
		SIGNAL RS_IN_empty : STD_LOGIC := '0';
		SIGNAL RS_IN_prog_empty : STD_LOGIC := '0';
		SIGNAL RS_CNTR_BYTE : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
		SIGNAL RS_CNTR_BYTE_WRITTEN : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
		SIGNAL RS_CNTR_BYTE_OUT : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
		SIGNAL RS_CNTR_BYTE_OUT_CONT : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
		CONSTANT RS_MAX_LEN : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"EE"; -- 238dec
		TYPE t_st_enc_len IS (wait_for_sof, loading, wait_for_eof);
		SIGNAL st_enc_len : t_st_enc_len := wait_for_sof;
		TYPE t_st_enc_in IS (check_can_read, nop_check_can_read,
									do_check_can_read, write_count_1_wfs, write_count_2_first,
									write_count_1, write_count_2, write_count_2_wfs,
									write_data, wait_for_sob, write_padding_and_sof,
									wait_for_sob_last, write_to_out_fifo, write_data_last);
		SIGNAL st_enc_in : t_st_enc_in := nop_check_can_read;
		SIGNAL st_enc_in_last : t_st_enc_in := nop_check_can_read;
		SIGNAL ENCODED_FRAME_STOP_CONDITION : Boolean := true;
		SIGNAL ENCODED_FRAME_START_CONDITION : Boolean := false;
		TYPE t_st_out_fifo IS (prefetch_sof, wait_for_rdy, write_payload);
		SIGNAL st_out_fifo : t_st_out_fifo := prefetch_sof;
		SIGNAL FIFO_OUT_RS_wr_en : STD_LOGIC := '0';
		SIGNAL FIFO_OUT_RS_wr_ack : STD_LOGIC := '0';
		SIGNAL FIFO_OUT_RS_din : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		SIGNAL FIFO_OUT_RS_full : STD_LOGIC := '0';
		SIGNAL FIFO_OUT_RS_prog_full : STD_LOGIC := '0';
		SIGNAL FIFO_OUT_RS_prog_empty : STD_LOGIC := '0';
		SIGNAL FIFO_OUT_RS_empty : STD_LOGIC := '0';
		SIGNAL FIFO_OUT_RS_rd_en : STD_LOGIC := '0';
		SIGNAL FIFO_OUT_RS_dout : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		-- Link State
		SIGNAL LINK_IS_DOWN_TxC : STD_LOGIC := '0';
		-- Channel Coder on/off
		SIGNAL CHANNEL_ENCODER_USED_TxC : STD_LOGIC := '0';
		-- Framer
		TYPE t_st_Tx IS (st_wait_for_not_empty, st_wait_for_sof,
								st_send_sof, st_send_data, st_tx_comma);
		SIGNAL st_Tx : t_st_Tx := st_wait_for_not_empty;
		--ATTRIBUTE fsm_encoding : STRING;
		--ATTRIBUTE fsm_encoding OF st_Tx : SIGNAL IS "sequential";
		-- auto|one-hot|compact|sequential|gray|johnson|speed1|user
		SIGNAL encode_Tx_DVout : STD_LOGIC;
		SIGNAL TxD_i : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0'); -- inverted / non-inverted output
		-- 8b10b constants
		CONSTANT Comma : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"BC"; -- K28.5
		CONSTANT Carrier_ext : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"F7"; -- K23.7
		CONSTANT Start_of_frame : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FB"; -- K27.7
		CONSTANT End_of_frame : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FD"; -- K29.7
		CONSTANT Error_propag : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FE"; -- K30.7
		--! DEBUG
		SIGNAL DEBUG_STATE : STD_LOGIC_VECTOR(3 DOWNTO 0) := X"0";
		
		-- Statistics
		SIGNAL TX_FRAMES_SENT_i :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		SIGNAL TX_FRAMES_TRUNCATED_i :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		SIGNAL TX_BYTES_SENT_i : STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		SIGNAL TX_BYTES_DROPPED_i :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		
		-- CRC to have a secured information about the length
		SIGNAL LENGTH_CRC_IN :  STD_LOGIC_VECTOR(11 downto 0) := (OTHERS => '0');
		SIGNAL LENGTH_CRC_OUT :  STD_LOGIC_VECTOR(3 downto 0) := (OTHERS => '0');
		
		----------------------------------------------------------------------------------
	BEGIN
		----------------------------------------------------------------------------------
		-- MII interface clock management
		--IBUFG_MII_RxCLK : IBUFG PORT MAP (O => MII_RxCLK, I => MII_RxCLK);
		
		--------------------------------------------------------------------------------
		----------------------------------------------------------------------------------
		-- clocking and reset
		----------------------------------------------------------------------------------
		rst_i <= rst;
		-- get rst into MII_RxCLK clock domain
		sync_rst_MII : ENTITY WORK.SignalSynchronizer
		PORT MAP(sig_clk1(0) => rst_i, clk1 => clk, clk2 => MII_RxCLK, sig_clk2(0) => rst_MII, arst => rst_i);
		
		----------------------------------------------------------------------------------
		-- read data from MII
		-- realignment from 4b to 8b (strip preamble???)
		-- insert into FIFO, mark start and end of a frame using special characters (8b => 9b words)
		-- FIFO write in MII_RxCLK_Rx clock domain (125 MHz), 8 bits every two clock cycles
		-- EOF: valid frame K29.7
		-- invalid frame K30.7
		-- each frame separated by at least 4 comma characters
		--
		----------------------------------------------------------------------------------
		--
		-- 01234567
		-- Preamble: 01010101 01010101 01010101 01010101 01010101 01010101 01010101 11010101
		-- 0x55 0x55 0x55 0x55 0x55 0x55 0x55 0xD5
		--
		-- L H L H L H L H L H L H L H L H L H
		-- RxD(0) X 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 D0 D4
		-- RxD(1) X 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 D1 D5
		-- RxD(2) X 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 D2 D6
		-- RxD(3) X 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 D3 D7
		-- Rx_DV 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1
		--
		--
		----------------------------------------------------------------------------------
		PROCESS (MII_RxCLK)
		VARIABLE try_start : INTEGER := 0;
		BEGIN
			IF rising_edge(MII_RxCLK) THEN
				IF rst_MII = '1' THEN
					FIFO_Rx_MII_wr_en <= '0';
					try_start := 1;
					st_MII_Rx <= st_idle;
					TX_FRAMES_TRUNCATED_i <= (OTHERS => '0');
					TX_FRAMES_SENT_i <= (OTHERS => '0');
					TX_BYTES_SENT_i <= (OTHERS => '0');
					TX_BYTES_DROPPED_i <= (OTHERS => '0');
				ELSE
					TX_FRAMES_TRUNCATED <= TX_FRAMES_TRUNCATED_i;
					TX_FRAMES_SENT <= TX_FRAMES_SENT_i;
					TX_BYTES_SENT <= TX_BYTES_SENT_i;
					TX_BYTES_DROPPED <= TX_BYTES_DROPPED_i;
					
					FIFO_Rx_MII_wr_en <= '0';
					MII_ACK <= '0';
					FIFO_Rx_MII_din(8 DOWNTO 0) <= '0' & MII_RxD;
					CASE st_MII_Rx IS
						WHEN st_idle => 
							try_start := 1;
							--------------------------------------------------------------------------
						WHEN st_wait => 
							st_MII_Rx <= st_data;
							MII_ACK <= '1';
							--------------------------------------------------------------------------
						WHEN st_data => 
						IF MII_RxDV = '0' THEN -- end-of-frame condition
							st_MII_Rx <= st_wait_fr_end;
							FIFO_Rx_MII_din <= '1' & End_of_frame;
							FIFO_Rx_MII_wr_en <= '1';
							TX_FRAMES_SENT_i <= TX_FRAMES_SENT_i + "1";
							-- FIFO full => terminate write of current packet and wait for its end
						ELSIF FIFO_Rx_MII_full = '1' OR MII_RxER = '1' THEN
							st_MII_Rx <= st_wait_fr_end_err;
							FIFO_Rx_MII_din <= '1' & Error_propag; -- EOF delimiter (incorrect frame)
							FIFO_Rx_MII_wr_en <= '1';
							TX_FRAMES_TRUNCATED_i <= TX_FRAMES_TRUNCATED_i + "1";
						ELSE
							-- normal data
							TX_BYTES_SENT_i <= TX_BYTES_SENT_i + "1";
							FIFO_Rx_MII_wr_en <= '1';
						END IF;
						---------------------------------------------------------------------------
						WHEN st_wait_fr_end =>
							IF FIFO_Rx_MII_prog_full = '0' THEN -- wait until FIFO is ready for another frame
								try_start := 1;
								st_MII_Rx <= st_idle;
							ELSIF MII_RxDV = '1' THEN
								TX_BYTES_DROPPED_i <= TX_BYTES_DROPPED_i + "1";
								st_MII_Rx <= st_wait_fr_end_err;
							END IF;
						WHEN st_wait_fr_end_err =>
							MII_ACK <= '1'; -- acknowledge any incoming packets, and drop them
							IF MII_RxDV = '0' AND FIFO_Rx_MII_prog_full = '0' THEN -- wait until FIFO is ready for another frame
								try_start := 1;
								st_MII_Rx <= st_idle;
							ELSE
								TX_BYTES_DROPPED_i <= TX_BYTES_DROPPED_i + "1";
							END IF;
							--------------------------------------------------------------------------
						WHEN OTHERS => st_MII_Rx <= st_wait_fr_end_err;
							--------------------------------------------------------------------------
					END CASE;
					IF try_start = 1 THEN
						try_start := 0;
						IF MII_RxDV = '1' THEN -- valid nibble of preamble
							-- do not start writing a new frame into FIFO until the FIFO is ready
							IF FIFO_Rx_MII_prog_full = '0' AND MII_RxER = '0' THEN
								FIFO_Rx_MII_din <= '1' & Start_of_frame; -- write SOF delimiter into FIFO
								FIFO_Rx_MII_wr_en <= '1';
								TX_BYTES_SENT_i <= TX_BYTES_SENT_i + "1";
								st_MII_Rx <= st_wait;
							ELSE
								st_MII_Rx <= st_wait_fr_end_err;
							END IF;
						END IF;
					END IF; -- try_start
				END IF;
			END IF;
		END PROCESS;
		--==============================================================================
		-- Clock Domain Boundary
		--==============================================================================
		-- FIFO: MII_Rx => Tx
--		FIFO_Rx_MII_i : ENTITY WORK.FIFO_Rx_MII
--			PORT MAP(
--				wr_clk      => MII_RxCLK,
--				din         => FIFO_Rx_MII_din,
--				wr_en       => FIFO_Rx_MII_wr_en,
--				wr_ack      => FIFO_Rx_MII_wr_ack,
--				full        => FIFO_Rx_MII_full,
--				prog_full   => FIFO_Rx_MII_prog_full,
--				rst         => rst_MII,
--				rd_clk      => TxC,
--				rd_en       => FIFO_Rx_MII_rd_en,
--				dout        => FIFO_Rx_MII_dout,
--				empty       => FIFO_Rx_MII_empty,
--				prog_empty  => FIFO_Rx_MII_prog_empty
--			);
		 wr_clk <= MII_RxCLK;
		 rd_clk <= TxC;
		
		 din <= FIFO_Rx_MII_din;
		 wr_en <= FIFO_Rx_MII_wr_en;
		
		 rd_en <= FIFO_Rx_MII_rd_en;
		 FIFO_Rx_MII_dout <= dout;
		 FIFO_Rx_MII_full <= full;
		 FIFO_Rx_MII_empty <= empty;
		
		 FIFO_Rx_MII_prog_empty <= empty_prog;
		 FIFO_Rx_MII_prog_full <= full;
		 
		crc_12to4_i : entity WORK.crc_12to4_single 
		PORT MAP(
			data_in => LENGTH_CRC_IN,
			crc_out => LENGTH_CRC_OUT
		);
		 
		--==============================================================================
		-- Clock Domain Boundary
		--==============================================================================
		--IBUFG_TxC : IBUFG PORT MAP (O => TxC_GCLK, I => TxC);
		--TxC <= NOT TxC_GCLK;
		----------------------------------------------------------------------------------
		-- FIFO read in TxC clock domain (external interface clock)
		-- read FIFO, whole frame at once, read one byte every 10 clock cycles
		-- the first word (SOF) is duplicated N times (N=1 now) and serves both as a preamble and SOF delimiter
		-- the 9th bit = CHAR_IS_K
		-- no data in FIFO => forward comma (K28.5) to 8b10b encoder
		-- encode 8b/10b
		-- transmit
		----------------------------------------------------------------------------------
		sync_rst_TxC : ENTITY WORK.SignalSynchronizer
		PORT MAP(sig_clk1(0) => rst_i, clk1 => clk, clk2 => TxC, sig_clk2(0) => rst_TxC, arst => rst_i);
		
		-- FIFO read, 8b10b feeder, 10b read synchronization (for transmitter) PROCESS(TxC) BEGIN
		PROCESS(TxC)
		VARIABLE comma_cntr : INTEGER := 8;
		BEGIN
			IF rising_edge(TxC) THEN
				IF rst_TxC = '1' THEN
					PROC_TX_rd_en <= '0';
					st_Tx <= st_wait_for_not_empty;
					encode_Tx_DVin <= '1';
					encode_Tx_Din <= Comma;
					encode_Tx_CharIsK <= '1';
				ELSE
					--------------------------------------------------------------------------
					PROC_TX_rd_en <= '0'; -- default assignment
					encode_Tx_DVin <= '1';
					encode_Tx_Din <= Comma;
					encode_Tx_CharIsK <= '1';
					--------------------------------------------------------------------------
					CASE st_Tx IS
						WHEN st_wait_for_not_empty =>
							IF PROC_TX_prog_empty = '0' THEN -- data available in FIFO
								IF PROC_TX_dout = '1' & Start_of_frame THEN
									st_Tx <= st_send_sof; -- send SOF state
								ELSE
									st_Tx <= st_wait_for_sof;
								END IF;
								PROC_TX_rd_en <= '1';
							END IF;
							
						WHEN st_wait_for_sof =>
							IF PROC_TX_dout = '1' & Start_of_frame THEN
								encode_Tx_Din <= Start_of_frame;
								PROC_TX_rd_en <= '1'; -- read first data
								st_Tx <= st_send_data; -- jump to data loop
							ELSE
								IF PROC_TX_prog_empty = '0' THEN -- data available in FIFO
									PROC_TX_rd_en <= '1';
								ELSE
									st_Tx <= st_wait_for_not_empty;
								END IF;
							END IF;

						WHEN st_send_sof =>
							-- the FIFO data look like this:
							-- |SOF|DATA0|DATA1|DATA2 .. |EOF|C1|C2|C3|C4|C5|C6|C7|C8|SOF|...
							-- so only the first byte (SOF) is fetched and replicated
							-- FWFT fifo architecture - SOF present at the output when empt = '0'
								encode_Tx_Din <= Start_of_frame;
								PROC_TX_rd_en <= '1'; -- read first data
								st_Tx <= st_send_data; -- jump to data loop
															
							-- data (FIFO not empty)
						WHEN st_send_data =>
							-- no data available in FIFO OR EOF
							IF PROC_TX_dout(8) = '1' OR PROC_TX_empty = '1' THEN -- EOF or ERR
								IF PROC_TX_dout = '1' & End_of_frame THEN
									encode_Tx_Din <= End_of_frame;
								ELSE
									encode_Tx_Din <= Error_propag;
								END IF;
								comma_cntr := 4; 
								st_Tx <= st_tx_comma;
							ELSE -- data available in FIFO
								PROC_TX_rd_en <= '1'; -- read next byte
								encode_Tx_Din <= PROC_TX_dout(7 DOWNTO 0);
								encode_Tx_CharIsK <= '0'; -- send data
							END IF;
							
						WHEN st_tx_comma =>
							comma_cntr := comma_cntr - 1;
							IF comma_cntr = 0 THEN
								st_Tx <= st_wait_for_not_empty;
							END IF;
					END CASE;
					--------------------------------------------------------------------------
				END IF;
			END IF;
		END PROCESS;
		--------------------------------------------------------------------------------
		Encoder_8B10B_i : ENTITY WORK.Encoder_8B10B
			PORT MAP(
				Tx_clk_in       => TxC,
				Tx_data_in      => encode_Tx_Din,
				Tx_DV_in        => encode_Tx_DVin,
				Tx_CharIsK_in   => encode_Tx_CharIsK,
				Tx_RunDisp_out  => encode_Tx_RunDisp,
				Tx_data_out     => encode_Tx_Dout,
				Tx_DV_out       => encode_Tx_DVout
			);
		--------------------------------------------------------------------------------
		-- Serializer
		PROCESS (TxC) BEGIN
		IF rising_edge(TxC) THEN
			IF rst_TxC = '1' THEN
				TxD_i <= (OTHERS => '0'); -- link LOW
			ELSE
				IF encode_Tx_DVout = '1' THEN
					TxD_i <= encode_Tx_Dout;
				END IF;
			END IF;
		END IF;
	END PROCESS;
	--------------------------------------------------------------------------------
	-- Optional inversion
	PROCESS (TxC) BEGIN
	IF rising_edge(TxC) THEN
		TxD <= TxD_i;
	END IF;
END PROCESS;
--------------------------------------------------------------------------------
-----RS ENCODER ----------------------------------------------------------------
--------------------------------------------------------------------------------
PROC_TX_dout <= FIFO_OUT_RS_dout;
RS_IN_dout <= FIFO_Rx_MII_dout;
FIFO_Rx_MII_rd_en <= RS_IN_rd_en;
FIFO_OUT_RS_rd_en <= PROC_TX_rd_en;
PROC_TX_empty <= FIFO_OUT_RS_empty;
PROC_TX_prog_empty <= FIFO_OUT_RS_prog_empty;
PROC_TX_empty <= FIFO_OUT_RS_empty;
RS_IN_prog_empty <= FIFO_Rx_MII_prog_empty;
RS_IN_empty <= FIFO_Rx_MII_empty;

FIFO_RS_rst_i <= rst_TxC OR FIFO_RS_rst;
FIFO_RSencoder_i : ENTITY WORK.FIFO_RSencoder
	PORT MAP(
		clk          => TxC,
		rst          => FIFO_RS_rst_i,
		din          => FIFO_RS_DIN,
		wr_en        => FIFO_RS_wr_en,
		full         => OPEN,
		almost_full  => FIFO_RS_almost_full,
		prog_full    => FIFO_RS_prog_full,
		data_count   => FIFO_RS_count,
		rd_en        => FIFO_RS_rd_en,
		dout         => FIFO_RS_DOUT,
		empty        => FIFO_RS_empty
	);
FIFO_SIG_RSencoder_i : ENTITY WORK.FIFO_SIG_RSencoder
	PORT MAP(
		clk           => TxC,
		rst           => rst_TxC,
		din           => FIFO_SIG_RS_DIN,
		wr_en         => FIFO_SIG_RS_wr_en,
		full          => OPEN,
		almost_full   => FIFO_SIG_RS_full,
		data_count    => FIFO_SIG_RS_count,
		almost_empty  => FIFO_SIG_RS_almost_empty,
		rd_en         => FIFO_SIG_RS_rd_en,
		dout          => FIFO_SIG_RS_DOUT,
		empty         => FIFO_SIG_RS_empty,
		valid         => FIFO_SIG_RS_valid
	);
FIFO_OUT_RSencoder_i : ENTITY WORK.FIFO_Rx_MII
	PORT MAP(
		wr_clk      => TxC,
		din         => FIFO_OUT_RS_din,
		wr_en       => FIFO_OUT_RS_wr_en,
		wr_ack      => FIFO_OUT_RS_wr_ack,
		full        => FIFO_OUT_RS_full,
		prog_full   => FIFO_OUT_RS_prog_full,
		rst         => rst_TxC,
		rd_clk      => TxC,
		rd_en       => FIFO_OUT_RS_rd_en,
		dout        => FIFO_OUT_RS_dout,
		empty       => FIFO_OUT_RS_empty,
		prog_empty  => FIFO_OUT_RS_prog_empty
	);
-- ENCODER INPUT --
PROCESS (TxC)
VARIABLE nop_cntr : INTEGER := 4;
BEGIN
IF rising_edge(TxC) THEN
	IF rst_TxC = '1' THEN
		RS_IN_rd_en <= '0';
		FIFO_RS_wr_en <= '0';
		FIFO_RS_rd_en <= '0';
		FIFO_SIG_RS_wr_en <= '0';
		FIFO_SIG_RS_rd_en <= '0';
		st_enc_len <= wait_for_sof;
		st_enc_in <= check_can_read;
		RS_CNTR_BYTE <= (OTHERS => '0');
		RS_CNTR_BYTE_OUT <= (OTHERS => '0');
		FIFO_RS_rst <= '0';
	ELSE
		--------------------------------------------------------------------------
		RS_IN_rd_en <= '0';
		RS_START <= '0';
		FIFO_RS_wr_en <= '0';
		FIFO_RS_rd_en <= '0';
		FIFO_SIG_RS_wr_en <= '0';
		FIFO_SIG_RS_rd_en <= '0';
		FIFO_OUT_RS_wr_en <= '0';
		FIFO_RS_rst <= '0';
		
		IF CHANNEL_ENCODER_USED_TxC = '1' THEN
			CASE st_enc_len IS
				WHEN wait_for_eof =>
					IF RS_IN_dout = '1' & End_of_frame THEN
						st_enc_len <= wait_for_sof;
					ELSE
						RS_IN_rd_en <= '1';
					END IF;
				WHEN wait_for_sof =>
					IF RS_IN_prog_empty = '0' AND
					 FIFO_SIG_RS_full = '0' AND
					 FIFO_RS_prog_full = '0' AND
					 st_enc_in /= check_can_read AND -- this state can reset the target FIFO 
					 FIFO_RS_rst = '0' AND -- if reset active, wait
					 RS_IN_dout = '1' & Start_of_frame THEN -- data available in FIFO
						RS_CNTR_BYTE <= (OTHERS => '0'); -- safer to set to 0
						st_enc_len <= loading;
						RS_IN_rd_en <= '1';
					END IF;
				WHEN loading =>
					IF RS_IN_dout = '1' & End_of_frame OR
					 RS_IN_empty = '1' THEN
						-- write count
						FIFO_SIG_RS_DIN <= RS_CNTR_BYTE;
						IF RS_CNTR_BYTE /= FIFO_SIG_LEN_ZERO THEN
							FIFO_SIG_RS_wr_en <= '1';
-- synopsys translate_off
-- pragma translate_off
-- synthesis translate_off
						ELSE
							assert false report "Error at RS_IN FIFO (DDR FIFO / regular FIFO)" severity failure;
-- synopsys translate_on
-- pragma translate_on
-- synthesis translate_on
						END IF;
						IF RS_IN_empty = '1' AND RS_IN_dout /= '1' & End_of_frame THEN
							st_enc_len <= wait_for_eof;
						ELSE
							st_enc_len <= wait_for_sof;
						END IF;
					ELSIF RS_IN_dout = '1' & Start_of_frame THEN --ignore SOF here
						RS_IN_rd_en <= '1';
					ELSE
						RS_IN_rd_en <= '1';
						FIFO_RS_DIN <= RS_IN_dout(7 DOWNTO 0);
						FIFO_RS_wr_en <= '1';
						RS_CNTR_BYTE <= RS_CNTR_BYTE + X"01";
					END IF;
					--------------------------------------------------------------------------
				WHEN OTHERS => st_enc_len <= wait_for_sof;
					--------------------------------------------------------------------------
			END CASE;
			--========================================================================
			st_enc_in_last <= st_enc_in;
			ENCODED_FRAME_STOP_CONDITION <= FIFO_OUT_RS_prog_full = '1' OR
													 FIFO_SIG_RS_empty = '1' OR
													 LINK_IS_DOWN_TxC = '1' OR
													 ((SUM_SENT_BYTES + FIFO_SIG_RS_DOUT) >= ENCODED_MAX_LEN) OR
													 (FIFO_RS_count < ('0' & X"030"));
			ENCODED_FRAME_START_CONDITION <= (NOT ENCODED_FRAME_STOP_CONDITION) AND (FIFO_SIG_RS_valid = '1');
			
			CASE st_enc_in IS
				WHEN check_can_read =>
					--NOP, if the frame was terminated, because we want to split a
					-- long encoded superframe, insert a wait state to let the
					-- encoder reader FSM insert start of frame and end of frame
					-- and to go to next superframe
	--!!!				IF st_enc_len = wait_for_sof AND
	--!!!				  FIFO_SIG_RS_count = X"00" AND
	--!!!			     FIFO_SIG_RS_wr_en = '0' THEN
	--!!!		   		FIFO_RS_rst <= '1';
	--!!!    			END IF;
					SUM_SENT_BYTES <= (OTHERS => '0');
					nop_cntr := 15;
					st_enc_in <= nop_check_can_read;
				WHEN nop_check_can_read =>
					IF nop_cntr = 0 THEN
						st_enc_in <= do_check_can_read;
					ELSE
						nop_cntr := nop_cntr - 1;
					END IF;
				WHEN do_check_can_read =>
					IF ENCODED_FRAME_START_CONDITION THEN
						st_enc_in <= write_count_1_wfs;
					END IF;
				WHEN write_count_1_wfs =>
					IF ENCODED_FRAME_STOP_CONDITION THEN
						st_enc_in <= check_can_read;
					ELSIF RS_READY_FOR_START = '1' THEN
						RS_CNTR_BYTE_OUT <= FIFO_SIG_RS_DOUT;
						RS_DIN <= '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
						RS_START <= '1';
						LENGTH_CRC_IN <= FIFO_SIG_RS_DOUT(10 DOWNTO 7) & '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
						st_enc_in <= write_count_2_first;
					END IF;
				WHEN write_count_2_first =>
					FIFO_SIG_RS_rd_en <= '1';
					FIFO_RS_rd_en <= '1';
					RS_DIN <= LENGTH_CRC_OUT & FIFO_SIG_RS_DOUT(10 DOWNTO 7);
					SUM_SENT_BYTES <= SUM_SENT_BYTES + FIFO_SIG_RS_DOUT;
					st_enc_in <= write_data;
				WHEN write_count_1 =>
					IF RS_READY_FOR_NEXT_DATA = '1' THEN
						IF ENCODED_FRAME_STOP_CONDITION THEN
							RS_DIN <= X"00";
							st_enc_in <= write_padding_and_sof;
						ELSE
							RS_CNTR_BYTE_OUT <= FIFO_SIG_RS_DOUT;
							RS_DIN <= '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
							LENGTH_CRC_IN <= FIFO_SIG_RS_DOUT(10 DOWNTO 7) & '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
							st_enc_in <= write_count_2;
						END IF;
					ELSE
						IF ENCODED_FRAME_STOP_CONDITION THEN
							st_enc_in <= check_can_read;
						ELSE
							st_enc_in <= write_count_1_wfs;
						END IF;
					END IF;
				WHEN write_count_2 =>
					IF RS_READY_FOR_NEXT_DATA = '1' THEN
						FIFO_SIG_RS_rd_en <= '1';
						FIFO_RS_rd_en <= '1';
						RS_DIN <= LENGTH_CRC_OUT & FIFO_SIG_RS_DOUT(10 DOWNTO 7);
						SUM_SENT_BYTES <= SUM_SENT_BYTES + FIFO_SIG_RS_DOUT;
						st_enc_in <= write_data;
					ELSE
						IF st_enc_in_last = write_data THEN
							st_enc_in <= write_count_1_wfs;
						ELSE
							st_enc_in <= write_count_2_wfs;
						END IF;
					END IF;
				WHEN write_count_2_wfs =>
					IF RS_READY_FOR_START = '1' THEN
						FIFO_SIG_RS_rd_en <= '1';
						FIFO_RS_rd_en <= '1';
						RS_DIN <= LENGTH_CRC_OUT & FIFO_SIG_RS_DOUT(10 DOWNTO 7);
						SUM_SENT_BYTES <= SUM_SENT_BYTES + FIFO_SIG_RS_DOUT;
						st_enc_in <= write_data;
						RS_START <= '1';
					END IF;
				WHEN write_data =>
					IF RS_READY_FOR_NEXT_DATA = '1' THEN
						RS_DIN <= FIFO_RS_DOUT;
						IF (RS_CNTR_BYTE_OUT) = "000" & X"00" THEN
							IF ENCODED_FRAME_STOP_CONDITION THEN
								RS_DIN <= X"00";
								st_enc_in <= write_padding_and_sof;
							ELSE
								RS_CNTR_BYTE_OUT <= FIFO_SIG_RS_DOUT;
								RS_DIN <= '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
								LENGTH_CRC_IN <= FIFO_SIG_RS_DOUT(10 DOWNTO 7) & '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
								st_enc_in <= write_count_2;
							END IF;
						ELSE
							IF (RS_CNTR_BYTE_OUT) > "000" & X"01" THEN
									FIFO_RS_rd_en <= '1';
							END IF;
							RS_CNTR_BYTE_OUT <= RS_CNTR_BYTE_OUT - X"1";
						END IF;
					ELSE -- reached end of one RS block
						RS_DIN_TEMP <= FIFO_RS_DOUT;
						st_enc_in <= wait_for_sob;
					END IF;
				WHEN wait_for_sob =>
					IF RS_READY_FOR_START = '1' THEN
						-- at RS_DIN, there is valid data
						RS_START <= '1';
						IF (RS_CNTR_BYTE_OUT) = "000" & X"00" THEN
							st_enc_in <= write_count_1;
						ELSE
							RS_CNTR_BYTE_OUT <= RS_CNTR_BYTE_OUT - X"1";
							st_enc_in <= wait_for_sob_last;
						END IF;
					END IF;
				WHEN wait_for_sob_last =>
					RS_DIN <= RS_DIN_TEMP;
					IF (RS_CNTR_BYTE_OUT) = "000" & X"00" THEN
						st_enc_in <= write_data_last;
					ELSE
						FIFO_RS_rd_en <= '1';
						st_enc_in <= write_data;
					END IF;
				WHEN write_data_last =>
					IF ENCODED_FRAME_STOP_CONDITION THEN
						RS_DIN <= X"00";
						st_enc_in <= write_padding_and_sof;
					ELSE
						RS_CNTR_BYTE_OUT <= FIFO_SIG_RS_DOUT;
						RS_DIN <= '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
						LENGTH_CRC_IN <= FIFO_SIG_RS_DOUT(10 DOWNTO 7) & '1' & FIFO_SIG_RS_DOUT(6 DOWNTO 0);
						st_enc_in <= write_count_2;
					END IF;
				WHEN write_padding_and_sof =>
					IF RS_READY_FOR_NEXT_DATA = '0' THEN
						st_enc_in <= check_can_read;
					END IF;
					--------------------------------------------------------------------------
				WHEN OTHERS => st_enc_in <= check_can_read;
					--------------------------------------------------------------------------
			END CASE;
			CASE st_out_fifo IS
				WHEN prefetch_sof =>
					IF FIFO_OUT_RS_prog_full = '0' THEN
						FIFO_OUT_RS_din <= '1' & Start_of_frame;
						FIFO_OUT_RS_wr_en <= '1';
						st_out_fifo <= wait_for_rdy;
					END IF;
				WHEN wait_for_rdy =>
					IF RS_DV_OUT = '1' THEN
						FIFO_OUT_RS_din <= '0' & RS_DOUT;
						FIFO_OUT_RS_wr_en <= '1';
						st_out_fifo <= write_payload;
					END IF;
				WHEN write_payload =>
					IF RS_DV_OUT = '1' THEN
						RS_DV_TIMEOUT <= "10";
						FIFO_OUT_RS_din <= '0' & RS_DOUT;
						FIFO_OUT_RS_wr_en <= '1';
					ELSE
						RS_DV_TIMEOUT <= RS_DV_TIMEOUT - "1";
						IF RS_DV_TIMEOUT = "00" THEN
							FIFO_OUT_RS_din <= '1' & End_of_frame;
							FIFO_OUT_RS_wr_en <= '1';
							st_out_fifo <= prefetch_sof;
						END IF;
					END IF;
				WHEN OTHERS => st_out_fifo <= prefetch_sof;
			END CASE;
		ELSE -- NO RS ENCODER
			FIFO_OUT_RS_din <= RS_IN_dout;
			FIFO_OUT_RS_wr_en <= '0';
			CASE st_enc_len IS
				WHEN wait_for_sof =>
					IF RS_IN_prog_empty = '0' AND
					 LINK_IS_DOWN_TxC = '0' AND
					 FIFO_RS_prog_full = '0' THEN -- data available in FIFO
						st_enc_len <= loading;
						RS_IN_rd_en <= '1';
					END IF;
				WHEN loading =>
					-- RS_IN_empty = '1'
					-- SHOULD NOT HAPPEN
					-- SHOULD TERMINATE VIA EOF OF LAST FRAME
					FIFO_OUT_RS_wr_en <= '1';
					IF RS_IN_dout = '1' & End_of_frame OR
					 RS_IN_empty = '1' OR
					 FIFO_RS_almost_full = '1' THEN
						IF RS_IN_EMPTY = '1' AND
						 RS_IN_dout /= '1' & End_of_frame THEN
							FIFO_OUT_RS_din <= '1' & Error_propag;
						END IF;
						-- continue or go idling?
						IF RS_IN_prog_empty = '1' OR
						 LINK_IS_DOWN_TxC = '1' OR
						 FIFO_RS_prog_full = '1' THEN
							st_enc_len <= wait_for_sof;
						ELSE
							RS_IN_rd_en <= '1';
						END IF;
					ELSE
						RS_IN_rd_en <= '1';
					END IF;
					--------------------------------------------------------------------------
				WHEN OTHERS => st_enc_len <= wait_for_sof;
					--------------------------------------------------------------------------
			END CASE;
		END IF;
	END IF;
END IF;
END PROCESS;
RSencoder_i : ENTITY WORK.RSencoder
	PORT MAP(
		data_in   => RS_DIN,
		start     => RS_START,
		bypass    => '0',
		sclr      => rst_TxC,
		data_out  => RS_DOUT,
		info      => OPEN,
		rdy       => RS_DV_OUT,
		rffd      => RS_READY_FOR_START_i,
		rfd       => RS_READY_FOR_NEXT_DATA,
		clk       => TxC
	);
PROCESS (TxC) BEGIN
IF rising_edge(TxC) THEN
	IF rst_TxC = '1' THEN
		RS_READY_FOR_START <= '0';
	ELSE
		RS_READY_FOR_START <= RS_READY_FOR_START_i;
	END IF;
END IF;
END PROCESS;
sync_LINK_IS_DOWN_TxC : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => LINK_IS_DOWN, clk1 => clk, clk2 => TxC, sig_clk2(0) => LINK_IS_DOWN_TxC, arst => rst);
sync_CHANNEL_ENCODER_USED_TxC : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => CHANNEL_ENCODER_USED, clk1 => clk, clk2 => TxC, sig_clk2(0) => CHANNEL_ENCODER_USED_TxC, arst => rst);

----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------