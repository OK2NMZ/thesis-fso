library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library WORK;
use WORK.modules_pkg.ALL;
use WORK.parameters_pkg.ALL;
use WORK.types_pkg.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity SFP_Simplified_MAC is
	Port (
			--=========================================================================
			--! To RX PHY
			RAW_RX : in  STD_LOGIC_VECTOR (9 downto 0) := (OTHERS => '0');
		   CLK_RX : in  STD_LOGIC := '0';
			ALIGNED : out STD_LOGIC := '0';
			--=========================================================================
		   
			--=========================================================================
			--! To TX PHY
		   RAW_TX : out  STD_LOGIC_VECTOR (9 downto 0) := (OTHERS => '0');
		   CLK_TX : in  STD_LOGIC := '0';
			--=========================================================================
		   
			--=========================================================================
			--! MAC interface
		   SYS_CLK : in STD_LOGIC := '0';
		   SYS_RST : in STD_LOGIC := '0';
			CHANNEL_CODING_USED : in STD_LOGIC := '0';
			DBG_BTN_IN : in STD_LOGIC := '0';
			BER_SIMULATION_EN : in STD_LOGIC := '0';
			BER_SIMULATION_COMP : in STD_LOGIC_VECTOR (31 downto 0) := (OTHERS => '0');
			
			--! Statistics, synchronized with SYS_CLK
			RX_FRAMES_RECEIVED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			RX_FRAMES_BAD  : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');	
			RX_BYTES_CORRECTED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			RX_BYTES_RECEIVED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			TX_FRAMES_SENT : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			TX_FRAMES_TRUNCATED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			TX_BYTES_SENT : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			TX_BYTES_DROPPED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			STAT_DOUT : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
			STAT_COUNT : OUT STD_LOGIC_VECTOR(9 downto 0) := (OTHERS => '0');
			STAT_RD_EN : IN STD_LOGIC := '0';
			
		   
		   DV_RX : out  STD_LOGIC := '0';
		   GF_RX : out  STD_LOGIC := '0';
		   BF_RX : out  STD_LOGIC := '0';
		   DIN_RX : out  STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
		   
		   DV_TX : in  STD_LOGIC := '0';
		   ACK_TX : out  STD_LOGIC := '0';
		   DOUT_TX : in  STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
			--=========================================================================
			
			--! 9bit FIFO for transmission
			wr_clk : OUT STD_LOGIC := '0';
			rd_clk : OUT STD_LOGIC := '0';
			din : OUT STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
			wr_en : OUT STD_LOGIC := '0';

			rd_en : OUT STD_LOGIC := '0';
			dout : IN STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
			full : IN STD_LOGIC := '0';
			empty_prog : IN STD_LOGIC := '0';
			empty : IN STD_LOGIC := '0');
end SFP_Simplified_MAC;

architecture Behavioral of SFP_Simplified_MAC is

signal DV_RX_i   :   STD_LOGIC := '0';
signal GF_RX_i   :   STD_LOGIC := '0';
signal BF_RX_i   :   STD_LOGIC := '0';
signal ALIGNED_i :   STD_LOGIC := '0';
signal DIN_RX_i  :  STD_LOGIC_VECTOR (7 downto 0) := X"00";

signal ACK_TX_i  : STD_LOGIC := '0';

-- Link down detection BER improvement technique
signal LINK_IS_DOWN   : STD_LOGIC := '0';
signal LINK_IS_DOWN_i : STD_LOGIC := '0';
signal LINK_DOWN_TICK :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
signal INFO_FRAME :  STD_LOGIC_VECTOR(4 downto 0) := (OTHERS => '0');
signal INFO_FRAME_i :  STD_LOGIC_VECTOR(4 downto 0) := (OTHERS => '0');
signal INFO_FRAME_last :  STD_LOGIC_VECTOR(4 downto 0) := (OTHERS => '0');

signal STAT_DIN :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
signal STAT_DOUT_i :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
signal STAT_WR_EN : STD_LOGIC := '0';
signal STAT_RD_EN_i : STD_LOGIC := '0';
signal STAT_FULL : STD_LOGIC := '0';
signal STAT_EMPTY : STD_LOGIC := '0';
signal STAT_COUNT_i :  STD_LOGIC_VECTOR(9 downto 0) := (OTHERS => '0');


--! for BER simulation
COMPONENT rng
PORT(
	clk : IN std_logic;
	reset : IN std_logic;
	loadseed_i : IN std_logic;
	seed_i : IN std_logic_vector(31 downto 0);          
	number_o : OUT std_logic_vector(31 downto 0)
	);
END COMPONENT;
signal RAW_RX_i : STD_LOGIC_VECTOR (9 downto 0) := (OTHERS => '0');
signal BER_SIMULATION_RANDOM :  STD_LOGIC_VECTOR (31 downto 0) := (OTHERS => '0');
signal BER_SIMULATION_LOAD_SEED :  STD_LOGIC := '0';
signal BER_SIMULATION_SEED :  STD_LOGIC_VECTOR (31 downto 0) := (OTHERS => '0');
signal BER_SIMULATION_RST :  STD_LOGIC := '0';
signal BER_SIMULATION_EN_sync : STD_LOGIC := '0';
signal BER_SIMULATION_COMP_sync : STD_LOGIC_VECTOR (31 downto 0) := (OTHERS => '0');

signal ERR_FRAMING :  STD_LOGIC := '0';
signal ERR_RS_BAD_BLOCK :  STD_LOGIC := '0';
signal ERR_8B10B :  STD_LOGIC := '0';
signal ERR_ALIGN :  STD_LOGIC := '0';

begin
--! SFP RX interface
SFP_RX_IF : entity WORK.SM_Rx_no_DCM 
PORT MAP(
		RxC => CLK_RX,
		RxD => RAW_RX_i,
		MII_TxCLK => SYS_CLK,
		MII_TxD => DIN_RX_i,
		MII_TXEN => DV_RX_i,
		MII_GF => GF_RX_i,
		MII_BF => BF_RX_i,
		ALIGNED => ALIGNED_i,
		LINK_IS_DOWN => LINK_IS_DOWN_i,
		ERR_FRAMING => ERR_FRAMING,
		ERR_RS_BAD_BLOCK => ERR_RS_BAD_BLOCK,
		ERR_8B10B => ERR_8B10B,
		ERR_ALIGN => ERR_ALIGN,
		CHANNEL_DECODER_USED => CHANNEL_CODING_USED,
		clk => SYS_CLK,
		rst => SYS_RST,
		RX_FRAMES_RECEIVED => RX_FRAMES_RECEIVED,
		RX_FRAMES_BAD => RX_FRAMES_BAD,
		RX_BYTES_CORRECTED => RX_BYTES_CORRECTED,
		RX_BYTES_RECEIVED => RX_BYTES_RECEIVED
	);
  
DIN_RX <= DIN_RX_i;
DV_RX <= DV_RX_i;
GF_RX <= GF_RX_i;
BF_RX <= BF_RX_i;
ALIGNED <= ALIGNED_i;
LINK_IS_DOWN <= LINK_IS_DOWN_i OR DBG_BTN_IN;

--! SFP TX interface
SFP_TX_IF: entity WORK.SM_Tx_no_DCM 
PORT MAP(
		TxC => CLK_TX,
		TxD => RAW_TX,
		MII_RxCLK => SYS_CLK,
		MII_RxD => DOUT_TX,
		MII_RxDV => DV_TX,
		MII_RxER => '0',
		MII_ACK => ACK_TX_i,
		LINK_IS_DOWN => LINK_IS_DOWN,
		CHANNEL_ENCODER_USED => CHANNEL_CODING_USED,
		TX_FRAMES_SENT => TX_FRAMES_SENT,
		TX_FRAMES_TRUNCATED => TX_FRAMES_TRUNCATED,
		TX_BYTES_SENT => TX_BYTES_SENT,
		TX_BYTES_DROPPED => TX_BYTES_DROPPED,
		
		wr_clk => wr_clk,
		rd_clk => rd_clk,
		din => din,
		wr_en => wr_en,
		rd_en => rd_en,
		dout => dout,
		full => full,
		empty => empty,
		empty_prog => empty_prog,
		
		clk => SYS_CLK,
		rst => SYS_RST 
	);
ACK_TX <= ACK_TX_i;

STAT_FIFO_i : entity WORK.STAT_FIFO
  PORT MAP (
    clk => SYS_CLK,
    srst => SYS_RST,
    din => STAT_DIN,
    wr_en => STAT_WR_EN,
    rd_en => STAT_RD_EN_i,
    dout => STAT_DOUT_i,
    full => STAT_FULL,
    empty => STAT_EMPTY,
    data_count => STAT_COUNT_i
  );
  
STAT_RD_EN_i <= STAT_RD_EN;
STAT_DOUT <= STAT_DOUT_i;
STAT_COUNT <= STAT_COUNT_i;

INFO_FRAME <= LINK_IS_DOWN & ERR_FRAMING & ERR_RS_BAD_BLOCK
					& ERR_8B10B & ERR_ALIGN;

PROCESS(SYS_CLK)BEGIN
	IF rising_edge(SYS_CLK) THEN
		IF SYS_RST = '1' THEN
			LINK_DOWN_TICK <= (OTHERS => '0');
			STAT_WR_EN <= '0';
			INFO_FRAME_last <= INFO_FRAME;
			INFO_FRAME_i <= INFO_FRAME;
		ELSE
			INFO_FRAME_i <= INFO_FRAME;
			STAT_WR_EN <= '0';
			INFO_FRAME_last <= INFO_FRAME_i;
			LINK_DOWN_TICK <= LINK_DOWN_TICK + "1";
			
			IF INFO_FRAME_i /= INFO_FRAME_last THEN
				IF STAT_FULL = '0' THEN
					-- MSB is new state, 58 downto 0 is timestamp, 150 years to overflow
					STAT_DIN <= INFO_FRAME_i & LINK_DOWN_TICK(58 downto 0);
					STAT_WR_EN <= '1';
				END IF;	
			END IF;
		END IF;
	END IF;
END PROCESS;


PROCESS(CLK_RX)BEGIN
	IF rising_edge(CLK_RX) THEN
		IF BER_SIMULATION_EN_sync = '1' THEN
			IF BER_SIMULATION_RST = '1' THEN
				--! make sure not only '0', LFSR would fail
				BER_SIMULATION_SEED <= RAW_RX & "1" & RAW_RX & "1" & RAW_RX;
				BER_SIMULATION_LOAD_SEED <= '1';
			ELSE
				IF BER_SIMULATION_LOAD_SEED = '1' THEN
					BER_SIMULATION_LOAD_SEED <= '0';
				END IF;
				
				IF BER_SIMULATION_RANDOM < BER_SIMULATION_COMP_sync THEN
					RAW_RX_i <= RAW_RX XOR BER_SIMULATION_RANDOM(9 downto 0); 
				ELSE
					RAW_RX_i <= RAW_RX;
				END IF;

			END IF;
		ELSE
			RAW_RX_i <= RAW_RX;
		END IF;
	END IF;
END PROCESS;

rng_i : rng PORT MAP(
	clk => CLK_RX,
	reset => NOT BER_SIMULATION_RST,
	loadseed_i => BER_SIMULATION_LOAD_SEED,
	seed_i => BER_SIMULATION_SEED,
	number_o => BER_SIMULATION_RANDOM
);

sync_BER_SIMULATION_EN : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => BER_SIMULATION_EN, clk1 => SYS_CLK,
			sig_clk2(0) => BER_SIMULATION_EN_sync, clk2 => CLK_RX, arst => SYS_RST);

sync_BER_SIMULATION_COMP : ENTITY WORK.SignalSynchronizer
GENERIC MAP(DATA_WIDTH => 32)
PORT MAP(sig_clk1 => BER_SIMULATION_COMP, clk1 => SYS_CLK,
			sig_clk2 => BER_SIMULATION_COMP_sync, clk2 => CLK_RX, arst => SYS_RST);
			
sync_BER_SIMULATION_RST : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => SYS_RST, clk1 => SYS_CLK,
			sig_clk2(0) => BER_SIMULATION_RST, clk2 => CLK_RX, arst => SYS_RST);



end Behavioral;

