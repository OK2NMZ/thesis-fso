--------------------------------------------------------------------------------
-- use WORK.types_pkg.ALL;
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use WORK.parameters_pkg.ALL;
--------------------------------------------------------------------------------
PACKAGE types_pkg IS
--------------------------------------------------------------------------------

 -- TYPE t_Array_16b  IS ARRAY (NATURAL RANGE <>) OF STD_LOGIC_VECTOR( C_COEF_width-1 DOWNTO 0);


 ---------------------------------------------------------------------------------
 ------------------------------MPU_SYS_INTERFACE----------------------------------
 ---------------------------------------------------------------------------------

TYPE MPU_SYS_INTERFACE_INOUT IS
RECORD

    -- LCD -----------------------------------------------------------------------
    LCD_DB                  :  STD_LOGIC_VECTOR(7 DOWNTO 4);

    -- SFP control ---------------------------------------------------------------
    SFP_M2                  :  STD_LOGIC;        --! SDA; serial interface data

END RECORD;

TYPE MPU_SYS_INTERFACE_IN IS
RECORD

     -- UART ----------------------------------------------------------------------
    UART_RXD                :     STD_LOGIC;

    -- GPIO ----------------------------------------------------------------------;
    GPIO_DIP                :     STD_LOGIC_VECTOR( 7 DOWNTO 0);
    GPIO_BTN                :     STD_LOGIC_VECTOR( 4 DOWNTO 0);

    -- SPI interface -------------------------------------------------------------
    SPI_SCK                 :     STD_LOGIC;
    SPI_SDI                 :     STD_LOGIC;
    SPI_CSN                 :     STD_LOGIC;

    -- GTP control -------------------------------------------------------------
    --PLL_locked_PHY          :     STD_LOGIC;
    PLL_locked_SFP_SMA      :     STD_LOGIC;


    -- BERT --------------------------------------------------------------------
    BERT_IRQ_1s             :   STD_LOGIC;                                --! 1s interrupt request
    BERT_toggle_1s          :   STD_LOGIC;                                --! toggle each 1s (for data consistency check)
    BERT_Err_cnt_1s         :   STD_LOGIC_VECTOR(31 DOWNTO 0);            --! number of errors in last 1s
    BERT_LFSR_locked        :   STD_LOGIC;                                --! PRBS is detected correctly (BER < 10%)

    -- SFP control ---------------------------------------------------------------
    SFP_M0                  :     STD_LOGIC;        --! L when module present
    SFP_TXF                 :     STD_LOGIC;        --! Tx fault; active H
    SFP_LOS                 :     STD_LOGIC;        --! Los of Signal; active H

    -- UBLAZE control ------------------------------------------------------------
    UBLAZE_addr             :     STD_LOGIC_VECTOR(7 DOWNTO 0);
    UBLAZE_write_data       :     STD_LOGIC_VECTOR(7 DOWNTO 0);
    UBLAZE_write_strobe     :     STD_LOGIC;

    -- MAC HOST Interface --------------------------------------------------------
    HOST_1_MIIMRDY          :     STD_LOGIC;
    HOST_1_RDDATA           :     STD_LOGIC_VECTOR(31 DOWNTO 0);


END RECORD;

TYPE MPU_SYS_INTERFACE_OUT IS
RECORD

     -- UART ----------------------------------------------------------------------
    UART_TXD                :    STD_LOGIC;

    -- GPIO ----------------------------------------------------------------------
    GPIO_LED                :    STD_LOGIC_VECTOR( 7 DOWNTO 0);

    -- LCD -----------------------------------------------------------------------
    LCD_E                   :    STD_LOGIC;
    LCD_RS                  :    STD_LOGIC;
    LCD_RW                  :    STD_LOGIC;

    -- SPI interface -------------------------------------------------------------
    SPI_SDO                 :    STD_LOGIC;
    Seq_sel                 :    STD_LOGIC_VECTOR( 7 DOWNTO 0);

    -- GTP control -------------------------------------------------------------
    rst_GTP_SFP_SMA         :    STD_LOGIC;
    rst_Rx_CDR_SFP          :    STD_LOGIC;
    rst_Rx_CDR_SMA          :    STD_LOGIC;

    -- GTP SFP control ---------------------------------------------------------
    SFP_Tx_Inhibit          :    STD_LOGIC;
    SFP_Tx_Polarity         :    STD_LOGIC;
    SFP_Tx_Swing            :    STD_LOGIC_VECTOR(2 DOWNTO 0);
    SFP_Tx_Preemphasis      :    STD_LOGIC_VECTOR(2 DOWNTO 0);
    SFP_Rx_EqMix            :    STD_LOGIC_VECTOR(1 DOWNTO 0);
    SFP_Rx_EqPole           :    STD_LOGIC_VECTOR(3 DOWNTO 0);

    -- SFP control ---------------------------------------------------------------
    SFP_M1                  :    STD_LOGIC;        --! SCL; serial interface clock; max 100 kHz, active rising edge
    SFP_RS                  :    STD_LOGIC;        --! Rate select; H for full-bandwidth
  --SFP_TXENA               :    STD_LOGIC;        --! Tx enable; active H; hold L for > 10 us to clear Tx Falut

    -- MUX control ---------------------------------------------------------------
    MUX_Select              :    STD_LOGIC_VECTOR(7 DOWNTO 0); --! To redirect the input signal from SFP

    -- UBLAZE control ------------------------------------------------------------
    UBLAZE_read_data        :     STD_LOGIC_VECTOR(7 DOWNTO 0); --! MicroBlaze interface - for MicroBlaze to read
    UBLAZE_data_ack         :     STD_LOGIC; --! MicroBlaze interface -- when writing is done, '1' pulse is generated
    -- MAC HOST Interface --------------------------------------------------------
    HOST_1_OPCODE           :    STD_LOGIC_VECTOR( 1 DOWNTO 0);
    HOST_1_REQ              :    STD_LOGIC;
    HOST_1_MIIMSEL          :    STD_LOGIC;
    HOST_1_ADDR             :    STD_LOGIC_VECTOR( 9 DOWNTO 0); 
    HOST_1_WRDATA           :    STD_LOGIC_VECTOR(31 DOWNTO 0);
    HOST_1_EMAC1SEL         :    STD_LOGIC;

END RECORD;

 ---------------------------------------------------------------------------------
 -------------------------MICROBLAZE_WRAPPER_INTERFACE----------------------------
 ---------------------------------------------------------------------------------

TYPE MICROBLAZE_WRAPPER_INTERFACE_OUT IS
RECORD

    -- SRAM Interface ------------------------------------------------------------
    SRAM_Mem_A              :     STD_LOGIC_VECTOR(7 to 30);
    SRAM_Mem_CEN            :     STD_LOGIC_VECTOR(0 to 1);
    SRAM_Mem_OEN            :     STD_LOGIC_VECTOR(0 to 1);
    SRAM_Mem_WEN            :     STD_LOGIC;
    SRAM_Mem_BEN            :     STD_LOGIC_VECTOR(0 to 3);
    SRAM_Mem_ADV_LDN        :     STD_LOGIC;
    SRAM_ZBT_CLK_OUT        :     STD_LOGIC;
    SRAM_Mem_LBON           :     STD_LOGIC;

    -- DDR2 SDRAM Interface ------------------------------------------------------
    DDR2_SDRAM_Clk          :    STD_LOGIC_VECTOR(1 downto 0);
    DDR2_SDRAM_Clk_n        :    STD_LOGIC_VECTOR(1 downto 0);
    DDR2_SDRAM_CE           :    STD_LOGIC_VECTOR(1 downto 0);
    DDR2_SDRAM_CS_n         :    STD_LOGIC_VECTOR(1 downto 0);
    DDR2_SDRAM_ODT          :    STD_LOGIC_VECTOR(1 downto 0);
    DDR2_SDRAM_RAS_n        :    STD_LOGIC;
    DDR2_SDRAM_CAS_n        :    STD_LOGIC;
    DDR2_SDRAM_WE_n         :    STD_LOGIC;
    DDR2_SDRAM_BankAddr     :    STD_LOGIC_VECTOR(1 downto 0);
    DDR2_SDRAM_Addr         :    STD_LOGIC_VECTOR(12 downto 0);
    DDR2_SDRAM_DM           :    STD_LOGIC_VECTOR(7 downto 0);

    -- SysACE CF Interface -------------------------------------------------------
    SysACE_CF_MPA           :    STD_LOGIC_VECTOR(6 downto 0);
    SysACE_CF_CEN           :    STD_LOGIC;
    SysACE_CF_OEN           :    STD_LOGIC;
    SysACE_CF_WEN           :    STD_LOGIC;

    -- MAC Interface -------------------------------------------------------------
    MAC_TemacPhy_RST_n      :    STD_LOGIC;
    MAC_GMII_TXD            :    STD_LOGIC_VECTOR(7 downto 0);
    MAC_GMII_TX_EN          :    STD_LOGIC;
    MAC_GMII_TX_ER          :    STD_LOGIC;
    MAC_GMII_TX_CLK         :    STD_LOGIC;
    MAC_MDC                 :    STD_LOGIC;

    -- Picoblaze Interface -------------------------------------------------------
    PBLAZE_address          :    STD_LOGIC_VECTOR(0 to 7);    --! Picoblaze interface - Address bus
    PBLAZE_data_OUT         :    STD_LOGIC_VECTOR(0 to 7);    --! Picoblaze interface - Data bus |UB| --> |PB|
    PBLAZE_write_strobe     :    STD_LOGIC;                   --! Picoblaze interface - write strobe

    -- UART tx -------------------------------------------------------------------
    UART_tx                 :    STD_LOGIC;

END RECORD;

TYPE MICROBLAZE_WRAPPER_INTERFACE_INOUT IS
RECORD

  -- IIC EEPROM Interface --------------------------------------------------------
    IIC_EEPROM_Sda          :    STD_LOGIC;
    IIC_EEPROM_Scl          :    STD_LOGIC;

    -- SRAM Interface ------------------------------------------------------------
    SRAM_Mem_DQ             :    STD_LOGIC_VECTOR(0 to 31);

    -- DDR2 SDRAM Interface ------------------------------------------------------
    DDR2_SDRAM_DDR2_DQ      :    STD_LOGIC_VECTOR(63 downto 0);
    DDR2_SDRAM_DDR2_DQS     :    STD_LOGIC_VECTOR(7 downto 0);
    DDR2_SDRAM_DDR2_DQS_n   :    STD_LOGIC_VECTOR(7 downto 0);

    -- SysACE CF Interface -------------------------------------------------------
    SysACE_CF_MPD           :    STD_LOGIC_VECTOR(15 downto 0);

    -- IIC Interface -------------------------------------------------------------
    IIC_Sda                 :    STD_LOGIC;
    IIC_Scl                 :    STD_LOGIC;

    

END RECORD;

TYPE MICROBLAZE_WRAPPER_INTERFACE_IN IS
RECORD

    -- SRAM Interface ------------------------------------------------------------
    SRAM_ZBT_CLK_FB         :  STD_LOGIC;

    -- SysACE CF Interface -------------------------------------------------------
    SysACE_CF_CLK           :  STD_LOGIC;
    SysACE_CF_MPIRQ         :  STD_LOGIC;

    -- MAC Interface -------------------------------------------------------------
    MAC_MII_TX_CLK          :  STD_LOGIC;
    MAC_GMII_RXD            :  STD_LOGIC_VECTOR(7 downto 0);
    MAC_GMII_RX_DV          :  STD_LOGIC;
    MAC_GMII_RX_ER          :  STD_LOGIC;
    MAC_GMII_RX_CLK         :  STD_LOGIC;
    MAC_PHY_MII_I           :  STD_LOGIC;  

    -- Picoblaze Interface -------------------------------------------------------
    PBLAZE_data_IN          :  STD_LOGIC_VECTOR(0 to 7);  --! Picoblaze interface - Data bus |PB| --> |UB|

    -- UART rx -------------------------------------------------------------------
    UART_Rx                 :  STD_LOGIC;

    -- Buttons -------------------------------------------------------------------
    BUTTONS                 :  STD_LOGIC_VECTOR(0 to 3);

END RECORD;


--------------------------------------------------------------------------------
END types_pkg;
--------------------------------------------------------------------------------
