--------------------------------------------------------------------------------
--
--
--
--
--             data_width   _________    TIG    _________    data_width
--                     /   |         |         |         |   /
--  sig_async ===>>>--/--->| D     Q |-------->| D     Q |--/-----===>>> sig_sync
--                   /     |         |         |         | /
--                      /->| C       |      /->| C       |
--                      |  |_________|      |  |_________|
--                      |                   |
--                      |                   |
--                      \-------------------+---------------------<<<=== clk
--
--
--
--------------------------------------------------------------------------------
-- simple synchronization module
--      shift n-bit data signal over clock domain boundary with TIG
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.vcomponents.ALL;
--------------------------------------------------------------------------------
ENTITY Sync_Nb_level IS
  GENERIC(
    data_width              : INTEGER := 8);
  PORT(
    sig_async               : IN    STD_LOGIC_VECTOR( (data_width-1) DOWNTO 0);         -- signal in original clock domain
    clk                     : IN    STD_LOGIC;                                          -- target clock domain
    sig_sync                : OUT   STD_LOGIC_VECTOR( (data_width-1) DOWNTO 0));        -- signal synchronized to target clock domain
END Sync_Nb_level;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF Sync_Nb_level IS
--------------------------------------------------------------------------------

  COMPONENT Sync_1b_level IS
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
  END COMPONENT;

--------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------

  sync_vector: FOR i IN 0 TO (data_width-1) GENERATE
    Sync_1b_level_i : Sync_1b_level PORT MAP(sig_async(i), clk, sig_sync(i));
  END GENERATE;

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
