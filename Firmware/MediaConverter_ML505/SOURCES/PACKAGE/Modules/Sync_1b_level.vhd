--------------------------------------------------------------------------------
--
--
--
--
--                          _________    TIG    _________
--                         |         |         |         |
--  sig_async ===>>>------>| D     Q |-------->| D     Q |--------===>>> sig_sync
--                         |         |         |         |
--                      /->| C       |      /->| C       |
--                      |  |_________|      |  |_________|
--                      |                   |
--                      |                   |
--                      \-------------------+---------------------<<<=== clk
--
--
--
--------------------------------------------------------------------------------
-- simple synchronization module
--      shift 1-bit data signal over clock domain boundary with TIG
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.vcomponents.ALL;
--------------------------------------------------------------------------------
ENTITY Sync_1b_level IS
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
END Sync_1b_level;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF Sync_1b_level IS
--------------------------------------------------------------------------------

  SIGNAL reset_sync_reg1    : STD_LOGIC := '0';
  SIGNAL reset_sync_reg2    : STD_LOGIC := '0';

  ATTRIBUTE ASYNC_REG                           : STRING;
  ATTRIBUTE ASYNC_REG OF reset_sync_reg1        : SIGNAL IS "TRUE";
  ATTRIBUTE ASYNC_REG OF reset_sync_reg2        : SIGNAL IS "TRUE";

  -- These attributes will stop XST translating the desired flip-flops into an
  -- SRL based shift register.
  ATTRIBUTE SHREG_EXTRACT                       : STRING;
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg1    : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg2    : SIGNAL IS "NO";

--------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------

  reset_sync1 : FDPE
  GENERIC MAP(
    INIT => '0')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => '0',
    D    => sig_async,
    Q    => reset_sync_reg1);

  ------------------------------------------------------------------------------

  reset_sync2 : FDPE
  GENERIC MAP(
    INIT => '0')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => '0',
    D    => reset_sync_reg1,
    Q    => reset_sync_reg2);

  ------------------------------------------------------------------------------

  sig_sync <= reset_sync_reg2;

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
