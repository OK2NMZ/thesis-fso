--------------------------------------------------------------------------------
--
--
--
--
--                          _________    TIG    _________
--                         |         |         |         |
--  sig_async ===>>>------>| D     Q |-------->| D     Q |--------===>>> sig_sync
--                         |         |         |         |
--                      /->| C       |      /->| C       |
--                      |  |_________|      |  |_________|
--                      |                   |
--                      |                   |
--                      \-------------------+---------------------<<<=== clk
--
--
--
--------------------------------------------------------------------------------
-- simple synchronization module
--      shift 1-bit data signal over clock domain boundary with TIG
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.vcomponents.ALL;
--------------------------------------------------------------------------------
ENTITY Sync_1b_level_adj IS
  GENERIC(
    flops                   : INTEGER := 2);
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
END Sync_1b_level_adj;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF Sync_1b_level_adj IS
--------------------------------------------------------------------------------

  SIGNAL Shift_Reg          : STD_LOGIC_VECTOR(flops DOWNTO 1) := (OTHERS => '0');

  ATTRIBUTE ASYNC_REG                           : STRING;
  ATTRIBUTE ASYNC_REG OF Shift_Reg              : SIGNAL IS "TRUE";

  -- These attributes will stop XST translating the desired flip-flops into an
  -- SRL based shift register.
  ATTRIBUTE SHREG_EXTRACT                       : STRING;
  ATTRIBUTE SHREG_EXTRACT OF Shift_Reg          : SIGNAL IS "NO";

--------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------

  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      Shift_Reg <= Shift_Reg(Shift_Reg'HIGH-1 DOWNTO 1) & sig_async;
    END IF;
  END PROCESS;

  ------------------------------------------------------------------------------

  sig_sync <= Shift_Reg(Shift_Reg'HIGH);

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
