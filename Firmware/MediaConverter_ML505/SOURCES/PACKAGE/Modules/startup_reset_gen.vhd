--------------------------------------------------------------------------------
-- Reset after start-up signal generator
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--------------------------------------------------------------------------------
ENTITY startup_reset_gen IS
  GENERIC(
    delay_clk_cycles    : INTEGER := 40);
  PORT(
    clk                 : IN  STD_LOGIC;
    rst                 : OUT STD_LOGIC);
END startup_reset_gen;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF startup_reset_gen IS
--------------------------------------------------------------------------------

  SIGNAL cnt_rst        : UNSIGNED((LOG(delay_clk_cycles)) DOWNTO 0) := (OTHERS => '0');
  SIGNAL rst_i          : STD_LOGIC := '1';

--------------------------------------------------------------------------------
BEGIN
--------------------------------------------------------------------------------

  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF cnt_rst(cnt_rst'HIGH) = '0' THEN
        cnt_rst <= cnt_rst + 1;
        rst_i <= '1';
      ELSE
        rst_i <= '0';
      END IF;
    END IF;
  END PROCESS;

  rst <= rst_i;

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
