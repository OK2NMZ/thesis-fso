--------------------------------------------------------------------------------
-- Cascadable N_bit counter 0 to cnt_max
-- counter is incremented at rising edge when carry_in is active
-- at the last state (when cnt_max is on output) the carry_out is asserted
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--------------------------------------------------------------------------------
ENTITY counter_cascadable IS
  GENERIC(
    N_bit           : INTEGER := 4;
    cnt_max         : INTEGER := 9);
  PORT(
    clk             : IN  STD_LOGIC;
    rst             : IN  STD_LOGIC;

    carry_in        : IN  STD_LOGIC;
    carry_out       : OUT STD_LOGIC;

    cnt_out         : OUT STD_LOGIC_VECTOR( (N_bit-1) DOWNTO 0));
END counter_cascadable;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF counter_cascadable IS
--------------------------------------------------------------------------------

  SIGNAL cnt_i          : UNSIGNED( (N_bit-1) DOWNTO 0) := (OTHERS => '0');
  SIGNAL carry_out_i    : STD_LOGIC := '0';

--------------------------------------------------------------------------------
BEGIN
--------------------------------------------------------------------------------

  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF rst = '1' THEN
        cnt_i       <= (OTHERS => '0');
        carry_out_i <= '0';
      ELSE
        IF carry_in = '1' THEN

          IF cnt_i = TO_UNSIGNED(cnt_max,N_bit) THEN
            cnt_i <= (OTHERS => '0');
          ELSE
            cnt_i <= cnt_i + 1;
          END IF;

          IF cnt_i = TO_UNSIGNED(cnt_max-1,N_bit) THEN
            carry_out_i <= '1';
          ELSE
            carry_out_i <= '0';
          END IF;

        END IF;
      END IF;
    END IF;
  END PROCESS;

  ------------------------------------------------------------------------------

  cnt_out   <= STD_LOGIC_VECTOR(cnt_i);
  carry_out <= carry_out_i;

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
