--------------------------------------------------------------------------------
--                                                                                                               _________
--                                                                                                  _____       |         |
--                                                                         /---------------------->|     |----->| D     Q |----===>>> sig_sync
--                                                                         |                       | AND |      |         |
--                                                                         |                  /-->O|_____|   /->| C       |
--                                                                         |                  |              |  |_________|
--                                                                         |                  |              |
--                                                                         |                  |              |
--                                                                         |                  |              |
--                                                                         |                  |              |
--             _____        _________        _________        _________    |     _________    |              |
--            |     |      |         |      |         |      |         |   |    |         |   |              |
--            | '0' |----->| D     Q |----->| D     Q |----->| D     Q |---+--->| D     Q |---/              |
--            |_____|      |         |      |         |      |         |        |         |                  |
--                      /->| C       |   /->| C       |   /->| C       |     /->| C       |                  |
--                      |  |___PRE___|   |  |___PRE___|   |  |___PRE___|     |  |___PRE___|                  |
--                      |       |        |       |        |                  |                               |
--                      |       |        |       |        |                  |                               |
--                      \-------|--------+-------|--------+------------------+-------------------------------+------------------<<<=== clk
--                              |                |
--  sig_async ===>>>------------+----------------/
--
--
--------------------------------------------------------------------------------
-- simple synchronization module
--      synchronizes positive pulse between two clock domains
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.vcomponents.ALL;
--------------------------------------------------------------------------------
ENTITY Sync_1b_pulse_p IS
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
END Sync_1b_pulse_p;
--------------------------------------------------------------------------------
ARCHITECTURE RTL OF Sync_1b_pulse_p IS
--------------------------------------------------------------------------------

  SIGNAL Q_1    : STD_LOGIC;
  SIGNAL Q_2    : STD_LOGIC;
  SIGNAL Q_3    : STD_LOGIC;
  SIGNAL Q_4    : STD_LOGIC;
  SIGNAL Q_5    : STD_LOGIC;

  SIGNAL compare_out        : STD_LOGIC;

  ATTRIBUTE ASYNC_REG                           : STRING;
  ATTRIBUTE ASYNC_REG OF Q_1        : SIGNAL IS "TRUE";
  ATTRIBUTE ASYNC_REG OF Q_2        : SIGNAL IS "TRUE";
  ATTRIBUTE ASYNC_REG OF Q_3        : SIGNAL IS "TRUE";

  -- These attributes will stop XST translating the desired flip-flops into an
  -- SRL based shift register.
  ATTRIBUTE SHREG_EXTRACT                       : STRING;
  ATTRIBUTE SHREG_EXTRACT OF Q_1    : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF Q_2    : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF Q_3    : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF Q_4    : SIGNAL IS "NO";

--------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------

  reset_sync1 : FDPE
  GENERIC MAP(
    INIT => '0')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => sig_async,
    D    => '0',
    Q    => Q_1);

  ------------------------------------------------------------------------------

  reset_sync2 : FDPE
  GENERIC MAP(
    INIT => '0')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => sig_async,
    D    => Q_1,
    Q    => Q_2);

  ------------------------------------------------------------------------------

  reset_sync3 : FDPE
  GENERIC MAP(
    INIT => '0')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => '0',
    D    => Q_2,
    Q    => Q_3);

  ------------------------------------------------------------------------------

  reset_sync4 : FDPE
  GENERIC MAP(
    INIT => '0')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => '0',
    D    => Q_3,
    Q    => Q_4);

  ------------------------------------------------------------------------------

   compare_out <= Q_3 AND NOT Q_4;

  ------------------------------------------------------------------------------

  reset_sync5 : FDPE
  GENERIC MAP(
    INIT => '0')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => '0',
    D    => compare_out,
    Q    => Q_5);

  ------------------------------------------------------------------------------

  sig_sync <= Q_5;

--------------------------------------------------------------------------------
END RTL;
--------------------------------------------------------------------------------
