--------------------------------------------------------------------------------
-- simple synchronization module
--      forward one-bit status/control signal over clock domain boundary
--      suitable for active-high signals
--      short pulses of logic low are not propagated
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.vcomponents.ALL;
--------------------------------------------------------------------------------
ENTITY sync_sig IS
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_clk                 : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
END sync_sig;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF sync_sig IS
--------------------------------------------------------------------------------

  SIGNAL reset_sync_reg1    : STD_LOGIC := '0';
  SIGNAL reset_sync_reg2    : STD_LOGIC := '0';
  SIGNAL reset_sync_reg3    : STD_LOGIC := '0';
  SIGNAL reset_sync_reg4    : STD_LOGIC := '0';

  ATTRIBUTE ASYNC_REG                           : STRING;
  ATTRIBUTE ASYNC_REG OF reset_sync_reg1        : SIGNAL IS "TRUE";
  ATTRIBUTE ASYNC_REG OF reset_sync_reg2        : SIGNAL IS "TRUE";
  ATTRIBUTE ASYNC_REG OF reset_sync_reg3        : SIGNAL IS "TRUE";
  ATTRIBUTE ASYNC_REG OF reset_sync_reg4        : SIGNAL IS "TRUE";

  -- These attributes will stop XST translating the desired flip-flops into an
  -- SRL based shift register.
  ATTRIBUTE SHREG_EXTRACT                       : STRING;
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg1    : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg2    : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg3    : SIGNAL IS "NO";
  ATTRIBUTE SHREG_EXTRACT OF reset_sync_reg4    : SIGNAL IS "NO";

--------------------------------------------------------------------------------
BEGIN
--------------------------------------------------------------------------------
--
--
--
--
--  ===>>>-----------------+------------------------\
--                         |                        |
--                         |                        |
--                     ____V____                ____V____                _________                _________
--          ___       |   PRE   |              |   PRE   |              |   PRE   |              |   PRE   |
--         |   |      |         |              |         |              |         |              |         |
--         | 0 |----->| D     Q |------------->| D     Q |------------->| D     Q |------------->| D     Q |-----------===>>>
--         |___|      |         |              |         |              |         |              |         |
--                 /->| C       |           /->| C       |           /->| C       |           /->| C       |
--                 |  |_________|           |  |_________|           |  |_________|           |  |_________|
--                 |                        |                        |                        |
--                 |                        |                        |                        |
--                 \------------------------+------------------------+------------------------+------------------------<<<===
--
--
--
--
--------------------------------------------------------------------------------

  reset_sync1 : FDPE
  GENERIC MAP(
    INIT => '1')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => sig_async,
    D    => '0',
    Q    => reset_sync_reg1);

  reset_sync2 : FDPE
  GENERIC MAP(
    INIT => '1')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => sig_async,
    D    => reset_sync_reg1,
    Q    => reset_sync_reg2);

  reset_sync3 : FDPE
  GENERIC MAP(
    INIT => '1')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => '0',
    D    => reset_sync_reg2,
    Q    => reset_sync_reg3);

  reset_sync4 : FDPE
  GENERIC MAP(
    INIT => '1')
  PORT MAP(
    C    => clk,
    CE   => '1',
    PRE  => '0',
    D    => reset_sync_reg3,
    Q    => reset_sync_reg4);

  sig_clk <= reset_sync_reg4;

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
