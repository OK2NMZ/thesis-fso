--------------------------------------------------------------------------------
-- simple pulse stretcher
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.vcomponents.ALL;
--------------------------------------------------------------------------------
ENTITY pulse_stretcher IS
  GENERIC(
    periods         : INTEGER := 10);           -- stretched pulse length (number of clock periods)
  PORT(
    pulse_S_p       : IN    STD_LOGIC;          -- short pulse input
    clk             : IN    STD_LOGIC;          -- clock
    pulse_L_p       : OUT   STD_LOGIC);         -- stretched (long) pulse output
END pulse_stretcher;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF pulse_stretcher IS
--------------------------------------------------------------------------------

  SIGNAL cnt_stretch        : INTEGER RANGE 1 TO periods := 1;
  SIGNAL pulse_L_p_i        : STD_LOGIC := '0';

--------------------------------------------------------------------------------
BEGIN
--------------------------------------------------------------------------------

  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN

      IF pulse_S_p = '1' THEN
        cnt_stretch <= 1;
        pulse_L_p_i <= '1';
      ELSE
        IF cnt_stretch = periods THEN
          pulse_L_p_i <= '0';
        ELSE
          cnt_stretch <= cnt_stretch + 1;
        END IF;
      END IF;

    END IF;
  END PROCESS;

  pulse_L_p <= pulse_L_p_i;

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
