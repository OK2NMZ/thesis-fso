--------------------------------------------------------------------------------
-- Check functionality of a clock signal by comparing it to a reference clock
--      (known to be good)
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.modules_pkg.ALL;
--------------------------------------------------------------------------------
ENTITY clk_checker IS
  GENERIC(
    clk_ref_cycles          : INTEGER := 100;
    clk_check_min           : INTEGER :=  80;
    clk_check_max           : INTEGER := 120);
  PORT(     
    clk_ref                 : IN  STD_LOGIC;
    clk_check               : IN  STD_LOGIC;
    clk_OK                  : OUT STD_LOGIC);
END clk_checker;
--------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF clk_checker IS
--------------------------------------------------------------------------------

  SIGNAL cnt_ref        : INTEGER RANGE 1 TO clk_ref_cycles+4 := 1;

  SIGNAL rst_cnt_check  : STD_LOGIC := '0';
  SIGNAL cnt_check      : INTEGER RANGE 1 TO clk_check_max+1  := 1;

  SIGNAL clk_OK_i       : STD_LOGIC := '0';
  SIGNAL clk_OK_ii      : STD_LOGIC;
  SIGNAL clk_OK_iii     : STD_LOGIC := '0';

--------------------------------------------------------------------------------
BEGIN
--------------------------------------------------------------------------------

  -- reference clock counter
  PROCESS(clk_ref) BEGIN
    IF rising_edge(clk_ref) THEN

      IF cnt_ref = clk_ref_cycles+1 THEN
        rst_cnt_check   <= '0';
        cnt_ref         <= cnt_ref + 1;

      ELSIF cnt_ref = clk_ref_cycles+4 THEN
        rst_cnt_check   <= '1';
        cnt_ref         <=  1;
        clk_OK_iii      <= clk_OK_ii;

      ELSE
        rst_cnt_check   <= '0';
        cnt_ref         <= cnt_ref + 1;

      END IF;
    END IF;
  END PROCESS;

  clk_OK <= clk_OK_iii;

  ------------------------------------------------------------------------------

  -- clk_check counter
  PROCESS(rst_cnt_check, clk_check) BEGIN
    IF rst_cnt_check = '1' THEN
      cnt_check     <=  1;
      clk_OK_i      <= '0';
    ELSIF rising_edge(clk_check) THEN

      IF cnt_check = clk_check_min THEN clk_OK_i <= '1'; END IF;

      IF cnt_check > clk_check_max THEN
        clk_OK_i <= '0';
      ELSE
        cnt_check <= cnt_check + 1;
      END IF;

    END IF;
  END PROCESS;

  ------------------------------------------------------------------------------

  Sync_clk_OK : Sync_1b_level
  PORT MAP(
    sig_async   => clk_OK_i,
    clk         => clk_ref,
    sig_sync    => clk_OK_ii);

--------------------------------------------------------------------------------
END Behavioral;
--------------------------------------------------------------------------------
