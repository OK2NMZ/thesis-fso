--------------------------------------------------------------------------------
-- use WORK.modules_pkg.ALL;
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;
use WORK.parameters_pkg.ALL;
--------------------------------------------------------------------------------
PACKAGE modules_pkg IS
--------------------------------------------------------------------------------

  COMPONENT counter_cascadable
  GENERIC(
    N_bit                   : INTEGER := 4;
    cnt_max                 : INTEGER := 9);
  PORT(
    clk                     : IN  STD_LOGIC;
    rst                     : IN  STD_LOGIC;
    carry_in                : IN  STD_LOGIC;
    carry_out               : OUT STD_LOGIC;
    cnt_out                 : OUT STD_LOGIC_VECTOR( (N_bit-1) DOWNTO 0));
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT sync_sig
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_clk                 : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT Sync_1b_level
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT Sync_1b_level_adj
  GENERIC(
    flops                   : INTEGER := 2);
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT Sync_Nb_level
  GENERIC(
    data_width              : INTEGER := 8);
  PORT(
    sig_async               : IN    STD_LOGIC_VECTOR( (data_width-1) DOWNTO 0);         -- signal in original clock domain
    clk                     : IN    STD_LOGIC;                                          -- target clock domain
    sig_sync                : OUT   STD_LOGIC_VECTOR( (data_width-1) DOWNTO 0));        -- signal synchronized to target clock domain
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT Sync_Nb_level_adj
  GENERIC(
    data_width              : INTEGER := 8;
    flops                   : INTEGER := 2);
  PORT(
    sig_async               : IN    STD_LOGIC_VECTOR( (data_width-1) DOWNTO 0);         -- signal in original clock domain
    clk                     : IN    STD_LOGIC;                                          -- target clock domain
    sig_sync                : OUT   STD_LOGIC_VECTOR( (data_width-1) DOWNTO 0));        -- signal synchronized to target clock domain
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT Sync_1b_pulse_p
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT Sync_1b_pulse_n
  PORT(
    sig_async               : IN    STD_LOGIC;          -- signal in original clock domain
    clk                     : IN    STD_LOGIC;          -- target clock domain
    sig_sync                : OUT   STD_LOGIC);         -- signal synchronized to target clock domain
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT pulse_stretcher
  GENERIC(
    periods                 : INTEGER := 10);           -- stretched pulse length (number of clock periods)
  PORT(
    pulse_S_p               : IN    STD_LOGIC;          -- short pulse input
    clk                     : IN    STD_LOGIC;          -- clock
    pulse_L_p               : OUT   STD_LOGIC);         -- stretched (long) pulse output
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT clk_checker
  GENERIC(
    clk_ref_cycles          : INTEGER := 100;
    clk_check_min           : INTEGER :=  80;
    clk_check_max           : INTEGER := 120);
  PORT(
    clk_ref                 : IN  STD_LOGIC;
    clk_check               : IN  STD_LOGIC;
    clk_OK                  : OUT STD_LOGIC);
  END COMPONENT;

--------------------------------------------------------------------------------
END modules_pkg;
--------------------------------------------------------------------------------
