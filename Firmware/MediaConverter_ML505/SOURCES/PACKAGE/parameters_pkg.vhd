--------------------------------------------------------------------------------
-- use WORK.parameters_pkg.ALL;
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;
--------------------------------------------------------------------------------
PACKAGE parameters_pkg IS
--------------------------------------------------------------------------------


-- PicoBlaze peripherals PORT IDs
--CONSTANT ID_SFP_control               : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";
  CONSTANT ID_SFP_data                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"01";
  CONSTANT ID_SFP_addr                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"02";
  CONSTANT ID_SFP_stat                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"03";
  CONSTANT ID_SFP_TW_addr               : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"04";

  CONSTANT ID_PHY_datL                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"08";
  CONSTANT ID_PHY_datH                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"09";
  CONSTANT ID_PHY_addr                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0A";
  CONSTANT ID_PHY_SMI_addr_reg          : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0B";
  CONSTANT ID_PHY_reset_reg             : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0C";

  CONSTANT ID_UART_data_PORT            : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"10";
  CONSTANT ID_UART_stat_PORT            : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"11";
  CONSTANT ID_UART_reset                : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"12";

  CONSTANT ID_Seq_sel                   : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"20";

  CONSTANT ID_INT_controler_EN          : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"21";
  CONSTANT ID_INT_controler_SENSE       : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"22";
  CONSTANT ID_INT_controler_STATUS      : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"23";

  CONSTANT ID_GTP_reset                 : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"30";

  CONSTANT ID_SFP_Tx_Inhibit_REG        : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"31";
  CONSTANT ID_SFP_Tx_Polarity_REG       : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"32";
  CONSTANT ID_SFP_Tx_Swing_REG          : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"33";
  CONSTANT ID_SFP_Tx_Preemphasis_REG    : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"34";
  CONSTANT ID_SFP_Rx_EqMix_REG          : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"35";
  CONSTANT ID_SFP_Rx_EqPole_REG         : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"36";

  CONSTANT ID_Text_ROM_1                : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"40";

  CONSTANT reg_HOST_1_data_0_ID : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"51";
  CONSTANT reg_HOST_1_data_1_ID : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"52";
  CONSTANT reg_HOST_1_data_2_ID : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"53";
  CONSTANT reg_HOST_1_data_3_ID : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"54";
  CONSTANT reg_HOST_1_addr_L_ID : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"55";
  CONSTANT reg_HOST_1_addr_H_ID : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"56";

  CONSTANT ID_GPIO_LED                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"70";
  CONSTANT ID_GPIO_DIP                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"71";
  CONSTANT ID_GPIO_BTN                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"72";

  CONSTANT ID_BERT_Err_1s_00            : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"83";
  CONSTANT ID_BERT_Err_1s_01            : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"84";
  CONSTANT ID_BERT_Err_1s_02            : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"85";
  CONSTANT ID_BERT_Err_1s_03            : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"86";

  CONSTANT ID_SPI_RAM_Address           : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"A0";
  CONSTANT ID_SPI_RAM_Data              : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"A1";
  CONSTANT ID_SPI_global_status         : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"A2";

  CONSTANT ID_LCD_1_00                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B0";
  CONSTANT ID_LCD_1_01                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B1";
  CONSTANT ID_LCD_1_02                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B2";
  CONSTANT ID_LCD_1_03                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B3";
  CONSTANT ID_LCD_1_04                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B4";
  CONSTANT ID_LCD_1_05                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B5";
  CONSTANT ID_LCD_1_06                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B6";
  CONSTANT ID_LCD_1_07                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B7";
  CONSTANT ID_LCD_1_08                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B8";
  CONSTANT ID_LCD_1_09                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"B9";
  CONSTANT ID_LCD_1_10                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"BA";
  CONSTANT ID_LCD_1_11                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"BB";
  CONSTANT ID_LCD_1_12                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"BC";
  CONSTANT ID_LCD_1_13                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"BD";
  CONSTANT ID_LCD_1_14                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"BE";
  CONSTANT ID_LCD_1_15                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"BF";
  CONSTANT ID_LCD_2_00                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C0";
  CONSTANT ID_LCD_2_01                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C1";
  CONSTANT ID_LCD_2_02                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C2";
  CONSTANT ID_LCD_2_03                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C3";
  CONSTANT ID_LCD_2_04                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C4";
  CONSTANT ID_LCD_2_05                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C5";
  CONSTANT ID_LCD_2_06                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C6";
  CONSTANT ID_LCD_2_07                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C7";
  CONSTANT ID_LCD_2_08                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C8";
  CONSTANT ID_LCD_2_09                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"C9";
  CONSTANT ID_LCD_2_10                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"CA";
  CONSTANT ID_LCD_2_11                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"CB";
  CONSTANT ID_LCD_2_12                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"CC";
  CONSTANT ID_LCD_2_13                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"CD";
  CONSTANT ID_LCD_2_14                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"CE";
  CONSTANT ID_LCD_2_15                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"CF";
  
  CONSTANT ID_MUX_Select                : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"D0";

  CONSTANT ID_SYS_reset                 : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"FF";

  ---------------------------------------------------------------------

--CONSTANT UART_period                  : INTEGER := 13020;                     --   9.600 Bd @ 125.00 MHz
  CONSTANT UART_period                  : INTEGER :=  1085;                     -- 115.200 Bd @ 125.00 MHz

  CONSTANT I2C_clk_div                  : INTEGER :=  500;                      --  0.125 MHz @ 125.00 MHz
--CONSTANT I2C_clk_div                  : INTEGER :=   25;                      --  2.500 MHz @ 125.00 MHz
--CONSTANT I2C_clk_div                  : INTEGER :=    5;                      -- 12.500 MHz @ 125.00 MHz
  ---------------------------------------------------------------------
  CONSTANT C_Comma              : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"BC";    -- K_28_5; /C/ Comma



  ------------------------------------------------------------------------------
  -- HW build number; reported by PicoBlaze in terminal console
  ------------------------------------------------------------------------------

  CONSTANT C_HW_build               : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"01";

--------------------------------------------------------------------------------
END parameters_pkg;
--------------------------------------------------------------------------------
