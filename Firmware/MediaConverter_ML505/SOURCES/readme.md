To generate the DoxyGen documentation, please follow these steps:
- in console: sudo apt-get install doxygen doxygen-gui
- type 'doxywizard Doxyfile' to observe the configuration, modify it if you want to
- in the wizard, at the last tab, click generate
- have a nice day
