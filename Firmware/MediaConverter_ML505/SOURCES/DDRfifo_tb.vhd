--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:38:44 02/05/2016
-- Design Name:   
-- Module Name:   /home/ok2nmz/Desktop/DP/thesis-fso/Firmware/MediaConverter_ML505/SOURCES/DDRfifo_tb.vhd
-- Project Name:  MediaConverter_ML505_v5
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: DDRfifo
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY DDRfifo_tb IS
END DDRfifo_tb;
 
ARCHITECTURE behavior OF DDRfifo_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DDRfifo
    PORT(
         rst : IN  std_logic;
         wr_clk : IN  std_logic;
         rd_clk : IN  std_logic;
         din : IN  std_logic_vector(7 downto 0);
         wr_en : IN  std_logic;
         rd_en : IN  std_logic;
         dout : OUT  std_logic_vector(7 downto 0);
         full : OUT  std_logic;
         empty : OUT  std_logic;
         sys_rst_n : OUT  std_logic;
         phy_init_done : IN  std_logic;
         rst0_tb : IN  std_logic;
         clk0_tb : IN  std_logic;
         app_wdf_afull : IN  std_logic;
         app_af_afull : IN  std_logic;
         rd_data_valid : IN  std_logic;
         app_wdf_wren : OUT  std_logic;
         app_af_wren : OUT  std_logic;
         app_af_addr : OUT  std_logic_vector(30 downto 0);
         app_af_cmd : OUT  std_logic_vector(2 downto 0);
         rd_data_fifo_out : IN  std_logic_vector(127 downto 0);
         app_wdf_data : OUT  std_logic_vector(127 downto 0);
         app_wdf_mask_data : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal rst : std_logic := '0';
   signal wr_clk : std_logic := '0';
   signal rd_clk : std_logic := '0';
   signal din : std_logic_vector(7 downto 0) := (others => '0');
   signal wr_en : std_logic := '0';
   signal rd_en : std_logic := '0';
   signal phy_init_done : std_logic := '0';
   signal rst0_tb : std_logic := '0';
   signal clk0_tb : std_logic := '0';
   signal app_wdf_afull : std_logic := '0';
   signal app_af_afull : std_logic := '0';
   signal rd_data_valid : std_logic := '0';
   signal rd_data_fifo_out : std_logic_vector(127 downto 0) := (others => '0');

 	--Outputs
   signal dout : std_logic_vector(7 downto 0);
   signal full : std_logic;
   signal empty : std_logic;
   signal sys_rst_n : std_logic;
   signal app_wdf_wren : std_logic;
   signal app_af_wren : std_logic;
   signal app_af_addr : std_logic_vector(30 downto 0);
   signal app_af_cmd : std_logic_vector(2 downto 0);
   signal app_wdf_data : std_logic_vector(127 downto 0);
   signal app_wdf_mask_data : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant wr_clk_period : time := 8 ns;
   constant rd_clk_period : time := 8 ns;
   constant clk0_tb_period : time := 4 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DDRfifo PORT MAP (
          rst => rst,
          wr_clk => wr_clk,
          rd_clk => rd_clk,
          din => din,
          wr_en => wr_en,
          rd_en => rd_en,
          dout => dout,
          full => full,
          empty => empty,
          sys_rst_n => sys_rst_n,
          phy_init_done => phy_init_done,
          rst0_tb => rst0_tb,
          clk0_tb => clk0_tb,
          app_wdf_afull => app_wdf_afull,
          app_af_afull => app_af_afull,
          rd_data_valid => rd_data_valid,
          app_wdf_wren => app_wdf_wren,
          app_af_wren => app_af_wren,
          app_af_addr => app_af_addr,
          app_af_cmd => app_af_cmd,
          rd_data_fifo_out => rd_data_fifo_out,
          app_wdf_data => app_wdf_data,
          app_wdf_mask_data => app_wdf_mask_data
        );

   -- Clock process definitions
   wr_clk_process :process
   begin
		wr_clk <= '0';
		wait for wr_clk_period/2;
		wr_clk <= '1';
		wait for wr_clk_period/2;
   end process;
 
   rd_clk_process :process
   begin
		rd_clk <= '0';
		wait for rd_clk_period/2;
		rd_clk <= '1';
		wait for rd_clk_period/2;
   end process;
 
   clk0_tb_process :process
   begin
		clk0_tb <= '0';
		wait for clk0_tb_period/2;
		clk0_tb <= '1';
		wait for clk0_tb_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
	
		rst <= '1';
		rst0_tb <= '1';
      wait for clk0_tb_period*10;
		rst <= '0';
		rst0_tb <= '0';
		wait for clk0_tb_period*10;
		phy_init_done <= '1';
		wait for clk0_tb_period*4;
		
		rd_data_fifo_out <= X"123456789abcdef0123456789abcdef0";
		
		wr_en <= '1';
		for I in 0 to 256 loop
			din <= X"12";
			wait for wr_clk_period;
			din <= X"34";
			wait for wr_clk_period;
			din <= X"56";
			wait for wr_clk_period;
			din <= X"78";
			wait for wr_clk_period;
			din <= X"9a";
			wait for wr_clk_period;
			din <= X"bc";
			wait for wr_clk_period;
			din <= X"de";
			wait for wr_clk_period;
			din <= X"f0";
			wait for wr_clk_period;
		end loop;

		wr_en <= '0';
		
		wait for clk0_tb_period*32;
		rd_data_valid <= '1';
		wait for clk0_tb_period*16;
		rd_data_valid <= '0';
		wait for clk0_tb_period*16;
		rd_data_valid <= '1';
		wait for clk0_tb_period*16;
		rd_data_valid <= '0';
		wait for clk0_tb_period*16;
		rd_data_valid <= '1';
		
		rd_en <= '1';
		for I in 0 to 256 loop
			
			wait for wr_clk_period;
			wait for wr_clk_period;
			wait for wr_clk_period;
			wait for wr_clk_period;
			wait for wr_clk_period;
			wait for wr_clk_period;
			wait for wr_clk_period;
			wait for wr_clk_period;
		end loop;

		rd_en <= '0';

      -- insert stimulus here 

      wait;
   end process;

END;
