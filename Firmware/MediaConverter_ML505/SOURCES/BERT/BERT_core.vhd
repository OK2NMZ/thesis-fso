--#####################################################################
--
-- Access to peripheral registers:
--  => Determine the target register by writing appropriate address to
--     "Internal Address Register" (Address_port_ID).
--  => Access the desired data register (specified in "Internal
--     Address Register") by using "Data_port_ID" address.
--
-----------------------------------------------------------------------
-- Address | Description                                              |
-----------------------------------------------------------------------
--    00h  | Tx PRBS setting register (Tx_PRBS_reg)                   |
--    01h  | Rx PRBS setting register (Rx_PRBS_reg)                   |
--    02h  | Interrupt time control register; LSB                     |
--    03h  | Interrupt time control register                          |
--    04h  | Interrupt time control register                          |
--    05h  | Interrupt time control register; MSB                     |
--         |   Number of bytes to be received before an interrupt     |
--         |   is requested. Only lower nibble of MSB is used,        |
--         |   the upper one is ignored.                              |
-----------------------------------------------------------------------
--    11h  | BERT_1 err_0  (LSB)                                        |
--    12h  | BERT_1 err_1                                               |
--    13h  | BERT_1 err_2                                               |
--    14h  | BERT_1 err_3  (MSB)                                        |
--    15h  | BERT_1 load_0 (LSB)                                        |
--    16h  | BERT_1 load_1                                              |
--    17h  | BERT_1 load_2                                              |
--    18h  | BERT_1 load_3 (MSB)                                        |
-----------------------------------------------------------------------
--
--
-----------------------------------------------------------------------
-- PRBS_reg: PRBS generator control input (register)                  |
-----------------------------------------------------------------------
--  PRBS_reg(2 DOWNTO 0) = PRBS sequence                              |
--                         000: 2^3  Fbk(2,3)                         |
--                         001: 2^5  Fbk(3,5)                         |
--                         010: 2^7  Fbk(6,7)                         |
--                         011: 2^9  Fbk(5,9)                         |
--                         100: 2^11 Fbk(9,11)                        |
--                         101: 2^15 Fbk(14,15)                       |
--                         110: 2^20 Fbk(17,20)                       |
--                         111: 2^23 Fbk(18,23)                       |
--                                                                    |
--  PRBS_reg(3)          = inversion of PRBS sequence                 |
--                                                                    |
-----------------------------------------------------------------------
--
--#####################################################################
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VComponents.all;
--#####################################################################
ENTITY BERT_core IS
  GENERIC(
    Address_port_ID : IN  STD_LOGIC_VECTOR(7 DOWNTO 0):=X"01";   -- Address of "internal address register"
    Data_port_ID    : IN  STD_LOGIC_VECTOR(7 DOWNTO 0):=X"02");  -- Address of "data register"
  PORT(
    clk_Tx          : IN  STD_LOGIC;
    Tx_data         : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    Tx_EN           : IN  STD_LOGIC;

    clk_Rx          : IN  STD_LOGIC;
    Rx_data         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    Rx_DV           : IN  STD_LOGIC;

    Address         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);         -- PicoBlaze Port ID
    Data_in         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);         -- Data IN (PicoBlaze OUT)
    Data_out        : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);         -- Data OUT (to PicoBlaze input MUX)
    write_strobe    : IN  STD_LOGIC;                            -- PicoBlaze write strobe

    Int_BERT_1      : OUT STD_LOGIC);                           -- Interrupt request CDR1
END BERT_core;
--#####################################################################
ARCHITECTURE Behavioral OF BERT_core IS
--#####################################################################
  -- Tx PRBS generator

  COMPONENT LFSR_Tx_Par PORT(
    clk             : IN  STD_LOGIC;
    Tx_EN           : IN  STD_LOGIC;
    seq_sel         : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);   -- selection of PRBS sequence
    Data_out        : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));  -- transmit LSB first, MSB last
  END COMPONENT;

--#####################################################################
  -- Single channel BERT (error checker and counter); use one BERT per tested CDR
  COMPONENT BERT_1CH PORT(
    clk:                IN  STD_LOGIC;                          -- System clock (freq >= 1/8 * Clk_Rx)
    rec_Data:           IN  STD_LOGIC_VECTOR(7 DOWNTO 0);       -- Rx data
    rec_DV:             IN  STD_LOGIC;                          -- Rx data valid
    seq_sel:            IN  STD_LOGIC_VECTOR(3 DOWNTO 0);       -- selection of PRBS sequence (for LFSR Rx)
    int_time:           IN  STD_LOGIC_VECTOR(31 DOWNTO 0);      -- number of bytes to receive before interrupt is set
    int_rq:             OUT STD_LOGIC;                          -- interrupt strobe, must be processed (catched) by processor interface block
    error_count:        OUT STD_LOGIC_VECTOR(31 DOWNTO 0);      -- number of "errors bits" since the last interrupt strobe
    load_count:         OUT STD_LOGIC_VECTOR(31 DOWNTO 0));     -- number of "loading bits" since the last interrupt strobe
  END COMPONENT;
--#####################################################################
  -- PicoBlaze interface registers
  SIGNAL ADDR_i         : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";          -- "Internal address" register
  SIGNAL Data_out_i     : STD_LOGIC_VECTOR( 7 DOWNTO 0);

  SIGNAL Tx_PRBS_reg    : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0A";          -- "Tx PRBS setting" register
  SIGNAL Rx_PRBS_reg    : STD_LOGIC_VECTOR( 3 DOWNTO 0):= X"A";           -- "Rx PRBS setting" register
  SIGNAL int_time_reg   : STD_LOGIC_VECTOR(31 DOWNTO 0):= X"00EE6B28";    -- interrupt period control register; 125 Mb/s

  SIGNAL BERT_1_err     : STD_LOGIC_VECTOR(31 DOWNTO 0):= X"00000000";    -- CDR1 number of error bits
  SIGNAL BERT_1_load    : STD_LOGIC_VECTOR(31 DOWNTO 0):= X"00000000";    -- CDR1 number of load bits
--#####################################################################
BEGIN
--#####################################################################
  -- Processor write access to "internal address register"
  set_internal_address: PROCESS(clk_Rx) BEGIN
    IF rising_edge(clk_Rx) THEN

      -- valid access to the "internal address register"
      IF Address = Address_port_ID AND write_strobe = '1' THEN
        ADDR_i <= data_in;
      END IF;
    END IF;
  END PROCESS set_internal_address;
  ---------------------------------------------------------------------
  -- Processor write access to data registers
  set_internal_register: PROCESS(clk_Rx) BEGIN
    IF rising_edge(clk_Rx) THEN
      -- valid access to the "internal address register"
      IF Address = Data_port_ID AND write_strobe = '1' THEN

        CASE ADDR_i IS
          -------------------------------------------------------------
          WHEN X"00"  => Tx_PRBS_reg <= Data_in;
          WHEN X"01"  => Rx_PRBS_reg <= Data_in(3 DOWNTO 0);
          WHEN X"02"  => int_time_reg( 7 DOWNTO  0) <= Data_in;     -- interrupt time LSB
          WHEN X"03"  => int_time_reg(15 DOWNTO  8) <= Data_in;
          WHEN X"04"  => int_time_reg(23 DOWNTO 16) <= Data_in;
          WHEN X"05"  => int_time_reg(31 DOWNTO 24) <= Data_in;     -- interrupt time MSB
          -------------------------------------------------------------
          WHEN OTHERS => NULL;
          -------------------------------------------------------------
        END CASE;
      END IF;
    END IF;
  END PROCESS set_internal_register;

  ---------------------------------------------------------------------


  ---------------------------------------------------------------------
  read_register: PROCESS (Tx_PRBS_reg, Rx_PRBS_reg, int_time_reg, ADDR_i, BERT_1_err, BERT_1_load)
  BEGIN
    CASE ADDR_i IS
      -------------------------------------------------------------
      WHEN X"00"  => Data_out_i <= Tx_PRBS_reg;                 -- Tx PRBS setting
      WHEN X"01"  => Data_out_i <= (X"0" & Rx_PRBS_reg);        -- Rx PRBS setting
      WHEN X"02"  => Data_out_i <= int_time_reg( 7 DOWNTO  0);  -- interrupt time LSB
      WHEN X"03"  => Data_out_i <= int_time_reg(15 DOWNTO  8);
      WHEN X"04"  => Data_out_i <= int_time_reg(23 DOWNTO 16);
      WHEN X"05"  => Data_out_i <= int_time_reg(31 DOWNTO 24);  -- interrupt time MSB
      -------------------------------------------------------------
      WHEN X"11"  => Data_out_i <= BERT_1_err ( 7 DOWNTO  0);
      WHEN X"12"  => Data_out_i <= BERT_1_err (15 DOWNTO  8);
      WHEN X"13"  => Data_out_i <= BERT_1_err (23 DOWNTO 16);
      WHEN X"14"  => Data_out_i <= BERT_1_err (31 DOWNTO 24);
      WHEN X"15"  => Data_out_i <= BERT_1_load( 7 DOWNTO  0);
      WHEN X"16"  => Data_out_i <= BERT_1_load(15 DOWNTO  8);
      WHEN X"17"  => Data_out_i <= BERT_1_load(23 DOWNTO 16);
      WHEN X"18"  => Data_out_i <= BERT_1_load(31 DOWNTO 24);
      -------------------------------------------------------------
      WHEN OTHERS => Data_out_i <= X"00";
    END CASE;
  END PROCESS read_register;

  Data_out <= Data_out_i;

  --------------------------------------------------------------------------------
  -- Tx LFSR generating PRBS test sequence
  --------------------------------------------------------------------------------

  LFSR_Tx_Par_i : LFSR_Tx_Par
  PORT MAP(
    clk             => clk_Tx,                      -- clk 125 MHz for 1 Gb/s
    Tx_EN           => Tx_EN,
    seq_sel         => Tx_PRBS_reg(3 DOWNTO 0),     -- can be asynchronous to clk_Tx
    Data_out        => Tx_data);                    -- PRBS data output; transmit LSB first, MSB last

  --------------------------------------------------------------------------------
  -- BERT
  --------------------------------------------------------------------------------

  BERT_1CH_i : BERT_1CH
  PORT MAP(
    clk             => clk_Rx,
    rec_Data        => Rx_data,
    rec_DV          => Rx_DV,
    seq_sel         => Rx_PRBS_reg,
    int_time        => int_time_reg,
    int_rq          => Int_BERT_1,
    error_count     => BERT_1_err,
    load_count      => BERT_1_load);

----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------
