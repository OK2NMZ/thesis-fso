-----------------------------------------------------------------------
--  PRBS_23:     Fbk(18,23)
-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.modules_pkg.ALL;
-----------------------------------------------------------------------
ENTITY LFSR_23_Rx_10b IS
  PORT(
    clk_Rx                  : IN  STD_LOGIC;                        -- Rx data clock
    data_Rx                 : IN  STD_LOGIC_VECTOR(9 DOWNTO 0);     -- Rx data vector; LSB is the first received (oldest) bit

    clk_sys                 : IN  STD_LOGIC;                        -- system (MCU) clock
    IRQ_1s                  : OUT STD_LOGIC;                        -- 1s interrupt request
    toggle_1s               : OUT STD_LOGIC;                        -- toggle each 1s (for data consistency check)
    Err_cnt_1s              : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);    -- number of errors in last 1s
    LFSR_locked             : OUT STD_LOGIC);                       -- PRBS is detected correctly (BER < 10%)
END LFSR_23_Rx_10b;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF LFSR_23_Rx_10b IS
-----------------------------------------------------------------------

  COMPONENT Err_cnt_elem_10b
  PORT(
    clk                     : IN  STD_LOGIC;
    Err_vec                 : IN  STD_LOGIC_VECTOR (9 DOWNTO 0);
    Err_count               : OUT STD_LOGIC_VECTOR (3 DOWNTO 0));
  END COMPONENT;

  ---------------------------------------------------------------------
  SIGNAL data_Rx_i          : STD_LOGIC_VECTOR( 9 DOWNTO 0):= (OTHERS => '0');        -- Received data signal; MSB is the first received (oldest) bit

  SIGNAL LFSR               : STD_LOGIC_VECTOR(22 DOWNTO 0):= (OTHERS => '0');
  CONSTANT LFSR_zeros       : STD_LOGIC_VECTOR(22 DOWNTO 0):= (OTHERS => '0');
  SIGNAL FBk                : STD_LOGIC_VECTOR( 9 DOWNTO 0);

  SIGNAL Err_vec_i          : STD_LOGIC_VECTOR( 9 DOWNTO 0) := (OTHERS => '0');
  SIGNAL Err_count_i        : STD_LOGIC_VECTOR( 3 DOWNTO 0) := (OTHERS => '0');

  TYPE t_Err_cnt_ShReg IS ARRAY (9 DOWNTO 0) OF STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL Err_cnt_ShReg      : t_Err_cnt_ShReg:= (OTHERS => X"0");                       -- shift reg of errors
  SIGNAL Err_cnt            : UNSIGNED( 7 DOWNTO 0) := (OTHERS => '0');

  -- threshold of number of errors in ShReg for LFSR reload request
  CONSTANT ShReg_max_err    : INTEGER := 10;            -- maximum errors in Err_cnt_ShReg

  SIGNAL Reload_req         : STD_LOGIC := '0';
  SIGNAL LFSR_Reload        : STD_LOGIC := '0';

  SIGNAL cnt_resync         : UNSIGNED( 3 DOWNTO 0) := (OTHERS => '0');

  SIGNAL LFSR_locked_i      : STD_LOGIC := '0';

  SIGNAL rst_Err_cnt        : STD_LOGIC := '0';


  SIGNAL cnt_1s             : UNSIGNED(31 DOWNTO 0) := (OTHERS => '0');


  -- DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
  -- DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
  CONSTANT cnt_1s_MAX       : UNSIGNED(31 DOWNTO 0) := TO_UNSIGNED(125000000-1,32);
--CONSTANT cnt_1s_MAX       : UNSIGNED(31 DOWNTO 0) := TO_UNSIGNED(1250-1,32);
  -- DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
  -- DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG

  SIGNAL IRQ_1s_i           : STD_LOGIC := '0';
  SIGNAL IRQ_1s_del         : STD_LOGIC := '0';
  SIGNAL toggle_1s_i        : STD_LOGIC := '0';

  SIGNAL Err_cnt_1s_u       : UNSIGNED(31 DOWNTO 0) := (OTHERS => '0');
  SIGNAL Err_cnt_1s_i       : STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');


-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  -----------------------------------------------------------------------
  -- Reorder received data (MSB/LSB)
  -----------------------------------------------------------------------

  data_reorder: PROCESS(clk_Rx) BEGIN
    IF rising_edge(clk_Rx) THEN
      data_Rx_i(0) <= data_Rx(9);
      data_Rx_i(1) <= data_Rx(8);
      data_Rx_i(2) <= data_Rx(7);
      data_Rx_i(3) <= data_Rx(6);
      data_Rx_i(4) <= data_Rx(5);
      data_Rx_i(5) <= data_Rx(4);
      data_Rx_i(6) <= data_Rx(3);
      data_Rx_i(7) <= data_Rx(2);
      data_Rx_i(8) <= data_Rx(1);
      data_Rx_i(9) <= data_Rx(0);
    END IF;
  END PROCESS data_reorder;

  -----------------------------------------------------------------------
  -- Reference LFSR
  -----------------------------------------------------------------------

  LFSR_Rx: PROCESS(clk_Rx) BEGIN
    IF rising_edge(clk_Rx) THEN

      -----------------------------------------------------------------
      -- Resynchronization of LFSR
      IF LFSR_Reload = '1' THEN
        LFSR <= LFSR(LFSR'HIGH-10 DOWNTO 0) & data_Rx_i;        -- load (synchronize) register
        Err_vec_i <= "0000000000";                              -- do not count errors
      -----------------------------------------------------------------
      -- Normal operation (checking errors)
      ELSE
        LFSR <= LFSR(LFSR'HIGH-10 DOWNTO 0) & FBk;
        Err_vec_i <= FBk XOR data_Rx_i;
      END IF;
      -----------------------------------------------------------------

    END IF;
  END PROCESS LFSR_Rx;

  ---------------------------------------------------------------------
  -- Reference LFSR feedback
  ---------------------------------------------------------------------

  FBk(9) <= LFSR(17) XOR LFSR(22);     -- standard LFSR output
  FBk(8) <= LFSR(16) XOR LFSR(21);     -- predictions
  FBk(7) <= LFSR(15) XOR LFSR(20);
  FBk(6) <= LFSR(14) XOR LFSR(19);
  FBk(5) <= LFSR(13) XOR LFSR(18);
  FBk(4) <= LFSR(12) XOR LFSR(17);
  FBk(3) <= LFSR(11) XOR LFSR(16);
  FBk(2) <= LFSR(10) XOR LFSR(15);
  FBk(1) <= LFSR( 9) XOR LFSR(14);
  FBk(0) <= LFSR( 8) XOR LFSR(13);


  ---------------------------------------------------------------------
  -- Error counter
  ---------------------------------------------------------------------

  Err_cnt_elem_10b_i : Err_cnt_elem_10b
  PORT MAP(
    clk         => clk_Rx,
    Err_vec     => Err_vec_i,
    Err_count   => Err_count_i);


  ---------------------------------------------------------------------
  -- Counter of errors in a floating window of 100 bits
  -- Invokes resynchronization at more than 10 errors in the window
  ---------------------------------------------------------------------

  err_cnt_win_proc: PROCESS(clk_Rx) BEGIN
    IF rising_edge(clk_Rx) THEN

      rst_Err_cnt <= LFSR_Reload OR Reload_req;

      IF rst_Err_cnt = '1' THEN             -- Resynchronization of LFSR
        Err_cnt_ShReg <= (OTHERS => X"0");
        Err_cnt <= (OTHERS => '0');
      ELSE                                  -- Shift of ShReg
        Err_cnt_ShReg <= Err_cnt_ShReg(Err_cnt_ShReg'HIGH-1 DOWNTO 0) & Err_count_i;
        Err_cnt <= Err_cnt + UNSIGNED(Err_count_i) - UNSIGNED(Err_cnt_ShReg(Err_cnt_ShReg'HIGH));
      END IF;

      -- reload request comparator
      IF Err_cnt > TO_UNSIGNED(ShReg_max_err,Err_cnt'LENGTH) THEN
        Reload_req <= '1';
      ELSE
        Reload_req <= '0';
      END IF;

      -- detect LFSR false lock and initial load (LFSR is initialized with all zeros)
      IF LFSR = LFSR_zeros THEN
        Reload_req <= '1';
      END IF;

    END IF;
  END PROCESS err_cnt_win_proc;


  ---------------------------------------------------------------------
  -- LFSR Rx control
  ---------------------------------------------------------------------
  LFSR_ctrl_proc: PROCESS(clk_Rx) BEGIN
    IF rising_edge(clk_Rx) THEN

      IF Reload_req = '1' THEN
        LFSR_Reload <= '1';
        cnt_resync <= X"0";
        LFSR_locked_i <= '0';
      ELSE
        LFSR_Reload <= '0';
        IF cnt_resync = X"D" THEN
          LFSR_locked_i <= '1';
        ELSE
          cnt_resync <= cnt_resync + 1;
        --IF cnt_resync = X"2" THEN
        --  LFSR_Reload <= '0';     -- terminate resynchronization of LFSR, begin verification
        --END IF;
        END IF;

      END IF;
    -------------------------------------------------------------------
    END IF;
  END PROCESS LFSR_ctrl_proc;

  ---------------------------------------------------------------------
  -- counter of errors in 1s + IRQ generator
  ---------------------------------------------------------------------

  PROCESS(clk_Rx) BEGIN
    IF rising_edge(clk_Rx) THEN

      -- 1s counter + IRQ generator
      cnt_1s <= cnt_1s + 1;
      IRQ_1s_i <= '0';
      IF cnt_1s = cnt_1s_MAX THEN
        cnt_1s <= (OTHERS => '0');
        IRQ_1s_i <= '1';
        toggle_1s_i <= NOT toggle_1s_i;
      END IF;

      -- Error counter (accumulator)
      IF cnt_1s = cnt_1s_MAX THEN
        IF LFSR_Reload = '1' THEN
          Err_cnt_1s_u <= X"0000000" & X"5";
        ELSE
          Err_cnt_1s_u <= X"0000000" & UNSIGNED(Err_count_i);
        END IF;
        Err_cnt_1s_i <= STD_LOGIC_VECTOR(Err_cnt_1s_u);
      ELSIF LFSR_Reload = '1' THEN
        Err_cnt_1s_u <= Err_cnt_1s_u + X"5";                        -- 50% error rate
      ELSE
        Err_cnt_1s_u <= Err_cnt_1s_u + UNSIGNED(Err_count_i);
      END IF;


    END IF;
  END PROCESS;


  ---------------------------------------------------------------------
  -- clock domain crossing
  ---------------------------------------------------------------------

  Sync_LFSR_locked : Sync_1b_level_adj
  GENERIC MAP(
    flops               => 4)
  PORT MAP(
    sig_async           => LFSR_locked_i,
    clk                 => clk_sys,
    sig_sync            => LFSR_locked);


  Sync_toggle_1s : Sync_1b_level_adj
  GENERIC MAP(
    flops               => 4)
  PORT MAP(
    sig_async           => toggle_1s_i,
    clk                 => clk_sys,
    sig_sync            => toggle_1s);


  Sync_Err_count : Sync_Nb_level_adj
  GENERIC MAP(
    data_width          => 32,
    flops               => 4)
  PORT MAP(
    sig_async           => Err_cnt_1s_i,
    clk                 => clk_sys,
    sig_sync            => Err_cnt_1s);


--  delay_IRQ_1s : Sync_1b_level_adj
--  GENERIC MAP(
--    flops               => 10)
--  PORT MAP(
--    sig_async           => IRQ_1s_i,
--    clk                 => clk_Rx,
--    sig_sync            => IRQ_1s_del);
--
--
--  Sync_IRQ_1s_i : Sync_1b_pulse_p
--  PORT MAP(
--    sig_async           => IRQ_1s_del,
--    clk                 => clk_sys,
--    sig_sync            => IRQ_1s);

  stretcher_IRQ_1s : pulse_stretcher
  GENERIC MAP(
    periods         => 10)
  PORT MAP(
    pulse_S_p       => IRQ_1s_i,
    clk             => clk_Rx,
    pulse_L_p       => IRQ_1s_del);



  Sync_IRQ_1s : Sync_1b_level_adj
  GENERIC MAP(
    flops               => 4)
  PORT MAP(
    sig_async           => IRQ_1s_del,
    clk                 => clk_sys,
    sig_sync            => IRQ_1s);



-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
