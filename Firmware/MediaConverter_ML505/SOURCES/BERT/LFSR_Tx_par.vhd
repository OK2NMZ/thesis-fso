-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-----------------------------------------------------------------------
ENTITY LFSR_Tx_Par IS
  PORT (clk         : IN  STD_LOGIC;
        Tx_EN       : IN  STD_LOGIC;
        seq_sel     : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);   -- selection of PRBS sequence
        Data_out    : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));  -- transmit LSB first, MSB last
END LFSR_Tx_Par;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF LFSR_Tx_Par IS
  SIGNAL Lfsr:          STD_LOGIC_VECTOR(22 DOWNTO 0):= "10101010101010101010101";
  SIGNAL FBk:           STD_LOGIC_VECTOR(7 DOWNTO 0);

  CONSTANT Ones:        STD_LOGIC_VECTOR(7 DOWNTO 0):= (OTHERS => '1');
  CONSTANT Zeros:       STD_LOGIC_VECTOR(7 DOWNTO 0):= (OTHERS => '0');
  SIGNAL one:           STD_LOGIC:= '0';                    -- Active when Fbk = all ones
  SIGNAL zero:          STD_LOGIC:= '0';                    -- Active when Fbk = all zeros
  SIGNAL lock_cnt_z:    STD_LOGIC_VECTOR(2 DOWNTO 0):= (OTHERS => '0');
  SIGNAL lock_cnt_o:    STD_LOGIC_VECTOR(2 DOWNTO 0):= (OTHERS => '0');

  SIGNAL False_lock:    STD_LOGIC:= '0';

-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  LFSR_Tx: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF Tx_EN = '1' THEN
        ---------------------------------------------------------------
        IF seq_sel(3) = '0' THEN          -- direct sequence
          Data_out(0) <= FBk(7);          -- bit FBk(7) is the first to transmit
          Data_out(1) <= FBk(6);
          Data_out(2) <= FBk(5);
          Data_out(3) <= FBk(4);
          Data_out(4) <= FBk(3);
          Data_out(5) <= FBk(2);
          Data_out(6) <= FBk(1);
          Data_out(7) <= FBk(0);
        ELSE                                  -- inverted sequence
          Data_out(0) <= NOT FBk(7);          -- bit FBk(7) is the first to transmit
          Data_out(1) <= NOT FBk(6);
          Data_out(2) <= NOT FBk(5);
          Data_out(3) <= NOT FBk(4);
          Data_out(4) <= NOT FBk(3);
          Data_out(5) <= NOT FBk(2);
          Data_out(6) <= NOT FBk(1);
          Data_out(7) <= NOT FBk(0);
        END IF;
        ---------------------------------------------------------------
        IF False_lock = '1' THEN
          Lfsr <= "10101010101010101010101";
        ELSE
          Lfsr <= Lfsr(Lfsr'HIGH-8 DOWNTO 0) & FBk;
        END IF;

        ---------------------------------------------------------------
        -- check for locked state (all-zeros or all-ones) -------------
        IF FBk = Ones  THEN one  <= '1';
                       ELSE one  <= '0'; END IF;
        IF FBk = Zeros THEN zero <= '1';
                       ELSE zero <= '0'; END IF;
        ---------------------------------------------------------------
        IF zero = '1' THEN
          IF lock_cnt_z(lock_cnt_z'HIGH) = '0' THEN
            lock_cnt_z <= lock_cnt_z + 1;
          END IF;
        ELSE
          lock_cnt_z <= (OTHERS => '0');
        END IF;

        IF one = '1' THEN
          IF lock_cnt_o(lock_cnt_o'HIGH) = '0' THEN
            lock_cnt_o <= lock_cnt_o + 1;
          END IF;
        ELSE
          lock_cnt_o <= (OTHERS => '0');
        END IF;
        ---------------------------------------------------------------
        IF lock_cnt_z(lock_cnt_z'HIGH) = '1' OR lock_cnt_o(lock_cnt_o'HIGH) = '1' THEN
          False_lock <= '1';
        ELSE
          False_lock <= '0';
        END IF;
        ---------------------------------------------------------------
      END IF;
    END IF;
  END PROCESS LFSR_Tx;


  Feedback: PROCESS(seq_sel,Lfsr) BEGIN
      -- feedback selection = sequence selection ----------------------
        CASE seq_sel(2 DOWNTO 0) IS

          WHEN "000" =>  FBk(7) <= Lfsr( 1) XOR Lfsr( 2);     -- standard LFSR output                  -- Lfsr(1) XOR Lfsr(2)
                         FBk(6) <= Lfsr( 0) XOR Lfsr( 1);     -- predictions                           -- Lfsr(0) XOR Lfsr(1)
                         FBk(5) <= Lfsr( 1) XOR Lfsr( 2) XOR Lfsr( 0);                                 --  Fbk(7) XOR Lfsr(0)
                       --FBk(4) <= Lfsr( 0) XOR Lfsr( 1) XOR Lfsr( 1) XOR Lfsr( 2);                    --  Fbk(6) XOR Lfsr(7)
                         FBk(4) <= Lfsr( 0) XOR Lfsr( 2);
                       --FBk(3) <= Lfsr( 1) XOR Lfsr( 2) XOR Lfsr( 0) XOR Lfsr( 0) XOR Lfsr( 1);       --  Fbk(5) XOR Lfsr(6)
                         FBk(3) <= Lfsr( 2);
                       --FBk(2) <= Lfsr( 0) XOR Lfsr( 2) XOR Lfsr( 1) XOR Lfsr( 2) XOR Lfsr( 0);       --  Fbk(4) XOR Lfsr(5)
                         FBk(2) <= Lfsr( 1);
                       --FBk(1) <= Lfsr( 2) XOR Lfsr( 0) XOR Lfsr( 2);                                 --  Fbk(3) XOR Lfsr(4)
                         FBk(1) <= Lfsr( 0);
                       --FBk(0) <= Lfsr( 1) XOR Lfsr( 2);                                              --  Fbk(2) XOR Lfsr(3)
                         FBk(0) <= Lfsr( 1) XOR Lfsr( 2);

          WHEN "001" =>  FBk(7) <= Lfsr( 2) XOR Lfsr( 4);     -- standard LFSR output
                         FBk(6) <= Lfsr( 1) XOR Lfsr( 3);     -- predictions
                         FBk(5) <= Lfsr( 0) XOR Lfsr( 2);
                         FBk(4) <= Lfsr( 2) XOR Lfsr( 4) XOR Lfsr( 1);
                         FBk(3) <= Lfsr( 1) XOR Lfsr( 3) XOR Lfsr( 0);
                       --FBk(2) <= Lfsr( 0) XOR Lfsr( 2) XOR Lfsr( 2) XOR Lfsr( 4);
                         FBk(2) <= Lfsr( 0) XOR Lfsr( 4);
                       --FBk(1) <= Lfsr( 2) XOR Lfsr( 4) XOR Lfsr( 1) XOR Lfsr( 1) XOR Lfsr( 3);
                         FBk(1) <= Lfsr( 2) XOR Lfsr( 4) XOR Lfsr( 3);
                       --FBk(0) <= Lfsr( 1) XOR Lfsr( 3) XOR Lfsr( 0) XOR Lfsr( 0) XOR Lfsr( 2);
                         FBk(0) <= Lfsr( 1) XOR Lfsr( 3) XOR Lfsr( 2);

          WHEN "010" =>  FBk(7) <= Lfsr( 5) XOR Lfsr( 6);     -- standard LFSR output
                         FBk(6) <= Lfsr( 4) XOR Lfsr( 5);     -- predictions
                         FBk(5) <= Lfsr( 3) XOR Lfsr( 4);
                         FBk(4) <= Lfsr( 2) XOR Lfsr( 3);
                         FBk(3) <= Lfsr( 1) XOR Lfsr( 2);
                         FBk(2) <= Lfsr( 0) XOR Lfsr( 1);
                         FBk(1) <= Lfsr( 5) XOR Lfsr( 6) XOR Lfsr( 0);
                       --FBk(0) <= Lfsr( 4) XOR Lfsr( 5) XOR Lfsr( 5) XOR Lfsr( 6);
                         FBk(0) <= Lfsr( 4) XOR Lfsr( 6);

          WHEN "011" =>  FBk(7) <= Lfsr( 4) XOR Lfsr( 8);     -- standard LFSR output
                         FBk(6) <= Lfsr( 3) XOR Lfsr( 7);     -- predictions
                         FBk(5) <= Lfsr( 2) XOR Lfsr( 6);
                         FBk(4) <= Lfsr( 1) XOR Lfsr( 5);
                         FBk(3) <= Lfsr( 0) XOR Lfsr( 4);
                         FBk(2) <= Lfsr( 4) XOR Lfsr( 8) XOR Lfsr( 3);
                         FBk(1) <= Lfsr( 3) XOR Lfsr( 7) XOR Lfsr( 2);
                         FBk(0) <= Lfsr( 2) XOR Lfsr( 6) XOR Lfsr( 1);


          WHEN "100"  => FBk(7) <= Lfsr( 8) XOR Lfsr(10);     -- standard LFSR output
                         FBk(6) <= Lfsr( 7) XOR Lfsr( 9);     -- predictions
                         FBk(5) <= Lfsr( 6) XOR Lfsr( 8);
                         FBk(4) <= Lfsr( 5) XOR Lfsr( 7);
                         FBk(3) <= Lfsr( 4) XOR Lfsr( 6);
                         FBk(2) <= Lfsr( 3) XOR Lfsr( 5);
                         FBk(1) <= Lfsr( 2) XOR Lfsr( 4);
                         FBk(0) <= Lfsr( 1) XOR Lfsr( 3);


          WHEN "101"  => FBk(7) <= Lfsr(13) XOR Lfsr(14);     -- standard LFSR output
                         FBk(6) <= Lfsr(12) XOR Lfsr(13);     -- predictions
                         FBk(5) <= Lfsr(11) XOR Lfsr(12);
                         FBk(4) <= Lfsr(10) XOR Lfsr(11);
                         FBk(3) <= Lfsr( 9) XOR Lfsr(10);
                         FBk(2) <= Lfsr( 8) XOR Lfsr( 9);
                         FBk(1) <= Lfsr( 7) XOR Lfsr( 8);
                         FBk(0) <= Lfsr( 6) XOR Lfsr( 7);

          WHEN "110"  => FBk(7) <= Lfsr(16) XOR Lfsr(19);     -- standard LFSR output
                         FBk(6) <= Lfsr(15) XOR Lfsr(18);     -- predictions
                         FBk(5) <= Lfsr(14) XOR Lfsr(17);
                         FBk(4) <= Lfsr(13) XOR Lfsr(16);
                         FBk(3) <= Lfsr(12) XOR Lfsr(15);
                         FBk(2) <= Lfsr(11) XOR Lfsr(14);
                         FBk(1) <= Lfsr(10) XOR Lfsr(13);
                         FBk(0) <= Lfsr( 9) XOR Lfsr(12);

          WHEN OTHERS => FBk(7) <= Lfsr(17) XOR Lfsr(22);     -- standard LFSR output
                         FBk(6) <= Lfsr(16) XOR Lfsr(21);     -- predictions
                         FBk(5) <= Lfsr(15) XOR Lfsr(20);
                         FBk(4) <= Lfsr(14) XOR Lfsr(19);
                         FBk(3) <= Lfsr(13) XOR Lfsr(18);
                         FBk(2) <= Lfsr(12) XOR Lfsr(17);
                         FBk(1) <= Lfsr(11) XOR Lfsr(16);
                         FBk(0) <= Lfsr(10) XOR Lfsr(15);
        END CASE;                                       
      -----------------------------------------------------------------
  END PROCESS Feedback;


-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
