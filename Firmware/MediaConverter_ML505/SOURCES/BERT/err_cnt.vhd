-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-----------------------------------------------------------------------
ENTITY err_cnt IS
    PORT( clk_in:       IN  STD_LOGIC;
          err_cnt_in:   IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
          load_in:      IN  STD_LOGIC;
          dv_cnt:       IN  STD_LOGIC;

          int_time:     IN  STD_LOGIC_VECTOR(31 DOWNTO 0);      -- number of bytes to receive before interrupt is asserted

          int_rq:       OUT STD_LOGIC;                                          -- interrupt output (rising edge sensitive)
          error_count:  OUT STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');   -- number of errors from the last strobe
          load_count:   OUT STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0'));  -- number of "loading bytes" since the last int_rq
END err_cnt;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF err_cnt IS
  SIGNAL err_cnt_i:         STD_LOGIC_VECTOR( 3 DOWNTO 0) := (OTHERS => '0');   -- internal error signal, zero when loading

  SIGNAL cnt_err:           STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');   -- counter of errors
  SIGNAL cnt_load:          STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');   -- counter of reloads
  SIGNAL byte_cnt:          STD_LOGIC_VECTOR(27 DOWNTO 0) := X"0000001";        -- number of received bytes

  SIGNAL int_time_buf:      STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');   -- buffered int_time signal for change detection
  SIGNAL int_time_match:    STD_LOGIC:= '0';                -- interrupt time match flag
  SIGNAL change:            STD_LOGIC:= '0';                -- change of interrupt_time detection flag
-----------------------------------------------------------------------
BEGIN
-- 1 0010 1000 1010 0001 1000 0000 = 19.44M /   1 s
--      1 1101 1010 1001 1100 0000 = 1.944M / 100 ms
--          10 1111 0111 0110 0000 = 194.4k /  10 ms
--              100 1011 1111 0000 = 19.44k /   1 ms
-----------------------------------------------------------------------
  loading_MUX: PROCESS(load_in,err_cnt_in) BEGIN
    IF load_in = '1' THEN
      err_cnt_i <= X"0";        -- when loading, do not count errors
    ELSE
      err_cnt_i <= err_cnt_in;      
    END IF;
  END PROCESS loading_MUX;
-----------------------------------------------------------------------

  cnt_proc: PROCESS(clk_in) BEGIN
    IF rising_edge(clk_in) THEN

      IF int_time_match = '1' THEN
        error_count <= cnt_err;                 -- forward new values
        load_count <= cnt_load;
        IF dv_cnt = '1' THEN
          cnt_err  <= X"0000000" & err_cnt_i;  -- clear counters
          cnt_load <= "0000000000000000000000000000000" & load_in;
        ELSE
          cnt_err  <= X"00000000";              -- clear counters
          cnt_load <= X"00000000";
        END IF;
      ELSIF dv_cnt = '1' THEN
        cnt_err <= cnt_err + err_cnt_i;        -- counting errors and loads
        cnt_load <= cnt_load + load_in;
      END IF;
    END IF;
  END PROCESS cnt_proc;

---------------------------------------------------------------------
  -- process for interrupt period measurement
  int_time_check: PROCESS(clk_in) BEGIN
    IF rising_edge(clk_in) THEN

      int_rq <= int_time_match;
      int_time_buf <= int_time;
      IF int_time_buf /= int_time THEN change <= '1';
                                  ELSE change <= '0'; END IF;

      IF change = '1' THEN
        byte_cnt <= X"0000001";     -- reset byte counter
        int_time_match <= '0';
      ELSIF dv_cnt = '1' THEN

        -- irq condition reached
      --IF byte_cnt = int_time(27 DOWNTO 0) THEN
        IF byte_cnt = int_time THEN
          byte_cnt <= X"0000001";      -- reset byte counter
          int_time_match <= '1';
        -- normal operation
        ELSE
          int_time_match <= '0';
          byte_cnt <= byte_cnt + 1;
        END IF;
      ELSE
        int_time_match <= '0';
      END IF;
    END IF;
  END PROCESS int_time_check;
-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
