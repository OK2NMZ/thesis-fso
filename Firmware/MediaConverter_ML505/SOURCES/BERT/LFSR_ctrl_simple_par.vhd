-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-----------------------------------------------------------------------
ENTITY LFSR_ctrl_simple_par is
  PORT( 
    clk             : IN  STD_LOGIC;
    Error           : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    False_lock      : IN  STD_LOGIC;
    Reload          : OUT STD_LOGIC;
    Loading         : OUT STD_LOGIC := '1');  -- signal indicating resynchronization (including verification of synchronization)
END LFSR_ctrl_simple_par;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF LFSR_ctrl_simple_par IS
-----------------------------------------------------------------------
   -- threshold of number of errors in ShReg for LFSR reload request
  CONSTANT max_err:         STD_LOGIC_VECTOR(4 DOWNTO 1) := "0101";  -- default is 5 (reload at 6 errors)

  SUBTYPE t_err_w IS STD_LOGIC_VECTOR(3 DOWNTO 0);
  TYPE t_shreg IS ARRAY (15 DOWNTO 0) OF t_err_w;
  SIGNAL ShReg:             t_shreg:= (OTHERS => X"0");                         -- shift reg of errors

  SIGNAL Err_cnt:           STD_LOGIC_VECTOR(4 DOWNTO 0) := (OTHERS => '0');    -- counter of errors in ShReg_main
  SIGNAL reload_int:        STD_LOGIC := '1';                                   -- internal signal for LFSR reload
  SIGNAL verify:            STD_LOGIC := '1';                                    -- verify

  SIGNAL Cnt_load:          STD_LOGIC_VECTOR(4 DOWNTO 0) := (OTHERS => '0');    -- counter for LFSR resynchronization
-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  LFSR_ctrl_proc: PROCESS(clk) BEGIN

    IF rising_edge(clk) THEN
    -------------------------------------------------------------------
      IF Reload_int = '1' THEN   -- Reload request was accepted by LfsrRx
        -- False lock reload controll
        IF False_lock = '1' THEN
          Reload_int <= '1';
        ELSE
          ShReg <= (OTHERS => X"0");
          Err_cnt <= (OTHERS => '0');
          verify <= '1';
          Loading <= '1';

          -- counter for loading the LFSR
          Cnt_load <= Cnt_load(Cnt_load'HIGH-1 DOWNTO 0) & '1';
          Reload_int <= NOT Cnt_load(Cnt_load'HIGH);    -- clear request
        END IF;
      ---------------------------------------------------------------
      ELSIF verify = '1' THEN
        -- False lock reload control
        IF False_lock = '1' THEN
          Reload_int <= '1';
        ELSE
          Cnt_load <= Cnt_load(Cnt_load'HIGH-1 DOWNTO 0) & '0';
          verify <= Cnt_load(Cnt_load'HIGH);
          Loading <= '1';
          IF Error /= X"0" THEN
            Reload_int <= '1';
            Cnt_load <= (OTHERS => '0');
          END IF;
        END IF;
      ---------------------------------------------------------------
      ELSE
        -- False lock reload controll
        IF False_lock = '1' THEN
          Reload_int <= '1';
        ELSE
          Loading <= '0';
          -- Shift of ShReg
          ShReg <= ShReg(ShReg'HIGH-1 DOWNTO 0) & Error;

          -- add new error vectors and sub old ones
          Err_cnt <= Err_cnt + Error - ShReg(ShReg'HIGH);

          -- reload request comparator
          IF Err_cnt > max_err THEN Reload_int <= '1';
                                    Cnt_load <= (OTHERS => '0');
                               ELSE Reload_int <= '0'; END IF;
        END IF;
      ---------------------------------------------------------------
      END IF;
    -------------------------------------------------------------------
    END IF;
  END PROCESS LFSR_ctrl_proc;

  reload <= reload_int;

-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
