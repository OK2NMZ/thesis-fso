-----------------------------------------------------------------------
--  seq_sel(2 DOWNTO 0) = PRBS sequence                               |
--                         000: 2^3  Fbk(2,3)                         |
--                         001: 2^5  Fbk(3,5)                         |
--                         010: 2^7  Fbk(6,7)                         |
--                         011: 2^9  Fbk(5,9)                         |
--                         100: 2^11 Fbk(9,11)                        |
--                         101: 2^15 Fbk(14,15)                       |
--                         110: 2^20 Fbk(17,20)                       |
--                         111: 2^23 Fbk(18,23)                       |
--                                                                    |
--  seq_sel(3)           = inversion of PRBS sequence                 |
--                                                                    |
-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-----------------------------------------------------------------------
ENTITY LFSR_Rx_10b IS
  PORT(
    clk                 : IN  STD_LOGIC;                        -- Rx data clock
    Rx_Data             : IN  STD_LOGIC_VECTOR(9 DOWNTO 0);     -- Rx data vector; LSB is the first received (oldest) bit
    Rx_DV               : IN  STD_LOGIC;                        -- Rx data valid
    Reload              : IN  STD_LOGIC;                        -- resynchronization request
    seq_sel             : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);     -- selection of PRBS sequence
    Err_vec             : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);     -- vector of errors (number of ones = number of errors)
    Err_val             : OUT STD_LOGIC;                        -- data valid signal for Err_vec_i
    False_lock          : OUT STD_LOGIC);                       -- False lock state (all-ones or all-zeros)
END LFSR_Rx_10b;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF LFSR_Rx_10b IS
-----------------------------------------------------------------------
  SIGNAL LFSR           : STD_LOGIC_VECTOR(22 DOWNTO 0):= (OTHERS => '0');
  SIGNAL FBk            : STD_LOGIC_VECTOR( 9 DOWNTO 0);

  CONSTANT Ones         : STD_LOGIC_VECTOR( 9 DOWNTO 0):= (OTHERS => '1');
  CONSTANT Zeros        : STD_LOGIC_VECTOR( 9 DOWNTO 0):= (OTHERS => '0');
  SIGNAL one            : STD_LOGIC:= '0';                      -- Active when Fbk = all ones
  SIGNAL zero           : STD_LOGIC:= '0';                      -- Active when Fbk = all zeros

  SIGNAL lock_cnt_z     : UNSIGNED( 2 DOWNTO 0):= (OTHERS => '0');
  SIGNAL lock_cnt_o     : UNSIGNED( 2 DOWNTO 0):= (OTHERS => '0');
  SIGNAL Rx_DV_p        : STD_LOGIC;                            -- delayed data valid signal

  SIGNAL Rx_Data_i      : STD_LOGIC_VECTOR( 9 DOWNTO 0);        -- Received data signal; MSB is the first received (oldest) bit

  SIGNAL False_lock_i   : STD_LOGIC := '0';
  SIGNAL Err_vec        : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
  SIGNAL Err_val_i      : STD_LOGIC := '0';

-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  -----------------------------------------------------------------------
  -- Reorder received data (MSB/LSB)
  -----------------------------------------------------------------------

  data_reorder: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      Rx_DV_p <= Rx_DV;

      Rx_Data_i(0) <= Rx_Data(9);
      Rx_Data_i(1) <= Rx_Data(8);
      Rx_Data_i(2) <= Rx_Data(7);
      Rx_Data_i(3) <= Rx_Data(6);
      Rx_Data_i(4) <= Rx_Data(5);
      Rx_Data_i(5) <= Rx_Data(4);
      Rx_Data_i(6) <= Rx_Data(3);
      Rx_Data_i(7) <= Rx_Data(2);
      Rx_Data_i(8) <= Rx_Data(1);
      Rx_Data_i(9) <= Rx_Data(0);
    END IF;
  END PROCESS data_reorder;

  -----------------------------------------------------------------------
  -- Reference LFSR
  -----------------------------------------------------------------------

  LFSR_Rx: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
    -------------------------------------------------------------------
      -- output is delayed by one clock cycle
      Err_val <= Rx_DV_p;
      -----------------------------------------------------------------

      IF Rx_DV_p = '1' THEN      -- clock enable
        ---------------------------------------------------------------
        -- Resynchronization
        IF Reload = '1' THEN
          Err_vec_i <= (OTHERS => '0');
          IF seq_sel(3) = '0' THEN
            LFSR <= LFSR(LFSR'HIGH-10 DOWNTO 0) & Rx_Data_i;        -- direct sequence
          ELSE
            LFSR <= LFSR(LFSR'HIGH-10 DOWNTO 0) & NOT Rx_Data_i;    -- inverted sequence
          END IF;

        -- Normal operation (checking errors)
        ELSE

          IF seq_sel(3) = '0' THEN
            LFSR <= LFSR(LFSR'HIGH-10 DOWNTO 0) & FBk;
            Err_vec_i <= FBk XOR Rx_Data_i;
          ELSE
            LFSR <= LFSR(LFSR'HIGH-10 DOWNTO 0) & FBk;
            Err_vec_i <= FBk XNOR Rx_Data_i;
          END IF;
        END IF;

      -----------------------------------------------------------------
      -- check for locked state (all-zeros or all-ones) ---------------
        IF FBk = Ones  THEN one  <= '1';
                       ELSE one  <= '0'; END IF;
        IF FBk = Zeros THEN zero <= '1';
                       ELSE zero <= '0'; END IF;
        ---------------------------------------------------------------
        IF zero = '1' THEN
          IF lock_cnt_z(lock_cnt_z'HIGH) = '0' THEN
            lock_cnt_z <= lock_cnt_z + 1;
          END IF;
        ELSE
          lock_cnt_z <= (OTHERS => '0');
        END IF;

        IF one = '1' THEN
          IF lock_cnt_o(lock_cnt_o'HIGH) = '0' THEN
            lock_cnt_o <= lock_cnt_o + 1;
          END IF;
        ELSE
          lock_cnt_o <= (OTHERS => '0');
        END IF;
        ---------------------------------------------------------------
        IF lock_cnt_z(lock_cnt_z'HIGH) = '1' OR lock_cnt_o(lock_cnt_o'HIGH) = '1' THEN
          False_lock_i <= '1';
        ELSE
          False_lock_i <= '0';
        END IF;
      -----------------------------------------------------------------
      END IF;
    END IF;
  END PROCESS LFSR_Rx;

  False_lock <= False_lock_i;
  Err_vec    <= Err_vec_i;
  Err_val    <= Err_val_i;

  ---------------------------------------------------------------------
  -- Reference LFSR feedback
  ---------------------------------------------------------------------

  Feedback: PROCESS(seq_sel,LFSR) BEGIN
      -- feedback selection = sequence selection ----------------------
        CASE seq_sel(2 DOWNTO 0) IS

          -- PRBS(2,3)      ; 2^3
          WHEN "000" =>  FBk(9) <= LFSR( 1) XOR LFSR( 2);     -- standard LFSR output                  -- LFSR(1) XOR LFSR(2)
                         FBk(8) <= LFSR( 0) XOR LFSR( 1);     -- predictions                           -- LFSR(0) XOR LFSR(1)
                         FBk(7) <= LFSR( 1) XOR LFSR( 2) XOR LFSR( 0);                                 --  Fbk(9) XOR LFSR(0)
                       --FBk(6) <= LFSR( 0) XOR LFSR( 1) XOR LFSR( 1) XOR LFSR( 2);                    --  Fbk(8) XOR  Fbk(9)
                         FBk(6) <= LFSR( 0) XOR LFSR( 2);
                       --FBk(5) <= LFSR( 1) XOR LFSR( 2) XOR LFSR( 0) XOR LFSR( 0) XOR LFSR( 1);       --  Fbk(7) XOR  Fbk(8)
                         FBk(5) <= LFSR( 2);
                       --FBk(4) <= LFSR( 0) XOR LFSR( 2) XOR LFSR( 1) XOR LFSR( 2) XOR LFSR( 0);       --  Fbk(6) XOR  Fbk(7)
                         FBk(4) <= LFSR( 1);
                       --FBk(3) <= LFSR( 2) XOR LFSR( 0) XOR LFSR( 2);                                 --  Fbk(5) XOR  Fbk(6)
                         FBk(3) <= LFSR( 0);
                         FBk(2) <= LFSR( 1) XOR LFSR( 2);                                              --  Fbk(4) XOR  Fbk(5) = Fbk(9)
                         FBk(8) <= LFSR( 0) XOR LFSR( 1);     -- predictions                           --  Fbk(3) XOR  Fbk(4) = Fbk(8)
                         FBk(7) <= LFSR( 1) XOR LFSR( 2) XOR LFSR( 0);                                 --  Fbk(2) XOR  Fbk(3) = Fbk(7)

          -- PRBS(3,5)      ; 2^5
          WHEN "001" =>  FBk(9) <= LFSR( 2) XOR LFSR( 4);     -- standard LFSR output
                         FBk(8) <= LFSR( 1) XOR LFSR( 3);     -- predictions
                         FBk(7) <= LFSR( 0) XOR LFSR( 2);
                         FBk(6) <= LFSR( 2) XOR LFSR( 4) XOR LFSR( 1);                              -- Fbk(9) XOR LFSR(1)
                         FBk(5) <= LFSR( 1) XOR LFSR( 3) XOR LFSR( 0);                              -- Fbk(8) XOR LFSR(0)
                       --FBk(4) <= LFSR( 0) XOR LFSR( 2) XOR LFSR( 2) XOR LFSR( 4);                 -- Fbk(7) XOR  Fbk(9)
                         FBk(4) <= LFSR( 0) XOR LFSR( 4);                                           -- Fbk(7) XOR  Fbk(9)
                       --FBk(3) <= LFSR( 2) XOR LFSR( 4) XOR LFSR( 1) XOR LFSR( 1) XOR LFSR( 3);    -- Fbk(6) XOR  Fbk(8)
                         FBk(3) <= LFSR( 2) XOR LFSR( 4) XOR LFSR( 3);                              -- Fbk(6) XOR  Fbk(8)
                       --FBk(2) <= LFSR( 1) XOR LFSR( 3) XOR LFSR( 0) XOR LFSR( 0) XOR LFSR( 2);    -- Fbk(5) XOR  Fbk(7)
                         FBk(2) <= LFSR( 1) XOR LFSR( 3) XOR LFSR( 2);                              -- Fbk(5) XOR  Fbk(7)
                       --FBk(1) <= LFSR( 0) XOR LFSR( 4) XOR LFSR( 2) XOR LFSR( 4) XOR LFSR( 1);    -- Fbk(4) XOR  Fbk(6)
                         FBk(1) <= LFSR( 0) XOR LFSR( 2) XOR LFSR( 1);                              -- Fbk(4) XOR  Fbk(6)
                       --FBk(0) <= LFSR( 2) XOR LFSR( 4) XOR LFSR( 3) XOR LFSR( 1) XOR LFSR( 3) XOR LFSR( 0);    -- Fbk(3) XOR  Fbk(5)
                         FBk(0) <= LFSR( 2) XOR LFSR( 4) XOR LFSR( 1) XOR LFSR( 0);                              -- Fbk(3) XOR  Fbk(5)

          -- PRBS(6,7)      ; 2^7
          WHEN "010" =>  FBk(9) <= LFSR( 5) XOR LFSR( 6);     -- standard LFSR output
                         FBk(8) <= LFSR( 4) XOR LFSR( 5);     -- predictions
                         FBk(7) <= LFSR( 3) XOR LFSR( 4);
                         FBk(6) <= LFSR( 2) XOR LFSR( 3);
                         FBk(5) <= LFSR( 1) XOR LFSR( 2);
                         FBk(4) <= LFSR( 0) XOR LFSR( 1);
                         FBk(3) <= LFSR( 5) XOR LFSR( 6) XOR LFSR( 0);                  -- Fbk(9) XOR LFSR(0)
                       --FBk(2) <= LFSR( 4) XOR LFSR( 5) XOR LFSR( 5) XOR LFSR( 6);     -- Fbk(8) XOR  Fbk(9)
                         FBk(2) <= LFSR( 4) XOR LFSR( 6);                               -- Fbk(8) XOR  Fbk(9)
                       --FBk(1) <= LFSR( 3) XOR LFSR( 4) XOR LFSR( 4) XOR LFSR( 5);     -- Fbk(7) XOR  Fbk(8)
                         FBk(1) <= LFSR( 3) XOR LFSR( 5);                               -- Fbk(7) XOR  Fbk(8)
                       --FBk(0) <= LFSR( 2) XOR LFSR( 3) XOR LFSR( 3) XOR LFSR( 4);     -- Fbk(6) XOR  Fbk(7)
                         FBk(0) <= LFSR( 2) XOR LFSR( 4);                               -- Fbk(6) XOR  Fbk(7)

          -- PRBS(5,9)      ; 2^9
          WHEN "011" =>  FBk(9) <= LFSR( 4) XOR LFSR( 8);     -- standard LFSR output
                         FBk(8) <= LFSR( 3) XOR LFSR( 7);     -- predictions
                         FBk(7) <= LFSR( 2) XOR LFSR( 6);
                         FBk(6) <= LFSR( 1) XOR LFSR( 5);
                         FBk(5) <= LFSR( 0) XOR LFSR( 4);
                         FBk(4) <= LFSR( 4) XOR LFSR( 8) XOR LFSR( 3);                  -- Fbk(9) XOR LFSR(3)
                         FBk(3) <= LFSR( 3) XOR LFSR( 7) XOR LFSR( 2);                  -- Fbk(8) XOR LFSR(2)
                         FBk(2) <= LFSR( 2) XOR LFSR( 6) XOR LFSR( 1);                  -- Fbk(7) XOR LFSR(1)
                         FBk(1) <= LFSR( 1) XOR LFSR( 5) XOR LFSR( 0);                  -- Fbk(6) XOR LFSR(0)
                       --FBk(0) <= LFSR( 0) XOR LFSR( 4) XOR LFSR( 4) XOR LFSR( 8);     -- Fbk(5) XOR  Fbk(9)
                         FBk(0) <= LFSR( 0) XOR LFSR( 8);                               -- Fbk(5) XOR  Fbk(9)


          -- PRBS(9,11)     ; 2^11
          WHEN "100"  => FBk(9) <= LFSR( 8) XOR LFSR(10);       -- standard LFSR output
                         FBk(8) <= LFSR( 7) XOR LFSR( 9);       -- predictions
                         FBk(7) <= LFSR( 6) XOR LFSR( 8);
                         FBk(6) <= LFSR( 5) XOR LFSR( 7);
                         FBk(5) <= LFSR( 4) XOR LFSR( 6);
                         FBk(4) <= LFSR( 3) XOR LFSR( 5);
                         FBk(3) <= LFSR( 2) XOR LFSR( 4);
                         FBk(2) <= LFSR( 1) XOR LFSR( 3);
                         FBk(1) <= LFSR( 0) XOR LFSR( 2);
                         FBk(0) <= LFSR( 8) XOR LFSR(10) XOR LFSR( 1);          -- Fbk(9) XOR LFSR(1)


          -- PRBS(14,15)    ; 2^15
          WHEN "101"  => FBk(9) <= LFSR(13) XOR LFSR(14);       -- standard LFSR output
                         FBk(8) <= LFSR(12) XOR LFSR(13);       -- predictions
                         FBk(7) <= LFSR(11) XOR LFSR(12);
                         FBk(6) <= LFSR(10) XOR LFSR(11);
                         FBk(5) <= LFSR( 9) XOR LFSR(10);
                         FBk(4) <= LFSR( 8) XOR LFSR( 9);
                         FBk(3) <= LFSR( 7) XOR LFSR( 8);
                         FBk(2) <= LFSR( 6) XOR LFSR( 7);
                         FBk(1) <= LFSR( 5) XOR LFSR( 6);
                         FBk(0) <= LFSR( 4) XOR LFSR( 5);

          -- PRBS(17,20)    ; 2^20
          WHEN "110"  => FBk(9) <= LFSR(16) XOR LFSR(19);       -- standard LFSR output
                         FBk(8) <= LFSR(15) XOR LFSR(18);       -- predictions
                         FBk(7) <= LFSR(14) XOR LFSR(17);
                         FBk(6) <= LFSR(13) XOR LFSR(16);
                         FBk(5) <= LFSR(12) XOR LFSR(15);
                         FBk(4) <= LFSR(11) XOR LFSR(14);
                         FBk(3) <= LFSR(10) XOR LFSR(13);
                         FBk(2) <= LFSR( 9) XOR LFSR(12);
                         FBk(1) <= LFSR( 8) XOR LFSR(11);
                         FBk(0) <= LFSR( 7) XOR LFSR(10);

          -- PRBS(18,23)    ; 2^23
          WHEN OTHERS => FBk(7) <= LFSR(17) XOR LFSR(22);     -- standard LFSR output
                         FBk(6) <= LFSR(16) XOR LFSR(21);     -- predictions
                         FBk(5) <= LFSR(15) XOR LFSR(20);
                         FBk(4) <= LFSR(14) XOR LFSR(19);
                         FBk(3) <= LFSR(13) XOR LFSR(18);
                         FBk(2) <= LFSR(12) XOR LFSR(17);
                         FBk(1) <= LFSR(11) XOR LFSR(16);
                         FBk(0) <= LFSR(10) XOR LFSR(15);
                         FBk(0) <= LFSR( 9) XOR LFSR(14);
                         FBk(0) <= LFSR( 8) XOR LFSR(13);
        END CASE;
      -----------------------------------------------------------------
  END PROCESS Feedback;
  ---------------------------------------------------------------------

-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
