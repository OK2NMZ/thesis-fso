--#####################################################################
-- One channel Bit Error Rate Tester
--#####################################################################
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--library UNISIM;
--use UNISIM.VComponents.all;
--#####################################################################
ENTITY BERT_1CH IS PORT(
    clk:                IN  STD_LOGIC;                          -- system clock
    rec_Data:           IN  STD_LOGIC_VECTOR(7 DOWNTO 0);       -- Rx data
    rec_DV:             IN  STD_LOGIC;                          -- Rx data valid
    seq_sel:            IN  STD_LOGIC_VECTOR(3 DOWNTO 0);       -- selection of PRBS sequence (for LFSR Rx)
    int_time:           IN  STD_LOGIC_VECTOR(31 DOWNTO 0);      -- number of bytes to receive before interrupt is set
    int_rq:             OUT STD_LOGIC;                          -- interrupt request, edge sensitive
    error_count:        OUT STD_LOGIC_VECTOR(31 DOWNTO 0);      -- number of "errors bits" since the last interrupt request
    load_count:         OUT STD_LOGIC_VECTOR(31 DOWNTO 0));     -- number of "loading bits" since the last interrupt request
END BERT_1CH;
--#####################################################################
ARCHITECTURE Structural OF BERT_1CH IS
--#####################################################################
  COMPONENT LFSR_Rx_par PORT(
    clk:          IN  STD_LOGIC;                      -- main system clock
    rec_Data:     IN  STD_LOGIC_VECTOR(7 DOWNTO 0);   -- Rx data vector
    rec_DV:       IN  STD_LOGIC;                      -- Rx data valid
    Reload:       IN  STD_LOGIC;                      -- resynchronization request
    seq_sel:      IN  STD_LOGIC_VECTOR(3 DOWNTO 0);   -- selection of PRBS sequence
    Err_vec:      OUT STD_LOGIC_VECTOR(7 DOWNTO 0);   -- vector of errors (number of ones = number of errors)
    Err_val:      OUT STD_LOGIC;                      -- data valid signal for err_vec
    False_lock:   OUT STD_LOGIC);                     -- False lock state (all-ones or all-zeros)
  END COMPONENT;
  ---------------------------------------------------------------------
  COMPONENT LFSR_ctrl_simple_par PORT(
    clk:                IN  STD_LOGIC;
    Error:              IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    Err_val:            IN  STD_LOGIC;
    False_lock:         IN  STD_LOGIC;
    Reload:             OUT STD_LOGIC;
    Loading:            OUT STD_LOGIC);
  END COMPONENT;
  ---------------------------------------------------------------------
  COMPONENT Err_cnt_elem_8b
    PORT(clk:       IN  STD_LOGIC;
         data:      IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         number:    OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
  END COMPONENT;
  ---------------------------------------------------------------------
  COMPONENT err_cnt PORT(
    clk_in:             IN  STD_LOGIC;
    err_cnt_in:         IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    load_in:            IN  STD_LOGIC;
    dv_cnt:             IN  STD_LOGIC;
    int_time:           IN  STD_LOGIC_VECTOR(31 DOWNTO 0);      -- number of bytes to receive before interrupt is set
    int_rq:             OUT STD_LOGIC;          -- just a strobe, must be processed (catched) by processor interface block
    error_count:        OUT STD_LOGIC_VECTOR(31 DOWNTO 0);      -- number of errors from the last strobe
    load_count:         OUT STD_LOGIC_VECTOR(31 DOWNTO 0));     -- number of "loading bits" since the last int_rq
  END COMPONENT;
--#####################################################################
  SIGNAL Err_vec:       STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL Err_val:       STD_LOGIC;
  SIGNAL reload:        STD_LOGIC;

  SIGNAL loading:       STD_LOGIC;
  SIGNAL loading_i:     STD_LOGIC;

  SIGNAL err_cnt_8:     STD_LOGIC_VECTOR(3 DOWNTO 0);
  SIGNAL load_cnt_8:    STD_LOGIC;                      -- = 8 bits in load state
  SIGNAL cnt_8_val:     STD_LOGIC;
  SIGNAL False_lock:    STD_LOGIC;                      -- all-zeros / all-ones false lock

--#####################################################################
BEGIN
--#####################################################################
  LFSR_Rx : LFSR_Rx_par PORT MAP(
    clk             => clk,
    rec_Data        => rec_data,
    rec_DV          => rec_DV,
    Reload          => Reload,
    seq_sel         => seq_sel,
    Err_vec         => Err_vec,
    Err_val         => Err_val,
    False_lock      => False_lock);
  ---------------------------------------------------------------------
  LFSR_ctrl : LFSR_ctrl_simple_par PORT MAP(
    clk             => clk,
    Error           => err_cnt_8,
    Err_val         => Err_val,
    False_lock      => False_lock,
    Reload          => Reload,
    Loading         => Loading);
  ---------------------------------------------------------------------
  -- The following error counter block has one clock cycle latency
  cnt_8b_err:  Err_cnt_elem_8b PORT MAP(
    clk             => clk,
    data            => Err_vec,
    number          => err_cnt_8);

  -- one clock cycle latency
  PROCESS (clk) BEGIN
    IF rising_edge(clk) THEN
      Loading_i     <= Loading;
      load_cnt_8    <= Loading_i;
      cnt_8_val     <= Err_val;
    END IF;
  END PROCESS;
  ---------------------------------------------------------------------
  total_err_cnt : err_cnt PORT MAP(
    clk_in          => clk,
    err_cnt_in      => err_cnt_8,
    load_in         => load_cnt_8,
    dv_cnt          => cnt_8_val,
    int_time        => int_time,
    int_rq          => int_rq,
    error_count     => error_count,
    load_count      => load_count);
--#####################################################################
END Structural;
--#####################################################################
