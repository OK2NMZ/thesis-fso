-----------------------------------------------------------------------
--  PRBS_23:     Fbk(18,23)
-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-----------------------------------------------------------------------
ENTITY LFSR_23_Tx_10b IS
  PORT(
    clk                 : IN  STD_LOGIC;
    Data_out            : OUT STD_LOGIC_VECTOR( 9 DOWNTO 0));       -- transmit LSB first, MSB last
END LFSR_23_Tx_10b;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF LFSR_23_Tx_10b IS
-----------------------------------------------------------------------
  SIGNAL LFSR           : STD_LOGIC_VECTOR(22 DOWNTO 0):= "10101010101010101010101";
  SIGNAL FBk            : STD_LOGIC_VECTOR( 9 DOWNTO 0);
  SIGNAL Data_out_i     : STD_LOGIC_VECTOR( 9 DOWNTO 0) := (OTHERS => '0');
-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  ---------------------------------------------------------------------
  -- LFSR register
  ---------------------------------------------------------------------

  LFSR_Tx: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
        LFSR <= LFSR(LFSR'HIGH-10 DOWNTO 0) & FBk;
    END IF;
  END PROCESS LFSR_Tx;


  ---------------------------------------------------------------------
  -- LFSR feedback
  ---------------------------------------------------------------------

  FBk(9) <= LFSR(17) XOR LFSR(22);     -- standard LFSR output
  FBk(8) <= LFSR(16) XOR LFSR(21);     -- predictions
  FBk(7) <= LFSR(15) XOR LFSR(20);
  FBk(6) <= LFSR(14) XOR LFSR(19);
  FBk(5) <= LFSR(13) XOR LFSR(18);
  FBk(4) <= LFSR(12) XOR LFSR(17);
  FBk(3) <= LFSR(11) XOR LFSR(16);
  FBk(2) <= LFSR(10) XOR LFSR(15);
  FBk(1) <= LFSR( 9) XOR LFSR(14);
  FBk(0) <= LFSR( 8) XOR LFSR(13);


  ---------------------------------------------------------------------
  -- PRBS output
  ---------------------------------------------------------------------

  LFSR_out: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      Data_out_i(0) <= FBk(9);                -- bit FBk(9) is the first to transmit
      Data_out_i(1) <= FBk(8);
      Data_out_i(2) <= FBk(7);
      Data_out_i(3) <= FBk(6);
      Data_out_i(4) <= FBk(5);
      Data_out_i(5) <= FBk(4);
      Data_out_i(6) <= FBk(3);
      Data_out_i(7) <= FBk(2);
      Data_out_i(8) <= FBk(1);
      Data_out_i(9) <= FBk(0);
    END IF;
  END PROCESS LFSR_out;

  Data_out <= Data_out_i;

-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
