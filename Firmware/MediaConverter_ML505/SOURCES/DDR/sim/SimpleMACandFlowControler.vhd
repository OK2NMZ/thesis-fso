--------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:   07:55:28 03/30/2016
-- Design Name:
-- Module Name:   /home/ok2nmz/Desktop/DP/thesis-fso/Firmware/MediaConverter_ML505/SimpleMACandFlowControler.vhd
-- Project Name:  MediaConverter_ML505_v5
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: FlowController
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.math_real.all;


ENTITY SimpleMACandFlowControler IS
END SimpleMACandFlowControler;

ARCHITECTURE behavior OF SimpleMACandFlowControler IS

  -- memory controller parameters
  constant BANK_WIDTH            : integer := 2;      -- # of memory bank addr bits
  constant CKE_WIDTH             : integer := 1;      -- # of memory clock enable outputs
  constant CLK_WIDTH             : integer := 2;      -- # of clock outputs
  constant CLK_TYPE              : string  := "DIFFERENTIAL";       -- # of clock type
  constant COL_WIDTH             : integer := 10;     -- # of memory column bits
  constant CS_NUM                : integer := 1;      -- # of separate memory chip selects
  constant CS_WIDTH              : integer := 1;      -- # of total memory chip selects
  constant CS_BITS               : integer := 0;      -- set to log2(CS_NUM) (rounded up)
  constant DM_WIDTH              : integer := 8;      -- # of data mask bits
  constant DQ_WIDTH              : integer := 64;     -- # of data width
  constant DQ_PER_DQS            : integer := 8;      -- # of DQ data bits per strobe
  constant DQ_BITS               : integer := 6;      -- set to log2(DQS_WIDTH*DQ_PER_DQS)
  constant DQS_WIDTH             : integer := 8;      -- # of DQS strobes
  constant DQS_BITS              : integer := 3;      -- set to log2(DQS_WIDTH)
  constant HIGH_PERFORMANCE_MODE : boolean := TRUE; -- Sets the performance mode for IODELAY elements
  constant ODT_WIDTH             : integer := 1;      -- # of memory on-die term enables
  constant ROW_WIDTH             : integer := 13;     -- # of memory row & # of addr bits
  constant APPDATA_WIDTH         : integer := 128;     -- # of usr read/write data bus bits
  constant ADDITIVE_LAT          : integer := 0;      -- additive write latency
  constant BURST_LEN             : integer := 4;      -- burst length (in double words)
  constant BURST_TYPE            : integer := 0;      -- burst type (=0 seq; =1 interlved)
  constant CAS_LAT               : integer := 4;      -- CAS latency
  constant ECC_ENABLE            : integer := 0;      -- enable ECC (=1 enable)
  constant MULTI_BANK_EN         : integer := 1;      -- enable bank management
  constant TWO_T_TIME_EN         : integer := 1;      -- 2t timing for unbuffered dimms
  constant ODT_TYPE              : integer := 1;      -- ODT (=0(none),=1(75),=2(150),=3(50))
  constant REDUCE_DRV            : integer := 0;      -- reduced strength mem I/O (=1 yes)
  constant REG_ENABLE            : integer := 0;      -- registered addr/ctrl (=1 yes)
  constant TREFI_NS              : integer := 7800;   -- auto refresh interval (ns)
  constant TRAS                  : integer := 40000;  -- active->precharge delay
  constant TRCD                  : integer := 15000;  -- active->read/write delay
  constant TRFC                  : integer := 105000;  -- ref->ref, ref->active delay
  constant TRP                   : integer := 15000;  -- precharge->command delay
  constant TRTP                  : integer := 7500;   -- read->precharge delay
  constant TWR                   : integer := 15000;  -- used to determine wr->prech
  constant TWTR                  : integer := 7500;  -- write->read delay
  constant SIM_ONLY              : integer := 1;      -- = 0 to allow power up delay
  constant DEBUG_EN              : integer := 0;      -- Enable debug signals/controls
  constant RST_ACT_LOW           : integer := 1;      -- =1 for active low reset, =0 for active high
  constant DLL_FREQ_MODE         : string  := "HIGH"; -- DCM Frequency range
  constant CLK_PERIOD            : integer := 3750;   -- Core/Mem clk period (in ps)

  constant DEVICE_WIDTH    : integer := 16;      -- Memory device data width
  constant CLK_PERIOD_NS   : real := 3750.0 / 1000.0;
  constant TCYC_SYS        : real := CLK_PERIOD_NS/2.0;
  constant TCYC_SYS_0      : time := CLK_PERIOD_NS * 1 ns;
  constant TCYC_SYS_DIV2   : time := TCYC_SYS * 1 ns;
  constant TEMP2           : real := 5.0/2.0;
  constant TCYC_200        : time := TEMP2 * 1 ns;
  constant TPROP_DQS          : time := 0.01 ns;  -- Delay for DQS signal during Write Operation
  constant TPROP_DQS_RD       : time := 0.01 ns;  -- Delay for DQS signal during Read Operation
  constant TPROP_PCB_CTRL     : time := 0.01 ns;  -- Delay for Address and Ctrl signals
  constant TPROP_PCB_DATA     : time := 0.01 ns;  -- Delay for data signal during Write operation
  constant TPROP_PCB_DATA_RD  : time := 0.01 ns;  -- Delay for data signal during Read operation


  component DDRcontroller is
    generic (
      BANK_WIDTH            : integer;
      CKE_WIDTH             : integer;
      CLK_WIDTH             : integer;
      COL_WIDTH             : integer;
      CS_NUM                : integer;
      CS_WIDTH              : integer;
      CS_BITS               : integer;
      DM_WIDTH              : integer;
      DQ_WIDTH              : integer;
      DQ_PER_DQS            : integer;
      DQ_BITS               : integer;
      DQS_WIDTH             : integer;
      DQS_BITS              : integer;
      HIGH_PERFORMANCE_MODE : boolean;
      ODT_WIDTH             : integer;
      ROW_WIDTH             : integer;
      APPDATA_WIDTH         : integer;
      ADDITIVE_LAT          : integer;
      BURST_LEN             : integer;
      BURST_TYPE            : integer;
      CAS_LAT               : integer;
      ECC_ENABLE            : integer;
      MULTI_BANK_EN         : integer;
      ODT_TYPE              : integer;
      REDUCE_DRV            : integer;
      REG_ENABLE            : integer;
      TREFI_NS              : integer;
      TRAS                  : integer;
      TRCD                  : integer;
      TRFC                  : integer;
      TRP                   : integer;
      TRTP                  : integer;
      TWR                   : integer;
      TWTR                  : integer;
      SIM_ONLY              : integer;
      RST_ACT_LOW           : integer;
      CLK_TYPE                     : string;
      DLL_FREQ_MODE                : string;
      CLK_PERIOD            : integer
      );
    port (
      sys_rst_n             : in    std_logic;
      sys_clk_p             : in    std_logic;
      sys_clk_n             : in    std_logic;
      clk200_p              : in    std_logic;
      clk200_n              : in    std_logic;
      ddr2_a                : out   std_logic_vector((ROW_WIDTH-1) downto 0);
      ddr2_ba               : out   std_logic_vector((BANK_WIDTH-1) downto 0);
      ddr2_ras_n            : out   std_logic;
      ddr2_cas_n            : out   std_logic;
      ddr2_we_n             : out   std_logic;
      ddr2_cs_n             : out   std_logic_vector((CS_WIDTH-1) downto 0);
      ddr2_odt              : out   std_logic_vector((ODT_WIDTH-1) downto 0);
      ddr2_cke              : out   std_logic_vector((CKE_WIDTH-1) downto 0);
      ddr2_ck               : out   std_logic_vector((CLK_WIDTH-1) downto 0);
      ddr2_ck_n             : out   std_logic_vector((CLK_WIDTH-1) downto 0);
      ddr2_dq               : inout std_logic_vector((DQ_WIDTH-1) downto 0);
      ddr2_dqs              : inout std_logic_vector((DQS_WIDTH-1) downto 0);
      ddr2_dqs_n            : inout std_logic_vector((DQS_WIDTH-1) downto 0);
      ddr2_dm               : out   std_logic_vector((DM_WIDTH-1) downto 0);

      clk0_tb               : out   std_logic;
      rst0_tb               : out   std_logic;
      app_af_afull          : out   std_logic;
      app_wdf_afull         : out   std_logic;
      rd_data_valid         : out   std_logic;
      rd_data_fifo_out      : out   std_logic_vector(APPDATA_WIDTH-1 downto 0);
      app_af_wren           : in    std_logic;
      app_af_cmd            : in    std_logic_vector(2 downto 0);
      app_af_addr           : in    std_logic_vector(30 downto 0);
      app_wdf_wren          : in    std_logic;
      app_wdf_data          : in    std_logic_vector(APPDATA_WIDTH-1 downto 0);
      app_wdf_mask_data     : in    std_logic_vector(((APPDATA_WIDTH/8)-1) downto 0);

      phy_init_done         : out   std_logic
      );
  end component;

  component ddr2_model is
    port (
      ck      : in    std_logic;
      ck_n    : in    std_logic;
      cke     : in    std_logic;
      cs_n    : in    std_logic;
      ras_n   : in    std_logic;
      cas_n   : in    std_logic;
      we_n    : in    std_logic;
      dm_rdqs : inout std_logic_vector((DEVICE_WIDTH/16) downto 0);
      ba      : in    std_logic_vector((BANK_WIDTH - 1) downto 0);
      addr    : in    std_logic_vector((ROW_WIDTH - 1) downto 0);
      dq      : inout std_logic_vector((DEVICE_WIDTH - 1) downto 0);
      dqs     : inout std_logic_vector((DEVICE_WIDTH/16) downto 0);
      dqs_n   : inout std_logic_vector((DEVICE_WIDTH/16) downto 0);
      rdqs_n  : out   std_logic_vector((DEVICE_WIDTH/16) downto 0);
      odt     : in    std_logic
      );
  end component;

  component WireDelay
    generic (
      Delay_g : time;
      Delay_rd : time);
    port (
      A : inout Std_Logic;
      B : inout Std_Logic;
     reset : in Std_Logic);
  end component;

  component ddr2_tb_top is
    generic (
      BANK_WIDTH        : integer;
      COL_WIDTH         : integer;
      DM_WIDTH          : integer;
      DQ_WIDTH          : integer;
      ROW_WIDTH         : integer;
      APPDATA_WIDTH     : integer;
      ECC_ENABLE        : integer;
      BURST_LEN         : integer
      );
    port (
      clk0              : in  std_logic;
      rst0              : in  std_logic;
      app_af_afull      : in  std_logic;
      app_wdf_afull     : in  std_logic;
      rd_data_valid     : in  std_logic;
      rd_data_fifo_out  : in  std_logic_vector(APPDATA_WIDTH-1 downto 0);
      phy_init_done     : in  std_logic;
      app_af_wren       : out std_logic;
      app_af_cmd        : out std_logic_vector(2 downto 0);
      app_af_addr       : out std_logic_vector(30 downto 0);
      app_wdf_wren      : out std_logic;
      app_wdf_data      : out std_logic_vector(APPDATA_WIDTH-1 downto 0);
      app_wdf_mask_data : out std_logic_vector(((APPDATA_WIDTH/8)-1) downto 0);
      error             : out std_logic
      );
  end component;

  signal sys_clk                  : std_logic := '1'; -- RS Decoder requires clock to start at '0'
  signal sys_clk_n                : std_logic := '0';
  signal sys_clk_p                : std_logic := '0';
  signal sys_clk200               : std_logic:= '0';
  signal clk200_n                 : std_logic := '0';
  signal clk200_p                 : std_logic := '0';
  signal sys_rst_n                : std_logic := '0';
  signal sys_rst_out              : std_logic := '0';
  signal sys_rst_i                : std_logic := '0';
  signal gnd                      : std_logic_vector(1 downto 0) := (others => '0');

  signal ddr2_dq_sdram            : std_logic_vector((DQ_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_dqs_sdram           : std_logic_vector((DQS_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_dqs_n_sdram         : std_logic_vector((DQS_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_dm_sdram            : std_logic_vector((DM_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_clk_sdram           : std_logic_vector((CLK_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_clk_n_sdram         : std_logic_vector((CLK_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_address_sdram       : std_logic_vector((ROW_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_ba_sdram            : std_logic_vector((BANK_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_ras_n_sdram         : std_logic := '0';
  signal ddr2_cas_n_sdram         : std_logic := '0';
  signal ddr2_we_n_sdram          : std_logic := '0';
  signal ddr2_cs_n_sdram          : std_logic_vector((CS_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_cke_sdram           : std_logic_vector((CKE_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_odt_sdram           : std_logic_vector((ODT_WIDTH - 1) downto 0) := (others => '0');
  signal error                    : std_logic := '0';
  signal phy_init_done            : std_logic := '0';


  -- Only RDIMM memory parts support the reset signal,
  -- hence the ddr2_reset_n_sdram and ddr2_reset_n_fpga signals can be
  -- ignored for other memory parts
  signal ddr2_reset_n_sdram       : std_logic := '0';
  signal ddr2_reset_n_fpga        : std_logic := '0';
  signal ddr2_address_reg         : std_logic_vector((ROW_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_ba_reg              : std_logic_vector((BANK_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_cke_reg             : std_logic_vector((CKE_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_ras_n_reg           : std_logic := '0';
  signal ddr2_cas_n_reg           : std_logic := '0';
  signal ddr2_we_n_reg            : std_logic := '0';
  signal ddr2_cs_n_reg            : std_logic_vector((CS_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_odt_reg             : std_logic_vector((ODT_WIDTH - 1) downto 0) := (others => '0');

  signal dq_vector                : std_logic_vector(15 downto 0) := (others => '0');
  signal dqs_vector               : std_logic_vector(1 downto 0) := (others => '0');
  signal dqs_n_vector             : std_logic_vector(1 downto 0) := (others => '0');
  signal dm_vector                : std_logic_vector(1 downto 0) := (others => '0');
  signal command                  : std_logic_vector(2 downto 0) := (others => '0');
  signal enable                   : std_logic := '0';
  signal enable_o                 : std_logic := '0';
  signal ddr2_dq_fpga             : std_logic_vector((DQ_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_dqs_fpga            : std_logic_vector((DQS_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_dqs_n_fpga          : std_logic_vector((DQS_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_dm_fpga             : std_logic_vector((DM_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_clk_fpga            : std_logic_vector((CLK_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_clk_n_fpga          : std_logic_vector((CLK_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_address_fpga        : std_logic_vector((ROW_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_ba_fpga             : std_logic_vector((BANK_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_ras_n_fpga          : std_logic := '0';
  signal ddr2_cas_n_fpga          : std_logic := '0';
  signal ddr2_we_n_fpga           : std_logic := '0';
  signal ddr2_cs_n_fpga           : std_logic_vector((CS_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_cke_fpga            : std_logic_vector((CKE_WIDTH - 1) downto 0) := (others => '0');
  signal ddr2_odt_fpga            : std_logic_vector((ODT_WIDTH - 1) downto 0) := (others => '0');

  signal clk0_tb            : std_logic := '0';
  signal rst0_tb            : std_logic := '0';
  signal app_af_afull       : std_logic := '0';
  signal app_wdf_afull      : std_logic := '0';
  signal rd_data_valid      : std_logic := '0';
  signal rd_data_fifo_out   : std_logic_vector(APPDATA_WIDTH-1 downto 0) := (others => '0');
  signal app_af_wren        : std_logic := '0';
  signal app_af_cmd         : std_logic_vector(2 downto 0) := (others => '0');
  signal app_af_addr        : std_logic_vector(30 downto 0) := (others => '0');
  signal app_wdf_wren       : std_logic := '0';
  signal app_wdf_data       : std_logic_vector(APPDATA_WIDTH-1 downto 0) := (others => '0');
  signal app_wdf_mask_data  : std_logic_vector((APPDATA_WIDTH/8)-1 downto 0) := (others => '0');

  signal DBG_BTN_IN : std_logic := '0';

    -- Component Declaration for the Unit Under Test (UUT)

	COMPONENT FlowController
	PORT(
		ETH_DATA_IN : IN std_logic_vector(7 downto 0);
		ETH_DV_IN : IN std_logic;
		ETH_GF_IN : IN std_logic;
		ETH_BF_IN : IN std_logic;
		ETH_CLK_IN : IN std_logic;
		ETH_ACK_OUT : IN std_logic;
		ETH_CLK_OUT : IN std_logic;
		SFP_DATA_IN : IN std_logic_vector(7 downto 0);
		SFP_DV_IN : IN std_logic;
		SFP_GF_IN : IN std_logic;
		SFP_BF_IN : IN std_logic;
		SFP_CLK_IN : IN std_logic;
		SFP_ACK_OUT : IN std_logic;
		SFP_CLK_OUT : IN std_logic;
		FIFO_TO_UB_RD_EN_IN : IN std_logic;
		FIFO_FROM_UB_DATA_IN : IN std_logic_vector(8 downto 0);
		FIFO_FROM_UB_WR_EN_IN : IN std_logic;
		UBLAZE_MAC_IN : IN std_logic_vector(47 downto 0);
		CLK_UBLAZE_IN : IN std_logic;
		SYS_CLK : IN std_logic;
		SYS_RST : IN std_logic;          
		ETH_DATA_OUT : OUT std_logic_vector(7 downto 0);
		ETH_DV_OUT : OUT std_logic;
		SFP_DATA_OUT : OUT std_logic_vector(7 downto 0);
		SFP_DV_OUT : OUT std_logic;
		FIFO_TO_UB_DATA_OUT : OUT std_logic_vector(8 downto 0);
		FIFO_TO_UB_EMPTY : OUT std_logic;
		FIFO_FROM_UB_PROG_FULL : OUT std_logic
		);
	END COMPONENT;

	COMPONENT SFP_Simplified_MAC
	PORT(
		RAW_RX : IN std_logic_vector(9 downto 0);
		CLK_RX : IN std_logic;
		CLK_TX : IN std_logic;
		SYS_CLK : IN std_logic;
		SYS_RST : IN std_logic;
		DBG_BTN_IN : IN std_logic;
		DV_TX : IN std_logic;
		DOUT_TX : IN std_logic_vector(7 downto 0);
		dout : IN std_logic_vector(8 downto 0);
		full : IN std_logic;
		empty_prog : IN std_logic;
		empty : IN std_logic;
		RAW_TX : OUT std_logic_vector(9 downto 0);
		ALIGNED : OUT std_logic;
		CHANNEL_CODING_USED : in STD_LOGIC := '0';
		BER_SIMULATION_EN : in STD_LOGIC := '0';
		BER_SIMULATION_COMP : in STD_LOGIC_VECTOR (31 downto 0) := (OTHERS => '0');

		RX_FRAMES_RECEIVED : OUT std_logic_vector(63 downto 0);
		RX_FRAMES_BAD : OUT std_logic_vector(63 downto 0);
		RX_BYTES_CORRECTED : OUT std_logic_vector(63 downto 0);
		RX_BYTES_RECEIVED : OUT std_logic_vector(63 downto 0);
		TX_FRAMES_SENT : OUT std_logic_vector(63 downto 0);
		TX_FRAMES_TRUNCATED : OUT std_logic_vector(63 downto 0);
		TX_BYTES_SENT : OUT std_logic_vector(63 downto 0);
		TX_BYTES_DROPPED : OUT std_logic_vector(63 downto 0);
		STAT_DOUT : OUT STD_LOGIC_VECTOR(63 downto 0);
		STAT_COUNT : OUT STD_LOGIC_VECTOR(9 downto 0);
		STAT_RD_EN : IN STD_LOGIC;

		DV_RX : OUT std_logic;
		GF_RX : OUT std_logic;
		BF_RX : OUT std_logic;
		DIN_RX : OUT std_logic_vector(7 downto 0);
		ACK_TX : OUT std_logic;
		wr_clk : OUT std_logic;
		rd_clk : OUT std_logic;
		din : OUT std_logic_vector(8 downto 0);
		wr_en : OUT std_logic;
		rd_en : OUT std_logic
		);
	END COMPONENT;


   --Inputs
   signal ETH_DATA_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal ETH_DV_IN : std_logic := '0';
   signal ETH_GF_IN : std_logic := '0';
   signal ETH_BF_IN : std_logic := '0';
   signal ETH_CLK_IN : std_logic := '0';
   signal ETH_ACK_OUT : std_logic := '0';
   signal ETH_CLK_OUT : std_logic := '0';
   signal SFP_DATA_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal SFP_DV_IN : std_logic := '0';
   signal SFP_GF_IN : std_logic := '0';
   signal SFP_BF_IN : std_logic := '0';
   signal SFP_CLK_IN : std_logic := '0';
   signal SFP_ACK_OUT : std_logic := '0';
   signal SFP_CLK_OUT : std_logic := '0';
   signal FIFO_TO_UB_DATA_OUT      :  STD_LOGIC_VECTOR (8 downto 0) := (OTHERS => '0');
   signal FIFO_TO_UB_RD_EN_IN      :  STD_LOGIC := '0';
   signal FIFO_TO_UB_EMPTY         :  STD_LOGIC := '0';
   signal FIFO_FROM_UB_DATA_IN     : STD_LOGIC_VECTOR (8 downto 0) := (OTHERS => '0');
   signal FIFO_FROM_UB_WR_EN_IN    :  STD_LOGIC := '0';
   signal FIFO_FROM_UB_PROG_FULL        :  STD_LOGIC := '0';
   signal UBLAZE_MAC_IN            : STD_LOGIC_VECTOR(47 downto 0);
   signal CLK_UBLAZE_IN            : STD_LOGIC;
   signal SYS_RST : std_logic := '0';
	signal RAW_RX : std_logic_vector(9 downto 0) := (others => '0');

  --- DDR FIFO interface
   SIGNAL  DDR_FIFO_wr_clk     :    std_logic := '0';
   SIGNAL  DDR_FIFO_rd_clk     :    std_logic := '0';
   SIGNAL  DDR_FIFO_din        :    std_logic_vector(8 downto 0) := (OTHERS => '0');
   SIGNAL  DDR_FIFO_wr_en      :    std_logic := '0';
   SIGNAL  DDR_FIFO_rd_en      :    std_logic := '0';
   SIGNAL  DDR_FIFO_dout       :    std_logic_vector(8 downto 0) := (OTHERS => '0');
   SIGNAL  DDR_FIFO_full       :    std_logic := '0';
   SIGNAL  DDR_FIFO_empty      :    std_logic := '0';
	SIGNAL  DDR_FIFO_empty_prog :    std_logic := '0';


 	--Outputs
   signal ETH_DATA_OUT : std_logic_vector(7 downto 0) := (others => '0');
   signal ETH_DV_OUT : std_logic:= '0';
   signal SFP_DATA_OUT : std_logic_vector(7 downto 0) := (others => '0');
   signal SFP_DV_OUT : std_logic:= '0';
   signal UBLAZE_DATA_OUT : std_logic_vector(7 downto 0) := (others => '0');
   signal UBLAZE_DV_OUT : std_logic:= '0';
	signal ALIGNED : std_logic:= '0';
	signal RAW_TX : std_logic_vector(9 downto 0) := (others => '0');

	signal din : std_logic_vector(8 downto 0) := (others => '0');
	signal dout : std_logic_vector(8 downto 0) := (others => '0');

	signal CHECK_CNTR : std_logic_vector(7 downto 0) := (others => '0');

	signal rand_num : integer := 0;
	signal rand_len : integer := 60;
	signal this_len : integer := 60;

BEGIN

process(SYS_CLK)
    variable seed1, seed2: positive;               -- seed values for random generator
    variable rand: real;   -- random real-number value in range 0 to 1.0
    variable range_of_rand : real := 280.0;    -- the range of random values created will be 0 to +1000.
begin
    uniform(seed1, seed2, rand);   -- generate random number
    rand_num <= 1 + integer(rand*range_of_rand);  -- rescale to 0..1000, convert integer part
end process;


process(SYS_CLK)
    variable seed1, seed2: positive;               -- seed values for random generator
    variable rand: real;   -- random real-number value in range 0 to 1.0
    variable range_of_rand : real := 190.0;    -- the range of random values created will be 0 to +1000.
begin
    uniform(seed1, seed2, rand);   -- generate random number
    rand_len <= 60 + integer(rand*range_of_rand);  -- rescale to 0..1000, convert integer part
end process;


  gnd <= "00";
   --***************************************************************************
   -- Clock generation and reset
   --***************************************************************************
  process
  begin
    sys_clk <= not sys_clk;
    wait for (TCYC_SYS_DIV2);
  end process;

   sys_clk_p <= sys_clk;
   sys_clk_n <= not sys_clk;

   process
   begin
     sys_clk200 <= not sys_clk200;
     wait for (TCYC_200);
   end process;

   clk200_p <= sys_clk200;
   clk200_n <= not sys_clk200;

   process
   begin
      sys_rst <= '1';
      wait for 400 ns;
      sys_rst <= '0';
      wait;
   end process;

  sys_rst_i   <=  sys_rst;
  sys_rst_out <= (not sys_rst) when (RST_ACT_LOW = 1) else (sys_rst);


   --***************************************************************************
   -- FPGA memory controller
   --***************************************************************************

  u_mem_controller : DDRcontroller
    generic map (
      BANK_WIDTH            => BANK_WIDTH,
      CKE_WIDTH             => CKE_WIDTH,
      CLK_WIDTH             => CLK_WIDTH,
      COL_WIDTH             => COL_WIDTH,
      CS_NUM                => CS_NUM,
      CS_WIDTH              => CS_WIDTH,
      CS_BITS               => CS_BITS,
      DM_WIDTH                     => DM_WIDTH,
      DQ_WIDTH              => DQ_WIDTH,
      DQ_PER_DQS            => DQ_PER_DQS,
      DQ_BITS               => DQ_BITS,
      DQS_WIDTH             => DQS_WIDTH,
      DQS_BITS              => DQS_BITS,
      HIGH_PERFORMANCE_MODE => HIGH_PERFORMANCE_MODE,
      ODT_WIDTH             => ODT_WIDTH,
      ROW_WIDTH             => ROW_WIDTH,
      APPDATA_WIDTH         => APPDATA_WIDTH,
      ADDITIVE_LAT          => ADDITIVE_LAT,
      BURST_LEN             => BURST_LEN,
      BURST_TYPE            => BURST_TYPE,
      CAS_LAT               => CAS_LAT,
      ECC_ENABLE            => ECC_ENABLE,
      MULTI_BANK_EN         => MULTI_BANK_EN,
      ODT_TYPE              => ODT_TYPE,
      REDUCE_DRV            => REDUCE_DRV,
      REG_ENABLE            => REG_ENABLE,
      TREFI_NS              => TREFI_NS,
      TRAS                  => TRAS,
      TRCD                  => TRCD,
      TRFC                  => TRFC,
      TRP                   => TRP,
      TRTP                  => TRTP,
      TWR                   => TWR,
      TWTR                  => TWTR,
      SIM_ONLY              => SIM_ONLY,
      RST_ACT_LOW           => RST_ACT_LOW,
      CLK_TYPE              => CLK_TYPE,
      DLL_FREQ_MODE         => DLL_FREQ_MODE,
      CLK_PERIOD            => CLK_PERIOD
      )
    port map (
      sys_clk_p         => sys_clk_p,
      sys_clk_n         => sys_clk_n,
      clk200_p          => clk200_p,
      clk200_n          => clk200_n,
      sys_rst_n         => sys_rst_out,
      ddr2_ras_n        => ddr2_ras_n_fpga,
      ddr2_cas_n        => ddr2_cas_n_fpga,
      ddr2_we_n         => ddr2_we_n_fpga,
      ddr2_cs_n         => ddr2_cs_n_fpga,
      ddr2_cke          => ddr2_cke_fpga,
      ddr2_odt          => ddr2_odt_fpga,
      ddr2_dm           => ddr2_dm_fpga,
      ddr2_dq           => ddr2_dq_fpga,
      ddr2_dqs          => ddr2_dqs_fpga,
      ddr2_dqs_n        => ddr2_dqs_n_fpga,
      ddr2_ck           => ddr2_clk_fpga,
      ddr2_ck_n         => ddr2_clk_n_fpga,
      ddr2_ba           => ddr2_ba_fpga,
      ddr2_a            => ddr2_address_fpga,

      clk0_tb           => clk0_tb,
      rst0_tb           => rst0_tb,
      app_af_afull      => app_af_afull,
      app_wdf_afull     => app_wdf_afull,
      rd_data_valid     => rd_data_valid,
      rd_data_fifo_out  => rd_data_fifo_out,
      app_af_wren       => app_af_wren,
      app_af_cmd        => app_af_cmd,
      app_af_addr       => app_af_addr,
      app_wdf_wren      => app_wdf_wren,
      app_wdf_data      => app_wdf_data,
      app_wdf_mask_data => app_wdf_mask_data,

      phy_init_done     => phy_init_done
      );

  --***************************************************************************
  -- Delay insertion modules for each signal
  --***************************************************************************
  -- Use standard non-inertial (transport) delay mechanism for unidirectional
  -- signals from FPGA to SDRAM
  ddr2_address_sdram  <= TRANSPORT ddr2_address_fpga after TPROP_PCB_CTRL;
  ddr2_ba_sdram       <= TRANSPORT ddr2_ba_fpga      after TPROP_PCB_CTRL;
  ddr2_ras_n_sdram    <= TRANSPORT ddr2_ras_n_fpga   after TPROP_PCB_CTRL;
  ddr2_cas_n_sdram    <= TRANSPORT ddr2_cas_n_fpga   after TPROP_PCB_CTRL;
  ddr2_we_n_sdram     <= TRANSPORT ddr2_we_n_fpga    after TPROP_PCB_CTRL;
  ddr2_cs_n_sdram     <= TRANSPORT ddr2_cs_n_fpga    after TPROP_PCB_CTRL;
  ddr2_cke_sdram      <= TRANSPORT ddr2_cke_fpga     after TPROP_PCB_CTRL;
  ddr2_odt_sdram      <= TRANSPORT ddr2_odt_fpga     after TPROP_PCB_CTRL;
  ddr2_clk_sdram      <= TRANSPORT ddr2_clk_fpga     after TPROP_PCB_CTRL;
  ddr2_clk_n_sdram    <= TRANSPORT ddr2_clk_n_fpga   after TPROP_PCB_CTRL;
  ddr2_reset_n_sdram  <= TRANSPORT ddr2_reset_n_fpga after TPROP_PCB_CTRL;
  ddr2_dm_sdram       <= TRANSPORT ddr2_dm_fpga      after TPROP_PCB_DATA;

  dq_delay: for i in 0 to DQ_WIDTH - 1 generate
    u_delay_dq: WireDelay
      generic map (
        Delay_g => TPROP_PCB_DATA,
        Delay_rd => TPROP_PCB_DATA_RD)
      port map(
        A => ddr2_dq_fpga(i),
        B => ddr2_dq_sdram(i),
        reset => sys_rst_n);
  end generate;

  dqs_delay: for i in 0 to DQS_WIDTH - 1 generate
    u_delay_dqs: WireDelay
      generic map (
        Delay_g => TPROP_DQS,
        Delay_rd => TPROP_DQS_RD)
      port map(
        A => ddr2_dqs_fpga(i),
        B => ddr2_dqs_sdram(i),
        reset => sys_rst_n);
  end generate;

  dqs_n_delay: for i in 0 to DQS_WIDTH - 1 generate
    u_delay_dqs: WireDelay
      generic map (
        Delay_g => TPROP_DQS,
        Delay_rd => TPROP_DQS_RD)
      port map(
        A => ddr2_dqs_n_fpga(i),
        B => ddr2_dqs_n_sdram(i),
        reset => sys_rst_n);
  end generate;

  -- Extra one clock pipelining for RDIMM address and
  -- control signals is implemented here (Implemented external to memory model)
  process (ddr2_clk_sdram)
  begin
    if (rising_edge(ddr2_clk_sdram(0))) then
      if ( ddr2_reset_n_sdram = '0' ) then
        ddr2_ras_n_reg    <= '1';
        ddr2_cas_n_reg    <= '1';
        ddr2_we_n_reg     <= '1';
        ddr2_cs_n_reg     <= (others => '1');
        ddr2_odt_reg      <= (others => '0');
      else
        ddr2_address_reg  <= TRANSPORT ddr2_address_sdram after TCYC_SYS_DIV2;
        ddr2_ba_reg       <= TRANSPORT ddr2_ba_sdram      after TCYC_SYS_DIV2;
        ddr2_ras_n_reg    <= TRANSPORT ddr2_ras_n_sdram   after TCYC_SYS_DIV2;
        ddr2_cas_n_reg    <= TRANSPORT ddr2_cas_n_sdram   after TCYC_SYS_DIV2;
        ddr2_we_n_reg     <= TRANSPORT ddr2_we_n_sdram    after TCYC_SYS_DIV2;
        ddr2_cs_n_reg     <= TRANSPORT ddr2_cs_n_sdram    after TCYC_SYS_DIV2;
        ddr2_odt_reg      <= TRANSPORT ddr2_odt_sdram     after TCYC_SYS_DIV2;
      end if;
    end if;
  end process;

  -- to avoid tIS violations on CKE when reset is deasserted
  process (ddr2_clk_n_sdram)
  begin
    if (rising_edge(ddr2_clk_n_sdram(0))) then
      if ( ddr2_reset_n_sdram = '0' ) then
        ddr2_cke_reg      <= (others => '0');
      else
        ddr2_cke_reg      <= TRANSPORT ddr2_cke_sdram after TCYC_SYS_0;
      end if;
    end if;
  end process;


	-- Instantiate the Unit Under Test (UUT)
   uut: FlowController PORT MAP (
          ETH_DATA_IN => ETH_DATA_IN,
          ETH_DV_IN => ETH_DV_IN,
          ETH_GF_IN => ETH_GF_IN,
          ETH_BF_IN => ETH_BF_IN,
          ETH_CLK_IN => ETH_CLK_IN,

          ETH_DATA_OUT => ETH_DATA_OUT,
          ETH_DV_OUT => ETH_DV_OUT,
          ETH_ACK_OUT => ETH_ACK_OUT,
          ETH_CLK_OUT => ETH_CLK_OUT,

          SFP_DATA_IN => SFP_DATA_IN,
          SFP_DV_IN => SFP_DV_IN,
          SFP_GF_IN => SFP_GF_IN,
          SFP_BF_IN => SFP_BF_IN,
          SFP_CLK_IN => SFP_CLK_IN,

          SFP_DATA_OUT => SFP_DATA_OUT,
          SFP_DV_OUT => SFP_DV_OUT,
          SFP_ACK_OUT => SFP_ACK_OUT,
          SFP_CLK_OUT => SFP_CLK_OUT,

          FIFO_TO_UB_DATA_OUT      => FIFO_TO_UB_DATA_OUT,
          FIFO_TO_UB_RD_EN_IN      => FIFO_TO_UB_RD_EN_IN,
          FIFO_TO_UB_EMPTY         => FIFO_TO_UB_EMPTY,
          FIFO_FROM_UB_DATA_IN     => FIFO_FROM_UB_DATA_IN,
          FIFO_FROM_UB_WR_EN_IN    => FIFO_FROM_UB_WR_EN_IN,
          FIFO_FROM_UB_PROG_FULL   => FIFO_FROM_UB_PROG_FULL,
          UBLAZE_MAC_IN            => UBLAZE_MAC_IN,
          CLK_UBLAZE_IN            => CLK_UBLAZE_IN,

          SYS_CLK => SYS_CLK,
          SYS_RST => SYS_RST
        );
	  uut2: SFP_Simplified_MAC PORT MAP (
			RAW_RX => RAW_RX,
			CLK_RX => SYS_CLK,
			RAW_TX => RAW_TX,
			CLK_TX => SYS_CLK,

			SYS_CLK => SYS_CLK,
			SYS_RST => SYS_RST,
			ALIGNED => ALIGNED,
			DBG_BTN_IN => DBG_BTN_IN,
			CHANNEL_CODING_USED => '1',
			RX_FRAMES_RECEIVED => open,
			RX_FRAMES_BAD => open,
			RX_BYTES_CORRECTED => open,
			RX_BYTES_RECEIVED => open,
			TX_FRAMES_SENT => open,
			TX_FRAMES_TRUNCATED => open,
			TX_BYTES_SENT => open,
			TX_BYTES_DROPPED => open,
			STAT_DOUT => open,
			STAT_COUNT => open,
			STAT_RD_EN => '0',
			
			BER_SIMULATION_EN => '1',
			BER_SIMULATION_COMP => X"00000FFF", --! BER = ~10^-5

			DV_RX => SFP_DV_IN,
			GF_RX => SFP_GF_IN,
			BF_RX => SFP_BF_IN,
			DIN_RX => SFP_DATA_IN,

			DV_TX => SFP_DV_OUT,
			ACK_TX => SFP_ACK_OUT,
			DOUT_TX => SFP_DATA_OUT,

			wr_clk => DDR_FIFO_wr_clk,
			rd_clk => DDR_FIFO_rd_clk,
			din => DDR_FIFO_din,
			wr_en => DDR_FIFO_wr_en,
			rd_en => DDR_FIFO_rd_en,
			dout => DDR_FIFO_dout,
			full => DDR_FIFO_full,
			empty => DDR_FIFO_empty,
			empty_prog => DDR_FIFO_empty_prog
        );


		SFP_DDRfifo: entity WORK.DDRfifo
		GENERIC MAP (
				APPDATA_WIDTH => APPDATA_WIDTH,
				FIFO_SIZE_POW2 => 16
		)
		PORT MAP(
				rst => SYS_RST,
				wr_clk => sys_clk,
				rd_clk => sys_clk,
				din => DDR_FIFO_din,
				wr_en => DDR_FIFO_wr_en,
				rd_en => DDR_FIFO_rd_en,
				dout => DDR_FIFO_dout,
				full => DDR_FIFO_full,
				empty => DDR_FIFO_empty,
				empty_prog => DDR_FIFO_empty_prog,

				sys_rst_n => sys_rst_n, --sys_rst_n,
				phy_init_done => phy_init_done,
				rst0_tb => rst0_tb,
				clk0_tb => clk0_tb,
				app_wdf_afull => app_wdf_afull,
				app_af_afull => app_af_afull,
				rd_data_valid => rd_data_valid,
				app_wdf_wren => app_wdf_wren,
				app_af_wren => app_af_wren,
				app_af_addr => app_af_addr,
				app_af_cmd => app_af_cmd,
				rd_data_fifo_out => rd_data_fifo_out,
				app_wdf_data => app_wdf_data,
				app_wdf_mask_data => app_wdf_mask_data
			);

  comp_16: if (DEVICE_WIDTH = 16) generate
    -- if memory part is x16
    registered_dimm: if (REG_ENABLE = 1) generate
      -- if the memory part is Registered DIMM
      gen_cs: for j in 0 to (CS_NUM - 1) generate
        gen: for i in 0 to (DQS_WIDTH/2 - 1) generate
          u_mem0: ddr2_model
            port map (
              ck        => ddr2_clk_sdram(CLK_WIDTH*i/DQS_WIDTH),
              ck_n      => ddr2_clk_n_sdram(CLK_WIDTH*i/DQS_WIDTH),
              cke       => ddr2_cke_reg(j),
              cs_n      => ddr2_cs_n_reg((CS_WIDTH*i/DQS_WIDTH)),
              ras_n     => ddr2_ras_n_reg,
              cas_n     => ddr2_cas_n_reg,
              we_n      => ddr2_we_n_reg,
              dm_rdqs   => ddr2_dm_sdram((2*(i+1))-1 downto i*2),
              ba        => ddr2_ba_reg,
              addr      => ddr2_address_reg,
              dq        => ddr2_dq_sdram((16*(i+1))-1 downto i*16),
              dqs       => ddr2_dqs_sdram((2*(i+1))-1 downto i*2),
              dqs_n     => ddr2_dqs_n_sdram((2*(i+1))-1 downto i*2),
              rdqs_n    => open,
              odt       => ddr2_odt_reg(ODT_WIDTH*i/DQS_WIDTH)
              );
        end generate gen;
      end generate gen_cs;
    end generate registered_dimm;
    -- if the memory part is component or unbuffered DIMM
    comp16_mul8: if (((DQ_WIDTH mod 16) /= 0) and (REG_ENABLE = 0)) generate
      -- for the memory part x16, if the data width is not multiple
      -- of 16, memory models are instantiated for all data with x16
      -- memory model and except for MSB data. For the MSB data
      -- of 8 bits, all memory data, strobe and mask data signals are
      -- replicated to make it as x16 part. For example if the design
      -- is generated for data width of 72, memory model x16 parts
      -- instantiated for 4 times with data ranging from 0 to 63.
      -- For MSB data ranging from 64 to 71, one x16 memory model
      -- by replicating the 8-bit data twice and similarly
      -- the case with data mask and strobe.
      gen_cs: for j in 0 to (CS_NUM - 1) generate
        gen: for i in 0 to (DQ_WIDTH/16 - 1) generate
          u_mem0: ddr2_model
            port map (
              ck        => ddr2_clk_sdram(CLK_WIDTH*i/DQS_WIDTH),
            ck_n      => ddr2_clk_n_sdram(CLK_WIDTH*i/DQS_WIDTH),
              cke       => ddr2_cke_sdram(j),
              cs_n      => ddr2_cs_n_sdram(CS_WIDTH*i/DQS_WIDTH),
              ras_n     => ddr2_ras_n_sdram,
              cas_n     => ddr2_cas_n_sdram,
              we_n      => ddr2_we_n_sdram,
              dm_rdqs   => ddr2_dm_sdram((2*(i+1))-1 downto i*2),
              ba        => ddr2_ba_sdram,
              addr      => ddr2_address_sdram,
              dq        => ddr2_dq_sdram((16*(i+1))-1 downto i*16),
              dqs       => ddr2_dqs_sdram((2*(i+1))-1 downto i*2),
              dqs_n     => ddr2_dqs_n_sdram((2*(i+1))-1 downto i*2),
              rdqs_n    => open,
              odt       => ddr2_odt_sdram(ODT_WIDTH*i/DQS_WIDTH)
              );
        end generate gen;

        --Logic to assign the remaining bits of DQ and DQS
        u1: for i in 0 to 7 generate
           u_delay_dq: WireDelay
              generic map (
                 Delay_g  => 0 ps,
                 Delay_rd => 0 ps)
              port map(
                A => ddr2_dq_sdram(DQ_WIDTH - 8 + i),
                B => dq_vector(i),
                reset => sys_rst_n);
        end generate;

        u2: WireDelay
          generic map (
            Delay_g  => 0 ps,
            Delay_rd => 0 ps)
          port map(
            A => ddr2_dqs_sdram(DQS_WIDTH - 1),
            B => dqs_vector(0),
            reset => sys_rst_n);

        u3: WireDelay
          generic map (
            Delay_g  => 0 ps,
            Delay_rd => 0 ps)
          port map(
            A => ddr2_dqs_n_sdram(DQS_WIDTH - 1),
            B => dqs_n_vector(0),
            reset => sys_rst_n);

        dq_vector(15 downto 8) <= dq_vector(7 downto 0);
        dqs_vector(1)          <= dqs_vector(0);
        dqs_n_vector(1)        <= dqs_n_vector(0);
        dm_vector              <= (ddr2_dm_sdram(DM_WIDTH - 1) &
                                   ddr2_dm_sdram(DM_WIDTH - 1));

        u_mem1: ddr2_model
          port map (
            ck        => ddr2_clk_sdram(CLK_WIDTH-1),
            ck_n      => ddr2_clk_n_sdram(CLK_WIDTH-1),
            cke       => ddr2_cke_sdram(j),
            cs_n      => ddr2_cs_n_sdram(CS_WIDTH-1),
            ras_n     => ddr2_ras_n_sdram,
            cas_n     => ddr2_cas_n_sdram,
            we_n      => ddr2_we_n_sdram,
            dm_rdqs   => dm_vector,
            ba        => ddr2_ba_sdram,
            addr      => ddr2_address_sdram,
            dq        => dq_vector,
            dqs       => dqs_vector,
            dqs_n     => dqs_n_vector,
            rdqs_n    => open,
            odt       => ddr2_odt_sdram(ODT_WIDTH-1)
            );
      end generate gen_cs;
    end generate comp16_mul8;
    comp16_mul16: if (((DQ_WIDTH mod 16) = 0) and (REG_ENABLE = 0)) generate
      -- if the data width is multiple of 16
      gen_cs: for j in 0 to (CS_NUM - 1) generate
        gen: for i in 0 to ((DQS_WIDTH/2) - 1) generate
          u_mem0: ddr2_model
            port map (
              ck        => ddr2_clk_sdram(CLK_WIDTH*i/DQS_WIDTH),
            ck_n      => ddr2_clk_n_sdram(CLK_WIDTH*i/DQS_WIDTH),
              cke       => ddr2_cke_sdram(j),
              cs_n      => ddr2_cs_n_sdram(CS_WIDTH*i/DQS_WIDTH),
              ras_n     => ddr2_ras_n_sdram,
              cas_n     => ddr2_cas_n_sdram,
              we_n      => ddr2_we_n_sdram,
              dm_rdqs   => ddr2_dm_sdram((2*(i+1))-1 downto i*2),
              ba        => ddr2_ba_sdram,
              addr      => ddr2_address_sdram,
              dq        => ddr2_dq_sdram((16*(i+1))-1 downto i*16),
              dqs       => ddr2_dqs_sdram((2*(i+1))-1 downto i*2),
              dqs_n     => ddr2_dqs_n_sdram((2*(i+1))-1 downto i*2),
              rdqs_n    => open,
              odt       => ddr2_odt_sdram(ODT_WIDTH*i/DQS_WIDTH)
              );
        end generate gen;
      end generate gen_cs;
    end generate comp16_mul16;
  end generate comp_16;

	ETH_ACK_OUT <= ETH_DV_OUT;
	CLK_UBLAZE_IN <= SYS_CLK;
	ETH_CLK_IN <= SYS_CLK;
	ETH_CLK_OUT <= SYS_CLK;
	SFP_CLK_IN <= SYS_CLK;
	SFP_CLK_OUT <= SYS_CLK;
	
	-- Loopback
	RAW_RX <= RAW_TX;

   -- Stimulus process
   stim_proc: process
   begin
	
		-- Wait for first rising edge
		wait until rising_edge(SYS_CLK);
		
		-- Send 100000 frames
		for MASTERLOOP in 0 to 100000 loop

				-- Read random length from
				-- the random number generator
				this_len <= rand_len;
				
				-- Send the Frame
				ETH_DV_IN <= '1';
				ETH_DATA_IN <= X"00";
				wait for TCYC_SYS_0;
				for I in 0 to this_len loop
					ETH_DATA_IN <= ETH_DATA_IN + X"01";
					wait for TCYC_SYS_0;
				end loop;
				ETH_DV_IN <= '0';
				wait for TCYC_SYS_0;
				ETH_GF_IN <= '1';
				wait for TCYC_SYS_0;
				ETH_GF_IN <= '0';

				-- Insert a randomly long pause
				wait for rand_num*TCYC_SYS_0;
		end loop;
		
		-- Simulation is done
		assert false report "Simulation Finished" severity failure;
      wait;
   end process;


	-- Validation process
	process(SYS_CLK)
	begin
		IF rising_edge(SYS_CLK) THEN
			IF ETH_DV_OUT = '1' THEN
				CHECK_CNTR <= CHECK_CNTR + "1";
				IF CHECK_CNTR /= ETH_DATA_OUT THEN
					assert false report "Error in data" severity warning;
				END IF;
			ELSE
				CHECK_CNTR <= X"00";
			END IF;
		END IF;
	end process;

END;
