library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity SignalSynchronizer is
	 Generic (
			DATA_WIDTH : integer := 1
	 );
    Port ( sig_clk1 : in std_logic_vector (DATA_WIDTH-1 downto 0) := (OTHERS => '0');
           clk1 : in  STD_LOGIC;
           sig_clk2 : out std_logic_vector (DATA_WIDTH-1 downto 0) := (OTHERS => '0');
           clk2 : in  STD_LOGIC;
           arst : in  STD_LOGIC);
			  
end SignalSynchronizer;

architecture Behavioral of SignalSynchronizer is

	signal wr_en : std_logic := '0';
	signal rd_en : std_logic := '0';
	signal full : std_logic := '0';
	signal empty : std_logic := '0';
	signal clr : std_logic := '1';

	COMPONENT aFifo
    generic (
        DATA_WIDTH :integer := 8;
        ADDR_WIDTH :integer := 4
    );
    port (
        -- Reading port.
        Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
        Empty_out   :out std_logic;
        ReadEn_in   :in  std_logic;
        RClk        :in  std_logic;
        -- Writing port.
        Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
        Full_out    :out std_logic;
        WriteEn_in  :in  std_logic;
        WClk        :in  std_logic;
	 
        Clear_in    :in  std_logic
    );
	END COMPONENT;

begin
FIFO_SignalSynchronizer_i: aFifo 
	GENERIC MAP (
		DATA_WIDTH => DATA_WIDTH,
		ADDR_WIDTH => 2 -- 4 clock latency?
	)
	PORT MAP(
		Data_out => sig_clk2,
		Empty_out => empty,
		ReadEn_in => rd_en,
		RClk => clk2,
		
		Data_in => sig_clk1,
		Full_out => full,
		WriteEn_in => wr_en,
		WClk => clk1,
		Clear_in => clr
	);

process(clk1)
begin	
	if rising_edge(clk1) then
		wr_en <= '0';
		if full = '0' then
			wr_en <= '1';
		end if;
		
		if clr = '1' then
			clr <= '0';
		end if;
		
	end if;
end process;

process(clk2)
begin	
	if rising_edge(clk2) then
		rd_en <= '0';
		if empty = '0' then
			rd_en <= '1';
		end if;
	end if;
end process;


end Behavioral;

