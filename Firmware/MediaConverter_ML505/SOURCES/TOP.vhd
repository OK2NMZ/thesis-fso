----------------------------------------------------------------------------------
--
--  ML505 clock settings:
--
--     for 125 MHz (Gigabit Ethernet) set:
--     N0  N1  N2  M0  M1  M2  SEL1  SEL0
--     0   0   1   1   1   0   1     0
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library WORK;
use WORK.modules_pkg.ALL;
use WORK.parameters_pkg.ALL;
use WORK.types_pkg.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;
----------------------------------------------------------------------------------
ENTITY Splitter_TOP IS
 GENERIC(
 -- DDR  ----------------------------------------------------------------------
   BANK_WIDTH               : integer := 2;
                              -- # of memory bank addr bits.
   CKE_WIDTH                : integer := 1;
                              -- # of memory clock enable outputs.
   CLK_WIDTH                : integer := 2;
                              -- # of clock outputs.
   COL_WIDTH                : integer := 10;
                              -- # of memory column bits.
   CS_NUM                   : integer := 1;
                              -- # of separate memory chip selects.
   CS_WIDTH                 : integer := 1;
                              -- # of total memory chip selects.
   CS_BITS                  : integer := 0;
                              -- set to log2(CS_NUM) (rounded up).
   DM_WIDTH                 : integer := 8;
                              -- # of data mask bits.
   DQ_WIDTH                 : integer := 64;
                              -- # of data width.
   DQ_PER_DQS               : integer := 8;
                              -- # of DQ data bits per strobe.
   DQS_WIDTH                : integer := 8;
                              -- # of DQS strobes.
   DQ_BITS                  : integer := 6;
                              -- set to log2(DQS_WIDTH*DQ_PER_DQS).
   DQS_BITS                 : integer := 3;
                              -- set to log2(DQS_WIDTH).
   ODT_WIDTH                : integer := 1;
                              -- # of memory on-die term enables.
   ROW_WIDTH                : integer := 13;
                              -- # of memory row and # of addr bits.
   ADDITIVE_LAT             : integer := 0;
                              -- additive write latency.
   BURST_LEN                : integer := 4;
                              -- burst length (in double words).
   BURST_TYPE               : integer := 0;
                              -- burst type (=0 seq; =1 interleaved).
   CAS_LAT                  : integer := 4;
                              -- CAS latency.
   ECC_ENABLE               : integer := 0;
                              -- enable ECC (=1 enable).
   APPDATA_WIDTH            : integer := 128;
                              -- # of usr read/write data bus bits.
   MULTI_BANK_EN            : integer := 1;
                              -- Keeps multiple banks open. (= 1 enable).
   TWO_T_TIME_EN            : integer := 1;
                              -- 2t timing for unbuffered dimms.
   ODT_TYPE                 : integer := 1;
                              -- ODT (=0(none),=1(75),=2(150),=3(50)).
   REDUCE_DRV               : integer := 0;
                              -- reduced strength mem I/O (=1 yes).
   REG_ENABLE               : integer := 0;
                              -- registered addr/ctrl (=1 yes).
   TREFI_NS                 : integer := 7800;
                              -- auto refresh interval (ns).
   TRAS                     : integer := 40000;
                              -- active->precharge delay.
   TRCD                     : integer := 15000;
                              -- active->read/write delay.
   TRFC                     : integer := 105000;
                              -- refresh->refresh, refresh->active delay.
   TRP                      : integer := 15000;
                              -- precharge->command delay.
   TRTP                     : integer := 7500;
                              -- read->precharge delay.
   TWR                      : integer := 15000;
                              -- used to determine write->precharge.
   TWTR                     : integer := 7500;
                              -- write->read delay.
   HIGH_PERFORMANCE_MODE    : boolean := TRUE;
                              -- # = TRUE, the IODELAY performance mode is set
                              -- to high.
                              -- # = FALSE, the IODELAY performance mode is set
                              -- to low.
   SIM_ONLY                 : integer := 0;
                              -- = 1 to skip SDRAM power up delay.
   DEBUG_EN                 : integer := 0;
                              -- Enable debug signals/controls.
                              -- When this parameter is changed from 0 to 1,
                              -- make sure to uncomment the coregen commands
                              -- in ise_flow.bat or create_ise.bat files in
                              -- par folder.
   CLK_PERIOD               : integer := 3750;
                              -- Core/Memory clock period (in ps).
   DLL_FREQ_MODE            : string := "HIGH";
                              -- DCM Frequency range.
   CLK_TYPE                 : string := "DIFFERENTIAL";
                              -- # = "DIFFERENTIAL " ->; Differential input clocks ,
                              -- # = "SINGLE_ENDED" -> Single ended input clocks.
   NOCLK200                 : boolean := FALSE;
                              -- clk200 enable and disable
   RST_ACT_LOW              : integer := 1
                              -- =1 for active low reset, =0 for active high.
  -- DDR_END ---------------------------------------------------------------------
   );
PORT(
    -- 125 MHz differential reference clock (SFP + SMA); programable -------------
    -- MGT_116
    CLK_MGT_SFP_REF_N       : IN    STD_LOGIC;      -- H3
    CLK_MGT_SFP_REF_P       : IN    STD_LOGIC;      -- H4

    -- 125 MHz differential reference clock (PHY + Loopback) fixed 125 MHz -------
    -- MGT_112
    CLK_MGT_PHY_REF_N       : IN    STD_LOGIC;      -- P3
    CLK_MGT_PHY_REF_P       : IN    STD_LOGIC;      -- P4

    -- SFP SGMII interface; MGT_116_0 (LX50T: X0Y4, LX110T: X0Y5) ----------------
    SFP_RX_P                : IN    STD_LOGIC;      -- G1
    SFP_RX_N                : IN    STD_LOGIC;      -- H1
    SFP_TX_P                : OUT   STD_LOGIC;      -- F2
    SFP_TX_N                : OUT   STD_LOGIC;      -- G2

    -- SMA interface; MGT_116_1 (LX50T: X0Y4, LX110T: X0Y5) ----------------------
    SMA_RX_P                : IN    STD_LOGIC;      -- K1
    SMA_RX_N                : IN    STD_LOGIC;      -- J1
    SMA_TX_P                : OUT   STD_LOGIC;      -- L2
    SMA_TX_N                : OUT   STD_LOGIC;      -- K2

    -- Marvell 88E1111 SGMII interface; MGT_112_0 (LX50T: X0Y3, LX110T: X0Y4) ----
    PHY_RX_P                : IN    STD_LOGIC;      -- N1
    PHY_RX_N                : IN    STD_LOGIC;      -- P1
    PHY_TX_P                : OUT   STD_LOGIC;      -- M2
    PHY_TX_N                : OUT   STD_LOGIC;      -- N2

    -- unused loopback interface; MGT_112_1 (LX50T: X0Y3, LX110T: X0Y4) ----------
    LOOP_RX_P               : IN    STD_LOGIC;      -- T1
    LOOP_RX_N               : IN    STD_LOGIC;      -- R1
    LOOP_TX_P               : OUT   STD_LOGIC;      -- U2
    LOOP_TX_N               : OUT   STD_LOGIC;      -- T2

    -- UART ----------------------------------------------------------------------
    UART_RXD                : IN    STD_LOGIC;
    UART_TXD                : OUT   STD_LOGIC;

    -- UART_2_RXD              : IN    STD_LOGIC;
    -- UART_2_TXD              : OUT   STD_LOGIC;

    UART_3_RXD              : IN    STD_LOGIC;
    UART_3_TXD              : OUT   STD_LOGIC;
    UART_3_TXEN             : OUT   STD_LOGIC;

    -- PHY controll --------------------------------------------------------------
    PHY_MDC                 : OUT   STD_LOGIC;        -- serial interface clock
    PHY_MDIO                : INOUT STD_LOGIC;        -- serial interface data
    PHY_RESET               : OUT   STD_LOGIC;        -- PHY HW reset, active LOW

    -- SFP controll --------------------------------------------------------------
    SFP_M1                  : OUT   STD_LOGIC;        -- SCL; serial interface clock; max 100 kHz, active rising edge
    SFP_M2                  : INOUT STD_LOGIC;        -- SDA; serial interface data
    SFP_TXENA               : OUT   STD_LOGIC;        -- Tx enable; active H; hold L for > 10 us to clear Tx Falut

  -- the following SFP ports are not routed to FPGA on ML505 board
  --SFP_M0                  : IN    STD_LOGIC;        -- L when module present
  --SFP_RS                  : OUT   STD_LOGIC;        -- Rate select; H for full-bandwidth; on-board jumper
  --SFP_TXF                 : IN    STD_LOGIC;        -- Tx fault; active H
  --SFP_LOS                 : IN    STD_LOGIC);       -- Los of Signal; active H

    -- SPI interface -------------------------------------------------------------
    SPI_SCK                 : IN    STD_LOGIC;
    SPI_SDI                 : IN    STD_LOGIC;
    SPI_SDO                 : OUT   STD_LOGIC;
    SPI_CSN                 : IN    STD_LOGIC;


    -- LCD -----------------------------------------------------------------------
    LCD_DB                  : INOUT STD_LOGIC_VECTOR(7 DOWNTO 4);
    LCD_E                   : OUT   STD_LOGIC;
    LCD_RS                  : OUT   STD_LOGIC;
    LCD_RW                  : OUT   STD_LOGIC;

    -- GPIO ----------------------------------------------------------------------
    GPIO_DIP                : IN    STD_LOGIC_VECTOR( 7 DOWNTO 0);
    GPIO_BTN                : IN    STD_LOGIC_VECTOR( 4 DOWNTO 0);      -- CWESN
    GPIO_LED                : OUT   STD_LOGIC_VECTOR( 7 DOWNTO 0);
  --HDR1                    : OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);

    -- DDR  ----------------------------------------------------------------------
	ddr2_dq               : inout  std_logic_vector((DQ_WIDTH-1) downto 0);
   ddr2_a                : out   std_logic_vector((ROW_WIDTH-1) downto 0);
   ddr2_ba               : out   std_logic_vector((BANK_WIDTH-1) downto 0);
   ddr2_ras_n            : out   std_logic;
   ddr2_cas_n            : out   std_logic;
   ddr2_we_n             : out   std_logic;
   ddr2_cs_n             : out   std_logic_vector((CS_WIDTH-1) downto 0);
   ddr2_odt              : out   std_logic_vector((ODT_WIDTH-1) downto 0);
   ddr2_cke              : out   std_logic_vector((CKE_WIDTH-1) downto 0);
   ddr2_dm               : out   std_logic_vector((DM_WIDTH-1) downto 0);
   ddr2_dqs              : inout  std_logic_vector((DQS_WIDTH-1) downto 0);
   ddr2_dqs_n            : inout  std_logic_vector((DQS_WIDTH-1) downto 0);
   ddr2_ck               : out   std_logic_vector((CLK_WIDTH-1) downto 0);
   ddr2_ck_n             : out   std_logic_vector((CLK_WIDTH-1) downto 0);
	 sys_clk_p             : in    std_logic;
   sys_clk_n             : in    std_logic;
   clk200_p              : in    std_logic;
   clk200_n              : in    std_logic;

   -- SRAM ---------------------------------------------------------------------
   SRAM_FLASH_A          : out std_logic_vector(23 downto 0);
   SRAM_CEN              : out std_logic_vector(0 to 1);
   SRAM_OEN              : out std_logic_vector(0 to 1);
   SRAM_WEN              : out std_logic;
   SRAM_BEN              : out std_logic_vector(0 to 3);
   SRAM_ADV_LDN          : out std_logic;
   SRAM_DQ               : inout std_logic_vector(0 to 31);
   SRAM_CLK_OUT          : out std_logic;
   SRAM_CLK_FB           : in std_logic;
   SRAM_LBON             : out std_logic;

   -- HDR  ---------------------------------------------------------------------
   HDR1_50              : out std_logic;
   HDR1_52              : out std_logic;
   HDR1_54              : out std_logic
);
END Splitter_TOP;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF Splitter_TOP IS

-- HW Test Bench
--	COMPONENT ddr2_tb_top
--	GENERIC (
--	 BANK_WIDTH : integer;
--    COL_WIDTH : integer;
--    DM_WIDTH : integer;
--    DQ_WIDTH : integer;
--    ROW_WIDTH : integer;
--    APPDATA_WIDTH : integer;
--    ECC_ENABLE : integer;
--    BURST_LEN : integer
--	);
--	PORT(
--		clk0 : IN std_logic;
--		rst0 : IN std_logic;
--		app_af_afull : IN std_logic;
--		app_wdf_afull : IN std_logic;
--		rd_data_valid : IN std_logic;
--		rd_data_fifo_out : IN std_logic_vector(127 downto 0);
--		phy_init_done : IN std_logic;
--		app_af_wren : OUT std_logic;
--		app_af_cmd : OUT std_logic_vector(2 downto 0);
--		app_af_addr : OUT std_logic_vector(30 downto 0);
--		app_wdf_wren : OUT std_logic;
--		app_wdf_data : OUT std_logic_vector(127 downto 0);
--		app_wdf_mask_data : OUT std_logic_vector((APPDATA_WIDTH/8-1) downto 0);
--		error : OUT std_logic;
--		error_cmp : OUT std_logic
--		);
--	END COMPONENT;


----------------------------------------------------------------------------------
  -- clocking and reset
  SIGNAL clk_sys                : STD_LOGIC;                            -- main system clock (125 MHz; not active when GTP reset is active)
  SIGNAL clk_REF_out            : STD_LOGIC;                            -- onboard reference clock output (125 MHz; always available)

  SIGNAL rst                    : STD_LOGIC;                            -- PicoBlaze driven reset
  SIGNAL GTP_reset              : STD_LOGIC := '0';                     -- asynchronous reset of both GTP transceivers (interrupts clk_sys!!!)

  -- high speed GTP data signals
  SIGNAL RXP_IN                 : STD_LOGIC_VECTOR( 1 DOWNTO 0);
  SIGNAL RXN_IN                 : STD_LOGIC_VECTOR( 1 DOWNTO 0);
  SIGNAL TXP_OUT                : STD_LOGIC_VECTOR( 1 DOWNTO 0);
  SIGNAL TXN_OUT                : STD_LOGIC_VECTOR( 1 DOWNTO 0);

  SIGNAL Seq_sel_i              : STD_LOGIC_VECTOR( 7 DOWNTO 0) := (OTHERS => '0');
  SIGNAL Data_Gen_data_out      : STD_LOGIC_VECTOR( 9 DOWNTO 0);
  SIGNAL LFSR_23_data_in        : STD_LOGIC_VECTOR( 9 DOWNTO 0);

  SIGNAL GPIO_LED_i             : STD_LOGIC_VECTOR( 7 DOWNTO 0);

  SIGNAL cnt_clk_sys            : STD_LOGIC_VECTOR(27 DOWNTO 0) := (OTHERS => '0');
  SIGNAL LED_clk_sys            : STD_LOGIC := '0';

  SIGNAL cnt_clk_gtp            : STD_LOGIC_VECTOR(27 DOWNTO 0) := (OTHERS => '0');
  SIGNAL LED_clk_gtp            : STD_LOGIC := '0';

  SIGNAL clk_Tx_SFP_out         : STD_LOGIC;
  SIGNAL Tx_SFP_Data_10b        : STD_LOGIC_VECTOR(9 DOWNTO 0);

  SIGNAL SFP_Tx_Inhibit         : STD_LOGIC;
  SIGNAL SFP_Tx_Polarity        : STD_LOGIC;
  SIGNAL SFP_Tx_Swing           : STD_LOGIC_VECTOR(2 DOWNTO 0);
  SIGNAL SFP_Tx_Preemphasis     : STD_LOGIC_VECTOR(2 DOWNTO 0);
  SIGNAL SFP_Rx_EqMix           : STD_LOGIC_VECTOR(1 DOWNTO 0);
  SIGNAL SFP_Rx_EqPole          : STD_LOGIC_VECTOR(3 DOWNTO 0);

    -- GTP control
  SIGNAL rst_GTP_SFP_SMA        : STD_LOGIC;
  SIGNAL rst_Rx_CDR_SFP         : STD_LOGIC;
  SIGNAL rst_Rx_CDR_SMA         : STD_LOGIC;
  SIGNAL PLL_locked_SFP_SMA     : STD_LOGIC;
  SIGNAL CDR_aligned            : STD_LOGIC := '0';

  SIGNAL RX_FRAMES_RECEIVED : STD_LOGIC_VECTOR( 63 DOWNTO 0);
  SIGNAL RX_FRAMES_BAD : STD_LOGIC_VECTOR( 63 DOWNTO 0);
  SIGNAL RX_BYTES_CORRECTED : STD_LOGIC_VECTOR( 63 DOWNTO 0);
  SIGNAL RX_BYTES_RECEIVED : STD_LOGIC_VECTOR( 63 DOWNTO 0);
  SIGNAL TX_FRAMES_SENT : STD_LOGIC_VECTOR( 63 DOWNTO 0);
  SIGNAL TX_FRAMES_TRUNCATED : STD_LOGIC_VECTOR( 63 DOWNTO 0);
  SIGNAL TX_BYTES_SENT : STD_LOGIC_VECTOR( 63 DOWNTO 0);
  SIGNAL TX_BYTES_DROPPED : STD_LOGIC_VECTOR( 63 DOWNTO 0);

  SIGNAL Seq_sel                : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL MUX_Select             : STD_LOGIC_VECTOR( 7 DOWNTO 0);

  SIGNAL clk_Rx_SFP_out         : STD_LOGIC;
  SIGNAL Rx_SFP_Data_10b        : STD_LOGIC_VECTOR( 9 DOWNTO 0);

  SIGNAL BERT_IRQ_1s            : STD_LOGIC;
  SIGNAL BERT_toggle_1s         : STD_LOGIC;
  SIGNAL BERT_Err_cnt_1s        : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL BERT_LFSR_locked       : STD_LOGIC;

  SIGNAL BERT_IRQ_1s_stretched  : STD_LOGIC;
  SIGNAL LED_clk_Rx_Ok          : STD_LOGIC;

  SIGNAL CONV_ENC_data_out      : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
  SIGNAL CONV_ENC_data_in       : STD_LOGIC_VECTOR(4 DOWNTO 0) := (OTHERS => '0');
  SIGNAL CONV_DEC_data_in       : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
  SIGNAL CONV_DEC_data_out      : STD_LOGIC_VECTOR(4 DOWNTO 0) := (OTHERS => '0');

--#############################################################################################################

  SIGNAL CLK_125_OUT_SFP        : STD_LOGIC;
  SIGNAL CLK_125_SYS_SFP        : STD_LOGIC;        -- 125 MHz global clock (derived from SFP)
  SIGNAL CLK_TX_SFP_MAC_OUT     : STD_LOGIC;
  SIGNAL CLK_TX_SFP             : STD_LOGIC;        -- Tri-speed global clock (derived from SFP)
  SIGNAL GTP_REFCLK_SFP         : STD_LOGIC;

  SIGNAL CLK_125_OUT_PHY        : STD_LOGIC;
  SIGNAL CLK_125_SYS_PHY        : STD_LOGIC;        -- 125 MHz global clock (derived from PHY)
  SIGNAL CLK_TX_PHY_MAC_OUT     : STD_LOGIC;
  SIGNAL CLK_TX_PHY             : STD_LOGIC;        -- Tri-speed global clock (derived from PHY)
  SIGNAL GTP_REFCLK_PHY         : STD_LOGIC;


  SIGNAL DataGen_SFP_Data       : STD_LOGIC_VECTOR(15 DOWNTO 0);
  SIGNAL DataGen_SFP_DV         : STD_LOGIC;
  SIGNAL DataGen_SFP_First      : STD_LOGIC;
  SIGNAL DataGen_SFP_Last       : STD_LOGIC;
  SIGNAL DataGen_SFP_Ready      : STD_LOGIC;

  SIGNAL DataGen_PHY_Data       : STD_LOGIC_VECTOR(15 DOWNTO 0);
  SIGNAL DataGen_PHY_DV         : STD_LOGIC;
  SIGNAL DataGen_PHY_First      : STD_LOGIC;
  SIGNAL DataGen_PHY_Last       : STD_LOGIC;
  SIGNAL DataGen_PHY_Ready      : STD_LOGIC;

  --- SIMPLE_MAC signals ---
  SIGNAL SFP_RAW_OUT            : STD_LOGIC_VECTOR (9 downto 0) := (OTHERS => '0');
  SIGNAL SFP_RAW_IN             : STD_LOGIC_VECTOR (9 downto 0) := (OTHERS => '0');
  SIGNAL SFP_RAW_TX             : STD_LOGIC_VECTOR (9 downto 0) := (OTHERS => '0');

  SIGNAL SFP_DV_IN              : STD_LOGIC := '0';
  SIGNAL SFP_GF_IN              : STD_LOGIC := '0';
  SIGNAL SFP_BF_IN              : STD_LOGIC := '0';
  SIGNAL SFP_DATA_IN            : STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');

  SIGNAL SFP_DV_OUT             : STD_LOGIC := '0';
  SIGNAL SFP_ACK_OUT            : STD_LOGIC := '0';
  SIGNAL SFP_DATA_OUT           : STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');

  SIGNAL SFP_TX_DATA            : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL SFP_TX_DV              : STD_LOGIC;
  SIGNAL SFP_TX_ACK             : STD_LOGIC;

  SIGNAL PHY_TX_DATA            : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL PHY_TX_DV              : STD_LOGIC;
  SIGNAL PHY_TX_ACK             : STD_LOGIC;

  SIGNAL PHY_RX_DATA            : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL PHY_RX_DV              : STD_LOGIC;
  SIGNAL PHY_RX_BF              : STD_LOGIC;
  SIGNAL PHY_RX_GF              : STD_LOGIC;

  SIGNAL STAT_DOUT              : STD_LOGIC_VECTOR(63 downto 0);
  SIGNAL STAT_COUNT             : STD_LOGIC_VECTOR(9 downto 0);
  SIGNAL STAT_RD_EN             : STD_LOGIC;


  SIGNAL cnt_CLK_TX_SFP         : STD_LOGIC_VECTOR(23 DOWNTO 0) := (OTHERS => '0');
  SIGNAL LED_CLK_TX_SFP         : STD_LOGIC := '0';

  SIGNAL cnt_CLK_TX_PHY         : STD_LOGIC_VECTOR(23 DOWNTO 0) := (OTHERS => '0');
  SIGNAL LED_CLK_TX_PHY         : STD_LOGIC := '0';

  SIGNAL cnt_CLK_125_SYS_SFP    : STD_LOGIC_VECTOR(27 DOWNTO 0) := (OTHERS => '0');
  SIGNAL LED_CLK_125_SYS_SFP    : STD_LOGIC := '0';

  SIGNAL cnt_CLK_125_SYS_PHY    : STD_LOGIC_VECTOR(27 DOWNTO 0) := (OTHERS => '0');
  SIGNAL LED_CLK_125_SYS_PHY    : STD_LOGIC := '0';


  SIGNAL LED_SYNC_STATUS_SFP    : STD_LOGIC := '0';
  SIGNAL LED_SYNC_STATUS_PHY    : STD_LOGIC := '0';

  SIGNAL ANEG_IRQ_SFP           : STD_LOGIC;
  SIGNAL ANEG_IRQ_PHY           : STD_LOGIC;

  SIGNAL LED_ANEG_IRQ_SFP       : STD_LOGIC := '0';
  SIGNAL LED_ANEG_IRQ_PHY       : STD_LOGIC := '0';

--  SIGNAL cnt_LED_ANEG_IRQ_SFP   : STD_LOGIC_VECTOR(23 DOWNTO 0) := (OTHERS => '0');
--  SIGNAL cnt_LED_ANEG_IRQ_PHY   : STD_LOGIC_VECTOR(23 DOWNTO 0) := (OTHERS => '0');

  SIGNAL HOST_1_OPCODE          : STD_LOGIC_VECTOR( 1 DOWNTO 0);
  SIGNAL HOST_1_REQ             : STD_LOGIC;
  SIGNAL HOST_1_MIIMSEL         : STD_LOGIC;
  SIGNAL HOST_1_ADDR            : STD_LOGIC_VECTOR( 9 DOWNTO 0);
  SIGNAL HOST_1_WRDATA          : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL HOST_1_MIIMRDY         : STD_LOGIC;
  SIGNAL HOST_1_RDDATA          : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL HOST_1_EMAC1SEL        : STD_LOGIC;

--SIGNAL MDIO_0_I               : STD_LOGIC;
--SIGNAL MDIO_0_O               : STD_LOGIC;
--SIGNAL MDIO_0_T               : STD_LOGIC;

  SIGNAL MDIO_1_I               : STD_LOGIC;
  SIGNAL MDIO_1_O               : STD_LOGIC;
  SIGNAL MDIO_1_T               : STD_LOGIC;

  --- DDR controller application interface signals --
  SIGNAL sys_rst_n             :  std_logic := '0';
	SIGNAL rst0_tb               :  std_logic := '0';
	SIGNAL clk0_tb               :  std_logic := '0';
	SIGNAL app_wdf_afull         :  std_logic := '0';
	SIGNAL app_af_afull          :  std_logic := '0';
	SIGNAL rd_data_valid         :  std_logic := '0';
	SIGNAL app_wdf_wren          :  std_logic := '0';
	SIGNAL app_af_wren           :  std_logic := '0';
	SIGNAL phy_init_done         :  std_logic := '0';
	SIGNAL app_af_addr           :  std_logic_vector(30 downto 0) := (OTHERS => '0');
	SIGNAL app_af_cmd            :  std_logic_vector(2 downto 0) := (OTHERS => '0');
	SIGNAL rd_data_fifo_out      :  std_logic_vector((APPDATA_WIDTH-1) downto 0) := (OTHERS => '0');
	SIGNAL app_wdf_data          :  std_logic_vector((APPDATA_WIDTH-1) downto 0) := (OTHERS => '0');
	SIGNAL app_wdf_mask_data     :  std_logic_vector((APPDATA_WIDTH/8-1) downto 0) := (OTHERS => '0');

  --- DDR FIFO interface
   SIGNAL  DDR_FIFO_wr_clk     :    std_logic := '0';
   SIGNAL  DDR_FIFO_rd_clk     :    std_logic := '0';
   SIGNAL  DDR_FIFO_din        :    std_logic_vector(8 downto 0) := (OTHERS => '0');
   SIGNAL  DDR_FIFO_wr_en      :    std_logic := '0';
   SIGNAL  DDR_FIFO_rd_en      :    std_logic := '0';
   SIGNAL  DDR_FIFO_dout       :    std_logic_vector(8 downto 0) := (OTHERS => '0');
   SIGNAL  DDR_FIFO_full       :    std_logic := '0';
   SIGNAL  DDR_FIFO_empty      :    std_logic := '0';
   SIGNAL  DDR_FIFO_empty_prog :    std_logic := '0';


--#### DEBUG
  type DisplayLine is array (0 to 15) of std_logic_vector(7 downto 0);
  SIGNAL LCD_LINE_1 : DisplayLine;
  SIGNAL LCD_LINE_2 : DisplayLine;

  SIGNAL  dummy_LCD_DB           :    STD_LOGIC_VECTOR(7 DOWNTO 4);
  SIGNAL  dummy_LCD_E            :    STD_LOGIC;
  SIGNAL  dummy_LCD_RS           :    STD_LOGIC;
  SIGNAL  dummy_LCD_RW           :    STD_LOGIC;

  -- UBLAZE SIGNALS
  SIGNAL  UBLAZE_display_out    : STD_LOGIC_VECTOR(0 to 255);
  SIGNAL  FIFO_TO_UB_DATA_OUT   : STD_LOGIC_VECTOR(8 downto 0);
  SIGNAL  FIFO_TO_UB_RD_EN_IN   : STD_LOGIC;
  SIGNAL  FIFO_TO_UB_EMPTY      : STD_LOGIC;

  SIGNAL  FIFO_FROM_UB_DATA_IN  : STD_LOGIC_VECTOR(8 downto 0);
  SIGNAL  FIFO_FROM_UB_WR_EN_IN : STD_LOGIC;
  SIGNAL  FIFO_FROM_UB_PROG_FULL : STD_LOGIC;

  SIGNAL  BRAM_WR_DATA          : STD_LOGIC_VECTOR(31 downto 0);
  SIGNAL  BRAM_RD_DATA          : STD_LOGIC_VECTOR(0 to 31);

  SIGNAL  BRAM_WR_ADDR          : STD_LOGIC_VECTOR(31 downto 0);
  SIGNAL  BRAM_WR_EN            : STD_LOGIC;
  SIGNAL  BRAM_WR_WEN           : STD_LOGIC_VECTOR(3 downto 0);

  SIGNAL  BRAM_RD_ADDR          : STD_LOGIC_VECTOR(31 downto 0);
  SIGNAL  BRAM_RD_EN            : STD_LOGIC;
  SIGNAL  UB_BRAM_len_out       : STD_LOGIC_VECTOR(10 downto 0);
  SIGNAL  UB_BRAM_len_in        : STD_LOGIC_VECTOR(10 downto 0);

  SIGNAL  UBLAZE_BRAM_CTRL_IN   : STD_LOGIC_VECTOR(7 downto 0);
  SIGNAL  UBLAZE_BRAM_CTRL_OUT  : STD_LOGIC_VECTOR(7 downto 0);
  SIGNAL  UBLAZE_MAC_ADDRESS    : STD_LOGIC_VECTOR(0 to 47);

  SIGNAL CLKGEN_CLKFBOUT_i      : STD_LOGIC;
  SIGNAL SRAM_CLK_OUT_i         : STD_LOGIC;

  --enable resets on startup
  SIGNAL CONFIG_REG0_OUT        : STD_LOGIC_VECTOR(15 downto 0) := X"00C3"; --X"0030";
  SIGNAL CONFIG_REG0_IN         : STD_LOGIC_VECTOR(15 downto 0) := X"0000";
  SIGNAL CONFIG_REG1            : STD_LOGIC_VECTOR(31 downto 0) := (OTHERS => '0');

  SIGNAL DATAGEN_SEQ_SEL       : STD_LOGIC_VECTOR(7 downto 0) := (OTHERS => '0');
  SIGNAL DATAGEN_DATA_OUT      : STD_LOGIC_VECTOR(9 downto 0) := (OTHERS => '0');
----------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------------

--################################################################################
--
--  #####  ####### ######     #     #    #     #####
-- #     # #       #     #    ##   ##   # #   #     #
-- #       #       #     #    # # # #  #   #  #
--  #####  #####   ######     #  #  # #     # #
--       # #       #          #     # ####### #
-- #     # #       #          #     # #     # #     #
--  #####  #       #          #     # #     #  #####
--
--################################################################################

  REFCLK_PHY_IBUFGDS : IBUFGDS PORT MAP(
    O  => GTP_REFCLK_PHY,
    I  => CLK_MGT_PHY_REF_P,
    IB => CLK_MGT_PHY_REF_N);

  TX_CLK_PHY_BUFG : BUFG PORT MAP(
    O  => CLK_TX_PHY,
    I  => CLK_TX_PHY_MAC_OUT);

  CLK_125_PHY_BUFG : BUFG PORT MAP(
    O  => CLK_125_SYS_PHY,
    I  => CLK_125_OUT_PHY);


-- This module handles internally the clock domain crossing from the
-- recovered RX clock / TX clock to/from the system clock.
-- Please connect with attention the clock signals!
  SFP_Simplified_MAC_i : entity WORK.SFP_Simplified_MAC PORT MAP(
    RAW_RX => Rx_SFP_Data_10b,
    CLK_RX => clk_Rx_SFP_out,
	  ALIGNED => CDR_aligned,

    -- hard-wired connection MAC => SFP
    -- deprecated as there is currently a MUX for selection of TX data source
    --
    -- RAW_TX => Tx_SFP_Data_10b,
    RAW_TX => SFP_RAW_OUT,

    CLK_TX => clk_Tx_SFP_out,

    SYS_CLK => clk_sys,
    SYS_RST => CONFIG_REG0_OUT(1),
    CHANNEL_CODING_USED => CONFIG_REG0_OUT(5),
    DBG_BTN_IN          => GPIO_BTN(1),

    RX_FRAMES_RECEIVED  => RX_FRAMES_RECEIVED,
    RX_FRAMES_BAD       => RX_FRAMES_BAD,
    RX_BYTES_CORRECTED  => RX_BYTES_CORRECTED,
    RX_BYTES_RECEIVED   => RX_BYTES_RECEIVED,
    TX_FRAMES_SENT      => TX_FRAMES_SENT,
    TX_FRAMES_TRUNCATED => TX_FRAMES_TRUNCATED,
    TX_BYTES_SENT       => TX_BYTES_SENT,
    TX_BYTES_DROPPED    => TX_BYTES_DROPPED,
    STAT_DOUT           => STAT_DOUT,
    STAT_COUNT          => STAT_COUNT,
    STAT_RD_EN          => STAT_RD_EN,
    BER_SIMULATION_EN   => CONFIG_REG0_OUT(4),
    BER_SIMULATION_COMP => CONFIG_REG1, --! BER ~ 10^-3

    DV_RX => SFP_DV_IN,
    GF_RX => SFP_GF_IN,
    BF_RX => SFP_BF_IN,
    DIN_RX => SFP_DATA_IN,

    DV_TX => SFP_DV_OUT,
    ACK_TX => SFP_ACK_OUT,
    DOUT_TX => SFP_DATA_OUT,

	  wr_clk => DDR_FIFO_wr_clk,
	  rd_clk => DDR_FIFO_rd_clk,
	  din => DDR_FIFO_din,
	  wr_en => DDR_FIFO_wr_en,
	  rd_en => DDR_FIFO_rd_en,
	  dout => DDR_FIFO_dout,
	  full => DDR_FIFO_full,
	  empty => DDR_FIFO_empty,
	  empty_prog => DDR_FIFO_empty_prog
  );
--
--  PROCESS(clk_Rx_SFP_out) BEGIN
--    IF rising_edge(clk_Rx_SFP_out) THEN
--      GPIO_LED(0) <= Rx_SFP_Data_10b(0);
--      GPIO_LED(1) <= Rx_SFP_Data_10b(1);
--      GPIO_LED(2) <= Rx_SFP_Data_10b(2);
--      GPIO_LED(3) <= Rx_SFP_Data_10b(3);
--      GPIO_LED(4) <= Rx_SFP_Data_10b(4);
--      GPIO_LED(5) <= Rx_SFP_Data_10b(5);
--      GPIO_LED(6) <= Rx_SFP_Data_10b(6);
--      GPIO_LED(7) <= Rx_SFP_Data_10b(7);
--    END IF;
--  END PROCESS;

    --GPIO_LED(0) <= GPIO_BTN(0);
    --GPIO_LED(1) <= '1';
    --GPIO_LED(2) <= '1';
    --GPIO_LED(3) <= '1';
    --GPIO_LED(4) <= '1';
    --GPIO_LED(5) <= '1';
    --GPIO_LED(6) <= '1';
    --GPIO_LED(7) <= '1';


--################################################################################
--
-- ####### ####### #     #    #     #    #     #####
-- #          #    #     #    ##   ##   # #   #     #
-- #          #    #     #    # # # #  #   #  #
-- #####      #    #######    #  #  # #     # #
-- #          #    #     #    #     # ####### #
-- #          #    #     #    #     # #     # #     #
-- #######    #    #     #    #     # #     #  #####
--
--################################################################################


    PHY_TEMAC : entity WORK.TEMAC_SGMII_block PORT MAP (

      CLK125_OUT                      => CLK_125_OUT_PHY,       -- 125MHz clock output from transceiver (refclkout form GTP transceiver)
      CLK125                          => CLK_125_SYS_PHY,       -- 125MHz clock input from BUFG (GTP USRCLK2)
      CLIENT_CLK_OUT_0                => CLK_TX_PHY_MAC_OUT,    -- Tri-speed clock output from EMAC0 (EMAC0CLIENTTXCLIENTCLKOUT)
      CLIENT_CLK_0                    => CLK_TX_PHY,            -- EMAC0 Tri-speed clock input from BUFG (MAC Rx and Tx client clock)

      EMAC0CLIENTRXD                  => PHY_RX_DATA,                  -- Client Receiver Interface - EMAC0
      EMAC0CLIENTRXDVLD               => PHY_RX_DV,
      EMAC0CLIENTRXGOODFRAME          => PHY_RX_GF,
      EMAC0CLIENTRXBADFRAME           => PHY_RX_BF,
      EMAC0CLIENTRXFRAMEDROP          => OPEN,
      EMAC0CLIENTRXSTATS              => OPEN,
      EMAC0CLIENTRXSTATSVLD           => OPEN,
      EMAC0CLIENTRXSTATSBYTEVLD       => OPEN,

      CLIENTEMAC0TXD                  => PHY_TX_DATA,           -- Client Transmitter Interface - EMAC0
      CLIENTEMAC0TXDVLD               => PHY_TX_DV,
      EMAC0CLIENTTXACK                => PHY_TX_ACK,
      CLIENTEMAC0TXFIRSTBYTE          => '0',                   -- tie low (see ug194, page 33)
      CLIENTEMAC0TXUNDERRUN           => '0',                   -- assert to force data corruption
      EMAC0CLIENTTXCOLLISION          => OPEN,
      EMAC0CLIENTTXRETRANSMIT         => OPEN,
      CLIENTEMAC0TXIFGDELAY           => X"00",
      EMAC0CLIENTTXSTATS              => OPEN,
      EMAC0CLIENTTXSTATSVLD           => OPEN,
      EMAC0CLIENTTXSTATSBYTEVLD       => OPEN,

      CLIENTEMAC0PAUSEREQ             => '0',                   -- MAC Control Interface - EMAC0
      CLIENTEMAC0PAUSEVAL             => X"0000",

      EMAC0CLIENTSYNCACQSTATUS        => LED_SYNC_STATUS_PHY,   -- EMAC-MGT link status
      EMAC0ANINTERRUPT                => ANEG_IRQ_PHY,          -- EMAC0 Interrupt

      TXP_0                           => PHY_TX_P,              -- SGMII Interface - EMAC0
      TXN_0                           => PHY_TX_N,
      RXP_0                           => PHY_RX_P,
      RXN_0                           => PHY_RX_N,
      PHYAD_0                         => "00001",
      RESETDONE_0                     => OPEN,

      TXN_1_UNUSED                    => LOOP_TX_N,             -- unused transceiver
      TXP_1_UNUSED                    => LOOP_TX_P,
      RXN_1_UNUSED                    => LOOP_RX_N,
      RXP_1_UNUSED                    => LOOP_RX_P,

      MDC_0                           => PHY_MDC,
      MDIO_0_I                        => MDIO_1_I,
      MDIO_0_O                        => MDIO_1_O,
      MDIO_0_T                        => MDIO_1_T,

      HOSTCLK                         => clk_sys,       --# Generic Host Interface
      HOSTOPCODE                      => HOST_1_OPCODE,
      HOSTREQ                         => HOST_1_REQ,
      HOSTMIIMSEL                     => HOST_1_MIIMSEL,
      HOSTADDR                        => HOST_1_ADDR,
      HOSTWRDATA                      => HOST_1_WRDATA,
      HOSTMIIMRDY                     => HOST_1_MIIMRDY,
      HOSTRDDATA                      => HOST_1_RDDATA,
      HOSTEMAC1SEL                    => HOST_1_EMAC1SEL,

--    HOSTOPCODE                      => "00",
--    HOSTREQ                         => '0',
--    HOSTMIIMSEL                     => '0',                   -- 0 for MAC access, 1 for MDIO access
--    HOSTADDR                        => "0000000000",
--    HOSTWRDATA                      => X"00000000",
--    HOSTMIIMRDY                     => OPEN,
--    HOSTRDDATA                      => OPEN,
--    HOSTEMAC1SEL                    => '0',                   -- MAC 0/1 select

      CLK_DS                          => GTP_REFCLK_PHY,        -- SGMII RocketIO Reference Clock buffer inputs
      GTRESET                         => '0',                   -- RocketIO Reset input
      RESET                           => CONFIG_REG0_OUT(6));                  -- Asynchronous Reset

  OBUFT_PHY : OBUFT PORT MAP(
      O  => PHY_MDIO,
      I  => MDIO_1_O,
      T  => MDIO_1_T);

  IBUF_PHY : IBUF PORT MAP(
      O  => MDIO_1_I,
      I  => PHY_MDIO);

  PHY_RESET <= not CONFIG_REG0_OUT(7); --reset active low

--################################################################################
--
--  #####  ####### ######     #     # ######     #    ######
-- #     #    #    #     #    #  #  # #     #   # #   #     #
-- #          #    #     #    #  #  # #     #  #   #  #     #
-- #  ####    #    ######     #  #  # ######  #     # ######
-- #     #    #    #          #  #  # #   #   ####### #
-- #     #    #    #          #  #  # #    #  #     # #
--  #####     #    #           ## ##  #     # #     # #
--
--################################################################################

  GTP_Wrapper_i : entity WORK.GTP_Wrapper
  PORT MAP(
    -- clocking and reset
    REFCLK_SFP_P_IN               => CLK_MGT_SFP_REF_P,
    REFCLK_SFP_N_IN               => CLK_MGT_SFP_REF_N,

    clk_REF_out                   => clk_REF_out,
    GTP_reset_in                  => GTP_reset,

    -- SGMII interface signals (high speed GTP Rx/Tx)
    RXP_IN                        => RXP_IN,
    RXN_IN                        => RXN_IN,
    TXP_OUT                       => TXP_OUT,
    TXN_OUT                       => TXN_OUT,

    -- SFP application interface
    clk_Rx_SFP_out                => clk_Rx_SFP_out,
    Rx_SFP_Data_10b               => Rx_SFP_Data_10b,
    clk_Tx_SFP_out                => clk_Tx_SFP_out,
    Tx_SFP_Data_10b               => Tx_SFP_Data_10b,

    -- GTP control
    rst_GTP_SFP_SMA               => rst_GTP_SFP_SMA,
    rst_Rx_CDR_SFP                => rst_Rx_CDR_SFP,
    rst_Rx_CDR_SMA                => rst_Rx_CDR_SMA,
    PLL_locked_SFP_SMA            => PLL_locked_SFP_SMA,
  	CDR_aligned					  => CDR_aligned,

    -- SFP control interface
    SFP_Tx_Inhibit                => SFP_Tx_Inhibit,
    SFP_Tx_Polarity               => SFP_Tx_Polarity,
    SFP_Tx_Swing                  => SFP_Tx_Swing,
    SFP_Tx_Preemphasis            => SFP_Tx_Preemphasis,
    SFP_Rx_EqMix                  => SFP_Rx_EqMix,
    SFP_Rx_EqPole                 => SFP_Rx_EqPole);

    -- SFP SGMII interface; MGT_116_0 (LX50T: X0Y4, LX110T: X0Y5) ----------------
    RXP_IN(0) <= SFP_RX_P;
    RXN_IN(0) <= SFP_RX_N;
    SFP_TX_P  <= TXP_OUT(0);
    SFP_TX_N  <= TXN_OUT(0);

    -- SMA interface; MGT_116_1 (LX50T: X0Y4, LX110T: X0Y5) ----------------------
    RXP_IN(1) <= SMA_RX_P;
    RXN_IN(1) <= SMA_RX_N;
    SMA_TX_P  <= TXP_OUT(1);
    SMA_TX_N  <= TXN_OUT(1);

  clk_sys <= clk_REF_out;

--################################################################################
--
-- ####### #        #####
-- #       #       #     #
-- #       #       #
-- #####   #       #
-- #       #       #
-- #       #       #     #
-- #       #######  #####
--
--################################################################################

  FlowCtrl: entity WORK.FlowController PORT MAP(
		ETH_DATA_IN  => PHY_RX_DATA,
		ETH_DV_IN    => PHY_RX_DV,
		ETH_GF_IN    => PHY_RX_GF,
		ETH_BF_IN    => PHY_RX_BF,
		ETH_CLK_IN   => CLK_TX_PHY,

		ETH_DATA_OUT => PHY_TX_DATA,
		ETH_DV_OUT   => PHY_TX_DV,
		ETH_ACK_OUT  => PHY_TX_ACK,
		ETH_CLK_OUT  => CLK_TX_PHY,

		SFP_DATA_IN => SFP_DATA_IN,
		SFP_DV_IN   => SFP_DV_IN,
		SFP_GF_IN   => SFP_GF_IN,
		SFP_BF_IN   => SFP_BF_IN,
		SFP_CLK_IN  => clk_sys,

		SFP_DATA_OUT => SFP_DATA_OUT,
		SFP_DV_OUT   => SFP_DV_OUT,
		SFP_ACK_OUT  => SFP_ACK_OUT,
		SFP_CLK_OUT  => clk_sys,

    FIFO_TO_UB_DATA_OUT   =>  FIFO_TO_UB_DATA_OUT,
    FIFO_TO_UB_RD_EN_IN   =>  FIFO_TO_UB_RD_EN_IN,
    FIFO_TO_UB_EMPTY      =>  FIFO_TO_UB_EMPTY,

    FIFO_FROM_UB_DATA_IN  =>  FIFO_FROM_UB_DATA_IN,
    FIFO_FROM_UB_WR_EN_IN =>  FIFO_FROM_UB_WR_EN_IN,
    FIFO_FROM_UB_PROG_FULL     =>  FIFO_FROM_UB_PROG_FULL,

    UBLAZE_MAC_IN => UBLAZE_MAC_ADDRESS,

    CLK_UBLAZE_IN         =>  clk_sys,

		SYS_CLK => clk_sys,
		SYS_RST => CONFIG_REG0_OUT(0)
	);

    BRAM_Controller_i: entity WORK.BRAM_Controller PORT MAP(
    BRAM_WR_DATA_OUT  => BRAM_WR_DATA,
    BRAM_WR_ADDR_OUT  => BRAM_WR_ADDR,
    BRAM_WR_EN_OUT    => BRAM_WR_EN,
    BRAM_WR_WEN_OUT   => BRAM_WR_WEN,
    BRAM_WR_DATA_LEN_OUT => UB_BRAM_len_in,

    UBLAZE_WR_ACK_OUT => UBLAZE_BRAM_CTRL_IN(0),
    UBLAZE_WR_ACK_IN  => UBLAZE_BRAM_CTRL_OUT(0),

    BRAM_RD_DATA_IN     => BRAM_RD_DATA,
    BRAM_RD_ADDR_OUT    => BRAM_RD_ADDR,
    BRAM_RD_EN_OUT      => BRAM_RD_EN,
    BRAM_RD_DATA_LEN_IN => UB_BRAM_len_out,

    UBLAZE_RD_ACK_OUT => UBLAZE_BRAM_CTRL_IN(1),
    UBLAZE_RD_ACK_IN  => UBLAZE_BRAM_CTRL_OUT(1),

    FC_DATA_IN   => FIFO_TO_UB_DATA_OUT,
    FC_RD_EN_OUT => FIFO_TO_UB_RD_EN_IN,
    FC_EMPTY     => FIFO_TO_UB_EMPTY,
    FC_DATA_OUT  => FIFO_FROM_UB_DATA_IN,
    FC_WR_EN_OUT => FIFO_FROM_UB_WR_EN_IN,
    FC_PROG_FULL      => FIFO_FROM_UB_PROG_FULL,

    SYS_RST => CONFIG_REG0_OUT(0),
    CLK => clk_sys
  );
--################################################################################
--
-- ######  ###  #####  ######  #          #    #     #
-- #     #  #  #     # #     # #         # #    #   #
-- #     #  #  #       #     # #        #   #    # #
-- #     #  #   #####  ######  #       #     #    #
-- #     #  #        # #       #       #######    #
-- #     #  #  #     # #       #       #     #    #
-- ######  ###  #####  #       ####### #     #    #
--
--################################################################################
GEN_LCD_LINE_1:
for I in 0 to 15 generate
	HEX2ASCII_1_I : ENTITY WORK.HEX2ASCII
	port map (HEX => RX_BYTES_CORRECTED(((4*(16-I))-1) downto (4*(15-I))), ASCII => LCD_LINE_1(I));
end generate GEN_LCD_LINE_1;

GEN_LCD_LINE_2:
for I in 0 to 15 generate
	HEX2ASCII_2_I : ENTITY WORK.HEX2ASCII
	port map (HEX => TX_FRAMES_TRUNCATED(((4*(16-I))-1) downto (4*(15-I))), ASCII => LCD_LINE_2(I));
end generate GEN_LCD_LINE_2;


  Debug_LCD : entity WORK.LCD_driver
  PORT MAP(
		clk => clk_sys,
		lcd_e => LCD_E,
		lcd_rs => LCD_RS,
		lcd_rw => LCD_RW,
		lcd_db => LCD_DB,
		line1_00 => LCD_LINE_1(0),
		line1_01 => LCD_LINE_1(1),
		line1_02 => LCD_LINE_1(2),
		line1_03 => LCD_LINE_1(3),
		line1_04 => LCD_LINE_1(4),
		line1_05 => LCD_LINE_1(5),
		line1_06 => LCD_LINE_1(6),
		line1_07 => LCD_LINE_1(7),
		line1_08 => LCD_LINE_1(8),
		line1_09 => LCD_LINE_1(9),
		line1_10 => LCD_LINE_1(10),
		line1_11 => LCD_LINE_1(11),
		line1_12 => LCD_LINE_1(12),
		line1_13 => LCD_LINE_1(13),
		line1_14 => LCD_LINE_1(14),
		line1_15 => LCD_LINE_1(15),
		line2_00 => LCD_LINE_2(0),
		line2_01 => LCD_LINE_2(1),
		line2_02 => LCD_LINE_2(2),
		line2_03 => LCD_LINE_2(3),
		line2_04 => LCD_LINE_2(4),
		line2_05 => LCD_LINE_2(5),
		line2_06 => LCD_LINE_2(6),
		line2_07 => LCD_LINE_2(7),
		line2_08 => LCD_LINE_2(8),
		line2_09 => LCD_LINE_2(9),
		line2_10 => LCD_LINE_2(10),
		line2_11 => LCD_LINE_2(11),
		line2_12 => LCD_LINE_2(12),
		line2_13 => LCD_LINE_2(13),
		line2_14 => LCD_LINE_2(14),
		line2_15 => LCD_LINE_2(15)
	);

	SFP_TXENA <= '1';       -- enable Tx

--################################################################################
--
-- ######  ######  #          #    ####### #######
-- #     # #     # #         # #        #  #
-- #     # #     # #        #   #      #   #
-- ######  ######  #       #     #    #    #####
-- #       #     # #       #######   #     #
-- #       #     # #       #     #  #      #
-- #       ######  ####### #     # ####### #######
--
-- PicoBlaze with 2xI2C (PHY + SFP) and UART
--################################################################################

  MPU_SYS_i : entity WORK.MPU_SYS
  PORT MAP(
    clk                                     => clk_sys,
    SYS_RST                                 => rst,

    MPU_SYS_INOUT.SFP_M2                    => SFP_M2,          -- SDA; serial interface data
    MPU_SYS_INOUT.LCD_DB                    => dummy_LCD_DB,

    MPU_SYS_IN.UBLAZE_addr                  => X"00",  --! TODO: connect to MicroBlaze
    MPU_SYS_IN.UBLAZE_write_data            => X"00",
    MPU_SYS_IN.UBLAZE_write_strobe          => '0',
    MPU_SYS_IN.UART_RXD                     => UART_RXD,
    MPU_SYS_IN.GPIO_DIP                     => X"00", --GPIO_DIP,
    MPU_SYS_IN.GPIO_BTN                     => GPIO_BTN,
    MPU_SYS_IN.SPI_SCK                      => SPI_SCK,
    MPU_SYS_IN.SPI_SDI                      => SPI_SDI,
    MPU_SYS_IN.SPI_CSN                      => SPI_CSN,
    MPU_SYS_IN.PLL_locked_SFP_SMA           => PLL_locked_SFP_SMA,
    MPU_SYS_IN.BERT_IRQ_1s                  => BERT_IRQ_1s,
    MPU_SYS_IN.BERT_toggle_1s               => BERT_toggle_1s,
    MPU_SYS_IN.BERT_Err_cnt_1s              => BERT_Err_cnt_1s,
    MPU_SYS_IN.BERT_LFSR_locked             => BERT_LFSR_locked,
    MPU_SYS_IN.SFP_M0                       => '0',             -- L when module present; unconnected on ML505
    MPU_SYS_IN.SFP_TXF                      => '0',             -- Tx fault; active H; unconnected on ML505
    MPU_SYS_IN.SFP_LOS                      => '0',            -- Los of Signal; active H; unconnected on ML505
    MPU_SYS_IN.HOST_1_MIIMRDY               => HOST_1_MIIMRDY,
    MPU_SYS_IN.HOST_1_RDDATA                => HOST_1_RDDATA,

    MPU_SYS_OUT.HOST_1_OPCODE               => HOST_1_OPCODE,
    MPU_SYS_OUT.HOST_1_REQ                  => HOST_1_REQ,
    MPU_SYS_OUT.HOST_1_MIIMSEL              => HOST_1_MIIMSEL,
    MPU_SYS_OUT.HOST_1_ADDR                 => HOST_1_ADDR,
    MPU_SYS_OUT.HOST_1_WRDATA               => HOST_1_WRDATA,
    MPU_SYS_OUT.HOST_1_EMAC1SEL             => HOST_1_EMAC1SEL,
    MPU_SYS_OUT.UBLAZE_read_data            => OPEN,  --! TODO: connect to MicroBlaze
    MPU_SYS_OUT.UBLAZE_data_ack             => OPEN,
    MPU_SYS_OUT.MUX_Select                  => MUX_Select,
    MPU_SYS_OUT.SPI_SDO                     => SPI_SDO,
    MPU_SYS_OUT.UART_TXD                    => UART_TXD,
    MPU_SYS_OUT.GPIO_LED                    => GPIO_LED_i,
    MPU_SYS_OUT.LCD_E                       => dummy_LCD_E,
    MPU_SYS_OUT.LCD_RS                      => dummy_LCD_RS,
    MPU_SYS_OUT.LCD_RW                      => dummy_LCD_RW,
    MPU_SYS_OUT.Seq_sel                     => Seq_sel,
    MPU_SYS_OUT.rst_GTP_SFP_SMA             => rst_GTP_SFP_SMA,
    MPU_SYS_OUT.rst_Rx_CDR_SFP              => rst_Rx_CDR_SFP,
    MPU_SYS_OUT.rst_Rx_CDR_SMA              => rst_Rx_CDR_SMA,
    MPU_SYS_OUT.SFP_Tx_Inhibit              => SFP_Tx_Inhibit,
    MPU_SYS_OUT.SFP_Tx_Polarity             => SFP_Tx_Polarity,
    MPU_SYS_OUT.SFP_Tx_Swing                => SFP_Tx_Swing,
    MPU_SYS_OUT.SFP_Tx_Preemphasis          => SFP_Tx_Preemphasis,
    MPU_SYS_OUT.SFP_Rx_EqMix                => SFP_Rx_EqMix,
    MPU_SYS_OUT.SFP_Rx_EqPole               => SFP_Rx_EqPole,
    MPU_SYS_OUT.SFP_M1                      => SFP_M1,          -- SCL; serial interface clock; max 100 kHz, active rising edge
    MPU_SYS_OUT.SFP_RS                      => OPEN);            -- Rate select; H for full-bandwidth; unconnected on ML505

    --################################################################################
    --
    -- #     # ######  #          #    ####### #######
    -- #     # #     # #         # #        #  #
    -- #     # #     # #        #   #      #   #
    -- #     # ######  #       #     #    #    #####
    -- #     # #     # #       #######   #     #
    -- #     # #     # #       #     #  #      #
    --  #####  ######  ####### #     # ####### #######
    --
    -- Microblaze
    --################################################################################

  Ublaze_i: entity work.ublaze_top PORT MAP(
    -- System Pins
    sys_clk_pin                => clk_sys,
    sys_rst_pin                => rst,
    -- User Interface
    LEDs_pin                   =>  GPIO_LED,
    Buttons_pin                => GPIO_BTN,
    DIP_Switches_pin           => GPIO_DIP,
    -- BRAM Write Interface -- FC -> BRAM
    BRAM_WR_Data_pin           => BRAM_WR_DATA,
    BRAM_WR_EN_pin             => BRAM_WR_EN,
    BRAM_WR_WEN_pin            => BRAM_WR_WEN,
    BRAM_WR_Addr_pin           => BRAM_WR_ADDR,
    BRAM_WR_Clk_pin            => clk_sys,
    BRAM_WR_Rst_pin            => rst,
    -- BRAM Read Interface -- BRAM -> FC
    BRAM_RD_Data_pin           => BRAM_RD_DATA,
    BRAM_RD_EN_pin             => BRAM_RD_EN,
    BRAM_RD_Addr_pin           => BRAM_RD_ADDR,
    BRAM_RD_Clk_pin            => clk_sys,
    BRAM_RD_Rst_pin            => rst,
    DATA_OUT_LEN_out_pin       => UB_BRAM_len_out,
    DATA_IN_LEN_in_pin         => UB_BRAM_len_in,
    -- BRAM Control interface (R,W)
    BRAM_IF_W_pin              => UBLAZE_BRAM_CTRL_OUT,
    BRAM_IF_R_pin              => UBLAZE_BRAM_CTRL_IN,
    -- Display output
    display_out_pin            => UBLAZE_display_out,
    -- Software Programmable MAC Address output
    MAC_REG_OUT_pin            => UBLAZE_MAC_ADDRESS,
    -- Statistics from codec
    RX_FRAMES_RECEIVED_in_pin  => RX_FRAMES_RECEIVED,
		RX_FRAMES_BAD_in_pin       => RX_FRAMES_BAD,
		RX_BYTES_CORRECTED_in_pin  => RX_BYTES_CORRECTED,
		TX_FRAMES_SENT_in_pin      => TX_FRAMES_SENT,
		RX_BYTES_RECEIVED_in_pin   => RX_BYTES_RECEIVED,
		TX_FRAMES_TRUNCATED_in_pin => TX_FRAMES_TRUNCATED,
		TX_BYTES_SENT_in_pin       => TX_BYTES_SENT,
		TX_BYTES_DROPPED_in_pin    => TX_BYTES_DROPPED,
    -- Serial port
    SP_RX_pin                  => UART_3_RXD,
    SP_TX_pin                  => UART_3_TXD,
    -- Statistics-events
    SE_STAT_DATA_IN_pin        => STAT_DOUT,
    SE_STAT_COUNT_IN_pin       => STAT_COUNT,
    SE_STAT_RD_EN_OUT_pin      => STAT_RD_EN,
    -- SRAM Interface
    SRAM_Mem_CEN_pin           => SRAM_CEN,
    SRAM_Mem_OEN_pin           => SRAM_OEN,
    SRAM_Mem_WEN_pin           => SRAM_WEN,
    SRAM_Mem_BEN_pin           => SRAM_BEN,
    SRAM_Mem_ADV_LDN_pin       => SRAM_ADV_LDN,
    SRAM_Mem_DQ_pin            => SRAM_DQ,
    SRAM_Mem_A_pin             => SRAM_FLASH_A,
    SRAM_Mem_LBON_pin          => SRAM_LBON,
    -- SRAM Clocks
    CLKGEN_CLKFBIN_pin         => SRAM_CLK_FB,
	  CLKGEN_CLKFBOUT_pin        => SRAM_CLK_OUT,
	  -- Config Interface
	  config_reg0_out_pin => CONFIG_REG0_OUT,
    config_reg0_in_pin => CONFIG_REG0_IN,
	  config_reg1_pin => CONFIG_REG1
  );

  UART_3_TXEN    <= CONFIG_REG0_OUT(2);

--################################################################################
--
-- ######  ######  ######
-- #     # #     # #     #
-- #     # #     # #     #
-- #     # #     # ######
-- #     # #     # #   #
-- #     # #     # #    #
-- ######  ######  #     #
--
-- DDR Controller
--################################################################################


  DDR2controller : entity WORK.DDRcontroller
    generic map (
     BANK_WIDTH => BANK_WIDTH,
     CKE_WIDTH => CKE_WIDTH,
     CLK_WIDTH => CLK_WIDTH,
     COL_WIDTH => COL_WIDTH,
     CS_NUM => CS_NUM,
     CS_WIDTH => CS_WIDTH,
     CS_BITS => CS_BITS,
     DM_WIDTH => DM_WIDTH,
     DQ_WIDTH => DQ_WIDTH,
     DQ_PER_DQS => DQ_PER_DQS,
     DQS_WIDTH => DQS_WIDTH,
     DQ_BITS => DQ_BITS,
     DQS_BITS => DQS_BITS,
     ODT_WIDTH => ODT_WIDTH,
     ROW_WIDTH => ROW_WIDTH,
     ADDITIVE_LAT => ADDITIVE_LAT,
     BURST_LEN => BURST_LEN,
     BURST_TYPE => BURST_TYPE,
     CAS_LAT => CAS_LAT,
     ECC_ENABLE => ECC_ENABLE,
     APPDATA_WIDTH => APPDATA_WIDTH,
     MULTI_BANK_EN => MULTI_BANK_EN,
     TWO_T_TIME_EN => TWO_T_TIME_EN,
     ODT_TYPE => ODT_TYPE,
     REDUCE_DRV => REDUCE_DRV,
     REG_ENABLE => REG_ENABLE,
     TREFI_NS => TREFI_NS,
     TRAS => TRAS,
     TRCD => TRCD,
     TRFC => TRFC,
     TRP => TRP,
     TRTP => TRTP,
     TWR => TWR,
     TWTR => TWTR,
     HIGH_PERFORMANCE_MODE => HIGH_PERFORMANCE_MODE,
     SIM_ONLY => SIM_ONLY,
     DEBUG_EN => DEBUG_EN,
     CLK_PERIOD => CLK_PERIOD,
     DLL_FREQ_MODE => DLL_FREQ_MODE,
     CLK_TYPE => CLK_TYPE,
     NOCLK200 => NOCLK200,
     RST_ACT_LOW => RST_ACT_LOW
)
port map (
   ddr2_dq                    => ddr2_dq,
   ddr2_a                     => ddr2_a,
   ddr2_ba                    => ddr2_ba,
   ddr2_ras_n                 => ddr2_ras_n,
   ddr2_cas_n                 => ddr2_cas_n,
   ddr2_we_n                  => ddr2_we_n,
   ddr2_cs_n                  => ddr2_cs_n,
   ddr2_odt                   => ddr2_odt,
   ddr2_cke                   => ddr2_cke,
   ddr2_dm                    => ddr2_dm,
   sys_clk_p                  => sys_clk_p,
   sys_clk_n                  => sys_clk_n,
   clk200_p                   => clk200_p,
   clk200_n                   => clk200_n,
   sys_rst_n                  => sys_rst_n,
   phy_init_done              => phy_init_done,
   rst0_tb                    => rst0_tb,
   clk0_tb                    => clk0_tb,
   app_wdf_afull              => app_wdf_afull,
   app_af_afull               => app_af_afull,
   rd_data_valid              => rd_data_valid,
   app_wdf_wren               => app_wdf_wren,
   app_af_wren                => app_af_wren,
   app_af_addr                => app_af_addr,
   app_af_cmd                 => app_af_cmd,
   rd_data_fifo_out           => rd_data_fifo_out,
   app_wdf_data               => app_wdf_data,
   app_wdf_mask_data          => app_wdf_mask_data,
   ddr2_dqs                   => ddr2_dqs,
   ddr2_dqs_n                 => ddr2_dqs_n,
   ddr2_ck                    => ddr2_ck,
   ddr2_ck_n                  => ddr2_ck_n
);

SFP_DDRfifo: entity WORK.DDRfifo
GENERIC MAP (
		APPDATA_WIDTH => APPDATA_WIDTH,
		FIFO_SIZE_POW2 => 18 --! 25
)
PORT MAP(
		rst => rst_GTP_SFP_SMA,
		wr_clk => DDR_FIFO_wr_clk,
		rd_clk => DDR_FIFO_rd_clk,
		din => DDR_FIFO_din,
		wr_en => DDR_FIFO_wr_en,
		rd_en => DDR_FIFO_rd_en,
		dout => DDR_FIFO_dout,
		full => DDR_FIFO_full,
		empty => DDR_FIFO_empty,
		empty_prog => DDR_FIFO_empty_prog,

		sys_rst_n => OPEN, --sys_rst_n,
		phy_init_done => phy_init_done,
		rst0_tb => rst0_tb,
		clk0_tb => clk0_tb,
		app_wdf_afull => app_wdf_afull,
		app_af_afull => app_af_afull,
		rd_data_valid => rd_data_valid,
		app_wdf_wren => app_wdf_wren,
		app_af_wren => app_af_wren,
		app_af_addr => app_af_addr,
		app_af_cmd => app_af_cmd,
		rd_data_fifo_out => rd_data_fifo_out,
		app_wdf_data => app_wdf_data,
		app_wdf_mask_data => app_wdf_mask_data
	);
 sys_rst_n <= not GPIO_BTN(0); --! todo, connect somewhere else!

-- HW Test Bench
--	Inst_ddr2_tb_top: ddr2_tb_top
--	GENERIC MAP (
--    BANK_WIDTH    => BANK_WIDTH,
--    COL_WIDTH     => COL_WIDTH,
--    DM_WIDTH      => DM_WIDTH,
--    DQ_WIDTH      => DQ_WIDTH,
--    ROW_WIDTH     => ROW_WIDTH,
--    APPDATA_WIDTH => APPDATA_WIDTH,
--    ECC_ENABLE    => ECC_ENABLE,
--    BURST_LEN     => BURST_LEN
--	)
--
--	PORT MAP(
--		clk0 => clk0_tb,
--		rst0 => rst0_tb,
--		app_af_afull => app_af_afull,
--		app_wdf_afull => app_wdf_afull,
--		rd_data_valid => rd_data_valid,
--		rd_data_fifo_out => rd_data_fifo_out,
--		phy_init_done => phy_init_done,
--		app_af_wren => app_af_wren,
--		app_af_cmd => app_af_cmd,
--		app_af_addr => app_af_addr,
--		app_wdf_wren => app_wdf_wren,
--		app_wdf_data => app_wdf_data,
--		app_wdf_mask_data => app_wdf_mask_data,
--		error => GPIO_LED(1),
--		error_cmp => GPIO_LED(2)
--	);

-- ####################################################################
--
-- ######     #    #######    #     #####  ####### #     #
-- #     #   # #      #      # #   #     # #       ##    #
-- #     #  #   #     #     #   #  #       #       # #   #
-- #     # #     #    #    #     # #  #### #####   #  #  #
-- #     # #######    #    ####### #     # #       #   # #
-- #     # #     #    #    #     # #     # #       #    ##
-- ######  #     #    #    #     #  #####  ####### #     #
--
-- ####################################################################

-- !!! TODO: Slightly review the code and (especially) have a look at the
--           switch routing - it is currently wired both to microblaze
--           and picoblaze !!!

-- The idea:
-- DIP SW 1:   drives Tx MUX (0 = Tx data from ETH, 1 = Tx data from generator)
-- DIP SW 2:   drives Rx MUX (0 = Rx data from SFP to ETH, 1 = Rx data from SFP
--             to some PRBS detector or whatever else logic)
-- DIP SW 3-4: select generator pattern - PRBS, 60k square, etc.

DATAGEN_SEQ_SEL <= "00000" & GPIO_DIP(6 downto 4);

Inst_Data_Gen_10b: entity work.Data_Gen_10b PORT MAP(
  clk          => clk_Tx_SFP_out,
  Seq_sel      => DATAGEN_SEQ_SEL,
  data_out_10b => DATAGEN_DATA_OUT
);

PROCESS(clk_Tx_SFP_out) BEGIN
	IF rising_edge(clk_Tx_SFP_out) THEN
		IF (GPIO_DIP(7) = '0') THEN
      Tx_SFP_Data_10b <= SFP_RAW_OUT;
    ELSE
      Tx_SFP_Data_10b <= DATAGEN_DATA_OUT;
    END IF;
	END IF;
END PROCESS;


--------------------------------------------------------------------------------
-- Latency Test Output, use a scope

PROCESS(clk_Tx_SFP_out) BEGIN
	IF rising_edge(clk_Tx_SFP_out) THEN
		HDR1_50 <= PHY_RX_DV; -- ETH IN
		IF ((Tx_SFP_Data_10b /= "0011111010") AND
			(Tx_SFP_Data_10b /= "0011111010")) THEN
			HDR1_52 <= '1';
		ELSE
			HDR1_52 <= '0';
		END IF;
		HDR1_54 <= PHY_TX_DV; -- ETH OUT
	END IF;
END PROCESS;
----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------
