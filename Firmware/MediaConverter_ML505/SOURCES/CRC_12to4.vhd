-------------------------------------------------------------------------------
-- Copyright (C) 2009 OutputLogic.com
-- This source file may be used and distributed without restriction
-- provided that this copyright statement is not removed from the file
-- and that any derivative work contains the original copyright notice
-- and the associated disclaimer.
-- 
-- THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS
-- OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
-- WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
-------------------------------------------------------------------------------
-- CRC module for data(11:0)
--   lfsr(3:0)=1+x^1+x^2+x^3+x^4;
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity crc_12to4_single is
	port ( data_in : in std_logic_vector (11 downto 0);
			 crc_out : out std_logic_vector (3 downto 0));
end crc_12to4_single;

architecture imp_crc of crc_12to4_single is
	constant lfsr_q: std_logic_vector (3 downto 0) := b"1111";
	signal lfsr_c: std_logic_vector (3 downto 0) := b"1111";
begin
	crc_out <= lfsr_c;

	lfsr_c(0) <= lfsr_q(2) xor lfsr_q(3) xor data_in(0) xor data_in(1) xor data_in(5) xor data_in(6) xor data_in(10) xor data_in(11);
	lfsr_c(1) <= lfsr_q(2) xor data_in(0) xor data_in(2) xor data_in(5) xor data_in(7) xor data_in(10);
	lfsr_c(2) <= lfsr_q(0) xor lfsr_q(2) xor data_in(0) xor data_in(3) xor data_in(5) xor data_in(8) xor data_in(10);
	lfsr_c(3) <= lfsr_q(1) xor lfsr_q(2) xor data_in(0) xor data_in(4) xor data_in(5) xor data_in(9) xor data_in(10);

end architecture imp_crc; 
