----------------------------------------------------------------------------------
-- Symplified 8B/10B decoder (IEEE Std 802.3 Clause 36)
--
-- Michal Kubicek
-- michal.kubicek@email.cz
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;
----------------------------------------------------------------------------------
ENTITY Decoder_8B10B IS PORT(

    Rx_clk_in               : IN    STD_LOGIC;
    Rx_data_in              : IN    STD_LOGIC_VECTOR(9 DOWNTO 0);
    Rx_DV_in                : IN    STD_LOGIC;

    Rx_data_out             : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0) := X"00";
    Rx_DV_out               : OUT   STD_LOGIC := '0';
    Rx_CharIsK_out          : OUT   STD_LOGIC := '0';
    Rx_Char_err             : OUT   STD_LOGIC := '0');
  --Rx_RunDisp_out          : OUT   STD_LOGIC);

END Decoder_8B10B;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF Decoder_8B10B IS
----------------------------------------------------------------------------------

--SIGNAL CurrDisp           : STD_LOGIC := '0';

----------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------------

  PROCESS (Rx_clk_in) BEGIN
    IF rising_edge(Rx_clk_in) THEN

      Rx_DV_out     <= Rx_DV_in;
      Rx_Char_err   <= '0';

      IF Rx_DV_in = '1' THEN

          Rx_CharIsK_out <= '0';

          CASE Rx_data_in IS

        -- Data --------------------------------------------------------------------
            WHEN "1001110100" => Rx_data_out <= X"00"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D0.0
            WHEN "0111010100" => Rx_data_out <= X"01"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D1.0
            WHEN "1011010100" => Rx_data_out <= X"02"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D2.0
            WHEN "1100011011" => Rx_data_out <= X"03"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D3.0
            WHEN "1101010100" => Rx_data_out <= X"04"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D4.0
            WHEN "1010011011" => Rx_data_out <= X"05"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D5.0
            WHEN "0110011011" => Rx_data_out <= X"06"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D6.0
            WHEN "1110001011" => Rx_data_out <= X"07"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D7.0
            WHEN "1110010100" => Rx_data_out <= X"08"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D8.0
            WHEN "1001011011" => Rx_data_out <= X"09"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D9.0
            WHEN "0101011011" => Rx_data_out <= X"0A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D10.0
            WHEN "1101001011" => Rx_data_out <= X"0B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D11.0
            WHEN "0011011011" => Rx_data_out <= X"0C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D12.0
            WHEN "1011001011" => Rx_data_out <= X"0D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D13.0
            WHEN "0111001011" => Rx_data_out <= X"0E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D14.0
            WHEN "0101110100" => Rx_data_out <= X"0F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D15.0
            WHEN "0110110100" => Rx_data_out <= X"10"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D16.0
            WHEN "1000111011" => Rx_data_out <= X"11"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D17.0
            WHEN "0100111011" => Rx_data_out <= X"12"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D18.0
            WHEN "1100101011" => Rx_data_out <= X"13"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D19.0
            WHEN "0010111011" => Rx_data_out <= X"14"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D20.0
            WHEN "1010101011" => Rx_data_out <= X"15"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D21.0
            WHEN "0110101011" => Rx_data_out <= X"16"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D22.0
            WHEN "1110100100" => Rx_data_out <= X"17"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D23.0
            WHEN "1100110100" => Rx_data_out <= X"18"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D24.0
            WHEN "1001101011" => Rx_data_out <= X"19"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D25.0
            WHEN "0101101011" => Rx_data_out <= X"1A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D26.0
            WHEN "1101100100" => Rx_data_out <= X"1B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D27.0
            WHEN "0011101011" => Rx_data_out <= X"1C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D28.0
            WHEN "1011100100" => Rx_data_out <= X"1D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D29.0
            WHEN "0111100100" => Rx_data_out <= X"1E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D30.0
            WHEN "1010110100" => Rx_data_out <= X"1F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D31.0
            WHEN "1001111001" => Rx_data_out <= X"20"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.1
            WHEN "0111011001" => Rx_data_out <= X"21"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.1
            WHEN "1011011001" => Rx_data_out <= X"22"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.1
            WHEN "1100011001" => Rx_data_out <= X"23"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.1
            WHEN "1101011001" => Rx_data_out <= X"24"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.1
            WHEN "1010011001" => Rx_data_out <= X"25"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.1
            WHEN "0110011001" => Rx_data_out <= X"26"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.1
            WHEN "1110001001" => Rx_data_out <= X"27"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.1
            WHEN "1110011001" => Rx_data_out <= X"28"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.1
            WHEN "1001011001" => Rx_data_out <= X"29"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.1
            WHEN "0101011001" => Rx_data_out <= X"2A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.1
            WHEN "1101001001" => Rx_data_out <= X"2B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.1
            WHEN "0011011001" => Rx_data_out <= X"2C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.1
            WHEN "1011001001" => Rx_data_out <= X"2D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.1
            WHEN "0111001001" => Rx_data_out <= X"2E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.1
            WHEN "0101111001" => Rx_data_out <= X"2F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.1
            WHEN "0110111001" => Rx_data_out <= X"30"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.1
            WHEN "1000111001" => Rx_data_out <= X"31"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.1
            WHEN "0100111001" => Rx_data_out <= X"32"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.1
            WHEN "1100101001" => Rx_data_out <= X"33"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.1
            WHEN "0010111001" => Rx_data_out <= X"34"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.1
            WHEN "1010101001" => Rx_data_out <= X"35"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.1
            WHEN "0110101001" => Rx_data_out <= X"36"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.1
            WHEN "1110101001" => Rx_data_out <= X"37"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.1
            WHEN "1100111001" => Rx_data_out <= X"38"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.1
            WHEN "1001101001" => Rx_data_out <= X"39"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.1
            WHEN "0101101001" => Rx_data_out <= X"3A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.1
            WHEN "1101101001" => Rx_data_out <= X"3B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.1
            WHEN "0011101001" => Rx_data_out <= X"3C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.1
            WHEN "1011101001" => Rx_data_out <= X"3D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.1
            WHEN "0111101001" => Rx_data_out <= X"3E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.1
            WHEN "1010111001" => Rx_data_out <= X"3F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.1
            WHEN "1001110101" => Rx_data_out <= X"40"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.2
            WHEN "0111010101" => Rx_data_out <= X"41"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.2
            WHEN "1011010101" => Rx_data_out <= X"42"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.2
            WHEN "1100010101" => Rx_data_out <= X"43"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.2
            WHEN "1101010101" => Rx_data_out <= X"44"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.2
            WHEN "1010010101" => Rx_data_out <= X"45"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.2
            WHEN "0110010101" => Rx_data_out <= X"46"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.2
            WHEN "1110000101" => Rx_data_out <= X"47"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.2
            WHEN "1110010101" => Rx_data_out <= X"48"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.2
            WHEN "1001010101" => Rx_data_out <= X"49"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.2
            WHEN "0101010101" => Rx_data_out <= X"4A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.2
            WHEN "1101000101" => Rx_data_out <= X"4B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.2
            WHEN "0011010101" => Rx_data_out <= X"4C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.2
            WHEN "1011000101" => Rx_data_out <= X"4D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.2
            WHEN "0111000101" => Rx_data_out <= X"4E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.2
            WHEN "0101110101" => Rx_data_out <= X"4F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.2
            WHEN "0110110101" => Rx_data_out <= X"50"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.2
            WHEN "1000110101" => Rx_data_out <= X"51"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.2
            WHEN "0100110101" => Rx_data_out <= X"52"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.2
            WHEN "1100100101" => Rx_data_out <= X"53"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.2
            WHEN "0010110101" => Rx_data_out <= X"54"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.2
            WHEN "1010100101" => Rx_data_out <= X"55"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.2
            WHEN "0110100101" => Rx_data_out <= X"56"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.2
            WHEN "1110100101" => Rx_data_out <= X"57"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.2
            WHEN "1100110101" => Rx_data_out <= X"58"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.2
            WHEN "1001100101" => Rx_data_out <= X"59"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.2
            WHEN "0101100101" => Rx_data_out <= X"5A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.2
            WHEN "1101100101" => Rx_data_out <= X"5B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.2
            WHEN "0011100101" => Rx_data_out <= X"5C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.2
            WHEN "1011100101" => Rx_data_out <= X"5D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.2
            WHEN "0111100101" => Rx_data_out <= X"5E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.2
            WHEN "1010110101" => Rx_data_out <= X"5F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.2
            WHEN "1001110011" => Rx_data_out <= X"60"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.3
            WHEN "0111010011" => Rx_data_out <= X"61"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.3
            WHEN "1011010011" => Rx_data_out <= X"62"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.3
            WHEN "1100011100" => Rx_data_out <= X"63"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.3
            WHEN "1101010011" => Rx_data_out <= X"64"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.3
            WHEN "1010011100" => Rx_data_out <= X"65"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.3
            WHEN "0110011100" => Rx_data_out <= X"66"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.3
            WHEN "1110001100" => Rx_data_out <= X"67"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.3
            WHEN "1110010011" => Rx_data_out <= X"68"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.3
            WHEN "1001011100" => Rx_data_out <= X"69"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.3
            WHEN "0101011100" => Rx_data_out <= X"6A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.3
            WHEN "1101001100" => Rx_data_out <= X"6B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.3
            WHEN "0011011100" => Rx_data_out <= X"6C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.3
            WHEN "1011001100" => Rx_data_out <= X"6D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.3
            WHEN "0111001100" => Rx_data_out <= X"6E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.3
            WHEN "0101110011" => Rx_data_out <= X"6F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.3
            WHEN "0110110011" => Rx_data_out <= X"70"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.3
            WHEN "1000111100" => Rx_data_out <= X"71"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.3
            WHEN "0100111100" => Rx_data_out <= X"72"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.3
            WHEN "1100101100" => Rx_data_out <= X"73"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.3
            WHEN "0010111100" => Rx_data_out <= X"74"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.3
            WHEN "1010101100" => Rx_data_out <= X"75"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.3
            WHEN "0110101100" => Rx_data_out <= X"76"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.3
            WHEN "1110100011" => Rx_data_out <= X"77"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.3
            WHEN "1100110011" => Rx_data_out <= X"78"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.3
            WHEN "1001101100" => Rx_data_out <= X"79"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.3
            WHEN "0101101100" => Rx_data_out <= X"7A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.3
            WHEN "1101100011" => Rx_data_out <= X"7B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.3
            WHEN "0011101100" => Rx_data_out <= X"7C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.3
            WHEN "1011100011" => Rx_data_out <= X"7D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.3
            WHEN "0111100011" => Rx_data_out <= X"7E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.3
            WHEN "1010110011" => Rx_data_out <= X"7F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.3
            WHEN "1001110010" => Rx_data_out <= X"80"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D0.4
            WHEN "0111010010" => Rx_data_out <= X"81"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D1.4
            WHEN "1011010010" => Rx_data_out <= X"82"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D2.4
            WHEN "1100011101" => Rx_data_out <= X"83"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D3.4
            WHEN "1101010010" => Rx_data_out <= X"84"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D4.4
            WHEN "1010011101" => Rx_data_out <= X"85"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D5.4
            WHEN "0110011101" => Rx_data_out <= X"86"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D6.4
            WHEN "1110001101" => Rx_data_out <= X"87"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D7.4
            WHEN "1110010010" => Rx_data_out <= X"88"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D8.4
            WHEN "1001011101" => Rx_data_out <= X"89"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D9.4
            WHEN "0101011101" => Rx_data_out <= X"8A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D10.4
            WHEN "1101001101" => Rx_data_out <= X"8B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D11.4
            WHEN "0011011101" => Rx_data_out <= X"8C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D12.4
            WHEN "1011001101" => Rx_data_out <= X"8D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D13.4
            WHEN "0111001101" => Rx_data_out <= X"8E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D14.4
            WHEN "0101110010" => Rx_data_out <= X"8F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D15.4
            WHEN "0110110010" => Rx_data_out <= X"90"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D16.4
            WHEN "1000111101" => Rx_data_out <= X"91"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D17.4
            WHEN "0100111101" => Rx_data_out <= X"92"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D18.4
            WHEN "1100101101" => Rx_data_out <= X"93"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D19.4
            WHEN "0010111101" => Rx_data_out <= X"94"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D20.4
            WHEN "1010101101" => Rx_data_out <= X"95"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D21.4
            WHEN "0110101101" => Rx_data_out <= X"96"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D22.4
            WHEN "1110100010" => Rx_data_out <= X"97"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D23.4
            WHEN "1100110010" => Rx_data_out <= X"98"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D24.4
            WHEN "1001101101" => Rx_data_out <= X"99"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D25.4
            WHEN "0101101101" => Rx_data_out <= X"9A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D26.4
            WHEN "1101100010" => Rx_data_out <= X"9B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D27.4
            WHEN "0011101101" => Rx_data_out <= X"9C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D28.4
            WHEN "1011100010" => Rx_data_out <= X"9D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D29.4
            WHEN "0111100010" => Rx_data_out <= X"9E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D30.4
            WHEN "1010110010" => Rx_data_out <= X"9F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D31.4
            WHEN "1001111010" => Rx_data_out <= X"A0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.5
            WHEN "0111011010" => Rx_data_out <= X"A1"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.5
            WHEN "1011011010" => Rx_data_out <= X"A2"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.5
            WHEN "1100011010" => Rx_data_out <= X"A3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.5
            WHEN "1101011010" => Rx_data_out <= X"A4"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.5
            WHEN "1010011010" => Rx_data_out <= X"A5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.5
            WHEN "0110011010" => Rx_data_out <= X"A6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.5
            WHEN "1110001010" => Rx_data_out <= X"A7"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.5
            WHEN "1110011010" => Rx_data_out <= X"A8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.5
            WHEN "1001011010" => Rx_data_out <= X"A9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.5
            WHEN "0101011010" => Rx_data_out <= X"AA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.5
            WHEN "1101001010" => Rx_data_out <= X"AB"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.5
            WHEN "0011011010" => Rx_data_out <= X"AC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.5
            WHEN "1011001010" => Rx_data_out <= X"AD"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.5
            WHEN "0111001010" => Rx_data_out <= X"AE"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.5
            WHEN "0101111010" => Rx_data_out <= X"AF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.5
            WHEN "0110111010" => Rx_data_out <= X"B0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.5
            WHEN "1000111010" => Rx_data_out <= X"B1"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.5
            WHEN "0100111010" => Rx_data_out <= X"B2"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.5
            WHEN "1100101010" => Rx_data_out <= X"B3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.5
            WHEN "0010111010" => Rx_data_out <= X"B4"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.5
            WHEN "1010101010" => Rx_data_out <= X"B5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.5
            WHEN "0110101010" => Rx_data_out <= X"B6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.5
            WHEN "1110101010" => Rx_data_out <= X"B7"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.5
            WHEN "1100111010" => Rx_data_out <= X"B8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.5
            WHEN "1001101010" => Rx_data_out <= X"B9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.5
            WHEN "0101101010" => Rx_data_out <= X"BA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.5
            WHEN "1101101010" => Rx_data_out <= X"BB"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.5
            WHEN "0011101010" => Rx_data_out <= X"BC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.5
            WHEN "1011101010" => Rx_data_out <= X"BD"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.5
            WHEN "0111101010" => Rx_data_out <= X"BE"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.5
            WHEN "1010111010" => Rx_data_out <= X"BF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.5
            WHEN "1001110110" => Rx_data_out <= X"C0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.6
            WHEN "0111010110" => Rx_data_out <= X"C1"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.6
            WHEN "1011010110" => Rx_data_out <= X"C2"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.6
            WHEN "1100010110" => Rx_data_out <= X"C3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.6
            WHEN "1101010110" => Rx_data_out <= X"C4"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.6
            WHEN "1010010110" => Rx_data_out <= X"C5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.6
            WHEN "0110010110" => Rx_data_out <= X"C6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.6
            WHEN "1110000110" => Rx_data_out <= X"C7"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.6
            WHEN "1110010110" => Rx_data_out <= X"C8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.6
            WHEN "1001010110" => Rx_data_out <= X"C9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.6
            WHEN "0101010110" => Rx_data_out <= X"CA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.6
            WHEN "1101000110" => Rx_data_out <= X"CB"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.6
            WHEN "0011010110" => Rx_data_out <= X"CC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.6
            WHEN "1011000110" => Rx_data_out <= X"CD"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.6
            WHEN "0111000110" => Rx_data_out <= X"CE"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.6
            WHEN "0101110110" => Rx_data_out <= X"CF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.6
            WHEN "0110110110" => Rx_data_out <= X"D0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.6
            WHEN "1000110110" => Rx_data_out <= X"D1"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.6
            WHEN "0100110110" => Rx_data_out <= X"D2"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.6
            WHEN "1100100110" => Rx_data_out <= X"D3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.6
            WHEN "0010110110" => Rx_data_out <= X"D4"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.6
            WHEN "1010100110" => Rx_data_out <= X"D5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.6
            WHEN "0110100110" => Rx_data_out <= X"D6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.6
            WHEN "1110100110" => Rx_data_out <= X"D7"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.6
            WHEN "1100110110" => Rx_data_out <= X"D8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.6
            WHEN "1001100110" => Rx_data_out <= X"D9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.6
            WHEN "0101100110" => Rx_data_out <= X"DA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.6
            WHEN "1101100110" => Rx_data_out <= X"DB"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.6
            WHEN "0011100110" => Rx_data_out <= X"DC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.6
            WHEN "1011100110" => Rx_data_out <= X"DD"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.6
            WHEN "0111100110" => Rx_data_out <= X"DE"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.6
            WHEN "1010110110" => Rx_data_out <= X"DF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.6
            WHEN "1001110001" => Rx_data_out <= X"E0"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D0.7
            WHEN "0111010001" => Rx_data_out <= X"E1"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D1.7
            WHEN "1011010001" => Rx_data_out <= X"E2"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D2.7
            WHEN "1100011110" => Rx_data_out <= X"E3"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D3.7
            WHEN "1101010001" => Rx_data_out <= X"E4"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D4.7
            WHEN "1010011110" => Rx_data_out <= X"E5"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D5.7
            WHEN "0110011110" => Rx_data_out <= X"E6"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D6.7
            WHEN "1110001110" => Rx_data_out <= X"E7"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D7.7
            WHEN "1110010001" => Rx_data_out <= X"E8"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D8.7
            WHEN "1001011110" => Rx_data_out <= X"E9"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D9.7
            WHEN "0101011110" => Rx_data_out <= X"EA"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D10.7
            WHEN "1101001110" => Rx_data_out <= X"EB"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D11.7
            WHEN "0011011110" => Rx_data_out <= X"EC"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D12.7
            WHEN "1011001110" => Rx_data_out <= X"ED"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D13.7
            WHEN "0111001110" => Rx_data_out <= X"EE"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D14.7
            WHEN "0101110001" => Rx_data_out <= X"EF"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D15.7
            WHEN "0110110001" => Rx_data_out <= X"F0"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D16.7
            WHEN "1000110111" => Rx_data_out <= X"F1"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D17.7
            WHEN "0100110111" => Rx_data_out <= X"F2"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D18.7
            WHEN "1100101110" => Rx_data_out <= X"F3"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D19.7
            WHEN "0010110111" => Rx_data_out <= X"F4"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D20.7
            WHEN "1010101110" => Rx_data_out <= X"F5"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D21.7
            WHEN "0110101110" => Rx_data_out <= X"F6"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D22.7
            WHEN "1110100001" => Rx_data_out <= X"F7"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D23.7
            WHEN "1100110001" => Rx_data_out <= X"F8"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D24.7
            WHEN "1001101110" => Rx_data_out <= X"F9"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D25.7
            WHEN "0101101110" => Rx_data_out <= X"FA"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D26.7
            WHEN "1101100001" => Rx_data_out <= X"FB"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D27.7
            WHEN "0011101110" => Rx_data_out <= X"FC"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D28.7
            WHEN "1011100001" => Rx_data_out <= X"FD"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D29.7
            WHEN "0111100001" => Rx_data_out <= X"FE"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D30.7
            WHEN "1010110001" => Rx_data_out <= X"FF"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D31.7

            WHEN "0110001011" => Rx_data_out <= X"00"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D0.0
            WHEN "1000101011" => Rx_data_out <= X"01"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D1.0
            WHEN "0100101011" => Rx_data_out <= X"02"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D2.0
            WHEN "1100010100" => Rx_data_out <= X"03"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D3.0
            WHEN "0010101011" => Rx_data_out <= X"04"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D4.0
            WHEN "1010010100" => Rx_data_out <= X"05"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D5.0
            WHEN "0110010100" => Rx_data_out <= X"06"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D6.0
            WHEN "0001110100" => Rx_data_out <= X"07"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D7.0
            WHEN "0001101011" => Rx_data_out <= X"08"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D8.0
            WHEN "1001010100" => Rx_data_out <= X"09"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D9.0
            WHEN "0101010100" => Rx_data_out <= X"0A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D10.0
            WHEN "1101000100" => Rx_data_out <= X"0B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D11.0
            WHEN "0011010100" => Rx_data_out <= X"0C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D12.0
            WHEN "1011000100" => Rx_data_out <= X"0D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D13.0
            WHEN "0111000100" => Rx_data_out <= X"0E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D14.0
            WHEN "1010001011" => Rx_data_out <= X"0F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D15.0
            WHEN "1001001011" => Rx_data_out <= X"10"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D16.0
            WHEN "1000110100" => Rx_data_out <= X"11"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D17.0
            WHEN "0100110100" => Rx_data_out <= X"12"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D18.0
            WHEN "1100100100" => Rx_data_out <= X"13"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D19.0
            WHEN "0010110100" => Rx_data_out <= X"14"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D20.0
            WHEN "1010100100" => Rx_data_out <= X"15"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D21.0
            WHEN "0110100100" => Rx_data_out <= X"16"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D22.0
            WHEN "0001011011" => Rx_data_out <= X"17"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D23.0
            WHEN "0011001011" => Rx_data_out <= X"18"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D24.0
            WHEN "1001100100" => Rx_data_out <= X"19"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D25.0
            WHEN "0101100100" => Rx_data_out <= X"1A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D26.0
            WHEN "0010011011" => Rx_data_out <= X"1B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D27.0
            WHEN "0011100100" => Rx_data_out <= X"1C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D28.0
            WHEN "0100011011" => Rx_data_out <= X"1D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D29.0
            WHEN "1000011011" => Rx_data_out <= X"1E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D30.0
            WHEN "0101001011" => Rx_data_out <= X"1F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D31.0
            WHEN "0110001001" => Rx_data_out <= X"20"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.1
            WHEN "1000101001" => Rx_data_out <= X"21"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.1
            WHEN "0100101001" => Rx_data_out <= X"22"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.1
          --WHEN "1100011001" => Rx_data_out <= X"23"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.1
            WHEN "0010101001" => Rx_data_out <= X"24"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.1
          --WHEN "1010011001" => Rx_data_out <= X"25"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.1
          --WHEN "0110011001" => Rx_data_out <= X"26"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.1
            WHEN "0001111001" => Rx_data_out <= X"27"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.1
            WHEN "0001101001" => Rx_data_out <= X"28"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.1
          --WHEN "1001011001" => Rx_data_out <= X"29"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.1
          --WHEN "0101011001" => Rx_data_out <= X"2A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.1
          --WHEN "1101001001" => Rx_data_out <= X"2B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.1
          --WHEN "0011011001" => Rx_data_out <= X"2C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.1
          --WHEN "1011001001" => Rx_data_out <= X"2D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.1
          --WHEN "0111001001" => Rx_data_out <= X"2E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.1
            WHEN "1010001001" => Rx_data_out <= X"2F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.1
            WHEN "1001001001" => Rx_data_out <= X"30"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.1
          --WHEN "1000111001" => Rx_data_out <= X"31"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.1
          --WHEN "0100111001" => Rx_data_out <= X"32"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.1
          --WHEN "1100101001" => Rx_data_out <= X"33"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.1
          --WHEN "0010111001" => Rx_data_out <= X"34"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.1
          --WHEN "1010101001" => Rx_data_out <= X"35"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.1
          --WHEN "0110101001" => Rx_data_out <= X"36"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.1
            WHEN "0001011001" => Rx_data_out <= X"37"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.1
            WHEN "0011001001" => Rx_data_out <= X"38"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.1
          --WHEN "1001101001" => Rx_data_out <= X"39"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.1
          --WHEN "0101101001" => Rx_data_out <= X"3A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.1
            WHEN "0010011001" => Rx_data_out <= X"3B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.1
          --WHEN "0011101001" => Rx_data_out <= X"3C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.1
            WHEN "0100011001" => Rx_data_out <= X"3D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.1
            WHEN "1000011001" => Rx_data_out <= X"3E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.1
            WHEN "0101001001" => Rx_data_out <= X"3F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.1
            WHEN "0110000101" => Rx_data_out <= X"40"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.2
            WHEN "1000100101" => Rx_data_out <= X"41"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.2
            WHEN "0100100101" => Rx_data_out <= X"42"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.2
          --WHEN "1100010101" => Rx_data_out <= X"43"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.2
            WHEN "0010100101" => Rx_data_out <= X"44"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.2
          --WHEN "1010010101" => Rx_data_out <= X"45"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.2
          --WHEN "0110010101" => Rx_data_out <= X"46"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.2
            WHEN "0001110101" => Rx_data_out <= X"47"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.2
            WHEN "0001100101" => Rx_data_out <= X"48"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.2
          --WHEN "1001010101" => Rx_data_out <= X"49"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.2
          --WHEN "0101010101" => Rx_data_out <= X"4A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.2
          --WHEN "1101000101" => Rx_data_out <= X"4B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.2
          --WHEN "0011010101" => Rx_data_out <= X"4C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.2
          --WHEN "1011000101" => Rx_data_out <= X"4D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.2
          --WHEN "0111000101" => Rx_data_out <= X"4E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.2
            WHEN "1010000101" => Rx_data_out <= X"4F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.2
            WHEN "1001000101" => Rx_data_out <= X"50"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.2
          --WHEN "1000110101" => Rx_data_out <= X"51"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.2
          --WHEN "0100110101" => Rx_data_out <= X"52"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.2
          --WHEN "1100100101" => Rx_data_out <= X"53"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.2
          --WHEN "0010110101" => Rx_data_out <= X"54"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.2
          --WHEN "1010100101" => Rx_data_out <= X"55"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.2
          --WHEN "0110100101" => Rx_data_out <= X"56"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.2
            WHEN "0001010101" => Rx_data_out <= X"57"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.2
            WHEN "0011000101" => Rx_data_out <= X"58"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.2
          --WHEN "1001100101" => Rx_data_out <= X"59"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.2
          --WHEN "0101100101" => Rx_data_out <= X"5A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.2
            WHEN "0010010101" => Rx_data_out <= X"5B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.2
          --WHEN "0011100101" => Rx_data_out <= X"5C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.2
            WHEN "0100010101" => Rx_data_out <= X"5D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.2
            WHEN "1000010101" => Rx_data_out <= X"5E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.2
            WHEN "0101000101" => Rx_data_out <= X"5F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.2
            WHEN "0110001100" => Rx_data_out <= X"60"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.3
            WHEN "1000101100" => Rx_data_out <= X"61"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.3
            WHEN "0100101100" => Rx_data_out <= X"62"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.3
            WHEN "1100010011" => Rx_data_out <= X"63"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.3
            WHEN "0010101100" => Rx_data_out <= X"64"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.3
            WHEN "1010010011" => Rx_data_out <= X"65"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.3
            WHEN "0110010011" => Rx_data_out <= X"66"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.3
            WHEN "0001110011" => Rx_data_out <= X"67"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.3
            WHEN "0001101100" => Rx_data_out <= X"68"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.3
            WHEN "1001010011" => Rx_data_out <= X"69"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.3
            WHEN "0101010011" => Rx_data_out <= X"6A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.3
            WHEN "1101000011" => Rx_data_out <= X"6B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.3
            WHEN "0011010011" => Rx_data_out <= X"6C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.3
            WHEN "1011000011" => Rx_data_out <= X"6D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.3
            WHEN "0111000011" => Rx_data_out <= X"6E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.3
            WHEN "1010001100" => Rx_data_out <= X"6F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.3
            WHEN "1001001100" => Rx_data_out <= X"70"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.3
            WHEN "1000110011" => Rx_data_out <= X"71"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.3
            WHEN "0100110011" => Rx_data_out <= X"72"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.3
            WHEN "1100100011" => Rx_data_out <= X"73"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.3
            WHEN "0010110011" => Rx_data_out <= X"74"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.3
            WHEN "1010100011" => Rx_data_out <= X"75"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.3
            WHEN "0110100011" => Rx_data_out <= X"76"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.3
            WHEN "0001011100" => Rx_data_out <= X"77"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.3
            WHEN "0011001100" => Rx_data_out <= X"78"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.3
            WHEN "1001100011" => Rx_data_out <= X"79"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.3
            WHEN "0101100011" => Rx_data_out <= X"7A"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.3
            WHEN "0010011100" => Rx_data_out <= X"7B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.3
            WHEN "0011100011" => Rx_data_out <= X"7C"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.3
            WHEN "0100011100" => Rx_data_out <= X"7D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.3
            WHEN "1000011100" => Rx_data_out <= X"7E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.3
            WHEN "0101001100" => Rx_data_out <= X"7F"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.3
            WHEN "0110001101" => Rx_data_out <= X"80"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D0.4
            WHEN "1000101101" => Rx_data_out <= X"81"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D1.4
            WHEN "0100101101" => Rx_data_out <= X"82"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D2.4
            WHEN "1100010010" => Rx_data_out <= X"83"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D3.4
            WHEN "0010101101" => Rx_data_out <= X"84"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D4.4
            WHEN "1010010010" => Rx_data_out <= X"85"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D5.4
            WHEN "0110010010" => Rx_data_out <= X"86"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D6.4
            WHEN "0001110010" => Rx_data_out <= X"87"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D7.4
            WHEN "0001101101" => Rx_data_out <= X"88"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D8.4
            WHEN "1001010010" => Rx_data_out <= X"89"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D9.4
            WHEN "0101010010" => Rx_data_out <= X"8A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D10.4
            WHEN "1101000010" => Rx_data_out <= X"8B"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D11.4
            WHEN "0011010010" => Rx_data_out <= X"8C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D12.4
            WHEN "1011000010" => Rx_data_out <= X"8D"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D13.4
            WHEN "0111000010" => Rx_data_out <= X"8E"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D14.4
            WHEN "1010001101" => Rx_data_out <= X"8F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D15.4
            WHEN "1001001101" => Rx_data_out <= X"90"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D16.4
            WHEN "1000110010" => Rx_data_out <= X"91"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D17.4
            WHEN "0100110010" => Rx_data_out <= X"92"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D18.4
            WHEN "1100100010" => Rx_data_out <= X"93"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D19.4
            WHEN "0010110010" => Rx_data_out <= X"94"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D20.4
            WHEN "1010100010" => Rx_data_out <= X"95"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D21.4
            WHEN "0110100010" => Rx_data_out <= X"96"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D22.4
            WHEN "0001011101" => Rx_data_out <= X"97"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D23.4
            WHEN "0011001101" => Rx_data_out <= X"98"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D24.4
            WHEN "1001100010" => Rx_data_out <= X"99"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D25.4
            WHEN "0101100010" => Rx_data_out <= X"9A"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D26.4
            WHEN "0010011101" => Rx_data_out <= X"9B"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D27.4
            WHEN "0011100010" => Rx_data_out <= X"9C"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D28.4
            WHEN "0100011101" => Rx_data_out <= X"9D"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D29.4
            WHEN "1000011101" => Rx_data_out <= X"9E"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D30.4
            WHEN "0101001101" => Rx_data_out <= X"9F"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D31.4
            WHEN "0110001010" => Rx_data_out <= X"A0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.5
            WHEN "1000101010" => Rx_data_out <= X"A1"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.5
            WHEN "0100101010" => Rx_data_out <= X"A2"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.5
          --WHEN "1100011010" => Rx_data_out <= X"A3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.5
            WHEN "0010101010" => Rx_data_out <= X"A4"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.5
          --WHEN "1010011010" => Rx_data_out <= X"A5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.5
          --WHEN "0110011010" => Rx_data_out <= X"A6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.5
            WHEN "0001111010" => Rx_data_out <= X"A7"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.5
            WHEN "0001101010" => Rx_data_out <= X"A8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.5
          --WHEN "1001011010" => Rx_data_out <= X"A9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.5
          --WHEN "0101011010" => Rx_data_out <= X"AA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.5
          --WHEN "1101001010" => Rx_data_out <= X"AB"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.5
          --WHEN "0011011010" => Rx_data_out <= X"AC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.5
          --WHEN "1011001010" => Rx_data_out <= X"AD"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.5
          --WHEN "0111001010" => Rx_data_out <= X"AE"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.5
            WHEN "1010001010" => Rx_data_out <= X"AF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.5
            WHEN "1001001010" => Rx_data_out <= X"B0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.5
          --WHEN "1000111010" => Rx_data_out <= X"B1"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.5
          --WHEN "0100111010" => Rx_data_out <= X"B2"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.5
          --WHEN "1100101010" => Rx_data_out <= X"B3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.5
          --WHEN "0010111010" => Rx_data_out <= X"B4"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.5
          --WHEN "1010101010" => Rx_data_out <= X"B5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.5
          --WHEN "0110101010" => Rx_data_out <= X"B6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.5
            WHEN "0001011010" => Rx_data_out <= X"B7"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.5
            WHEN "0011001010" => Rx_data_out <= X"B8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.5
          --WHEN "1001101010" => Rx_data_out <= X"B9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.5
          --WHEN "0101101010" => Rx_data_out <= X"BA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.5
            WHEN "0010011010" => Rx_data_out <= X"BB"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.5
          --WHEN "0011101010" => Rx_data_out <= X"BC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.5
            WHEN "0100011010" => Rx_data_out <= X"BD"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.5
            WHEN "1000011010" => Rx_data_out <= X"BE"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.5
            WHEN "0101001010" => Rx_data_out <= X"BF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.5
            WHEN "0110000110" => Rx_data_out <= X"C0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D0.6
            WHEN "1000100110" => Rx_data_out <= X"C1"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D1.6
            WHEN "0100100110" => Rx_data_out <= X"C2"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D2.6
          --WHEN "1100010110" => Rx_data_out <= X"C3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D3.6
            WHEN "0010100110" => Rx_data_out <= X"C4"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D4.6
          --WHEN "1010010110" => Rx_data_out <= X"C5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D5.6
          --WHEN "0110010110" => Rx_data_out <= X"C6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D6.6
            WHEN "0001110110" => Rx_data_out <= X"C7"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D7.6
            WHEN "0001100110" => Rx_data_out <= X"C8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D8.6
          --WHEN "1001010110" => Rx_data_out <= X"C9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D9.6
          --WHEN "0101010110" => Rx_data_out <= X"CA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D10.6
          --WHEN "1101000110" => Rx_data_out <= X"CB"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D11.6
          --WHEN "0011010110" => Rx_data_out <= X"CC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D12.6
          --WHEN "1011000110" => Rx_data_out <= X"CD"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D13.6
          --WHEN "0111000110" => Rx_data_out <= X"CE"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D14.6
            WHEN "1010000110" => Rx_data_out <= X"CF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D15.6
            WHEN "1001000110" => Rx_data_out <= X"D0"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D16.6
          --WHEN "1000110110" => Rx_data_out <= X"D1"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D17.6
          --WHEN "0100110110" => Rx_data_out <= X"D2"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D18.6
          --WHEN "1100100110" => Rx_data_out <= X"D3"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D19.6
          --WHEN "0010110110" => Rx_data_out <= X"D4"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D20.6
          --WHEN "1010100110" => Rx_data_out <= X"D5"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D21.6
          --WHEN "0110100110" => Rx_data_out <= X"D6"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D22.6
            WHEN "0001010110" => Rx_data_out <= X"D7"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D23.6
            WHEN "0011000110" => Rx_data_out <= X"D8"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D24.6
          --WHEN "1001100110" => Rx_data_out <= X"D9"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D25.6
          --WHEN "0101100110" => Rx_data_out <= X"DA"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D26.6
            WHEN "0010010110" => Rx_data_out <= X"DB"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D27.6
          --WHEN "0011100110" => Rx_data_out <= X"DC"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D28.6
            WHEN "0100010110" => Rx_data_out <= X"DD"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D29.6
            WHEN "1000010110" => Rx_data_out <= X"DE"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D30.6
            WHEN "0101000110" => Rx_data_out <= X"DF"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D31.6
            WHEN "0110001110" => Rx_data_out <= X"E0"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D0.7
            WHEN "1000101110" => Rx_data_out <= X"E1"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D1.7
            WHEN "0100101110" => Rx_data_out <= X"E2"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D2.7
            WHEN "1100010001" => Rx_data_out <= X"E3"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D3.7
            WHEN "0010101110" => Rx_data_out <= X"E4"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D4.7
            WHEN "1010010001" => Rx_data_out <= X"E5"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D5.7
            WHEN "0110010001" => Rx_data_out <= X"E6"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D6.7
            WHEN "0001110001" => Rx_data_out <= X"E7"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D7.7
            WHEN "0001101110" => Rx_data_out <= X"E8"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   --  D8.7
            WHEN "1001010001" => Rx_data_out <= X"E9"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   --  D9.7
            WHEN "0101010001" => Rx_data_out <= X"EA"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D10.7
            WHEN "1101001000" => Rx_data_out <= X"EB"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D11.7
            WHEN "0011010001" => Rx_data_out <= X"EC"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D12.7
            WHEN "1011001000" => Rx_data_out <= X"ED"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D13.7
            WHEN "0111001000" => Rx_data_out <= X"EE"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D14.7
            WHEN "1010001110" => Rx_data_out <= X"EF"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D15.7
            WHEN "1001001110" => Rx_data_out <= X"F0"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D16.7
            WHEN "1000110001" => Rx_data_out <= X"F1"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D17.7
            WHEN "0100110001" => Rx_data_out <= X"F2"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D18.7
            WHEN "1100100001" => Rx_data_out <= X"F3"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D19.7
            WHEN "0010110001" => Rx_data_out <= X"F4"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D20.7
            WHEN "1010100001" => Rx_data_out <= X"F5"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D21.7
            WHEN "0110100001" => Rx_data_out <= X"F6"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D22.7
            WHEN "0001011110" => Rx_data_out <= X"F7"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D23.7
            WHEN "0011001110" => Rx_data_out <= X"F8"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D24.7
            WHEN "1001100001" => Rx_data_out <= X"F9"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D25.7
            WHEN "0101100001" => Rx_data_out <= X"FA"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D26.7
            WHEN "0010011110" => Rx_data_out <= X"FB"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D27.7
            WHEN "0011100001" => Rx_data_out <= X"FC"; --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- D28.7
            WHEN "0100011110" => Rx_data_out <= X"FD"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D29.7
            WHEN "1000011110" => Rx_data_out <= X"FE"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D30.7
            WHEN "0101001110" => Rx_data_out <= X"FF"; --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- D31.7

        -- K characters ------------------------------------------------------------

            WHEN "0011110100" => Rx_data_out <= X"1C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K28.0
            WHEN "0011111001" => Rx_data_out <= X"3C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.1
            WHEN "0011110101" => Rx_data_out <= X"5C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.2
            WHEN "0011110011" => Rx_data_out <= X"7C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.3
            WHEN "0011110010" => Rx_data_out <= X"9C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K28.4
            WHEN "0011111010" => Rx_data_out <= X"BC"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.5
            WHEN "0011110110" => Rx_data_out <= X"DC"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.6
            WHEN "0011111000" => Rx_data_out <= X"FC"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K28.7
            WHEN "1110101000" => Rx_data_out <= X"F7"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K23.7
            WHEN "1101101000" => Rx_data_out <= X"FB"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K27.7
            WHEN "1011101000" => Rx_data_out <= X"FD"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K29.7
            WHEN "0111101000" => Rx_data_out <= X"FE"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K30.7

            WHEN "1100001011" => Rx_data_out <= X"1C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K28.0
            WHEN "1100000110" => Rx_data_out <= X"3C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.1
            WHEN "1100001010" => Rx_data_out <= X"5C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.2
            WHEN "1100001100" => Rx_data_out <= X"7C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.3
            WHEN "1100001101" => Rx_data_out <= X"9C"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K28.4
            WHEN "1100000101" => Rx_data_out <= X"BC"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.5
            WHEN "1100001001" => Rx_data_out <= X"DC"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '1'; ELSE CurrDisp <= '0';END IF;   -- K28.6
            WHEN "1100000111" => Rx_data_out <= X"FC"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K28.7
            WHEN "0001010111" => Rx_data_out <= X"F7"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K23.7
            WHEN "0010010111" => Rx_data_out <= X"FB"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K27.7
            WHEN "0100010111" => Rx_data_out <= X"FD"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K29.7
            WHEN "1000010111" => Rx_data_out <= X"FE"; Rx_CharIsK_out <= '1';   --IF CurrDisp = '0' THEN CurrDisp <= '0'; ELSE CurrDisp <= '1';END IF;   -- K30.7

        -- Invalid character -------------------------------------------------------

            WHEN OTHERS       => Rx_data_out <= X"00"; Rx_CharIsK_out <= '1'; Rx_Char_err <= '1'; --CurrDisp <= '0';        -- error
          END CASE;
  

      END IF;
    END IF;
  END PROCESS;

----------------------------------------------------------------------------------
end Behavioral;
----------------------------------------------------------------------------------
