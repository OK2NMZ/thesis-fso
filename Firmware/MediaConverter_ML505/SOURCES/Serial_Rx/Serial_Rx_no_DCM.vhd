----------------------------------------------------------------------------------
-- Perform BO-CDR using Ccnt algorithm
-- use clock derived from MII_TxCLK (125 MHz) using CLK_GEN_Serial_Rx (156,25 MHz output clock) [! obsolete]
-- Find word boundaries (comma synchronization)
-- Decode 8b/10b
-- when not K28.5 (comma), save data to FIFO ('0' & data(7:0))
-- mark end of a correct frame using ('1' & X"FD")
-- mark end of an incorrect frame using ('1' & X"FE")
-- mark any frame containing Rx_dec_err as incorrect using ('1' & X"FE")
--
--
-- MII_TxCLK clock side:
-- if FIFO not empty, start transmitting preamble to MII interface (7x 0x55 + 1x 0x5D)
-- then read one whole frame from the FIFO and wait for 16 clock cycles of MII_TxCLK
-- 8b => 4b conversion
-- assert MII_TxD and MII_TXEN synchronously with FALLING edge of MII_TxCLK
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
LIBRARY unisim;
USE unisim.vcomponents.ALL;
USE WORK.modules_pkg.ALL;
USE WORK.parameters_pkg.ALL;
----------------------------------------------------------------------------------
ENTITY SM_Rx_no_DCM IS
	PORT (
		-- Interface Rx
		RxC : IN STD_LOGIC := '0';
		RxD : IN STD_LOGIC_VECTOR (9 DOWNTO 0);
		ALIGNED : OUT STD_LOGIC := '0';
		-- PHY MII Tx interface
		MII_TxCLK : IN STD_LOGIC := '0';
		MII_TxD : OUT STD_LOGIC_VECTOR (7 DOWNTO 0) := X"00";
		MII_TXEN : OUT STD_LOGIC := '0';
		MII_GF : OUT STD_LOGIC := '0';
		MII_BF : OUT STD_LOGIC := '0';
		LINK_IS_DOWN : OUT STD_LOGIC := '0';
		ERR_FRAMING : OUT STD_LOGIC := '0';
		ERR_RS_BAD_BLOCK : OUT STD_LOGIC := '0';
		ERR_8B10B : OUT STD_LOGIC := '0';
		ERR_ALIGN : OUT STD_LOGIC := '0';
		CHANNEL_DECODER_USED : IN STD_LOGIC := '0';
		RX_FRAMES_RECEIVED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		RX_FRAMES_BAD  : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');	
		RX_BYTES_CORRECTED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		RX_BYTES_RECEIVED : OUT STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
		-- FPGA system interface
		clk : IN STD_LOGIC := '0';
		rst : IN STD_LOGIC := '0');
END SM_Rx_no_DCM;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF SM_Rx_no_DCM IS
	----------------------------------------------------------------------------------
	component RSdecoder
		port (
		data_in: in std_logic_vector(7 downto 0);
		mark_in: in std_logic_vector(1 downto 0);
		sync: in std_logic;
		clk: in std_logic;
		sr: in std_logic;
		data_out: out std_logic_vector(7 downto 0);
		mark_out: out std_logic_vector(1 downto 0);
		blk_strt: out std_logic;
		blk_end: out std_logic;
		info_end: out std_logic;
		err_found: out std_logic;
		err_cnt: out std_logic_vector(4 downto 0);
		fail: out std_logic;
		ready: out std_logic;
		rffd: out std_logic);
	end component;
	
	
	-- clocking and reset
	SIGNAL rst_clk_Rx : STD_LOGIC;
	SIGNAL rst_MII : STD_LOGIC := '1';
	SIGNAL REC_Data : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
	-- ALIGNER and its output
	SIGNAL ALIGN_position_1 : STD_LOGIC_VECTOR(3 DOWNTO 0) := X"1";
	SIGNAL ALIGN_position_2 : STD_LOGIC_VECTOR(3 DOWNTO 0) := X"2";
	SIGNAL ALIGN_position_3 : STD_LOGIC_VECTOR(3 DOWNTO 0) := X"3";
	SIGNAL ALIGN_position_4 : STD_LOGIC_VECTOR(3 DOWNTO 0) := X"4";
	SIGNAL ALIGN_valid : STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";
	SIGNAL ALIGN_position : STD_LOGIC_VECTOR(3 DOWNTO 0) := X"0";
	SIGNAL ALIGN_reg : STD_LOGIC_VECTOR(18 DOWNTO 0) := (OTHERS => '0');
	SIGNAL ALIGN_Data : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
	SIGNAL ALIGN_Data_i : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
	SIGNAL ALIGN_DV : STD_LOGIC := '0';
	-- pipelining the module output
	SIGNAL MII_TxD_i : STD_LOGIC_VECTOR (7 DOWNTO 0) := X"00";
	SIGNAL MII_TXEN_i : STD_LOGIC := '0';
	SIGNAL MII_GF_i : STD_LOGIC := '0';
	SIGNAL MII_BF_i : STD_LOGIC := '0';
	-- 8b10b output
	SIGNAL Rx_dec_data_i : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL Rx_dec_DV_i : STD_LOGIC := '0';
	SIGNAL Rx_dec_K_i : STD_LOGIC := '0';
	SIGNAL Rx_dec_err_i : STD_LOGIC := '0';
	SIGNAL Rx_dec_err_MII : STD_LOGIC := '0';
	SIGNAL Rx_dec_data : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL Rx_dec_DV : STD_LOGIC := '0';
	SIGNAL Rx_dec_K : STD_LOGIC := '0';
	SIGNAL Rx_dec_err : STD_LOGIC := '0';
	-- FIFO input
	SIGNAL FIFO_Tx_MII_wr_en : STD_LOGIC := '0';
	SIGNAL FIFO_Tx_MII_din : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
	SIGNAL FIFO_Tx_MII_full : STD_LOGIC := '0';
	SIGNAL FIFO_Tx_MII_full_prog : STD_LOGIC := '0';
	TYPE t_st_FIFO_wr IS (st_idle, st_data_wr, st_sync, st_sync_delay);
	SIGNAL st_FIFO_wr : t_st_FIFO_wr := st_idle;
	SIGNAL st_FIFO_wr_last : t_st_FIFO_wr := st_idle;
	-- FIFO output
	SIGNAL FIFO_Tx_MII_rd_en : STD_LOGIC := '0';
	SIGNAL FIFO_Tx_MII_dout : STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
	SIGNAL FIFO_Tx_MII_empty : STD_LOGIC := '0';
	SIGNAL FIFO_Tx_MII_almost_empty : STD_LOGIC := '0';
	SIGNAL FIFO_Tx_MII_empty_prog : STD_LOGIC := '0';
	SIGNAL cnt_ALIGN : STD_LOGIC_VECTOR(15 DOWNTO 0) := (OTHERS => '0');
	-- MII interface
	TYPE t_st_MII_Tx IS (st_idle, st_drop, st_pre_data, st_data);
	SIGNAL st_MII_Tx : t_st_MII_Tx := st_idle;
	-- 8b10b constants
	CONSTANT Comma : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"BC"; -- K28.5
	CONSTANT Carrier_ext : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"F7"; -- K23.7
	CONSTANT Start_of_frame : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FB"; -- K27.7
	CONSTANT End_of_frame : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FD"; -- K29.7
	CONSTANT Error_propag : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FE"; -- K30.7
	CONSTANT Comma_P : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0011111010"; -- encoded K28.5 (P comma)
	CONSTANT Comma_N : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1100000101"; -- encoded K28.5 (N comma)
	-- auxiliary indication
	SIGNAL FLAG_K28_5 : STD_LOGIC := '0';
	SIGNAL FLAG_error : STD_LOGIC := '0';
	-- RS Decoder
	SIGNAL RS_SYNC : STD_LOGIC := '0';
	SIGNAL RS_SYNC_i : STD_LOGIC := '0';
	SIGNAL RS_SYNC_helper : STD_LOGIC := '0';
	SIGNAL RS_START_DATA : STD_LOGIC := '0';
	SIGNAL RS_END_DATA : STD_LOGIC := '0';
	SIGNAL RS_BLOCK_END : STD_LOGIC := '0';
	SIGNAL RS_BLOCK_END_MII : STD_LOGIC := '0';
	SIGNAL RS_BLOCK_ERR_COUNT : STD_LOGIC_VECTOR(4 downto 0) := (OTHERS => '0');
	SIGNAL RS_BLOCK_ERR_COUNT_MII : STD_LOGIC_VECTOR(4 downto 0) := (OTHERS => '0');
	SIGNAL RS_BAD_BLOCK : STD_LOGIC := '0';
	SIGNAL RS_BAD_BLOCK_MII : STD_LOGIC := '0';
	SIGNAL RS_BAD_BLOCK_last : STD_LOGIC := '0';
	SIGNAL RS_READY : STD_LOGIC := '0';
	SIGNAL RS_READY_FOR_START : STD_LOGIC := '0';
	SIGNAL RS_MARK_IN : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00";
	SIGNAL RS_MARK_IN_MII : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00";
	SIGNAL RS_MARK_OUT : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00";
	CONSTANT RS_MARK_ERR : STD_LOGIC_VECTOR(1 DOWNTO 0) := "11";
	CONSTANT RS_MARK_SOF : STD_LOGIC_VECTOR(1 DOWNTO 0) := "10";
	CONSTANT RS_MARK_NOP : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00";
	SIGNAL RS_DIN : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"00";
	SIGNAL RS_DOUT : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"00";
	SIGNAL RS_THIS_LEN : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
	SIGNAL CAN_START_NOW : STD_LOGIC := '0';
	SIGNAL CAN_START_NOW_SYNC : STD_LOGIC := '0';
	CONSTANT RS_LEN_ZERO : STD_LOGIC_VECTOR(10 DOWNTO 0) := (OTHERS => '0');
	TYPE t_st_RS_rd IS (st_first_len_1, st_first_len_2, st_len_1, st_len_2,
	st_len_2_wait_for_start, st_write_eof_first_len, st_payload,
	st_payload_wait_for_start);
	SIGNAL st_RS_rd : t_st_RS_rd := st_first_len_1;
	SIGNAL st_RS_rd_last : t_st_RS_rd := st_first_len_1;
	-- Link down detection
	SIGNAL LINK_IS_DOWN_CNTR : STD_LOGIC_VECTOR(31 DOWNTO 0) := (OTHERS => '0');
	
	-- Channel Coding 
	SIGNAL CHANNEL_DECODER_USED_RxC :  STD_LOGIC := '0';
	
	-- Statistics
	SIGNAL RX_FRAMES_RECEIVED_i :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
	SIGNAL RX_FRAMES_BAD_i  :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
	SIGNAL RX_BYTES_CORRECTED_i :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
	SIGNAL RX_BYTES_RECEIVED_i :  STD_LOGIC_VECTOR(63 downto 0) := (OTHERS => '0');
	
	-- CRC to have a secured information about the length
	SIGNAL LENGTH_CRC_IN :  STD_LOGIC_VECTOR(11 downto 0) := (OTHERS => '0');
	SIGNAL LENGTH_CRC_OUT :  STD_LOGIC_VECTOR(3 downto 0) := (OTHERS => '0');
	SIGNAL LENGTH_CRC_RECEIVED : STD_LOGIC_VECTOR(3 downto 0) := (OTHERS => '0');
	SIGNAL LENGTH_CRC_EN :  STD_LOGIC := '0';
	
	-- ERROR signals
	SIGNAL ERR_FRAMING_b : boolean := false;
	SIGNAL ERR_RS_BAD_BLOCK_b : boolean := false;
	SIGNAL ERR_8B10B_b : boolean := false;
	SIGNAL ERR_ALIGN_i : STD_LOGIC := '0';
	SIGNAL ERR_ALIGN_ii : STD_LOGIC := '0';
	
function bool_to_logic(L: BOOLEAN) return STD_LOGIC is
begin
	if L then
		return('1');
	else
		return('0');
	end if;
end function bool_to_logic;


	----------------------------------------------------------------------------------
BEGIN
	----------------------------------------------------------------------------------
	----------------------------------------------------------------------------------
	-- clocking and reset
	----------------------------------------------------------------------------------
	-- get rst into RxC clock domain
	sync_rst_clk_Rx : ENTITY WORK.SignalSynchronizer
	PORT MAP(sig_clk1(0) => rst, clk1 => clk, sig_clk2(0) => rst_clk_Rx, clk2 => RxC, arst => rst);
	sync_CHANNEL_DECODER_USED : ENTITY WORK.SignalSynchronizer
	PORT MAP(sig_clk1(0) => CHANNEL_DECODER_USED, clk1 => clk, sig_clk2(0) => CHANNEL_DECODER_USED_RxC, clk2 => RxC, arst => rst);
	REC_Data <= RxD;
	----------------------------------------------------------------------------------
	-- Decode 8b/10b
	----------------------------------------------------------------------------------
	Decoder_8B10B_i : ENTITY WORK.Decoder_8B10B
		PORT MAP(
			Rx_clk_in       => RxC,
			Rx_data_in      => ALIGN_Data,
			Rx_DV_in        => ALIGN_DV,
			Rx_data_out     => Rx_dec_data_i,
			Rx_DV_out       => Rx_dec_DV_i,
			Rx_CharIsK_out  => Rx_dec_K_i,
			Rx_Char_err     => Rx_dec_err_i
		);
	-- pipeline
	PROCESS (RxC) BEGIN
	IF rising_edge(RxC) THEN
		Rx_dec_data <= Rx_dec_data_i;
		Rx_dec_DV <= Rx_dec_DV_i;
		Rx_dec_K <= Rx_dec_K_i;
		Rx_dec_err <= Rx_dec_err_i;
	END IF;
END PROCESS;
----------------------------------------------------------------------------------
-- start saving data when SOF is received; format: ('0' & data(7:0))
-- mark start of a frame using ('1' & X"FB")
-- mark end of a correct frame using ('1' & X"FD")
-- mark end of an incorrect frame using ('1' & X"FE")
-- mark any frame containing Rx_dec_err as incorrect using ('1' & X"FE")
----------------------------------------------------------------------------------
FIFO_wr : PROCESS(RxC) BEGIN
	IF rising_edge(RxC) THEN
		-- synchronous reset
		IF rst_clk_Rx = '1' THEN
			FIFO_Tx_MII_wr_en <= '0';
			st_FIFO_wr <= st_idle;
			st_RS_rd <= st_first_len_1;
			RS_MARK_IN <= RS_MARK_NOP;
			RS_SYNC_i <= '0';
			RS_DIN <= Rx_dec_data;
			FIFO_Tx_MII_din <= '0' & RS_DOUT;
			FIFO_Tx_MII_wr_en <= '0';
			CAN_START_NOW <= '0';
			LENGTH_CRC_EN <= '0';
			ALIGNED <= '0';
		ELSE
			IF CHANNEL_DECODER_USED_RxC = '1' THEN
				RS_MARK_IN <= RS_MARK_NOP;
				RS_SYNC_i <= '0';
				RS_DIN <= Rx_dec_data;
				FIFO_Tx_MII_din <= '0' & RS_DOUT;
				FIFO_Tx_MII_wr_en <= '0';
				CAN_START_NOW <= '0';
				st_RS_rd_last <= st_RS_rd;
				LENGTH_CRC_EN <= '0';
				
				IF RS_MARK_OUT = RS_MARK_SOF THEN -- force reset RS read FSM
					RS_THIS_LEN(6 DOWNTO 0) <= RS_DOUT(6 DOWNTO 0);
					st_RS_rd <= st_first_len_2;
				ELSIF RS_MARK_OUT = RS_MARK_ERR OR
						RS_BAD_BLOCK = '1' OR
						(LENGTH_CRC_EN = '1' AND LENGTH_CRC_OUT /= LENGTH_CRC_RECEIVED ) THEN -- error detected
					st_RS_rd <= st_first_len_1;
					CASE st_RS_rd IS
						WHEN st_write_eof_first_len | st_len_1 =>
							FIFO_Tx_MII_din <= '1' & End_of_frame;
							FIFO_Tx_MII_wr_en <= '1';
							
						WHEN st_payload | st_payload_wait_for_start =>
							FIFO_Tx_MII_din <= '1' & Error_propag;
							FIFO_Tx_MII_wr_en <= '1';

						-- st_first_len_2, st_len_2, st_len_2_wait_for_start
						WHEN OTHERS =>
					END CASE;				
				ELSE
					CASE st_RS_rd IS
						WHEN st_first_len_1 =>
							-- ignore RS_START_DATA pulses, which start a new fresh frame,
							-- from a state, where no frame is being currently sent and
							-- RS_MARK_OUT pulse was not received along with RS_START_DATA
							IF RS_START_DATA = '1' AND st_RS_rd_last /= st_first_len_1 THEN
								RS_THIS_LEN(6 DOWNTO 0) <= RS_DOUT(6 DOWNTO 0);
								st_RS_rd <= st_first_len_2;
							END IF;
						WHEN st_first_len_2 =>
							FIFO_Tx_MII_din <= '1' & Start_of_frame;
							FIFO_Tx_MII_wr_en <= '1';
							RS_THIS_LEN(10 DOWNTO 7) <= RS_DOUT(3 DOWNTO 0);
							LENGTH_CRC_RECEIVED <= RS_DOUT(7 DOWNTO 4);
							LENGTH_CRC_EN <= '1';
							st_RS_rd <= st_payload;
						WHEN st_len_1 =>
							FIFO_Tx_MII_din <= '1' & End_of_frame;
							FIFO_Tx_MII_wr_en <= '1';
							RS_THIS_LEN(6 DOWNTO 0) <= RS_DOUT(6 DOWNTO 0);
							IF RS_DOUT = X"00" THEN
									st_RS_rd <= st_first_len_1; --padding incoming...
							ELSIF RS_END_DATA = '1' THEN
								st_RS_rd <= st_len_2_wait_for_start;
							ELSE
								st_RS_rd <= st_len_2;
							END IF;
						WHEN st_len_2 =>
							FIFO_Tx_MII_din <= '1' & Start_of_frame;
							FIFO_Tx_MII_wr_en <= '1';
							RS_THIS_LEN(10 DOWNTO 7) <= RS_DOUT(3 DOWNTO 0);
							LENGTH_CRC_RECEIVED <= RS_DOUT(7 DOWNTO 4);
							LENGTH_CRC_EN <= '1';
							IF RS_END_DATA = '1' THEN
								st_RS_rd <= st_payload_wait_for_start;
							ELSE
								st_RS_rd <= st_payload;
							END IF;
						WHEN st_len_2_wait_for_start =>
							IF RS_START_DATA = '1' THEN
								FIFO_Tx_MII_din <= '1' & Start_of_frame;
								FIFO_Tx_MII_wr_en <= '1';
								RS_THIS_LEN(10 DOWNTO 7) <= RS_DOUT(3 DOWNTO 0);
								LENGTH_CRC_RECEIVED <= RS_DOUT(7 DOWNTO 4);
								LENGTH_CRC_EN <= '1';
								st_RS_rd <= st_payload;
							END IF;
						WHEN st_write_eof_first_len =>
							FIFO_Tx_MII_din <= '1' & End_of_frame;
							FIFO_Tx_MII_wr_en <= '1';
							st_RS_rd <= st_first_len_1;
						WHEN st_payload =>
							RS_THIS_LEN <= RS_THIS_LEN - "1";
							FIFO_Tx_MII_wr_en <= '1';
							IF (RS_THIS_LEN - "1") = RS_LEN_ZERO THEN
								CAN_START_NOW <= '1';
								IF RS_END_DATA = '1' THEN
									st_RS_rd <= st_write_eof_first_len;
								ELSE
									st_RS_rd <= st_len_1;
								END IF;
							ELSIF RS_END_DATA = '1' THEN
								st_RS_rd <= st_payload_wait_for_start;
							END IF;
						WHEN st_payload_wait_for_start =>
							IF RS_START_DATA = '1' THEN
								RS_THIS_LEN <= RS_THIS_LEN - "1";
								FIFO_Tx_MII_wr_en <= '1';
								IF (RS_THIS_LEN - "1") = RS_LEN_ZERO THEN
										CAN_START_NOW <= '1';
									st_RS_rd <= st_len_1;
								ELSE
									st_RS_rd <= st_payload;
								END IF;
							END IF;
						WHEN OTHERS =>
							st_RS_rd <= st_first_len_1;
					END CASE;
				END IF;
				
				IF Rx_dec_DV = '1' THEN
					-- for RS_SYNC multiplexing purposes
					-- SYNC must be only one clock cycle long!
					st_FIFO_wr_last <= st_FIFO_wr;
					CASE st_FIFO_wr IS
						WHEN st_idle =>
							IF Rx_dec_K = '1' AND
							 Rx_dec_data = Start_of_frame AND
							 FIFO_Tx_MII_full_prog = '0' AND
							 RS_READY = '1'
							 THEN -- Start of frame
							 st_FIFO_wr <= st_sync;
						 END IF;
						 WHEN st_sync =>
							 RS_SYNC_i <= '1';
							 RS_MARK_IN <= RS_MARK_SOF;
							 st_FIFO_wr <= st_sync_delay;
						 WHEN st_sync_delay =>
							 st_FIFO_wr <= st_data_wr;
						 WHEN st_data_wr =>
							 IF Rx_dec_K = '1' THEN -- EOF most likely, keep it simple.
									st_FIFO_wr <= st_idle;
								END IF;
						WHEN OTHERS =>
							st_FIFO_wr <= st_idle;
					END CASE;
				ELSE -- Rx_dec_DV gone to '0', data coruption
					st_FIFO_wr_last <= st_idle;
					CASE st_FIFO_wr IS
						 WHEN st_sync =>
							 -- abort sending start of new frame
							 st_FIFO_wr <= st_idle;
						 WHEN st_sync_delay =>
						    -- start of new frame sent into RS decoder
							 -- along with the first byte of length
							 RS_MARK_IN <= RS_MARK_ERR;
							 st_FIFO_wr <= st_idle;
						 WHEN st_data_wr =>
							 RS_MARK_IN <= RS_MARK_ERR;
							 st_FIFO_wr <= st_idle;
						WHEN OTHERS =>
							st_FIFO_wr <= st_idle;
					END CASE;				
				END IF;
				
			ELSE -- NO DECODER
				IF Rx_dec_DV = '1' THEN
					-- default assignment
					FIFO_Tx_MII_din <= Rx_dec_K & Rx_dec_data;
					FIFO_Tx_MII_wr_en <= '0';
					CASE st_FIFO_wr IS
						WHEN st_idle =>
							IF Rx_dec_K = '1' AND
							 Rx_dec_data = Start_of_frame AND
							 FIFO_Tx_MII_full_prog = '0'
							 THEN -- Start of frame
								st_FIFO_wr <= st_data_wr;
								FIFO_Tx_MII_wr_en <= '1';
								FIFO_Tx_MII_din <= '1' & Start_of_frame;
							ELSE
								FIFO_Tx_MII_wr_en <= '0';
							END IF;
						 WHEN st_data_wr =>
							 FIFO_Tx_MII_wr_en <= '1';
							 IF Rx_dec_K = '1' THEN
									st_FIFO_wr <= st_idle;
									IF Rx_dec_data /= End_of_frame THEN -- correct end of frame?
										FIFO_Tx_MII_din <= '1' & Error_propag; -- mark frame in FIFO as incorrect
									END IF;
								END IF;
						WHEN OTHERS =>
							st_FIFO_wr <= st_idle;
					END CASE;
				END IF;
			END IF;	
   ----------------------------------------------------------------------------------
	-- Find word boundaries (comma synchronization)
	----------------------------------------------------------------------------------
			ALIGN_reg <= REC_Data & ALIGN_reg(18 DOWNTO 10);
			ALIGN_position_1 <= X"F"; -- no COMMA match
			IF ALIGN_reg(9 DOWNTO 0) = Comma_P THEN
				ALIGN_position_1 <= X"0";
			END IF;
			IF ALIGN_reg(10 DOWNTO 1) = Comma_P THEN
				ALIGN_position_1 <= X"1";
			END IF;
			IF ALIGN_reg(11 DOWNTO 2) = Comma_P THEN
				ALIGN_position_1 <= X"2";
			END IF;
			IF ALIGN_reg(12 DOWNTO 3) = Comma_P THEN
				ALIGN_position_1 <= X"3";
			END IF;
			IF ALIGN_reg(13 DOWNTO 4) = Comma_P THEN
				ALIGN_position_1 <= X"4";
			END IF;
			IF ALIGN_reg(14 DOWNTO 5) = Comma_P THEN
				ALIGN_position_1 <= X"5";
			END IF;
			IF ALIGN_reg(15 DOWNTO 6) = Comma_P THEN
				ALIGN_position_1 <= X"6";
			END IF;
			IF ALIGN_reg(16 DOWNTO 7) = Comma_P THEN
				ALIGN_position_1 <= X"7";
			END IF;
			IF ALIGN_reg(17 DOWNTO 8) = Comma_P THEN
				ALIGN_position_1 <= X"8";
			END IF;
			IF ALIGN_reg(18 DOWNTO 9) = Comma_P THEN
				ALIGN_position_1 <= X"9";
			END IF;
			IF ALIGN_reg(9 DOWNTO 0) = Comma_N THEN
				ALIGN_position_1 <= X"0";
			END IF;
			IF ALIGN_reg(10 DOWNTO 1) = Comma_N THEN
				ALIGN_position_1 <= X"1";
			END IF;
			IF ALIGN_reg(11 DOWNTO 2) = Comma_N THEN
				ALIGN_position_1 <= X"2";
			END IF;
			IF ALIGN_reg(12 DOWNTO 3) = Comma_N THEN
				ALIGN_position_1 <= X"3";
			END IF;
			IF ALIGN_reg(13 DOWNTO 4) = Comma_N THEN
				ALIGN_position_1 <= X"4";
			END IF;
			IF ALIGN_reg(14 DOWNTO 5) = Comma_N THEN
				ALIGN_position_1 <= X"5";
			END IF;
			IF ALIGN_reg(15 DOWNTO 6) = Comma_N THEN
				ALIGN_position_1 <= X"6";
			END IF;
			IF ALIGN_reg(16 DOWNTO 7) = Comma_N THEN
				ALIGN_position_1 <= X"7";
			END IF;
			IF ALIGN_reg(17 DOWNTO 8) = Comma_N THEN
				ALIGN_position_1 <= X"8";
			END IF;
			IF ALIGN_reg(18 DOWNTO 9) = Comma_N THEN
				ALIGN_position_1 <= X"9";
			END IF;
			--------------------------------------------------------------------------
			-- only 4 consecutive valid align detections may change the position of alignment
			ALIGN_position_2 <= ALIGN_position_1;
			ALIGN_position_3 <= ALIGN_position_2;
			ALIGN_position_4 <= ALIGN_position_3;
			
			IF ALIGN_position_1 = ALIGN_position_2 THEN
				ALIGN_valid(0) <= '1';
			ELSE ALIGN_valid(0) <= '0';
			END IF;
			IF ALIGN_position_1 = ALIGN_position_3 THEN
				ALIGN_valid(1) <= '1';
			ELSE ALIGN_valid(1) <= '0';
			END IF;
			IF ALIGN_position_1 = ALIGN_position_4 THEN
				ALIGN_valid(2) <= '1';
			ELSE ALIGN_valid(2) <= '0';
			END IF;
			
			IF ALIGN_position_1 = X"F" THEN
					ALIGN_valid <= "000";
			END IF;
			IF ALIGN_valid = "111" THEN
				ALIGN_DV <= '1';
				ERR_ALIGN_i <= '0';
				ALIGNED <= '1';
				cnt_ALIGN <= (OTHERS => '0');
				ALIGN_position <= ALIGN_position_4;
			ELSIF ALIGN_valid = "000" THEN
				cnt_ALIGN <= cnt_ALIGN + '1';
				IF st_FIFO_wr /= st_data_wr THEN
					IF cnt_ALIGN >= X"0040" THEN --! tune this
						ERR_ALIGN_i <= '1';
						st_FIFO_wr <= st_idle;
						ALIGN_DV <= '0';
						IF cnt_ALIGN >= X"0080" THEN --! tune this
							ALIGNED <= '0';
						END IF;
					END IF;
				ELSE
					IF cnt_ALIGN >= X"8000" THEN --! tune this
						ERR_ALIGN_i <= '1';
						st_FIFO_wr <= st_idle;
						ALIGN_DV <= '0';
						IF cnt_ALIGN >= X"F000" THEN --! tune this
							ALIGNED <= '0';
						END IF;
					END IF;
				END IF;
			END IF;
			--------------------------------------------------------------------------
			CASE ALIGN_position IS
				WHEN X"0" => ALIGN_Data_i <= ALIGN_reg(9 DOWNTO 0);
				WHEN X"1" => ALIGN_Data_i <= ALIGN_reg(10 DOWNTO 1);
				WHEN X"2" => ALIGN_Data_i <= ALIGN_reg(11 DOWNTO 2);
				WHEN X"3" => ALIGN_Data_i <= ALIGN_reg(12 DOWNTO 3);
				WHEN X"4" => ALIGN_Data_i <= ALIGN_reg(13 DOWNTO 4);
				WHEN X"5" => ALIGN_Data_i <= ALIGN_reg(14 DOWNTO 5);
				WHEN X"6" => ALIGN_Data_i <= ALIGN_reg(15 DOWNTO 6);
				WHEN X"7" => ALIGN_Data_i <= ALIGN_reg(16 DOWNTO 7);
				WHEN X"8" => ALIGN_Data_i <= ALIGN_reg(17 DOWNTO 8);
				WHEN OTHERS => ALIGN_Data_i <= ALIGN_reg(18 DOWNTO 9);
			END CASE;
			ALIGN_Data <= ALIGN_Data_i; -- pipelining
		--------------------------------------------------------------------------
		--------------------------------------------------------------------------
		END IF;
	END IF;
END PROCESS FIFO_wr;
--==============================================================================
-- Clock Domain Boundary
--==============================================================================
-- FIFO: Rx => MII_Tx
FIFO_Tx_MII_i : ENTITY WORK.FIFO_Tx_MII
	PORT MAP(
		rst           => rst_clk_Rx,
		wr_clk        => RxC,
		din           => FIFO_Tx_MII_din,
		wr_en         => FIFO_Tx_MII_wr_en,
		full          => FIFO_Tx_MII_full,
		prog_full     => FIFO_Tx_MII_full_prog,
		rd_clk        => MII_TxCLK,
		rd_en         => FIFO_Tx_MII_rd_en,
		dout          => FIFO_Tx_MII_dout,
		empty         => FIFO_Tx_MII_empty,
		almost_empty  => FIFO_Tx_MII_almost_empty,
		prog_empty    => FIFO_Tx_MII_empty_prog
	);
--==============================================================================
-- Clock Domain Boundary
--==============================================================================
----------------------------------------------------------------------------------
-- MII clock management
--IBUFG_MII_TxCLK : IBUFG PORT MAP (O => MII_TxCLK, I => MII_TxCLK);
-- get rst into MII_TxCLK clock domain
sync_rst_MII : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => rst, clk1 => clk, clk2 => MII_TxCLK, sig_clk2(0) => rst_MII, arst => rst);
sync_CAN_START_NOW : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => CAN_START_NOW, clk1 => clk, clk2 => MII_TxCLK, sig_clk2(0) => CAN_START_NOW_SYNC, arst => rst);
----------------------------------------------------------------------------------
-- MII_TxCLK clock side:
-- if FIFO not empty, start transmitting preamble to MII interface (7x 0x55 + 1x 0x5D)
-- then read one whole frame from the FIFO and wait for 16 clock cycles of MII_TxCLK
-- 8b => 4b conversion
-- assert MII_TxD and MII_TXEN synchronously with FALLING edge of MII_TxCLK
-- The MII_TxCLK_Tx clock is 180 deg shifted to the MII_TxCLK so the rising edge is used!!!
--
-- During the preamble the data is buffered in FIFO. The delay is sufficient
-- to cover any clock difference and avoid FIFO underflow.
----------------------------------------------------------------------------------
--
-- 01234567
-- Preamble: 01010101 01010101 01010101 01010101 01010101 01010101 01010101 11010101
-- 0x55 0x55 0x55 0x55 0x55 0x55 0x55 0xD5
--
-- L H L H L H L H L H L H L H L H L H
-- TxD(0) X 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 D0 D4
-- TxD(1) X 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 D1 D5
-- TxD(2) X 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 D2 D6
-- TxD(3) X 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 D3 D7
-- TxDV 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
--
--
----------------------------------------------------------------------------------
PROCESS (MII_TxCLK) BEGIN
IF rising_edge(MII_TxCLK) THEN
	IF rst_MII = '1' THEN
		FIFO_Tx_MII_rd_en <= '0';
		st_MII_Tx <= st_idle;
		MII_TXEN_i <= '0';
		MII_GF_i <= '0';
		MII_BF_i <= '0';
		RX_FRAMES_BAD_i <= (OTHERS => '0');
		RX_FRAMES_RECEIVED_i <= (OTHERS => '0');
		RX_BYTES_RECEIVED_i <= (OTHERS => '0');
	ELSE
	
		RX_FRAMES_BAD <= RX_FRAMES_BAD_i;
		RX_FRAMES_RECEIVED <= RX_FRAMES_RECEIVED_i;
		RX_BYTES_RECEIVED <= RX_BYTES_RECEIVED_i;
	
		IF MII_TXEN_i = '1' THEN
			RX_BYTES_RECEIVED_i <= RX_BYTES_RECEIVED_i + "1";
		END IF;
	
		-- default assignment
		FIFO_Tx_MII_rd_en <= '0';
		MII_TXEN_i <= '0';
		MII_GF_i <= '0';
		MII_BF_i <= '0';
		CASE st_MII_Tx IS
			--------------------------------------------------------------------------
			-- wait for data in FIFO and SOF delimiter
			WHEN st_idle =>
				IF FIFO_Tx_MII_empty_prog = '0' OR
				 (CAN_START_NOW_SYNC = '1' AND FIFO_Tx_MII_empty = '0') THEN -- data in FIFO?
					FIFO_Tx_MII_rd_en <= '1'; -- get next byte
					IF FIFO_Tx_MII_dout = '1' & Start_of_frame THEN
						st_MII_Tx <= st_pre_data;
					ELSE
						st_MII_Tx <= st_drop;
					END IF;
				END IF;
			WHEN st_drop =>
				st_MII_Tx <= st_idle;
			WHEN st_pre_data =>
				st_MII_Tx <= st_data;
				FIFO_Tx_MII_rd_en <= '1';
				--------------------------------------------------------------------------
				-- Data
			WHEN st_data => MII_TXEN_i <= '1';
				MII_TxD_i <= FIFO_Tx_MII_dout(7 DOWNTO 0);
				IF FIFO_Tx_MII_dout(8) = '1' OR FIFO_Tx_MII_almost_empty = '1' THEN
					RX_FRAMES_RECEIVED_i <= RX_FRAMES_RECEIVED_i + "1";
					IF FIFO_Tx_MII_dout(7 DOWNTO 0) = End_of_frame THEN
						MII_GF_i <= '1';
					ELSE
						RX_FRAMES_BAD_i <= RX_FRAMES_BAD_i + "1";
						MII_BF_i <= '1';
					END IF;
					st_MII_Tx <= st_idle;
					MII_TXEN_i <= '0';
				ELSE
					FIFO_Tx_MII_rd_en <= '1';
				END IF;
			WHEN OTHERS => st_MII_Tx <= st_idle;
		END CASE;
	END IF;
END IF;
END PROCESS;
--pipelining
PROCESS (MII_TxCLK) BEGIN
IF rising_edge(MII_TxCLK) THEN
	MII_TxD <= MII_TxD_i;
	MII_TXEN <= MII_TXEN_i;
	MII_GF <= MII_GF_i;
	MII_BF <= MII_BF_i;
END IF;
END PROCESS;
--=====================RS Decoder ===========================
RS_SYNC <= RS_READY_FOR_START WHEN
           (st_FIFO_wr = st_data_wr) AND
			  (st_FIFO_wr_last /= st_sync_delay) AND
			  (Rx_dec_K = '0')
           ELSE RS_SYNC_i;
			  
RSdecoder_i : RSdecoder
PORT MAP(
	mark_out   => RS_MARK_OUT,
	mark_in    => RS_MARK_IN,
	data_in    => RS_DIN,
	sync       => RS_SYNC,
	clk        => RxC,
	sr         => rst_clk_Rx,
	data_out   => RS_DOUT,
	blk_strt   => RS_START_DATA,
	blk_end    => RS_BLOCK_END,
	info_end   => RS_END_DATA,
	err_found  => OPEN,
	err_cnt    => RS_BLOCK_ERR_COUNT,
	fail       => RS_BAD_BLOCK,
	ready      => RS_READY,
	rffd       => RS_READY_FOR_START
);

crc_12to4_i : entity WORK.crc_12to4_single
PORT MAP(
	data_in => LENGTH_CRC_IN,
	crc_out => LENGTH_CRC_OUT
);
LENGTH_CRC_IN <= RS_THIS_LEN(10 downto 7) & '1' & RS_THIS_LEN(6 downto 0);


--=================== SYNCHRONIZERS ======================
sync_Rx_dec_err : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => Rx_dec_err, clk1 => RxC,  clk2 => MII_TxCLK, sig_clk2(0) => Rx_dec_err_MII, arst => rst);

sync_RS_BAD_BLOCK : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => RS_BAD_BLOCK, clk1 => RxC,  clk2 => MII_TxCLK, sig_clk2(0) => RS_BAD_BLOCK_MII, arst => rst);

sync_RS_BLOCK_END : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => RS_BLOCK_END, clk1 => RxC,  clk2 => MII_TxCLK, sig_clk2(0) => RS_BLOCK_END_MII, arst => rst);

sync_RS_BLOCK_ERR_COUNT : ENTITY WORK.SignalSynchronizer
GENERIC MAP(DATA_WIDTH => 5)
PORT MAP(sig_clk1 => RS_BLOCK_ERR_COUNT, clk1 => RxC,  clk2 => MII_TxCLK, sig_clk2 => RS_BLOCK_ERR_COUNT_MII, arst => rst); 

sync_RS_MARK_IN : ENTITY WORK.SignalSynchronizer
GENERIC MAP(DATA_WIDTH => 2)
PORT MAP(sig_clk1 => RS_MARK_IN, clk1 => RxC,  clk2 => MII_TxCLK, sig_clk2 => RS_MARK_IN_MII, arst => rst); 

--=================== FIXED BYTES COUNTER ======================
PROCESS (MII_TxCLK) BEGIN
IF rising_Edge(MII_TxCLK) THEN
IF rst_MII = '1' THEN
		RX_BYTES_CORRECTED_i <= (OTHERS => '0');
	ELSE
		RX_BYTES_CORRECTED <= RX_BYTES_CORRECTED_i;
		IF RS_BAD_BLOCK_MII = '0' THEN -- we can rely on err_cnt
			IF RS_BLOCK_END_MII = '1' THEN
				RX_BYTES_CORRECTED_i <= RX_BYTES_CORRECTED_i + RS_BLOCK_ERR_COUNT_MII;
			END IF;
		END IF;
	END IF;
END IF;
END PROCESS;

--===================LINK DOWN DETECTION ======================
ERR_FRAMING_b <= RS_MARK_IN_MII = RS_MARK_ERR;
ERR_RS_BAD_BLOCK_b <= (RS_BAD_BLOCK_MII /= RS_BAD_BLOCK_last AND RS_BAD_BLOCK_MII = '1');
ERR_8B10B_b <= Rx_dec_err_MII = '1';

ERR_FRAMING <= bool_to_logic(ERR_FRAMING_b);
ERR_RS_BAD_BLOCK <= bool_to_logic(ERR_RS_BAD_BLOCK_b);
ERR_8B10B <= bool_to_logic(ERR_8B10B_b);


sync_ERR_ALIGN : ENTITY WORK.SignalSynchronizer
PORT MAP(sig_clk1(0) => ERR_ALIGN_i, clk1 => RxC,  clk2 => MII_TxCLK, sig_clk2(0) => ERR_ALIGN_ii, arst => rst);
ERR_ALIGN <= ERR_ALIGN_ii;

PROCESS (MII_TxCLK) BEGIN
IF rising_edge(MII_TxCLK) THEN
	RS_BAD_BLOCK_last <= RS_BAD_BLOCK_MII;
	LINK_IS_DOWN <= '1';
	IF ERR_8B10B_b OR ERR_ALIGN_ii = '1' OR
		((ERR_RS_BAD_BLOCK_b OR ERR_FRAMING_b) AND CHANNEL_DECODER_USED = '1') THEN
		LINK_IS_DOWN_CNTR <= X"00000100"; --! change this to change the hysteresis!
	ELSE
		IF LINK_IS_DOWN_CNTR = X"00000000" THEN
				LINK_IS_DOWN <= '0';
		ELSE
			LINK_IS_DOWN_CNTR <= LINK_IS_DOWN_CNTR - "1";
		END IF;
	END IF;
END IF;
END PROCESS;
----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------
