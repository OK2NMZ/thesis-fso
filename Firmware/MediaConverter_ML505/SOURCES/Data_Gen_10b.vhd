----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
----------------------------------------------------------------------------------
ENTITY Data_Gen_10b IS
  PORT(
    clk                     : IN    STD_LOGIC;
    Seq_sel                 : IN    STD_LOGIC_VECTOR( 7 DOWNTO 0);
    data_out_10b            : OUT   STD_LOGIC_VECTOR( 9 DOWNTO 0));
END Data_Gen_10b;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF Data_Gen_10b IS
----------------------------------------------------------------------------------

  COMPONENT LFSR_23_Tx_10b
  PORT(
    clk                     : IN  STD_LOGIC;
    Data_out                : OUT STD_LOGIC_VECTOR( 9 DOWNTO 0));       -- transmit LSB first, MSB last
  END COMPONENT;

  ------------------------------------------------------------------------------

  SIGNAL cnt_gen_60k        : UNSIGNED(15 DOWNTO 0) := (OTHERS => '0');
  SIGNAL data_out_10b_60k   : STD_LOGIC_VECTOR( 9 DOWNTO 0) := (OTHERS => '0');
  SIGNAL data_out_PRBS      : STD_LOGIC_VECTOR( 9 DOWNTO 0) := (OTHERS => '0');

----------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------------

  PROCESS (clk) BEGIN
    IF rising_edge(clk) THEN
      CASE Seq_sel IS
        WHEN X"00"      =>  data_out_10b <= (OTHERS => '0');
        WHEN X"01"      =>  data_out_10b <= (OTHERS => '1');
        WHEN X"02"      =>  data_out_10b <= "1010101010";
        WHEN X"03"      =>  data_out_10b <= data_out_10b_60k;
        WHEN X"04"      =>  data_out_10b <= data_out_PRBS;
        WHEN X"05"      =>  data_out_10b <= NOT data_out_PRBS;
        WHEN X"06"      =>  data_out_10b <= "0000011111";

        WHEN OTHERS     =>  data_out_10b <= (OTHERS => '0');
      END CASE;
    END IF;
  END PROCESS;

  --------------------------------------------------------------------------------
  -- 60 kHz generator
  --------------------------------------------------------------------------------

  PROCESS (clk) BEGIN
    IF rising_edge(clk) THEN
      cnt_gen_60k <= cnt_gen_60k + 1;
      IF cnt_gen_60k = TO_UNSIGNED(1041, 16) THEN
        data_out_10b_60k <= (OTHERS => '1');
      END IF;
      IF cnt_gen_60k = TO_UNSIGNED(2083, 16) THEN
        data_out_10b_60k <= (OTHERS => '0');
        cnt_gen_60k      <= (OTHERS => '0');
      END IF;
    END IF;
  END PROCESS;

  --------------------------------------------------------------------------------
  -- PRBS_23(23,18) generator
  --------------------------------------------------------------------------------

  LFSR_23_Tx_10b_i : LFSR_23_Tx_10b
  PORT MAP(
    clk             => clk,
    Data_out        => data_out_PRBS);

----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------
