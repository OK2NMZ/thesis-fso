library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;



entity BRAM_Controller is
    Port ( BRAM_WR_DATA_OUT  : out  STD_LOGIC_VECTOR(0 to 31);
           BRAM_WR_ADDR_OUT  : out  STD_LOGIC_VECTOR(0 to 31);
           BRAM_WR_EN_OUT    : out  STD_LOGIC;
           BRAM_WR_WEN_OUT   : out  STD_LOGIC_VECTOR(0 to 3);
           BRAM_WR_DATA_LEN_OUT : out STD_LOGIC_VECTOR(10 downto 0);

           BRAM_RD_DATA_IN     : in   STD_LOGIC_VECTOR(0 to 31);
           BRAM_RD_ADDR_OUT    : out  STD_LOGIC_VECTOR(0 to 31);
           BRAM_RD_EN_OUT      : out  STD_LOGIC;
           BRAM_RD_DATA_LEN_IN : in STD_LOGIC_VECTOR(10 downto 0);

           UBLAZE_WR_ACK_OUT : out  STD_LOGIC;
           UBLAZE_WR_ACK_IN  : in   STD_LOGIC := '0';

           UBLAZE_RD_ACK_OUT : out  STD_LOGIC;
           UBLAZE_RD_ACK_IN  : in   STD_LOGIC := '0';

           FC_DATA_IN      : in   STD_LOGIC_VECTOR (8 downto 0) := (OTHERS => '0');
           FC_RD_EN_OUT    : out  STD_LOGIC := '0';
           FC_EMPTY        : in   STD_LOGIC := '0';

           FC_DATA_OUT     : out STD_LOGIC_VECTOR (8 downto 0) := (OTHERS => '0');
           FC_WR_EN_OUT    : out STD_LOGIC := '0';
           FC_PROG_FULL    : in  STD_LOGIC := '0';

			  SYS_RST         : in   STD_LOGIC := '0';
           CLK             : in   STD_LOGIC);
end BRAM_Controller;

architecture Behavioral of BRAM_Controller is

TYPE   BRAM_WRITE_STATE_T   IS (BW_IDLE, BW_1, BW_2, BW_3, BW_4, BW_END, BW_ACK, BW_ACK_DOWN);
TYPE   BRAM_READ_STATE_T    IS (BR_IDLE, BR_WAIT_ADDR, BR_FETCH_DATA, BR_1, BR_2, BR_3, BR_4, BR_END, BR_ACK, BR_ACK_DOWN);

SIGNAL clk_sys             : STD_LOGIC;
 
SIGNAL DATA_FROM_FC        : STD_LOGIC_VECTOR(31 downto 0);
SIGNAL DATA_TO_FC          : STD_LOGIC_VECTOR(31 downto 0);
 
SIGNAL ADDRESS_WR          : UNSIGNED(0 to 9) := "0000000000";
SIGNAL ADDRESS_RD          : UNSIGNED(0 to 9) := "0000000000";
SIGNAL FC_DATA_IN_BE       : STD_LOGIC_VECTOR(0 to 7);
SIGNAL BRAM_DATA_OUT_BE    : STD_LOGIC_VECTOR(0 to 31);
SIGNAL BRAM_DATA_IN_i      : STD_LOGIC_VECTOR(0 to 31);
SIGNAL BRAM_WEN_OUT_i      : STD_LOGIC_VECTOR(0 to 3);
 
SIGNAL BRAM_ADDRESS_i      : STD_LOGIC_VECTOR(0 to 31);
SIGNAL BRAM_RD_ADDRESS_i   : STD_LOGIC_VECTOR(0 to 31);
SIGNAL FRAME_END           : STD_LOGIC := '0';
SIGNAL FRAME_END_i         : STD_LOGIC := '0';
 
SIGNAL BRAM_WRITE_STATE    : BRAM_WRITE_STATE_T := BW_IDLE;
SIGNAL BRAM_READ_STATE     : BRAM_READ_STATE_T  := BR_IDLE;
SIGNAL BRAM_data_len       : UNSIGNED(10 downto 0);
SIGNAL FC_DATA_OUT_i       : STD_LOGIC_VECTOR(7 downto 0);
 
SIGNAL DATA_LEN_out        : STD_LOGIC_VECTOR(10 downto 0) := (others => '0');

begin

BRAM_WR_DATA_LEN_OUT <= DATA_LEN_out;
BRAM_WR_ADDR_OUT     <= BRAM_ADDRESS_i;
BRAM_WR_WEN_OUT      <= BRAM_WEN_OUT_i;
BRAM_WR_DATA_OUT     <= BRAM_DATA_OUT_BE;

BRAM_RD_ADDR_OUT     <= BRAM_RD_ADDRESS_i;

FC_DATA_IN_BE(0) <= FC_DATA_IN(7);
FC_DATA_IN_BE(1) <= FC_DATA_IN(6);
FC_DATA_IN_BE(2) <= FC_DATA_IN(5);
FC_DATA_IN_BE(3) <= FC_DATA_IN(4);
FC_DATA_IN_BE(4) <= FC_DATA_IN(3);
FC_DATA_IN_BE(5) <= FC_DATA_IN(2);
FC_DATA_IN_BE(6) <= FC_DATA_IN(1);
FC_DATA_IN_BE(7) <= FC_DATA_IN(0);

-- -----------------
-- WRITE TO BRAM FSM
-- -----------------
process (CLK)
begin
  if rising_edge(CLK) then
  
	 if SYS_RST = '1' then
			BRAM_WRITE_STATE <= BW_IDLE;
			ADDRESS_WR <= "0000000000";
	 
			-- Default assignments
			FC_RD_EN_OUT     <= '0';
			BRAM_WR_EN_OUT      <= '0';
			UBLAZE_WR_ACK_OUT <= '0';
			-- Increment address (actually += 4)
			BRAM_ADDRESS_i(20 to 29) <= STD_LOGIC_VECTOR(ADDRESS_WR(0 to 9));
	 
    else
			 -- Default assignments
			 FC_RD_EN_OUT     <= '0';
			 BRAM_WR_EN_OUT      <= '0';
			 -- Increment address (actually += 4)
			 BRAM_ADDRESS_i(20 to 29) <= STD_LOGIC_VECTOR(ADDRESS_WR(0 to 9));

			 case BRAM_WRITE_STATE is
			 when BW_IDLE =>
				if FC_EMPTY = '0' then
				  BRAM_WR_EN_OUT              <= '1';
				  FC_RD_EN_OUT             <= '1';
				  BRAM_WRITE_STATE         <= BW_1;
				end if;

			 when BW_1 => -- Write first byte
				  BRAM_WEN_OUT_i           <= "1000";
				  BRAM_WR_EN_OUT              <= '1';
				  FC_RD_EN_OUT              <= '1';
				  BRAM_DATA_OUT_BE(0 to 7) <= FC_DATA_IN_BE(0 to 7);

				  if FC_DATA_IN(8) = '1' or FC_EMPTY = '1' then
					 BRAM_WRITE_STATE <= BW_end;
				  else
					 BRAM_WRITE_STATE <= BW_2;
				  end if;

			 WHEN BW_2 => -- Write second byte
				BRAM_WEN_OUT_i      <= "0100";
				BRAM_WR_EN_OUT         <= '1';
				FC_RD_EN_OUT               <= '1';
				BRAM_DATA_OUT_BE(8 to 15)  <= FC_DATA_IN_BE(0 to 7);

				if FC_DATA_IN(8) = '1' or FC_EMPTY = '1' then
				  BRAM_WRITE_STATE <= BW_end;
				else
				  BRAM_WRITE_STATE <= BW_3;
				end if;

			 WHEN BW_3 => -- Write third byte
				BRAM_WEN_OUT_i      <= "0010";
				BRAM_WR_EN_OUT         <= '1';
				FC_RD_EN_OUT               <= '1';
				BRAM_DATA_OUT_BE(16 to 23) <= FC_DATA_IN_BE(0 to 7);

				if FC_DATA_IN(8) = '1' or FC_EMPTY = '1' then
				  BRAM_WRITE_STATE <= BW_end;
				else
				  BRAM_WRITE_STATE <= BW_4;
				end if;

			 WHEN BW_4 => -- Write fourth byte
				BRAM_WEN_OUT_i      <= "0001";
				BRAM_WR_EN_OUT         <= '1';
				ADDRESS_WR <= ADDRESS_WR + 1;
				FC_RD_EN_OUT               <= '1';
				BRAM_DATA_OUT_BE(24 to 31) <= FC_DATA_IN_BE(0 to 7);

				if FC_DATA_IN(8) = '1' or FC_EMPTY = '1' then
				  BRAM_WRITE_STATE <= BW_end;
				else
				  BRAM_WRITE_STATE <= BW_1;
				end if;

			 when BW_END => -- End of transaction
				DATA_LEN_out        <= "0" & STD_LOGIC_VECTOR(ADDRESS_WR);
				ADDRESS_WR          <= "0000000000";
				
				UBLAZE_WR_ACK_OUT <= '1';
				BRAM_WRITE_STATE <= BW_ACK;

			 when BW_ACK => -- ACK to UB
				BRAM_WEN_OUT_i   <= "0000";

				if UBLAZE_WR_ACK_IN = '1' then
				  UBLAZE_WR_ACK_OUT <= '0';
				  BRAM_WRITE_STATE <= BW_ACK_DOWN;
				end if;

			 when BW_ACK_DOWN => -- Wait for ack from UB low state
				if UBLAZE_WR_ACK_IN = '0' then
				  BRAM_WRITE_STATE <= BW_IDLE;
				end if;

			 when others =>
				BRAM_WRITE_STATE <= BW_IDLE;

		  end case;
	  end if;
  end if;
end process;

  -- ------------------
  -- READ FROM BRAM FSM
  -- ------------------
  BRAM_RD_ADDRESS_i(20 to 29) <= STD_LOGIC_VECTOR(ADDRESS_RD(0 to 9));

process (CLK)
begin
    if rising_edge(CLK) then

		if SYS_RST = '1' then
			BRAM_READ_STATE <= BR_IDLE;
			ADDRESS_RD <= "0000000000";
			
			UBLAZE_RD_ACK_OUT <= '0';
			FC_WR_EN_OUT   <= '0';
			BRAM_RD_EN_OUT <= '0';
		else

			FC_WR_EN_OUT   <= '0';
			BRAM_RD_EN_OUT <= '0';

			case BRAM_READ_STATE is
			when BR_IDLE =>
			  if unsigned(BRAM_RD_DATA_LEN_IN) > X"0" then
				 if FC_PROG_FULL = '0' then
					 BRAM_RD_EN_OUT   <= '1';
					 ADDRESS_RD       <= "0000000000";
					 BRAM_READ_STATE  <= BR_1;
					 BRAM_data_len    <= UNSIGNED(BRAM_RD_DATA_LEN_IN);

					 BRAM_READ_STATE <= BR_WAIT_ADDR;
				 else
				    UBLAZE_RD_ACK_OUT <= '1';
					 BRAM_READ_STATE <= BR_ACK;
				 end if;
			  end if;

			when BR_WAIT_ADDR => -- 1 clock delay
			  BRAM_RD_EN_OUT   <= '1';
			  BRAM_READ_STATE <= BR_1;

			when BR_1 => -- Write first byte
			  if BRAM_data_len > X"1" then
				 BRAM_RD_EN_OUT   <= '1';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "0" & BRAM_RD_DATA_IN(0 to 7);

				 BRAM_READ_STATE <= BR_2;
				 BRAM_data_len   <= BRAM_data_len - 1;
			  else
				 BRAM_RD_EN_OUT   <= '0';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "1" & BRAM_RD_DATA_IN(0 to 7);

				 UBLAZE_RD_ACK_OUT <= '1';
				 BRAM_READ_STATE <= BR_ACK;
			  end if;


			when BR_2 => -- Write second byte
			  if BRAM_data_len > X"1" then
				 BRAM_RD_EN_OUT   <= '1';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "0" & BRAM_RD_DATA_IN(8 to 15);

				 BRAM_READ_STATE  <= BR_3;
				 BRAM_data_len    <= BRAM_data_len - 1;
			  else
				 BRAM_RD_EN_OUT   <= '0';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "1" & BRAM_RD_DATA_IN(8 to 15);

				 UBLAZE_RD_ACK_OUT <= '1';
				 BRAM_READ_STATE <= BR_ACK;
			  end if;

			when BR_3 => -- Write third byte
			  if BRAM_data_len > X"1" then
				 BRAM_RD_EN_OUT   <= '1';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "0" & BRAM_RD_DATA_IN(16 to 23);

				 ADDRESS_RD      <= ADDRESS_RD + 1;    -- Increment address0
				 BRAM_READ_STATE <= BR_4;
				 BRAM_data_len   <= BRAM_data_len - 1;
			  else
				 BRAM_RD_EN_OUT   <= '0';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "1" & BRAM_RD_DATA_IN(16 to 23);

				 UBLAZE_RD_ACK_OUT <= '1';
				 BRAM_READ_STATE <= BR_ACK;
			  end if;

			when BR_4 => -- Write fourth byte
			  if BRAM_data_len > X"1" then
				 BRAM_RD_EN_OUT   <= '1';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "0" & BRAM_RD_DATA_IN(24 to 31);

				 BRAM_READ_STATE <= BR_1;
				 BRAM_data_len   <= BRAM_data_len - 1;

			  else
				 BRAM_RD_EN_OUT   <= '0';
				 FC_WR_EN_OUT     <= '1';
				 FC_DATA_OUT      <= "1" & BRAM_RD_DATA_IN(24 to 31);

				 UBLAZE_RD_ACK_OUT <= '1';
				 BRAM_READ_STATE <= BR_ACK;
			  end if;

			when BR_ACK =>

			  if UBLAZE_RD_ACK_IN  = '1' then
				 UBLAZE_RD_ACK_OUT <= '0';
				 BRAM_READ_STATE   <= BR_IDLE;
			  end if;


			when others =>
			  BRAM_READ_STATE <= BR_IDLE;

		 end case;
	  end if;
  end if;
end process;


end Behavioral;
