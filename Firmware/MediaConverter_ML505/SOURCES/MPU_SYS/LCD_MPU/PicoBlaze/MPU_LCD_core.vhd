-------------------------------------------------------------------------------------------
library IEEE ;
use IEEE.std_logic_1164.all ;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------------------------------
ENTITY MPU_LCD_core IS PORT(
    clk                 : IN  STD_LOGIC;
    port_id             : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0);
    write_strobe        : OUT STD_LOGIC;
    out_port            : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0);
    read_strobe         : OUT STD_LOGIC;
    in_port             : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    interrupt           : IN  STD_LOGIC;
    interrupt_ack       : OUT STD_LOGIC);
END MPU_LCD_core ;
-------------------------------------------------------------------------------------------
ARCHITECTURE Structural OF MPU_LCD_core IS
-------------------------------------------------------------------------------------------
  COMPONENT kcpsm3 IS PORT(
    address             : OUT STD_LOGIC_VECTOR( 9 DOWNTO 0);
    instruction         : IN  STD_LOGIC_VECTOR(17 DOWNTO 0);
    port_id             : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0);
    write_strobe        : OUT STD_LOGIC;
    out_port            : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0);
    read_strobe         : OUT STD_LOGIC;
    in_port             : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    interrupt           : IN  STD_LOGIC;
    interrupt_ack       : OUT STD_LOGIC;
    reset               : IN  STD_LOGIC;
    clk                 : IN  STD_LOGIC);
  END COMPONENT;
  -----------------------------------------------------------------------------------------
  COMPONENT ROM_LCD PORT(
    clk                 : IN  STD_LOGIC;
    reset               : OUT STD_LOGIC;
    address             : IN  STD_LOGIC_VECTOR( 9 DOWNTO 0);
    instruction         : OUT STD_LOGIC_VECTOR(17 DOWNTO 0));
  END COMPONENT;
  -----------------------------------------------------------------------------------------
  
  SIGNAL rom_addr       : STD_LOGIC_VECTOR( 9 DOWNTO 0);
  SIGNAL instruction    : STD_LOGIC_VECTOR(17 DOWNTO 0);
  SIGNAL reset          : STD_LOGIC;

-------------------------------------------------------------------------------------------
BEGIN
-------------------------------------------------------------------------------------------

  kcpsm3_i : kcpsm3 PORT MAP(
    address         => rom_addr,
    instruction     => instruction,
    in_port         => in_port,
    out_port        => out_port,
    port_id         => port_id,
    read_strobe     => read_strobe,
    write_strobe    => write_strobe,
    interrupt       => interrupt,
    interrupt_ack   => interrupt_ack,
    reset           => reset,
    clk             => clk);

  -----------------------------------------------------------------------------------------

  ROM_CODE_i : ROM_LCD PORT MAP(
    clk             => clk,
    reset           => reset,
    address         => rom_addr,
    instruction     => instruction);

-------------------------------------------------------------------------------------------
END Structural;
-------------------------------------------------------------------------------------------
