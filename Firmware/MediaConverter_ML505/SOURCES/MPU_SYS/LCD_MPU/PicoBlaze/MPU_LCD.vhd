----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;
----------------------------------------------------------------------------------
ENTITY MPU_LCD IS PORT(
    clk                     : IN    STD_LOGIC;                              -- system clock

    -- LCD -----------------------------------------------------------------------
    LCD_DB                  : INOUT STD_LOGIC_VECTOR(7 DOWNTO 4);
    LCD_E                   : OUT   STD_LOGIC;
    LCD_RS                  : OUT   STD_LOGIC;
    LCD_RW                  : OUT   STD_LOGIC;

    line1_buffer            : IN    STD_LOGIC_VECTOR(127 DOWNTO 0);
    line2_buffer            : IN    STD_LOGIC_VECTOR(127 DOWNTO 0));

END MPU_LCD;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF MPU_LCD IS
----------------------------------------------------------------------------------
  COMPONENT MPU_LCD_core PORT(
    clk                 : IN  STD_LOGIC;
    port_id             : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    write_strobe        : OUT STD_LOGIC;
    out_port            : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    read_strobe         : OUT STD_LOGIC;
    in_port             : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    interrupt           : IN  STD_LOGIC;
    interrupt_ack       : OUT STD_LOGIC);
  END COMPONENT;

--################################################################################

  -- PicoBlaze peripherals PORT IDs
  CONSTANT ID_LCD_1_00                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";
  CONSTANT ID_LCD_1_01                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"01";
  CONSTANT ID_LCD_1_02                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"02";
  CONSTANT ID_LCD_1_03                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"03";
  CONSTANT ID_LCD_1_04                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"04";
  CONSTANT ID_LCD_1_05                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"05";
  CONSTANT ID_LCD_1_06                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"06";
  CONSTANT ID_LCD_1_07                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"07";
  CONSTANT ID_LCD_1_08                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"08";
  CONSTANT ID_LCD_1_09                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"09";
  CONSTANT ID_LCD_1_10                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0A";
  CONSTANT ID_LCD_1_11                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0B";
  CONSTANT ID_LCD_1_12                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0C";
  CONSTANT ID_LCD_1_13                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0D";
  CONSTANT ID_LCD_1_14                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0E";
  CONSTANT ID_LCD_1_15                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"0F";

  CONSTANT ID_LCD_2_00                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"10";
  CONSTANT ID_LCD_2_01                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"11";
  CONSTANT ID_LCD_2_02                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"12";
  CONSTANT ID_LCD_2_03                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"13";
  CONSTANT ID_LCD_2_04                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"14";
  CONSTANT ID_LCD_2_05                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"15";
  CONSTANT ID_LCD_2_06                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"16";
  CONSTANT ID_LCD_2_07                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"17";
  CONSTANT ID_LCD_2_08                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"18";
  CONSTANT ID_LCD_2_09                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"19";
  CONSTANT ID_LCD_2_10                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"1A";
  CONSTANT ID_LCD_2_11                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"1B";
  CONSTANT ID_LCD_2_12                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"1C";
  CONSTANT ID_LCD_2_13                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"1D";
  CONSTANT ID_LCD_2_14                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"1E";
  CONSTANT ID_LCD_2_15                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"1F";

  CONSTANT ID_LCD_data                  : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"80";
  CONSTANT ID_LCD_Control               : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"81";
  CONSTANT ID_LCD_EN                    : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"82";


  ---------------------------------------------------------------------
  -- basic processor interface
  SIGNAL in_port                    : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL out_port                   : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL port_id                    : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL read_strobe                : STD_LOGIC;
  SIGNAL write_strobe               : STD_LOGIC;
  SIGNAL interrupt                  : STD_LOGIC := '0';
  SIGNAL interrupt_ack              : STD_LOGIC;

  ---------------------------------------------------------------------

  -- LCD signals
  SIGNAL LCD_Data_in                : STD_LOGIC_VECTOR(7 DOWNTO 4):= X"0";      -- PicoBlaze input
  SIGNAL LCD_Data_out               : STD_LOGIC_VECTOR(7 DOWNTO 4):= X"0";      -- PicoBlaze output

  SIGNAL LCD_Control_reg            : STD_LOGIC_VECTOR(1 DOWNTO 0):= "00";      -- PicoBlaze output
  SIGNAL LCD_EN_reg                 : STD_LOGIC_VECTOR(1 DOWNTO 0):= "10";      -- PicoBlaze output (K-optimized); tristate active by default
  SIGNAL LCD_Tristate               : STD_LOGIC := '0';

----------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------------



  --------------------------------------------------------------------------------
  -- PicoBlaze processor (Program ROM included in the MPU module)
  --------------------------------------------------------------------------------

  MPU_LCD_core_i : MPU_LCD_core PORT MAP(
    clk                 => clk,
    port_id             => port_id,
    write_strobe        => write_strobe,
    out_port            => out_port,
    read_strobe         => read_strobe,
    in_port             => in_port,
    interrupt           => interrupt,
    interrupt_ack       => interrupt_ack);

  --------------------------------------------------------------------------------
  -- Processor normal output port multiplexer
  --------------------------------------------------------------------------------

  output_ports: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF write_strobe = '1' THEN
        CASE port_id IS
          WHEN ID_LCD_data                  =>                          LCD_Data_out <= out_port(7 DOWNTO 4);
          WHEN ID_LCD_Control               =>           LCD_Control_reg(1 DOWNTO 0) <= out_port(1 DOWNTO 0);
          WHEN ID_LCD_EN                    =>                LCD_EN_reg(1 DOWNTO 0) <= out_port(1 DOWNTO 0);
          WHEN OTHERS                       => NULL;
        END CASE;
      END IF;
    END IF;
  END PROCESS output_ports;

  --------------------------------------------------------------------------------
  -- Processor input port multiplexer
  --------------------------------------------------------------------------------

  input_ports: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      CASE port_id IS

        WHEN ID_LCD_1_00                        =>    in_port <= line1_buffer(  7 DOWNTO   0);
        WHEN ID_LCD_1_01                        =>    in_port <= line1_buffer( 15 DOWNTO   8);
        WHEN ID_LCD_1_02                        =>    in_port <= line1_buffer( 23 DOWNTO  16);
        WHEN ID_LCD_1_03                        =>    in_port <= line1_buffer( 31 DOWNTO  24);
        WHEN ID_LCD_1_04                        =>    in_port <= line1_buffer( 39 DOWNTO  32);
        WHEN ID_LCD_1_05                        =>    in_port <= line1_buffer( 47 DOWNTO  40);
        WHEN ID_LCD_1_06                        =>    in_port <= line1_buffer( 55 DOWNTO  48);
        WHEN ID_LCD_1_07                        =>    in_port <= line1_buffer( 63 DOWNTO  56);
        WHEN ID_LCD_1_08                        =>    in_port <= line1_buffer( 71 DOWNTO  64);
        WHEN ID_LCD_1_09                        =>    in_port <= line1_buffer( 79 DOWNTO  72);
        WHEN ID_LCD_1_10                        =>    in_port <= line1_buffer( 87 DOWNTO  80);
        WHEN ID_LCD_1_11                        =>    in_port <= line1_buffer( 95 DOWNTO  88);
        WHEN ID_LCD_1_12                        =>    in_port <= line1_buffer(103 DOWNTO  96);
        WHEN ID_LCD_1_13                        =>    in_port <= line1_buffer(111 DOWNTO 104);
        WHEN ID_LCD_1_14                        =>    in_port <= line1_buffer(119 DOWNTO 112);
        WHEN ID_LCD_1_15                        =>    in_port <= line1_buffer(127 DOWNTO 120);

        WHEN ID_LCD_2_00                        =>    in_port <= line2_buffer(  7 DOWNTO   0);
        WHEN ID_LCD_2_01                        =>    in_port <= line2_buffer( 15 DOWNTO   8);
        WHEN ID_LCD_2_02                        =>    in_port <= line2_buffer( 23 DOWNTO  16);
        WHEN ID_LCD_2_03                        =>    in_port <= line2_buffer( 31 DOWNTO  24);
        WHEN ID_LCD_2_04                        =>    in_port <= line2_buffer( 39 DOWNTO  32);
        WHEN ID_LCD_2_05                        =>    in_port <= line2_buffer( 47 DOWNTO  40);
        WHEN ID_LCD_2_06                        =>    in_port <= line2_buffer( 55 DOWNTO  48);
        WHEN ID_LCD_2_07                        =>    in_port <= line2_buffer( 63 DOWNTO  56);
        WHEN ID_LCD_2_08                        =>    in_port <= line2_buffer( 71 DOWNTO  64);
        WHEN ID_LCD_2_09                        =>    in_port <= line2_buffer( 79 DOWNTO  72);
        WHEN ID_LCD_2_10                        =>    in_port <= line2_buffer( 87 DOWNTO  80);
        WHEN ID_LCD_2_11                        =>    in_port <= line2_buffer( 95 DOWNTO  88);
        WHEN ID_LCD_2_12                        =>    in_port <= line2_buffer(103 DOWNTO  96);
        WHEN ID_LCD_2_13                        =>    in_port <= line2_buffer(111 DOWNTO 104);
        WHEN ID_LCD_2_14                        =>    in_port <= line2_buffer(119 DOWNTO 112);
        WHEN ID_LCD_2_15                        =>    in_port <= line2_buffer(127 DOWNTO 120);

        WHEN ID_LCD_data                        =>    in_port <= LCD_Data_in & X"0";

        WHEN OTHERS                             =>    in_port <= (OTHERS => 'X');
      END CASE;
    END IF;
  END PROCESS input_ports;

  --------------------------------------------------------------------------------
  -- 4-wire LCD interface
  --------------------------------------------------------------------------------
  --                           to FPGA               PIN              from FPGA              from FPGA
  IOBUF_LCD_4 : IOBUF PORT MAP(O  => LCD_Data_in(4), IO => LCD_DB(4), I  => LCD_Data_out(4), T  => LCD_Tristate);
  IOBUF_LCD_5 : IOBUF PORT MAP(O  => LCD_Data_in(5), IO => LCD_DB(5), I  => LCD_Data_out(5), T  => LCD_Tristate);
  IOBUF_LCD_6 : IOBUF PORT MAP(O  => LCD_Data_in(6), IO => LCD_DB(6), I  => LCD_Data_out(6), T  => LCD_Tristate);
  IOBUF_LCD_7 : IOBUF PORT MAP(O  => LCD_Data_in(7), IO => LCD_DB(7), I  => LCD_Data_out(7), T  => LCD_Tristate);

  LCD_Tristate  <= LCD_EN_reg(1);
  LCD_E         <= LCD_EN_reg(0);

  LCD_RS        <= LCD_Control_reg(1);
  LCD_RW        <= LCD_Control_reg(0);

----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------
