-------------------------------------------------------------------------------
--! @file
--! @brief Wrapper for the 8-bit PicoBlaze MCU
--!
--! This file contains mutable device registers, which are accessible
--! via the I2C interface, by PicoBlaze, which is a component of this module
--! or by MicroBlaze, which is provided access via the MPU_SYS_IN/OUT ports.
--! The address space is only 8bit, which should be sufficient for configuration
--! and basic I/O.
--! 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

library WORK;
use WORK.modules_pkg.ALL;
use WORK.parameters_pkg.ALL;
use WORK.types_pkg.ALL;

----------------------------------------------------------------------------------
ENTITY MPU_SYS IS PORT(
    clk                     : IN    STD_LOGIC;                    --! system clock
    SYS_RST                 : OUT   STD_LOGIC;                    --! System reset (synchronous)

    MPU_SYS_INOUT           : INOUT MPU_SYS_INTERFACE_INOUT;      --! INOUT record
    MPU_SYS_IN              : IN MPU_SYS_INTERFACE_IN;            --! IN record
    MPU_SYS_OUT             : OUT MPU_SYS_INTERFACE_OUT);         --! OUT record
END MPU_SYS;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF MPU_SYS IS
----------------------------------------------------------------------------------

--################################################################################

  ---------------------------------------------------------------------
  -- basic processor interface
  SIGNAL in_port                    : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL out_port                   : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL port_id                    : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL read_strobe                : STD_LOGIC;
  SIGNAL write_strobe               : STD_LOGIC;
  SIGNAL interrupt                  : STD_LOGIC := '0';
  SIGNAL interrupt_ack              : STD_LOGIC;

  ---------------------------------------------------------------------
  -- SFP signals
  SIGNAL SFP_out                    : STD_LOGIC_VECTOR( 7 DOWNTO 0);            --! SFP I2C driver address/data out
--SIGNAL SFP_control                : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"80";    --! Status/control register of SFP for PicoBlaze access;
                                                                                -- Enable Tx and select high data rate as default

 

  SIGNAL PHY_reset_reg              : STD_LOGIC_VECTOR( 1 DOWNTO 0) := "00";    --! SOFT(1) and HARD(0) reset of PHY

  SIGNAL GPIO_LED_REG               : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"00";

  -- UART signals
  SIGNAL UART_write                 : STD_LOGIC := '0';                         --! write data  to  UART buffer
  SIGNAL UART_read                  : STD_LOGIC := '0';                         --!  read data from UART buffer

  SIGNAL UART_reset_reg             : STD_LOGIC_VECTOR(1 DOWNTO 0):= "00";      --! Reset IN reg (from PicoBlaze output MUX); (0) Tx, (1) Rx
  SIGNAL UART_Data_in_reg           : STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00";     --! Data IN reg (from PicoBlaze output MUX)

  SIGNAL UART_Status_reg            : STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00";     --! Status OUT reg (to PicoBlaze input MUX)
                                                                                -- (0) = TX_data_present;
                                                                                -- (1) = TX_half_full;
                                                                                -- (2) = TX_full;
                                                                                -- (3) = RX_data_present;
                                                                                -- (4) = RX_half_full;
                                                                                -- (5) = RX_full;

  SIGNAL UART_Data_out_reg          : STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00";     --! Data OUT reg (to PicoBlaze input MUX)

  -- System reset
  SIGNAL SYS_reset_REG              : STD_LOGIC := '0';

  -- LCD signals
  SIGNAL LCD_Data_in                : STD_LOGIC_VECTOR(7 DOWNTO 4):= X"0";      --! PicoBlaze input
  SIGNAL LCD_Data_out               : STD_LOGIC_VECTOR(7 DOWNTO 4):= X"0";      --! PicoBlaze output

  SIGNAL LCD_Control_reg            : STD_LOGIC_VECTOR(1 DOWNTO 0):= "00";      --! PicoBlaze output
  SIGNAL LCD_EN_reg                 : STD_LOGIC_VECTOR(1 DOWNTO 0):= "10";      --! PicoBlaze output (K-optimized); tristate active by default
  SIGNAL LCD_Tristate               : STD_LOGIC := '0';

  -- interrupt controller signals
  SIGNAL int_in                         : STD_LOGIC_VECTOR(8 DOWNTO 1):= (OTHERS => '0');   --! agregated interrupt signals
  SIGNAL reg_int_status                 : STD_LOGIC_VECTOR(7 DOWNTO 0);                     --! Active interrupt number OUT (PicoBlaze IN)
  SIGNAL reg_int_en_in                  : STD_LOGIC_VECTOR(8 DOWNTO 1):= (OTHERS => '0');   --! Enable status reg OUT (PicoBlaze IN)
  SIGNAL reg_int_sense_in               : STD_LOGIC_VECTOR(8 DOWNTO 1):= (OTHERS => '0');   --! Edge status reg OUT (PicoBlaze IN)

  -- Text ROM
  SIGNAL ROM_addr_1                 : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";    --! TxtROM_1 address
  SIGNAL ROM_data_1                 : STD_LOGIC_VECTOR( 7 DOWNTO 0);            --! TxtROM_1 data

  -- SPI Slave RAM signals
  SIGNAL SPI_global_status_REG      : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";    --! output from MCU
  SIGNAL SPI_global_status          : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";    --! input to SPI interface; some bits are driven directly:
                                                                                --      (7) -- MCU : fixed '1'
                                                                                --      (6) -- MCU : fixed '0'
                                                                                --      (5) -- MCU : fixed '0'
                                                                                --      (4) -- MCU : '1' when writing data to SPI registers
                                                                                --      (3) -- DIRECT: Toggle each 1s (BERT)
                                                                                --      (2) -- DIRECT: PRBS lost (when '1')
                                                                                --      (1) -- MCU : SFP present (when '1')
                                                                                --      (0) -- MCU : fixed '1'


  SIGNAL SPI_RAM_Address_REG        : STD_LOGIC_VECTOR( 5 DOWNTO 0):= "000000";
  SIGNAL SPI_RAM_Data_IN_REG        : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";
  SIGNAL SPI_RAM_WR                 : STD_LOGIC := '0';
  SIGNAL SPI_RAM_Data_OUT           : STD_LOGIC_VECTOR( 7 DOWNTO 0);

  SIGNAL Proc_RAM_Address_REG       : STD_LOGIC_VECTOR( 5 DOWNTO 0):= "000000";
  SIGNAL Proc_RAM_Data_IN_REG       : STD_LOGIC_VECTOR( 7 DOWNTO 0):= X"00";
  SIGNAL Proc_RAM_WR                : STD_LOGIC := '0';
  SIGNAL Proc_RAM_Data_OUT          : STD_LOGIC_VECTOR( 7 DOWNTO 0);

  -- LCD
  SIGNAL LCD_1_00                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_01                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_02                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_03                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_04                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_05                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_06                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_07                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_08                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_09                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_10                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_11                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_12                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_13                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_14                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_1_15                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_00                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_01                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_02                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_03                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_04                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_05                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_06                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_07                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_08                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_09                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_10                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_11                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_12                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_13                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_14                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";
  SIGNAL LCD_2_15                   : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"20";


  SIGNAL Seq_sel_REG                : STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"00";



  -- GTP control ---------------------------------------------------------------
  SIGNAL SFP_reset_REG              : STD_LOGIC_VECTOR(3 DOWNTO 0) := "0000";   --! rst_Rx_CDR_SMA & rst_Rx_CDR_SFP & rst_GTP_SFP_SMA & /*unused*/

  -- GTP SFP control -----------------------------------------------------------
  SIGNAL SFP_Tx_Inhibit_REG         : STD_LOGIC := '0';
  SIGNAL SFP_Tx_Polarity_REG        : STD_LOGIC := '0';
  SIGNAL SFP_Tx_Swing_REG           : STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";
  SIGNAL SFP_Tx_Preemphasis_REG     : STD_LOGIC_VECTOR(2 DOWNTO 0) := "000";
  SIGNAL SFP_Rx_EqMix_REG           : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00";
  SIGNAL SFP_Rx_EqPole_REG          : STD_LOGIC_VECTOR(3 DOWNTO 0) := "0000";
  
  SIGNAL MUX_Select_i               : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"00";
  SIGNAL UBLAZE_data_ack            : STD_LOGIC := '0';

  -- HOST interface signals
  SIGNAL reg_HOST_1_data_0      : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL reg_HOST_1_data_1      : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL reg_HOST_1_data_2      : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL reg_HOST_1_data_3      : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL reg_HOST_1_addr_L      : STD_LOGIC_VECTOR( 7 DOWNTO 0);
  SIGNAL reg_HOST_1_addr_H      : STD_LOGIC_VECTOR( 7 DOWNTO 0);

----------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------------

  --------------------------------------------------------------------------------
  -- PicoBlaze processor (Program ROM included in the MPU module)
  --------------------------------------------------------------------------------

  MPU_SYS_core_i : entity WORK.MPU_SYS_core PORT MAP(
    clk                 => clk,
    port_id             => port_id,
    write_strobe        => write_strobe,
    out_port            => out_port,
    read_strobe         => read_strobe,
    in_port             => in_port,
    interrupt           => interrupt,
    interrupt_ack       => OPEN);
  --interrupt_ack       => interrupt_ack);

  ------------------------------------------------------------------------------
  -- Interrupt controller
  ------------------------------------------------------------------------------

  INT_controler_8_i : entity WORK.INT_controler_8
  PORT MAP(
    clk                 => clk,
    rst                 => '0',
    int_in              => int_in,
    reg_int_status      => reg_int_status,
    reg_int_en_in       => reg_int_en_in,
    reg_int_sense_in    => reg_int_sense_in,
    proc_irq            => interrupt,
    proc_irq_ack        => interrupt_ack);


  int_in(1) <= UART_Status_reg(3);          -- level sensitive; UART RX_data_present (new data)
  int_in(2) <= MPU_SYS_IN.BERT_IRQ_1s;                 --  edge sensitive; BERT
  int_in(3) <= '0';                         --
  int_in(4) <= '0';                         --
  int_in(5) <= '0';                         --
  int_in(6) <= '0';                         --
  int_in(7) <= '0';                         --
  int_in(8) <= '0';                         --
  
  --------------------------------------------------------------------------------
  -- Processor normal output port multiplexer
  --------------------------------------------------------------------------------
  -----------------------------------------------------------------------------
--! @brief Process to manage the access to mutable registers
--! @vhdlflow
--!
--! On PicoBlaze write strobe (write_strobe), the output port
--! of PicoBlaze (out_port) is copied to a selected register,
--! selected by an appropriate address (port_id).
--! On external write strobe (UBLAZE_write_strobe), the data from
--! the external data source (UBLAZE_write_data) are copied to
--! the selected mutable register (UBLAZE_addr). An acknowledge
--! impolse signal to the external register writer
--! is generated (UBLAZE_data_ack).
--!
--! @param[in]   clk  Clock, used on rising edge
-----------------------------------------------------------------------------
  output_ports: PROCESS(clk) 
  
  VARIABLE src_port : STD_LOGIC_VECTOR(7 DOWNTO 0) := out_port;
  VARIABLE dst_addr : STD_LOGIC_VECTOR(7 DOWNTO 0) := port_id;
  VARIABLE do_write : STD_LOGIC := '0';
  
  BEGIN
    IF rising_edge(clk) THEN
      UART_write        <= '0';
      SYS_reset_REG     <= '0';
      Proc_RAM_WR       <= '0';
      UBLAZE_data_ack   <= '0';

      IF write_strobe = '1' THEN
     
        src_port := out_port;
        dst_addr := port_id;
        do_write := '1';

      ELSIF MPU_SYS_IN.UBLAZE_write_strobe = '1' THEN
      
        UBLAZE_data_ack <= '1';
        
        src_port := MPU_SYS_IN.UBLAZE_write_data;
        dst_addr := MPU_SYS_IN.UBLAZE_addr;
        do_write := '1';
        
      ELSE
      
        do_write := '0';
        
      END IF;
      
      IF do_write = '1' THEN
        CASE dst_addr IS
       --WHEN ID_SFP_control               =>               SFP_control(7 DOWNTO 6) <= src_port(7 DOWNTO 6);
       --WHEN ID_SFP_TW_addr               =>           SFP_TW_addr_reg(4 DOWNTO 0) <= src_port(4 DOWNTO 0);
         WHEN ID_PHY_reset_reg             =>             PHY_reset_reg(1 DOWNTO 0) <= src_port(1 DOWNTO 0);
         WHEN ID_UART_reset                =>            UART_reset_reg(1 DOWNTO 0) <= src_port(1 DOWNTO 0);
         WHEN ID_UART_data_PORT            =>                      UART_Data_in_reg <= src_port;
                                                                         UART_write <= '1';

         WHEN ID_INT_controler_EN          =>                         reg_int_en_in <= src_port;
         WHEN ID_INT_controler_SENSE       =>                      reg_int_sense_in <= src_port;

         WHEN ID_Seq_sel                   =>                           Seq_sel_REG <= src_port;

         WHEN ID_GTP_reset                 =>                         SFP_reset_REG <= src_port(3 DOWNTO 0);

         WHEN ID_SFP_Tx_Inhibit_REG        =>                    SFP_Tx_Inhibit_REG <= src_port(0);
         WHEN ID_SFP_Tx_Polarity_REG       =>                   SFP_Tx_Polarity_REG <= src_port(0);
         WHEN ID_SFP_Tx_Swing_REG          =>                      SFP_Tx_Swing_REG <= src_port(2 DOWNTO 0);
         WHEN ID_SFP_Tx_Preemphasis_REG    =>                SFP_Tx_Preemphasis_REG <= src_port(2 DOWNTO 0);
         WHEN ID_SFP_Rx_EqMix_REG          =>                      SFP_Rx_EqMix_REG <= src_port(1 DOWNTO 0);
         WHEN ID_SFP_Rx_EqPole_REG         =>                     SFP_Rx_EqPole_REG <= src_port(3 DOWNTO 0);

         WHEN ID_GPIO_LED                  =>                              MPU_SYS_OUT.GPIO_LED <= src_port;

         WHEN ID_SYS_reset                 =>                         SYS_reset_REG <= src_port(0);

         WHEN ID_Text_ROM_1                =>                            ROM_addr_1 <= src_port;

         WHEN ID_SPI_RAM_Address           =>                  Proc_RAM_Address_REG <= src_port(5 DOWNTO 0);
         WHEN ID_SPI_RAM_Data              =>                  Proc_RAM_Data_IN_REG <= src_port;
                                                                        Proc_RAM_WR <= '1';
         WHEN ID_SPI_global_status         =>                 SPI_global_status_REG <= src_port;

         WHEN ID_LCD_1_00                  =>                              LCD_1_00 <= src_port;
         WHEN ID_LCD_1_01                  =>                              LCD_1_01 <= src_port;
         WHEN ID_LCD_1_02                  =>                              LCD_1_02 <= src_port;
         WHEN ID_LCD_1_03                  =>                              LCD_1_03 <= src_port;
         WHEN ID_LCD_1_04                  =>                              LCD_1_04 <= src_port;
         WHEN ID_LCD_1_05                  =>                              LCD_1_05 <= src_port;
         WHEN ID_LCD_1_06                  =>                              LCD_1_06 <= src_port;
         WHEN ID_LCD_1_07                  =>                              LCD_1_07 <= src_port;
         WHEN ID_LCD_1_08                  =>                              LCD_1_08 <= src_port;
         WHEN ID_LCD_1_09                  =>                              LCD_1_09 <= src_port;
         WHEN ID_LCD_1_10                  =>                              LCD_1_10 <= src_port;
         WHEN ID_LCD_1_11                  =>                              LCD_1_11 <= src_port;
         WHEN ID_LCD_1_12                  =>                              LCD_1_12 <= src_port;
         WHEN ID_LCD_1_13                  =>                              LCD_1_13 <= src_port;
         WHEN ID_LCD_1_14                  =>                              LCD_1_14 <= src_port;
         WHEN ID_LCD_1_15                  =>                              LCD_1_15 <= src_port;
         WHEN ID_LCD_2_00                  =>                              LCD_2_00 <= src_port;
         WHEN ID_LCD_2_01                  =>                              LCD_2_01 <= src_port;
         WHEN ID_LCD_2_02                  =>                              LCD_2_02 <= src_port;
         WHEN ID_LCD_2_03                  =>                              LCD_2_03 <= src_port;
         WHEN ID_LCD_2_04                  =>                              LCD_2_04 <= src_port;
         WHEN ID_LCD_2_05                  =>                              LCD_2_05 <= src_port;
         WHEN ID_LCD_2_06                  =>                              LCD_2_06 <= src_port;
         WHEN ID_LCD_2_07                  =>                              LCD_2_07 <= src_port;
         WHEN ID_LCD_2_08                  =>                              LCD_2_08 <= src_port;
         WHEN ID_LCD_2_09                  =>                              LCD_2_09 <= src_port;
         WHEN ID_LCD_2_10                  =>                              LCD_2_10 <= src_port;
         WHEN ID_LCD_2_11                  =>                              LCD_2_11 <= src_port;
         WHEN ID_LCD_2_12                  =>                              LCD_2_12 <= src_port;
         WHEN ID_LCD_2_13                  =>                              LCD_2_13 <= src_port;
         WHEN ID_LCD_2_14                  =>                              LCD_2_14 <= src_port;
         WHEN ID_LCD_2_15                  =>                              LCD_2_15 <= src_port;
         WHEN ID_MUX_Select                =>                          MUX_Select_i <= src_port;

         WHEN OTHERS                       => NULL;
       END CASE;
      END IF;
    END IF;
  END PROCESS output_ports;

  MPU_SYS_OUT.rst_GTP_SFP_SMA       <= SFP_reset_REG(1);
  MPU_SYS_OUT.rst_Rx_CDR_SFP        <= SFP_reset_REG(2);
  MPU_SYS_OUT.rst_Rx_CDR_SMA        <= SFP_reset_REG(3);
  MPU_SYS_OUT.MUX_Select            <= MUX_Select_i;
  MPU_SYS_OUT.UBLAZE_data_ack       <= UBLAZE_data_ack;



  --------------------------------------------------------------------------------
  -- SPI_global_status register (some bits are driven directly, some from MCU)
  --------------------------------------------------------------------------------

  SPI_global_status(7) <= SPI_global_status_REG(7);         --  MCU : fixed '1'
  SPI_global_status(6) <= SPI_global_status_REG(6);         --  MCU : fixed '0'
  SPI_global_status(5) <= SPI_global_status_REG(5);         --  MCU : fixed '0'
  SPI_global_status(4) <= SPI_global_status_REG(4);         --  MCU : '1' when writing data to SPI registers
  SPI_global_status(3) <= MPU_SYS_IN.BERT_toggle_1s;        --  DIRECT: Toggle each 1s (BERT)
  SPI_global_status(2) <= MPU_SYS_IN.BERT_LFSR_locked;      --  DIRECT: PRBS detected (when '1')
  SPI_global_status(1) <= SPI_global_status_REG(1);         --  MCU : SFP present (when '1')
  SPI_global_status(0) <= SPI_global_status_REG(0);         --  MCU : fixed '1'



  --------------------------------------------------------------------------------
  -- Processor input port multiplexer
  --------------------------------------------------------------------------------
-----------------------------------------------------------------------------
--! @brief Process to manage input port of PicoBlaze
--! @vhdlflow
--!
--! Based on port_id, a selected register data is copied
--! to in_port PicoBlaze input port.
--!
--! @param[in]   clk  Clock, used on rising edge
-----------------------------------------------------------------------------
  input_ports: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN

      interrupt_ack <= '0';
      UART_read <= '0';

      CASE port_id IS
        WHEN ID_UART_data_PORT                  =>    in_port <= UART_Data_out_reg;
                                                      IF read_strobe = '1' THEN UART_read <= '1'; END IF;
        WHEN ID_UART_stat_PORT                  =>    in_port <= UART_Status_reg;
        WHEN ID_Text_ROM_1                      =>    in_port <= ROM_data_1;

        WHEN ID_SFP_TW_addr | ID_SFP_stat       =>    in_port <= SFP_out;
        WHEN ID_SFP_data | ID_SFP_addr          =>    in_port <= SFP_out;
      --WHEN ID_SFP_control                     =>    in_port <= SFP_control;

        WHEN ID_INT_controler_EN                =>    in_port <= reg_int_en_in;
        WHEN ID_INT_controler_SENSE             =>    in_port <= reg_int_sense_in;
        WHEN ID_INT_controler_STATUS            =>    in_port <= reg_int_status;
                                                      IF read_strobe = '1' THEN interrupt_ack <= '1'; END IF;

        WHEN ID_Seq_sel                         =>    in_port <= Seq_sel_REG;

        WHEN ID_GTP_reset                       =>    in_port <= MPU_SYS_IN.PLL_locked_SFP_SMA & '0' & "00" & SFP_reset_REG;

        WHEN ID_SFP_Tx_Inhibit_REG              =>    in_port <= "0000000" & SFP_Tx_Inhibit_REG;
        WHEN ID_SFP_Tx_Polarity_REG             =>    in_port <= "0000000" & SFP_Tx_Polarity_REG;
        WHEN ID_SFP_Tx_Swing_REG                =>    in_port <=   "00000" & SFP_Tx_Swing_REG;
        WHEN ID_SFP_Tx_Preemphasis_REG          =>    in_port <=   "00000" & SFP_Tx_Preemphasis_REG;
        WHEN ID_SFP_Rx_EqMix_REG                =>    in_port <=  "000000" & SFP_Rx_EqMix_REG;
        WHEN ID_SFP_Rx_EqPole_REG               =>    in_port <=    "0000" & SFP_Rx_EqPole_REG;

        WHEN reg_HOST_1_data_0_ID               =>    in_port <= reg_HOST_1_data_0;
        WHEN reg_HOST_1_data_1_ID               =>    in_port <= reg_HOST_1_data_1;
        WHEN reg_HOST_1_data_2_ID               =>    in_port <= reg_HOST_1_data_2;
        WHEN reg_HOST_1_data_3_ID               =>    in_port <= reg_HOST_1_data_3;
        WHEN reg_HOST_1_addr_L_ID               =>    in_port <= reg_HOST_1_addr_L;
        WHEN reg_HOST_1_addr_H_ID               =>    in_port <= reg_HOST_1_addr_H;

        WHEN ID_GPIO_DIP                        =>    in_port <= MPU_SYS_IN.GPIO_DIP;
        WHEN ID_GPIO_BTN                        =>    in_port <= "000" & MPU_SYS_IN.GPIO_BTN;

        WHEN ID_BERT_Err_1s_00                  =>    in_port <= MPU_SYS_IN.BERT_Err_cnt_1s( 7 DOWNTO  0);
        WHEN ID_BERT_Err_1s_01                  =>    in_port <= MPU_SYS_IN.BERT_Err_cnt_1s(15 DOWNTO  8);
        WHEN ID_BERT_Err_1s_02                  =>    in_port <= MPU_SYS_IN.BERT_Err_cnt_1s(23 DOWNTO 16);
        WHEN ID_BERT_Err_1s_03                  =>    in_port <= MPU_SYS_IN.BERT_Err_cnt_1s(31 DOWNTO 24);

        WHEN ID_SPI_RAM_Address                 =>    in_port <= "00" & Proc_RAM_Address_REG;
        WHEN ID_SPI_RAM_Data                    =>    in_port <= Proc_RAM_Data_OUT;
        WHEN ID_SPI_global_status               =>    in_port <= SPI_global_status;
        
        WHEN ID_MUX_Select                      =>    in_port <= MUX_Select_i;

        WHEN OTHERS                             =>    in_port <= (OTHERS => 'X');
      END CASE;
    END IF;
  END PROCESS input_ports;

  --------------------------------------------------------------------------------

  SYS_RST               <= SYS_reset_REG;               -- OUT

  MPU_SYS_OUT.Seq_sel               <= Seq_sel_REG;

  MPU_SYS_OUT.SFP_Tx_Inhibit        <= SFP_Tx_Inhibit_REG;
  MPU_SYS_OUT.SFP_Tx_Polarity       <= SFP_Tx_Polarity_REG;
  MPU_SYS_OUT.SFP_Tx_Swing          <= SFP_Tx_Swing_REG;
  MPU_SYS_OUT.SFP_Tx_Preemphasis    <= SFP_Tx_Preemphasis_REG;
  MPU_SYS_OUT.SFP_Rx_EqMix          <= SFP_Rx_EqMix_REG;
  MPU_SYS_OUT.SFP_Rx_EqPole         <= SFP_Rx_EqPole_REG;

  --------------------------------------------------------------------------------
  -- RS-232 transceiver
  --------------------------------------------------------------------------------

  UART_block_i : entity WORK.UART_block
  GENERIC MAP(
    period              => UART_period)
  PORT MAP(
    clk                 => clk,
    UART_Rx             => MPU_SYS_IN.UART_RXD,
    UART_Tx             => MPU_SYS_OUT.UART_TXD,

    UART_write          => UART_write,
    UART_read           => UART_read,
    UART_Reset_reg      => UART_Reset_reg,
    UART_Data_in_reg    => UART_Data_in_reg,
    UART_Status_reg     => UART_Status_reg,
    UART_Data_out_reg   => UART_Data_out_reg);

  --------------------------------------------------------------------------------
  -- Text ROMs; holds text data (messages) for user interfacing
  --------------------------------------------------------------------------------

  Text_ROM_1_i : entity WORK.Text_ROM_1 PORT MAP(
	clka                => clk,
	addra               => ROM_addr_1,
	douta               => ROM_data_1);

  --------------------------------------------------------------------------------
  -- SFP transceiver management
  --------------------------------------------------------------------------------
  TWI_controler_i : entity WORK.TWI_controler
  GENERIC MAP(
    data_port_ID        => ID_SFP_data,
    addr_port_ID        => ID_SFP_addr,
    stat_port_ID        => ID_SFP_stat,
    dadr_port_ID        => ID_SFP_TW_addr,
    clk_div             => 1000)                --!(max 100 kHz for SFP; (125 MHz / 1000) / 3 ~ 84 kb/s)
  PORT MAP(
    clk                 => clk,
    Data_in             => out_port,
    Data_out            => SFP_out,
    Address             => port_id,
    write_strobe        => write_strobe,
    SCL                 => MPU_SYS_OUT.SFP_M1,
    SDA                 => MPU_SYS_INOUT.SFP_M2);

  --------------------------------------------------------------------------------

    -- SFP output (status) signals
    --    SFP_M0            module present indicator; active LOW
    --    SFP_TXF           Tx fault indicator; active HIGH
    --    SFP_LOS           Los of Signal indicator; active HIGH
    -- SFP input (control) signals
    --    SFP_TXENA         set HIGH to enable Tx (TXDIS + On-board inversion)
    --    SFP_RS            set HIGH for high data rate

--SFP_control(5 DOWNTO 0) <= "000" & SFP_TXF & SFP_LOS & SFP_M0;

--SFP_TXENA     <= SFP_control(6);
--SFP_RS        <= SFP_control(7);
--SFP_TXENA     <= '0';
--SFP_RS        <= '1';

  --------------------------------------------------------------------------------
  -- Marvell 88E1111 PHY management
  --------------------------------------------------------------------------------

 

  --------------------------------------------------------------------------------

  SPI_Slave_i : entity WORK.SPI_Slave
  PORT MAP(
    clk                 => clk,
    rst                 => SYS_reset_REG,
    SPI_SCK             => MPU_SYS_IN.SPI_SCK,
    SPI_SDI             => MPU_SYS_IN.SPI_SDI,
    SPI_SDO             => MPU_SYS_OUT.SPI_SDO,
    SPI_CSN             => MPU_SYS_IN.SPI_CSN,
    SPI_global_status   => SPI_global_status,
    RAM_Address         => SPI_RAM_Address_REG,
    RAM_Data_IN         => SPI_RAM_Data_IN_REG,
    RAM_WR              => SPI_RAM_WR,
    RAM_Data_OUT        => SPI_RAM_Data_OUT);

  ------------------------------------------------------------------------------

  RAM_SPI_Wrapper_i : entity WORK.RAM_SPI_Wrapper
  PORT MAP(
    clk                 => clk,
    RAM_A_Addr_in       => Proc_RAM_Address_REG,
    RAM_A_Data_in       => Proc_RAM_Data_IN_REG,
    RAM_A_Wr_EN         => Proc_RAM_WR,
    RAM_A_Data_out      => Proc_RAM_Data_OUT,
    RAM_B_Addr_in       => SPI_RAM_Address_REG,
    RAM_B_Data_in       => SPI_RAM_Data_IN_REG,
    RAM_B_Wr_EN         => SPI_RAM_WR,
    RAM_B_Data_out      => SPI_RAM_Data_OUT);

  --------------------------------------------------------------------------------

  LCD_driver_MPU_i : entity WORK.LCD_driver_MPU
  PORT MAP(
    clk             => clk,
    lcd_e           => MPU_SYS_OUT.LCD_E,
    lcd_rs          => MPU_SYS_OUT.LCD_RS,
    lcd_rw          => MPU_SYS_OUT.LCD_RW,
    lcd_db          => MPU_SYS_INOUT.LCD_DB,
    line1_00        => LCD_1_00,
    line1_01        => LCD_1_01,
    line1_02        => LCD_1_02,
    line1_03        => LCD_1_03,
    line1_04        => LCD_1_04,
    line1_05        => LCD_1_05,
    line1_06        => LCD_1_06,
    line1_07        => LCD_1_07,
    line1_08        => LCD_1_08,
    line1_09        => LCD_1_09,
    line1_10        => LCD_1_10,
    line1_11        => LCD_1_11,
    line1_12        => LCD_1_12,
    line1_13        => LCD_1_13,
    line1_14        => LCD_1_14,
    line1_15        => LCD_1_15,
    line2_00        => LCD_2_00,
    line2_01        => LCD_2_01,
    line2_02        => LCD_2_02,
    line2_03        => LCD_2_03,
    line2_04        => LCD_2_04,
    line2_05        => LCD_2_05,
    line2_06        => LCD_2_06,
    line2_07        => LCD_2_07,
    line2_08        => LCD_2_08,
    line2_09        => LCD_2_09,
    line2_10        => LCD_2_10,
    line2_11        => LCD_2_11,
    line2_12        => LCD_2_12,
    line2_13        => LCD_2_13,
    line2_14        => LCD_2_14,
    line2_15        => LCD_2_15);

HOST_Interface_1 : entity WORK.Pico_HOST_Interface
    GENERIC MAP(
      reg_HOST_data_0_ID    => reg_HOST_1_data_0_ID,
      reg_HOST_data_1_ID    => reg_HOST_1_data_1_ID,
      reg_HOST_data_2_ID    => reg_HOST_1_data_2_ID,
      reg_HOST_data_3_ID    => reg_HOST_1_data_3_ID,
      reg_HOST_addr_L_ID    => reg_HOST_1_addr_L_ID,
      reg_HOST_addr_H_ID    => reg_HOST_1_addr_H_ID)
    PORT MAP(
      clk                   => clk,
      rst                   => '0',
      Pico_Address          => port_id,
      Pico_Data_in          => out_port,
      Pico_rd               => read_strobe,
      Pico_wr               => write_strobe,
      reg_HOST_data_0       => reg_HOST_1_data_0,
      reg_HOST_data_1       => reg_HOST_1_data_1,
      reg_HOST_data_2       => reg_HOST_1_data_2,
      reg_HOST_data_3       => reg_HOST_1_data_3,
      reg_HOST_addr_L       => reg_HOST_1_addr_L,
      reg_HOST_addr_H       => reg_HOST_1_addr_H,
      HOSTOPCODE            => MPU_SYS_OUT.HOST_1_OPCODE,
      HOSTREQ               => MPU_SYS_OUT.HOST_1_REQ,
      HOSTMIIMSEL           => MPU_SYS_OUT.HOST_1_MIIMSEL,
      HOSTADDR              => MPU_SYS_OUT.HOST_1_ADDR,
      HOSTWRDATA            => MPU_SYS_OUT.HOST_1_WRDATA,
      HOSTMIIMRDY           => MPU_SYS_IN.HOST_1_MIIMRDY,
      HOSTRDDATA            => MPU_SYS_IN.HOST_1_RDDATA,
      HOSTEMAC1SEL          => MPU_SYS_OUT.HOST_1_EMAC1SEL);

----------------------------------------------------------------------------------
END Behavioral;
----------------------------------------------------------------------------------
