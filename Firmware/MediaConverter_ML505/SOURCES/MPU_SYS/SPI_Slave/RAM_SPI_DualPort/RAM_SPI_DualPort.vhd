--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: RAM_SPI_DualPort.vhd
-- /___/   /\     Timestamp: Wed Apr 09 08:11:26 2014
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -w -sim -ofmt vhdl D:/Project/ISE/ML505/MediaConverter_ML505_v3/SOURCES/MPU_SYS/SPI_Slave/RAM_SPI_DualPort/tmp/_cg/RAM_SPI_DualPort.ngc D:/Project/ISE/ML505/MediaConverter_ML505_v3/SOURCES/MPU_SYS/SPI_Slave/RAM_SPI_DualPort/tmp/_cg/RAM_SPI_DualPort.vhd 
-- Device	: 5vlx110tff1136-1
-- Input file	: D:/Project/ISE/ML505/MediaConverter_ML505_v3/SOURCES/MPU_SYS/SPI_Slave/RAM_SPI_DualPort/tmp/_cg/RAM_SPI_DualPort.ngc
-- Output file	: D:/Project/ISE/ML505/MediaConverter_ML505_v3/SOURCES/MPU_SYS/SPI_Slave/RAM_SPI_DualPort/tmp/_cg/RAM_SPI_DualPort.vhd
-- # of Entities	: 1
-- Design Name	: RAM_SPI_DualPort
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------


-- synthesis translate_off
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity RAM_SPI_DualPort is
  port (
    clka : in STD_LOGIC := 'X'; 
    clkb : in STD_LOGIC := 'X'; 
    wea : in STD_LOGIC_VECTOR ( 0 downto 0 ); 
    addra : in STD_LOGIC_VECTOR ( 5 downto 0 ); 
    dina : in STD_LOGIC_VECTOR ( 7 downto 0 ); 
    web : in STD_LOGIC_VECTOR ( 0 downto 0 ); 
    addrb : in STD_LOGIC_VECTOR ( 5 downto 0 ); 
    dinb : in STD_LOGIC_VECTOR ( 7 downto 0 ); 
    douta : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    doutb : out STD_LOGIC_VECTOR ( 7 downto 0 ) 
  );
end RAM_SPI_DualPort;

architecture STRUCTURE of RAM_SPI_DualPort is
  signal N0 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_15_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_14_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_13_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_12_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_7_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_6_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_5_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_4_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_15_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_14_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_13_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_12_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_7_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_6_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_5_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_4_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPA_1_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPA_0_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPB_1_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPB_0_UNCONNECTED : STD_LOGIC;
 
begin
  XST_VCC : VCC
    port map (
      P => N0
    );
  XST_GND : GND
    port map (
      G => N1
    );
  U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP : RAMB18
    generic map(
      DOA_REG => 1,
      DOB_REG => 1,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000080000000000030000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      SIM_COLLISION_CHECK => "ALL",
      SIM_MODE => "SAFE",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
    port map (
      CLKA => clka,
      CLKB => clkb,
      ENA => N0,
      ENB => N0,
      REGCEA => N0,
      REGCEB => N0,
      SSRA => N1,
      SSRB => N1,
      ADDRA(13) => N1,
      ADDRA(12) => N1,
      ADDRA(11) => N1,
      ADDRA(10) => N1,
      ADDRA(9) => addra(5),
      ADDRA(8) => addra(4),
      ADDRA(7) => addra(3),
      ADDRA(6) => addra(2),
      ADDRA(5) => addra(1),
      ADDRA(4) => addra(0),
      ADDRA(3) => N1,
      ADDRA(2) => N1,
      ADDRA(1) => N1,
      ADDRA(0) => N1,
      ADDRB(13) => N1,
      ADDRB(12) => N1,
      ADDRB(11) => N1,
      ADDRB(10) => N1,
      ADDRB(9) => addrb(5),
      ADDRB(8) => addrb(4),
      ADDRB(7) => addrb(3),
      ADDRB(6) => addrb(2),
      ADDRB(5) => addrb(1),
      ADDRB(4) => addrb(0),
      ADDRB(3) => N1,
      ADDRB(2) => N1,
      ADDRB(1) => N1,
      ADDRB(0) => N1,
      DIA(15) => N1,
      DIA(14) => N1,
      DIA(13) => N1,
      DIA(12) => N1,
      DIA(11) => dina(7),
      DIA(10) => dina(6),
      DIA(9) => dina(5),
      DIA(8) => dina(4),
      DIA(7) => N1,
      DIA(6) => N1,
      DIA(5) => N1,
      DIA(4) => N1,
      DIA(3) => dina(3),
      DIA(2) => dina(2),
      DIA(1) => dina(1),
      DIA(0) => dina(0),
      DIB(15) => N1,
      DIB(14) => N1,
      DIB(13) => N1,
      DIB(12) => N1,
      DIB(11) => dinb(7),
      DIB(10) => dinb(6),
      DIB(9) => dinb(5),
      DIB(8) => dinb(4),
      DIB(7) => N1,
      DIB(6) => N1,
      DIB(5) => N1,
      DIB(4) => N1,
      DIB(3) => dinb(3),
      DIB(2) => dinb(2),
      DIB(1) => dinb(1),
      DIB(0) => dinb(0),
      DIPA(1) => N1,
      DIPA(0) => N1,
      DIPB(1) => N1,
      DIPB(0) => N1,
      DOA(15) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_15_UNCONNECTED,
      DOA(14) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_14_UNCONNECTED,
      DOA(13) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_13_UNCONNECTED,
      DOA(12) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_12_UNCONNECTED,
      DOA(11) => douta(7),
      DOA(10) => douta(6),
      DOA(9) => douta(5),
      DOA(8) => douta(4),
      DOA(7) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_7_UNCONNECTED,
      DOA(6) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_6_UNCONNECTED,
      DOA(5) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_5_UNCONNECTED,
      DOA(4) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOA_4_UNCONNECTED,
      DOA(3) => douta(3),
      DOA(2) => douta(2),
      DOA(1) => douta(1),
      DOA(0) => douta(0),
      DOB(15) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_15_UNCONNECTED,
      DOB(14) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_14_UNCONNECTED,
      DOB(13) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_13_UNCONNECTED,
      DOB(12) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_12_UNCONNECTED,
      DOB(11) => doutb(7),
      DOB(10) => doutb(6),
      DOB(9) => doutb(5),
      DOB(8) => doutb(4),
      DOB(7) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_7_UNCONNECTED,
      DOB(6) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_6_UNCONNECTED,
      DOB(5) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_5_UNCONNECTED,
      DOB(4) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOB_4_UNCONNECTED,
      DOB(3) => doutb(3),
      DOB(2) => doutb(2),
      DOB(1) => doutb(1),
      DOB(0) => doutb(0),
      DOPA(1) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPA_1_UNCONNECTED,
      DOPA(0) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPA_0_UNCONNECTED,
      DOPB(1) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPB_1_UNCONNECTED,
      DOPB(0) => 
NLW_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_v5_init_ram_TRUE_DP_SINGLE_PRIM18_TDP_DOPB_0_UNCONNECTED,
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEB(1) => web(0),
      WEB(0) => web(0)
    );

end STRUCTURE;

-- synthesis translate_on
