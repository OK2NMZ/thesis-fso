--==============================================================================
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--==============================================================================
ENTITY RAM_SPI_Wrapper IS
  PORT(
    clk                 : IN  STD_LOGIC;

    -- Processor interface (0x00 - 0x1F write)
    RAM_A_Addr_in       : IN  STD_LOGIC_VECTOR( 5 DOWNTO 0);
    RAM_A_Data_in       : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    RAM_A_Wr_EN         : IN  STD_LOGIC;
    RAM_A_Data_out      : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0);

    -- SPI interface (0x20 - 0x3F write)
    RAM_B_Addr_in       : IN  STD_LOGIC_VECTOR( 5 DOWNTO 0);
    RAM_B_Data_in       : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    RAM_B_Wr_EN         : IN  STD_LOGIC;
    RAM_B_Data_out      : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0));

END RAM_SPI_Wrapper;
--==============================================================================
ARCHITECTURE Behavioral OF RAM_SPI_Wrapper IS
--==============================================================================

  COMPONENT RAM_SPI_DualPort
  PORT(
    clka    : IN  STD_LOGIC;
    wea     : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra   : IN  STD_LOGIC_VECTOR(5 DOWNTO 0);
    dina    : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    douta   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    clkb    : IN  STD_LOGIC;
    web     : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb   : IN  STD_LOGIC_VECTOR(5 DOWNTO 0);
    dinb    : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    doutb   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));
END COMPONENT;

  ------------------------------------------------------------------------------

  SIGNAL WE_A       : STD_LOGIC_VECTOR(0 DOWNTO 0) := "0";
  SIGNAL WE_B       : STD_LOGIC_VECTOR(0 DOWNTO 0) := "0";

--==============================================================================
BEGIN
--==============================================================================

  ------------------------------------------------------------------------------
  -- HW protection of read-only sectors in Dual-port RAMs
  ------------------------------------------------------------------------------

  -- Processor interface (0x00 - 0x1F write)
  PROCESS(RAM_A_Wr_EN, RAM_A_Addr_in(5)) BEGIN
    IF RAM_A_Wr_EN = '1' AND RAM_A_Addr_in(5) = '0' THEN
      WE_A <= "1";
    ELSE
      WE_A <= "0";
    END IF;
  END PROCESS;

  -- SPI interface (0x20 - 0x3F write)
  PROCESS(RAM_B_Wr_EN, RAM_B_Addr_in(5)) BEGIN
    IF RAM_B_Wr_EN = '1' AND RAM_B_Addr_in(5) = '1' THEN
      WE_B <= "1";
    ELSE
      WE_B <= "0";
    END IF;
  END PROCESS;

  ------------------------------------------------------------------------------

  RAM_SPI_DualPort_i : RAM_SPI_DualPort
  PORT MAP (
    clka    => clk,
    wea     => WE_A,
    addra   => RAM_A_Addr_in,
    dina    => RAM_A_Data_in,
    douta   => RAM_A_Data_out,
    clkb    => clk,
    web     => WE_B,
    addrb   => RAM_B_Addr_in,
    dinb    => RAM_B_Data_in,
    doutb   => RAM_B_Data_out);

--==============================================================================
END Behavioral;
--==============================================================================
