----------------------------------------------------------------------------------
--
--      _______                                                     ______________
--  CSN        \___________________________________________________/
--
--                  ___     ___     ___     ___     ___     ___
--  SCK ___________/   \___/   \___/   \___/   \___/   \___/   \__________________
--
--      _______________ _______ _______ _______ _______ _______ __________________
--  SDI _______________X_______X_______X_______X_______X_______X__________________
--
--              ________ _______ _______ _______ _______ _______ __ 
--  SDO -------X________X_______X_______X_______X_______X_______X__X--------------
--
--
--
--
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--library UNISIM;
--use UNISIM.VComponents.all;
----------------------------------------------------------------------------------
ENTITY SPI_Slave IS
  PORT(
    clk                 : IN  STD_LOGIC;
    rst                 : IN  STD_LOGIC;

    -- SPI physical interface
    SPI_SCK             : IN  STD_LOGIC;        -- SCK
    SPI_SDI             : IN  STD_LOGIC;        -- MOSI
    SPI_SDO             : OUT STD_LOGIC;        -- MISO; tri-state when CSN = '1'
    SPI_CSN             : IN  STD_LOGIC;        -- /SS

    -- Dedicated status register
    SPI_Global_Status   : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);

    -- Dual-port RAM interface
    RAM_Address         : OUT STD_LOGIC_VECTOR( 5 DOWNTO 0) := "000000";
    RAM_Data_IN         : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0) := X"00";
    RAM_WR              : OUT STD_LOGIC;
    RAM_Data_OUT        : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0));

END SPI_Slave;
----------------------------------------------------------------------------------
ARCHITECTURE Behavioral OF SPI_Slave IS
----------------------------------------------------------------------------------

  TYPE t_state_SPI IS(
    st_idle,

    st_command_7_L,
    st_command_7_H,
    st_command_6_L,
    st_command_6_H,
    st_command_5_L,
    st_command_5_H,
    st_command_4_L,
    st_command_4_H,
    st_command_3_L,
    st_command_3_H,
    st_command_2_L,
    st_command_2_H,
    st_command_1_L,
    st_command_1_H,
    st_command_0_L,
    st_command_0_H,

    st_data_7_L,
    st_data_7_H,
    st_data_6_L,
    st_data_6_H,
    st_data_5_L,
    st_data_5_H,
    st_data_4_L,
    st_data_4_H,
    st_data_3_L,
    st_data_3_H,
    st_data_2_L,
    st_data_2_H,
    st_data_1_L,
    st_data_1_H,
    st_data_0_L,
    st_data_0_H,

    st_data_read_1,
    st_data_read_2,
    st_data_read_3,
    st_data_read_4);

  SIGNAL state_SPI              : t_state_SPI := st_idle;

  SIGNAL SPI_Global_Status_REG  : STD_LOGIC_VECTOR( 7 DOWNTO 0) := "00000000";

  SIGNAL SPI_Command_IN_REG     : STD_LOGIC_VECTOR( 7 DOWNTO 1) := (OTHERS => '0');
  SIGNAL SPI_Mode_REG           : STD_LOGIC_VECTOR( 1 DOWNTO 0) := (OTHERS => '0');
  SIGNAL SPI_Address_REG        : UNSIGNED( 5 DOWNTO 0) := (OTHERS => '0');
  SIGNAL SPI_Data_OUT_REG       : STD_LOGIC_VECTOR( 7 DOWNTO 0) := (OTHERS => '0');
  SIGNAL SPI_Data_IN_REG        : STD_LOGIC_VECTOR( 7 DOWNTO 1) := (OTHERS => '0');
  SIGNAL RAM_WR_i               : STD_LOGIC := '0';


  SIGNAL SPI_SCK_i              : STD_LOGIC := '0';
  SIGNAL SPI_SDI_i              : STD_LOGIC := '0';
  SIGNAL SPI_CSN_i              : STD_LOGIC := '0';

  SIGNAL SPI_SCK_ii             : STD_LOGIC := '0';
  SIGNAL SPI_SDI_ii             : STD_LOGIC := '0';
  SIGNAL SPI_CSN_ii             : STD_LOGIC := '0';

  SIGNAL SCK_rising             : STD_LOGIC := '0';
  SIGNAL SCK_falling            : STD_LOGIC := '0';
  SIGNAL CSN_rising             : STD_LOGIC := '0';
  SIGNAL CSN_falling            : STD_LOGIC := '0';

  SIGNAL SPI_SDO_i              : STD_LOGIC := '0';

  SIGNAL SPI_SDO_TriState       : STD_LOGIC := '1';

  CONSTANT C_Mode_Write         : STD_LOGIC_VECTOR( 1 DOWNTO 0) := "00";
  CONSTANT C_Mode_Read          : STD_LOGIC_VECTOR( 1 DOWNTO 0) := "01";
  CONSTANT C_Mode_Rd_Clr_Stat   : STD_LOGIC_VECTOR( 1 DOWNTO 0) := "10";
  CONSTANT C_Mode_Rd_Dev_Info   : STD_LOGIC_VECTOR( 1 DOWNTO 0) := "11";

--==============================================================================
BEGIN
--==============================================================================

  edge_detect: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      --------------------------------------------------------------------------
      IF rst = '1' THEN
        SCK_rising  <= '0';
        SCK_falling <= '0';
        CSN_rising  <= '0';
        CSN_falling <= '0';
      --------------------------------------------------------------------------
      ELSE

        -- default assignment
        SCK_rising  <= '0';
        SCK_falling <= '0';
        CSN_rising  <= '0';
        CSN_falling <= '0';

        SPI_SCK_i  <= SPI_SCK;
        SPI_SDI_i  <= SPI_SDI;
        SPI_CSN_i  <= SPI_CSN;

        SPI_SCK_ii <= SPI_SCK_i;
        SPI_SDI_ii <= SPI_SDI_i;
        SPI_CSN_ii <= SPI_CSN_i;

        -- edge detection
        IF SPI_SCK_ii = '0' AND SPI_SCK_i = '1' THEN SCK_rising  <= '1'; END IF;
        IF SPI_SCK_ii = '1' AND SPI_SCK_i = '0' THEN SCK_falling <= '1'; END IF;
        IF SPI_CSN_ii = '0' AND SPI_CSN_i = '1' THEN CSN_rising  <= '1'; END IF;
        IF SPI_CSN_ii = '1' AND SPI_CSN_i = '0' THEN CSN_falling <= '1'; END IF;
      --------------------------------------------------------------------------
      END IF;
    END IF;
  END PROCESS edge_detect;

  --============================================================================

  SDO_Tristate_buffer: PROCESS(SPI_SDO_TriState, SPI_CSN, SPI_SDO_i) BEGIN
    IF SPI_SDO_TriState = '1' OR SPI_CSN = '1' THEN
      SPI_SDO <= 'Z';
    ELSE
      SPI_SDO <= SPI_SDO_i;
    END IF;
  END PROCESS SDO_Tristate_buffer;

  --============================================================================

  -- Pipelined Moore FSM
  SPI_slave_FSM: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      --------------------------------------------------------------------------
      -- synchronous reset
      IF rst = '1' THEN
        state_SPI               <= st_idle;
        SPI_Data_OUT_REG        <= (OTHERS => '0');
        SPI_Address_REG         <= (OTHERS => '0');
        SPI_Data_IN_REG         <= (OTHERS => '0');
        SPI_Command_IN_REG      <= (OTHERS => '0');
        SPI_Mode_REG            <= (OTHERS => '0');
        RAM_WR_i                <= '0';
        SPI_Global_Status_REG   <= (OTHERS => '0');
        SPI_SDO_TriState        <= '1';
      ELSE
      --------------------------------------------------------------------------

        -- default assignment
        RAM_WR_i <= '0';

        -- FSM reset by CSN signal
        IF CSN_rising = '1' THEN
          state_SPI <= st_idle;
        ELSE
          ----------------------------------------------------------------------
          -- next state logic + state register
          CASE state_SPI IS

            WHEN st_idle            =>  IF  CSN_falling = '1' THEN
                                          state_SPI             <= st_command_7_L;
                                          SPI_Global_Status_REG <= SPI_Global_Status;
                                        END IF;

            WHEN st_command_7_L     =>  IF  SCK_rising  = '1' THEN state_SPI <= st_command_7_H; END IF;
            WHEN st_command_7_H     =>  IF  SCK_falling = '1' THEN state_SPI <= st_command_6_L; END IF;
            WHEN st_command_6_L     =>  IF  SCK_rising  = '1' THEN state_SPI <= st_command_6_H; END IF;
            WHEN st_command_6_H     =>  IF  SCK_falling = '1' THEN state_SPI <= st_command_5_L; END IF;
            WHEN st_command_5_L     =>  IF  SCK_rising  = '1' THEN state_SPI <= st_command_5_H; END IF;
            WHEN st_command_5_H     =>  IF  SCK_falling = '1' THEN state_SPI <= st_command_4_L; END IF;
            WHEN st_command_4_L     =>  IF  SCK_rising  = '1' THEN state_SPI <= st_command_4_H; END IF;
            WHEN st_command_4_H     =>  IF  SCK_falling = '1' THEN state_SPI <= st_command_3_L; END IF;
            WHEN st_command_3_L     =>  IF  SCK_rising  = '1' THEN state_SPI <= st_command_3_H; END IF;
            WHEN st_command_3_H     =>  IF  SCK_falling = '1' THEN state_SPI <= st_command_2_L; END IF;
            WHEN st_command_2_L     =>  IF  SCK_rising  = '1' THEN state_SPI <= st_command_2_H; END IF;
            WHEN st_command_2_H     =>  IF  SCK_falling = '1' THEN state_SPI <= st_command_1_L; END IF;
            WHEN st_command_1_L     =>  IF  SCK_rising  = '1' THEN state_SPI <= st_command_1_H; END IF;
            WHEN st_command_1_H     =>  IF  SCK_falling = '1' THEN state_SPI <= st_command_0_L; END IF;
            WHEN st_command_0_L     =>  IF  SCK_rising  = '1' THEN
                                          state_SPI         <= st_command_0_H;
                                          SPI_Address_REG   <= UNSIGNED(SPI_Command_IN_REG(5 DOWNTO 1) & SPI_SDI_ii);
                                          SPI_Mode_REG      <= SPI_Command_IN_REG(7 DOWNTO 6);
                                        END IF;
            WHEN st_command_0_H     =>  IF  SCK_falling = '1' THEN
                                          state_SPI         <= st_data_7_L;
                                          SPI_Data_OUT_REG  <= RAM_Data_OUT;
                                        END IF;

            WHEN st_data_7_L        =>  IF  SCK_rising  = '1' THEN state_SPI <= st_data_7_H;    END IF;
            WHEN st_data_7_H        =>  IF  SCK_falling = '1' THEN state_SPI <= st_data_6_L;    END IF;
            WHEN st_data_6_L        =>  IF  SCK_rising  = '1' THEN state_SPI <= st_data_6_H;    END IF;
            WHEN st_data_6_H        =>  IF  SCK_falling = '1' THEN state_SPI <= st_data_5_L;    END IF;
            WHEN st_data_5_L        =>  IF  SCK_rising  = '1' THEN state_SPI <= st_data_5_H;    END IF;
            WHEN st_data_5_H        =>  IF  SCK_falling = '1' THEN state_SPI <= st_data_4_L;    END IF;
            WHEN st_data_4_L        =>  IF  SCK_rising  = '1' THEN state_SPI <= st_data_4_H;    END IF;
            WHEN st_data_4_H        =>  IF  SCK_falling = '1' THEN state_SPI <= st_data_3_L;    END IF;
            WHEN st_data_3_L        =>  IF  SCK_rising  = '1' THEN state_SPI <= st_data_3_H;    END IF;
            WHEN st_data_3_H        =>  IF  SCK_falling = '1' THEN state_SPI <= st_data_2_L;    END IF;
            WHEN st_data_2_L        =>  IF  SCK_rising  = '1' THEN state_SPI <= st_data_2_H;    END IF;
            WHEN st_data_2_H        =>  IF  SCK_falling = '1' THEN state_SPI <= st_data_1_L;    END IF;
            WHEN st_data_1_L        =>  IF  SCK_rising  = '1' THEN state_SPI <= st_data_1_H;    END IF;
            WHEN st_data_1_H        =>  IF  SCK_falling = '1' THEN state_SPI <= st_data_0_L;    END IF;
            WHEN st_data_0_L        =>  IF  SCK_rising  = '1' THEN
                                          state_SPI <= st_data_0_H;
                                          -- write data to memory (after complete SPI WR transaction)
                                          IF SPI_Mode_REG = C_Mode_Write THEN
                                            RAM_Data_IN     <= SPI_Data_IN_REG(7 DOWNTO 1) & SPI_SDI_ii;
                                            RAM_WR_i        <= '1';
                                          END IF;
                                        END IF;

            WHEN st_data_0_H        =>  IF  SCK_falling = '1' THEN
                                          state_SPI         <= st_data_read_1;
                                          SPI_Address_REG   <= SPI_Address_REG + 1;
                                        END IF;

            WHEN st_data_read_1     =>  state_SPI        <= st_data_read_2;
            WHEN st_data_read_2     =>  state_SPI        <= st_data_read_3;
            WHEN st_data_read_3     =>  state_SPI        <= st_data_read_4;
            WHEN st_data_read_4     =>  state_SPI        <= st_data_7_L;
                                        SPI_Data_OUT_REG <= RAM_Data_OUT;

          END CASE;
  
          ----------------------------------------------------------------------
          -- Data output logic (SDO)
          CASE state_SPI IS

            WHEN st_idle            =>  SPI_SDO_i <= SPI_Global_Status(7);
            WHEN st_command_7_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(7);
            WHEN st_command_7_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(7);
            WHEN st_command_6_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(6);
            WHEN st_command_6_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(6);
            WHEN st_command_5_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(5);
            WHEN st_command_5_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(5);
            WHEN st_command_4_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(4);
            WHEN st_command_4_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(4);
            WHEN st_command_3_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(3);
            WHEN st_command_3_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(3);
            WHEN st_command_2_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(2);
            WHEN st_command_2_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(2);
            WHEN st_command_1_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(1);
            WHEN st_command_1_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(1);
            WHEN st_command_0_L     =>  SPI_SDO_i <= SPI_Global_Status_REG(0);
            WHEN st_command_0_H     =>  SPI_SDO_i <= SPI_Global_Status_REG(0);

            WHEN st_data_7_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(7);
            WHEN st_data_7_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(7);
            WHEN st_data_6_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(6);
            WHEN st_data_6_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(6);
            WHEN st_data_5_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(5);
            WHEN st_data_5_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(5);
            WHEN st_data_4_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(4);
            WHEN st_data_4_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(4);
            WHEN st_data_3_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(3);
            WHEN st_data_3_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(3);
            WHEN st_data_2_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(2);
            WHEN st_data_2_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(2);
            WHEN st_data_1_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(1);
            WHEN st_data_1_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(1);
            WHEN st_data_0_L        =>  SPI_SDO_i <= SPI_Data_OUT_REG(0);
            WHEN st_data_0_H        =>  SPI_SDO_i <= SPI_Data_OUT_REG(0);
  
            WHEN OTHERS             =>  NULL;

          END CASE;

          ----------------------------------------------------------------------
          -- Data input logic (SDI)
          CASE state_SPI IS

            WHEN st_command_7_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(7) <= SPI_SDI_ii; END IF;
            WHEN st_command_6_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(6) <= SPI_SDI_ii; END IF;
            WHEN st_command_5_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(5) <= SPI_SDI_ii; END IF;
            WHEN st_command_4_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(4) <= SPI_SDI_ii; END IF;
            WHEN st_command_3_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(3) <= SPI_SDI_ii; END IF;
            WHEN st_command_2_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(2) <= SPI_SDI_ii; END IF;
            WHEN st_command_1_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(1) <= SPI_SDI_ii; END IF;
          --WHEN st_command_0_L     =>  IF  SCK_rising  = '1' THEN SPI_Command_IN_REG(0) <= SPI_SDI_ii; END IF;

            WHEN st_data_7_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(7) <= SPI_SDI_ii; END IF;
            WHEN st_data_6_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(6) <= SPI_SDI_ii; END IF;
            WHEN st_data_5_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(5) <= SPI_SDI_ii; END IF;
            WHEN st_data_4_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(4) <= SPI_SDI_ii; END IF;
            WHEN st_data_3_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(3) <= SPI_SDI_ii; END IF;
            WHEN st_data_2_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(2) <= SPI_SDI_ii; END IF;
            WHEN st_data_1_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(1) <= SPI_SDI_ii; END IF;
          --WHEN st_data_0_L        =>  IF  SCK_rising  = '1' THEN    SPI_Data_IN_REG(0) <= SPI_SDI_ii; END IF;

            WHEN OTHERS             =>  NULL;

          END CASE;

          ----------------------------------------------------------------------

          -- Tri-State SDO output
          IF state_SPI = st_idle THEN
            SPI_SDO_TriState <= '1';
          ELSE
            SPI_SDO_TriState <= '0';
          END IF;

        ------------------------------------------------------------------------
        END IF;
      END IF;
    END IF;
  END PROCESS SPI_slave_FSM;

  RAM_Address   <= STD_LOGIC_VECTOR(SPI_Address_REG);
  RAM_WR        <= RAM_WR_i;

--==============================================================================
END Behavioral;
--==============================================================================
