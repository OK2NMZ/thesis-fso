-------------------------------------------------------------------------------
--! @file
--! @brief UART transceiver to be used with PicoBlaze softcore 8-bit MCU
--!
--! 
-------------------------------------------------------------------------------

-----------------------------------------------------------------------
--
-- PicoBlaze  UART Transceiver
--
----------------------------------------------------------------------
----------------------------------------------------------------------
--   period vale for typical data rates and frequences   |-----------|
----------------------------------------------------------------------
-------------|              clk  frequency               |-----------|
---------------------------------------------------------| max cable |
-- Baud Rate |   50 MHz |  100 MHz | 62,5 MHz |19,44 MHz |  length   |
-------------|----------|----------|----------|----------|-----------|
--      110  |  454 545 |  909 091 |  568 182 |  176 727 |           |
--      300  |  166 667 |  333 333 |  208 333 |   64 800 |           |
--      600  |   83 333 |  166 667 |  104 167 |   32 400 |           |
--    1 200  |   41 667 |   83 333 |   52 083 |   16 200 |           |
--    2 400  |   20 833 |   41 667 |   26 042 |    8 100 |   900m    |
--    4 800  |   10 417 |   20 833 |   13 021 |    4 050 |   300m    |
--    9 600  |    5 208 |   10 417 |    6 510 |    2 025 |   150m    |
--   14 400  |    3 472 |    6 944 |    4 340 |    1 350 |           |
--   19 200  |    2 604 |    5 208 |    3 255 |    1 013 |    15m    |
--   38 400  |    1 302 |    2 604 |    1 628 |      506 |           |
--   57 600  |      868 |    1 736 |    1 085 |      338 |           |
--  115 200  |      434 |      868 |      543 |      169 |           |
----------------------------------------------------------------------
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-----------------------------------------------------------------------

-------------------------------------------------------------------------------
ENTITY UART_block IS
  GENERIC(
    period              : INTEGER := 5208);                             --! 9600 Bd  @  50 MHz
  PORT(
    clk                 : IN  STD_LOGIC;                                --! system clock
    UART_Rx             : IN  STD_LOGIC;                                --! UART serial input
    UART_Tx             : OUT STD_LOGIC := '0';                         --! UART serial output
  
    UART_write          : IN  STD_LOGIC := '0';                         --! write data  to  UART buffer
    UART_read           : IN  STD_LOGIC := '0';                         --!  read data from UART buffer
  
    UART_reset_reg      : IN  STD_LOGIC_VECTOR(1 DOWNTO 0):= "00";      --! Reset IN reg (from PicoBlaze output MUX); (0) Tx, (1) Rx
    UART_Data_in_reg    : IN  STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00";     --! Data IN reg (from PicoBlaze output MUX)
  
    UART_Status_reg     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00";     --! Status OUT reg (to PicoBlaze input MUX)
                                                                        -- (0) = TX_data_present;
                                                                        -- (1) = TX_half_full;
                                                                        -- (2) = TX_full;
                                                                        -- (3) = RX_data_present;
                                                                        -- (4) = RX_half_full;
                                                                        -- (5) = RX_full;
  
    UART_Data_out_reg   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00");    --! Data OUT reg (to PicoBlaze input MUX)
END UART_block;

-------------------------------------------------------------------------------

ARCHITECTURE Behavioral OF UART_block IS

  COMPONENT UART_Basic_Rx
  GENERIC(
    period      : INTEGER := 5208);
  PORT(
    clk         : IN  STD_LOGIC;                            -- system clock
    rst         : IN  STD_LOGIC;                            -- reset
    Rx          : IN  STD_LOGIC;                            -- UART serial input
    Data        : OUT STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00"; -- received data
    Data_Ready  : OUT STD_LOGIC:= '0');                     -- new data received (check for rising edge;
                                                            -- pulse duration is equal to clk period)
  END COMPONENT;
  ---------------------------------------------------------------------
  COMPONENT UART_Basic_Tx
  GENERIC( 
    period      : INTEGER := 5208;
    stop_bits   : INTEGER := 3);
  PORT(
    clk         : IN  STD_LOGIC;
    rst         : IN  STD_LOGIC;
    Data        : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    Start       : IN  STD_LOGIC;
    Tx          : OUT STD_LOGIC;
    Ready       : OUT STD_LOGIC);
  END COMPONENT;
  ---------------------------------------------------------------------
  COMPONENT UART_buffer
  PORT(
    clk         : IN  STD_LOGIC;
    srst        : IN  STD_LOGIC;
    din         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    wr_en       : IN  STD_LOGIC;
    rd_en       : IN  STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    full        : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC;
    prog_empty  : OUT STD_LOGIC);
  END COMPONENT;
  ---------------------------------------------------------------------

  SIGNAL Tx_FIFO_rd         : STD_LOGIC := '0';
  SIGNAL Tx_FIFO_dout       : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL Tx_FIFO_full       : STD_LOGIC;
  SIGNAL Tx_FIFO_empty      : STD_LOGIC;
  SIGNAL Tx_FIFO_P_empty    : STD_LOGIC;
  
  
  SIGNAL Rx_FIFO_din        : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL Rx_FIFO_wr         : STD_LOGIC;

  SIGNAL Rx_FIFO_rd         : STD_LOGIC;
  SIGNAL Rx_FIFO_full       : STD_LOGIC;
  SIGNAL Rx_FIFO_empty      : STD_LOGIC;
  SIGNAL Rx_FIFO_P_empty    : STD_LOGIC;

  TYPE t_FIFO_rd IS (st_idle, st_rd_FIFO_1, st_rd_FIFO_2, st_rd_FIFO_3, st_wr_UART);
  SIGNAL st_Tx_FIFO_rd      : t_FIFO_rd := st_idle;
  SIGNAL st_Rx_FIFO_rd      : t_FIFO_rd := st_idle;

  SIGNAL Tx_start           : STD_LOGIC := '0';
  SIGNAL Tx_Ready           : STD_LOGIC;
  
-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  UART_Basic_Tx_i : UART_Basic_Tx
  GENERIC MAP(
    period      => period,
    stop_bits   => 3)
  PORT MAP(
    clk         => clk,
    rst         => UART_reset_reg(0),
    Data        => Tx_FIFO_dout,
    Start       => Tx_Start,
    Tx          => UART_Tx,
    Ready       => Tx_Ready);

  ---------------------------------------------------------------------

  UART_buffer_Tx : UART_buffer
  PORT MAP(
    clk         => clk,
    srst        => UART_reset_reg(0),
    din         => UART_Data_in_reg,
    wr_en       => UART_write,
    rd_en       => Tx_FIFO_rd,
    dout        => Tx_FIFO_dout,
    full        => Tx_FIFO_full,
    empty       => Tx_FIFO_empty,
    prog_empty  => Tx_FIFO_P_empty);
  
  UART_Status_reg(0) <= NOT Tx_FIFO_empty;      -- (0) = TX_data_present;
  UART_Status_reg(1) <= NOT Tx_FIFO_P_empty;    -- (1) = TX_half_full;
  UART_Status_reg(2) <= Tx_FIFO_full;           -- (2) = TX_full;

  ---------------------------------------------------------------------

  UART_Basic_Rx_i : UART_Basic_Rx
  GENERIC MAP(
    period      => period)
  PORT MAP(
    clk         => clk,
    rst         => UART_reset_reg(1),
    Rx          => UART_Rx,
    Data        => RX_FIFO_din,
    Data_Ready  => RX_FIFO_wr);

  ---------------------------------------------------------------------

  UART_buffer_Rx : UART_buffer
  PORT MAP(
    clk         => clk,
    srst        => UART_reset_reg(1),
    din         => RX_FIFO_din,
    wr_en       => RX_FIFO_wr,
    rd_en       => Rx_FIFO_rd,
    dout        => UART_Data_out_reg,
    full        => Rx_FIFO_full,
    empty       => Rx_FIFO_empty,
    prog_empty  => Rx_FIFO_P_empty);

  --UART_Status_reg(3) <= ;      -- (3) = RX_data_present;
  UART_Status_reg(4) <= NOT Rx_FIFO_P_empty;    -- (4) = RX_half_full;
  UART_Status_reg(5) <= Rx_FIFO_full;           -- (5) = RX_full;

  ---------------------------------------------------------------------
  
  
-----------------------------------------------------------------------------
--! @brief Process to manage reading from RX FIFO
--! @vhdlflow
--!
--! Implements the logic of the RX FIFO
--!
--! @param[in]   clk  Clock, used on rising edge
-----------------------------------------------------------------------------
  Rx_FIFO_read_proc: PROCESS (clk) BEGIN
    IF rising_edge(clk) THEN
      IF UART_reset_reg(1) = '1' THEN
        Rx_FIFO_rd  <= '0';
        UART_Status_reg(3) <= '0';      -- (3) = RX_data_present;
      ELSE
        Rx_FIFO_rd  <= '0';
        CASE st_Rx_FIFO_rd IS
          WHEN st_idle      =>  IF Rx_FIFO_empty = '0' THEN
                                  st_Rx_FIFO_rd <= st_rd_FIFO_1;
                                  Rx_FIFO_rd  <= '1';
                                END IF;

          WHEN st_rd_FIFO_1 =>  UART_Status_reg(3) <= '1';          -- (3) = RX_data_present;
                                IF UART_read = '1' THEN
                                  IF Rx_FIFO_empty = '1' THEN
                                    UART_Status_reg(3) <= '0';      -- (3) = RX_data_present;
                                    st_Rx_FIFO_rd <= st_idle;
                                  ELSE
                                    Rx_FIFO_rd  <= '1';
                                  END IF;
                                END IF;

          WHEN OTHERS       =>  st_Rx_FIFO_rd <= st_idle;

        END CASE;

      END IF;
    END IF;
  END PROCESS Rx_FIFO_read_proc;
  

-----------------------------------------------------------------------------
--! @brief Process to manage reading from TX FIFO
--! @vhdlflow
--!
--! Implements the logic of the TX FIFO
--!
--! @param[in]   clk  Clock, used on rising edge
-----------------------------------------------------------------------------
  Tx_FIFO_read_proc: PROCESS (clk) BEGIN
    IF rising_edge(clk) THEN
      IF UART_reset_reg(0) = '1' THEN
        Tx_FIFO_rd  <= '0';
        Tx_Start    <= '0';
      ELSE
        Tx_Start    <= '0';
        Tx_FIFO_rd  <= '0';
        CASE st_Tx_FIFO_rd IS
          WHEN st_idle      =>  IF Tx_FIFO_empty = '0' AND Tx_Ready = '1' THEN
                                  st_Tx_FIFO_rd <= st_rd_FIFO_1;
                                  Tx_FIFO_rd  <= '1';
                                END IF;
          WHEN st_rd_FIFO_1 =>  st_Tx_FIFO_rd <= st_rd_FIFO_2;
          WHEN st_rd_FIFO_2 =>  st_Tx_FIFO_rd <= st_rd_FIFO_3;
          WHEN st_rd_FIFO_3 =>  st_Tx_FIFO_rd <= st_wr_UART;
                                Tx_Start <= '1';

          WHEN st_wr_UART   =>  IF Tx_Ready = '0' THEN
                                  st_Tx_FIFO_rd <= st_idle;
                                END IF;
        END CASE;

      END IF;
    END IF;
  END PROCESS Tx_FIFO_read_proc;

  -----------------------------------------------------------------------
END Behavioral;
-------------------------------------------------------------------------------------------
