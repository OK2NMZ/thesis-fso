-----------------------------------------------------------------------
-- UART Rx module
--  Parity not supported, single stop bit.
-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-----------------------------------------------------------------------
ENTITY UART_Basic_Rx IS
    GENERIC( period:   INTEGER := 5208);                            -- 9600 Bd @ 50 MHz clk
    PORT( clk           : IN  STD_LOGIC;                            -- system clock
          rst           : IN  STD_LOGIC;                            -- reset
          Rx            : IN  STD_LOGIC;                            -- UART serial input
          Data          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0):= X"00"; -- received data
          Data_Ready    : OUT STD_LOGIC:= '0');                     -- new data received (check for rising edge;
                                                                    -- pulse duration is equal to clk period)
END UART_Basic_Rx;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF UART_Basic_Rx IS

  TYPE Rx_state IS (idle, startb, bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7, stopb);
  SIGNAL current_state  : Rx_state := idle;               -- control of transmission

  SIGNAL data_int       : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');    -- internal data buffer
  SIGNAL prev_Rx        : STD_LOGIC := '1';
  SIGNAL busy           : STD_LOGIC := '0';
    
  SIGNAL clk_EN         : STD_LOGIC := '0';               -- clock enable (frequency = baud rate)
  SIGNAL clk_gen        : STD_LOGIC_VECTOR(20 DOWNTO 0) := (OTHERS => '0');

  SIGNAL Rx_in_shr      : STD_LOGIC_VECTOR( 3 DOWNTO 0) := (OTHERS => '1');   -- shift register for metastability removal and edge detection
-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  clk_gen_proc: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF rst = '1' THEN
        clk_EN  <= '0';
        clk_gen <= (OTHERS => '0');
      ELSE
        clk_EN  <= '0';
        clk_gen <= clk_gen + 1;
    
        IF busy = '0' THEN
          clk_gen <= (OTHERS => '0');
        ELSE
    
          IF clk_gen = CONV_STD_LOGIC_VECTOR(period-1,clk_GEN'HIGH+1) THEN        -- whole bit period
          clk_gen <= (OTHERS => '0');
          END IF;
    
          IF clk_gen = CONV_STD_LOGIC_VECTOR(period/2-2,clk_GEN'HIGH+1) THEN        -- half of the bit period
          clk_EN <='1';
          END IF;
    
        END IF;
      END IF;
    END IF;
  END PROCESS clk_gen_proc;

-----------------------------------------------------------------------
-- data receive process
  receive_proc: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF rst = '1' THEN
        Data_Ready    <= '0';
        current_state <= idle;
        busy          <= '0';
      ELSE
        -- sampling input for edge detection and metastability prevention
        Rx_in_shr     <= Rx_in_shr(2 DOWNTO 0) & Rx;

        -- default assignment
        Data_Ready    <= '0';

        -- data sampling FSM
        IF clk_EN = '1' THEN                   -- middle of the data bit
          CASE (current_state) IS

            WHEN idle   => NULL;                              -- idle state (no operation)
    
            WHEN startb => current_state  <= bit0;            -- start bit (no operation)
            
            WHEN bit0   => data_int(0)    <= Rx_in_shr(3);    -- bit 0 (LSB)
                            current_state  <= bit1;
            
            WHEN bit1   => data_int(1)    <= Rx_in_shr(3);    -- bit 1
                            current_state  <= bit2;
            
            WHEN bit2   => data_int(2)    <= Rx_in_shr(3);    -- bit 2
                            current_state  <= bit3;
            
            WHEN bit3   => data_int(3)    <= Rx_in_shr(3);    -- bit 3
                            current_state  <= bit4;
            
            WHEN bit4   => data_int(4)    <= Rx_in_shr(3);    -- bit 4
                            current_state  <= bit5;
            
            WHEN bit5   => data_int(5)    <= Rx_in_shr(3);    -- bit 5
                            current_state  <= bit6;
            
            WHEN bit6   => data_int(6)    <= Rx_in_shr(3);    -- bit 6
                            current_state  <= bit7;
            
            WHEN bit7   => data_int(7)    <= Rx_in_shr(3);    -- bit 7
                            current_state  <= stopb;
            
            WHEN stopb  => Data_Ready     <= Rx_in_shr(3);    -- "Data_Ready" = '1' when correct stop-bit is detected
                            current_state  <= idle;
                            Data           <= data_int;
                            busy           <= '0';
          END CASE;
        END IF;

        ---------------------------------------------------------------
        -- Start bit detection (FSM reset)
        IF Rx_in_shr(2) = '0' AND Rx_in_shr(3) = '1' AND busy = '0' THEN
          current_state  <= startb;
          busy           <= '1';
        END IF;
      -----------------------------------------------------------------
      END IF;
    END IF;
  END PROCESS receive_proc;

-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
