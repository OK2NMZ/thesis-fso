-----------------------------------------------------------------------
-- UART Tx module, input data vector is stored when "Start" is asserter
--  (module must be "Ready" before the "Start" is asserted). Module
--  doesn't supports parity, supports multiple stop bits to enhance
--  reliability on receiver side.
-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-----------------------------------------------------------------------
ENTITY UART_Basic_Tx IS
    GENERIC( period     : INTEGER := 5208;
             stop_bits  : INTEGER := 3);                    -- 1, 2 or 3 stop bits
    PORT( clk           : IN  STD_LOGIC;                    -- System clock
          rst           : IN  STD_LOGIC;                    -- reset
          Data          : IN  STD_LOGIC_VECTOR(7 DOWNTO 0); -- data to be sent
          Start         : IN  STD_LOGIC;                    -- start of transmision
          Tx            : OUT STD_LOGIC;                    -- UART output
          Ready         : OUT STD_LOGIC);                   -- ready to sent next byte
END UART_Basic_Tx;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF UART_Basic_Tx IS

  TYPE Tx_state IS (idle, startb, bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7, stopb0, stopb1);

  SIGNAL current_state:     Tx_state := idle;               -- control of transmission
  SIGNAL data_int:          STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0'); -- internal data buffer
  SIGNAL busy:              STD_LOGIC := '0';

  SIGNAL clk_gen:           STD_LOGIC_VECTOR(20 DOWNTO 0) := (OTHERS => '0');
  SIGNAL clk_EN:            STD_LOGIC := '0';               -- clock enable (frequency = baud rate)
-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  Ready <= NOT Busy;

  ---------------------------------------------------------------------

  -- generate clock enable signal with frequency eaquel to baud rate
  clk_gen_proc: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF rst = '1' THEN
        clk_gen <= (OTHERS => '0');
        clk_EN <='0';
      ELSE
        IF clk_gen = CONV_STD_LOGIC_VECTOR(period-1,clk_GEN'HIGH+1) THEN
          clk_gen <= (OTHERS => '0');
          clk_EN <='1';
        ELSE
          clk_gen <= clk_gen + 1;
          clk_EN <='0';
        END IF;
      END IF;
    END IF;
  END PROCESS clk_gen_proc;

  ---------------------------------------------------------------------
  Tx_decoder: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      CASE current_state IS
        WHEN idle     =>  Tx <= '1';
        WHEN startb   =>  Tx <= '0';
        WHEN bit0     =>  Tx <= data_int(0);
        WHEN bit1     =>  Tx <= data_int(1);
        WHEN bit2     =>  Tx <= data_int(2);
        WHEN bit3     =>  Tx <= data_int(3);
        WHEN bit4     =>  Tx <= data_int(4);
        WHEN bit5     =>  Tx <= data_int(5);
        WHEN bit6     =>  Tx <= data_int(6);
        WHEN bit7     =>  Tx <= data_int(7);
        WHEN stopb0   =>  Tx <= '1';
        WHEN stopb1   =>  Tx <= '1';
      END CASE;
    END IF;
  END PROCESS Tx_decoder;

  ---------------------------------------------------------------------
  Tx_FSM: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN

      IF rst = '1' THEN
        busy <= '0';
        current_state <= idle;
      ELSE
        -- capture Tx start request
        IF busy = '0' AND Start = '1' THEN
            busy        <= '1';     -- disable acces to UART ("Ready" is set to '0')
            data_int    <= Data;    -- copy data to internal buffer
        END IF;
    
        IF clk_EN = '1' THEN
            CASE current_state IS
            WHEN idle     =>  IF busy = '1' THEN current_state <= startb; END IF;
            WHEN startb   =>  current_state <= bit0;
            WHEN bit0     =>  current_state <= bit1;
            WHEN bit1     =>  current_state <= bit2;
            WHEN bit2     =>  current_state <= bit3;
            WHEN bit3     =>  current_state <= bit4;
            WHEN bit4     =>  current_state <= bit5;
            WHEN bit5     =>  current_state <= bit6;
            WHEN bit6     =>  current_state <= bit7;
            WHEN bit7     =>  IF stop_bits = 1 THEN current_state <= idle;   busy <= '0';
                                               ELSE current_state <= stopb0; END IF;
    
            WHEN stopb0   =>  IF stop_bits = 2 THEN current_state <= idle;   busy <= '0';
                                               ELSE current_state <= stopb1; END IF;
    
            WHEN stopb1   =>                        current_state <= idle;   busy <= '0';
    
            END CASE;
        END IF;
      END IF;
    END IF;
  END PROCESS Tx_FSM;

-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
