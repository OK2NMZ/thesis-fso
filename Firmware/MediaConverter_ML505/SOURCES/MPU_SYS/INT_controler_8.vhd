-----------------------------------------------------------------------
-- PicoBlaze IRQ controller
--
-- reg_int_en_in:       [x] enable interrupt x
-- reg_int_sens_in:     [x] interrupt x edge sensitive (when H)
--
-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-----------------------------------------------------------------------
ENTITY INT_controler_8 IS
  PORT(
    clk                 : IN  STD_LOGIC;
    rst                 : IN  STD_LOGIC;
    int_in              : IN  STD_LOGIC_VECTOR(8 DOWNTO 1);   -- 1 = highest priority, 8 = lowest priority
    reg_int_status      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);   -- Active interrupt number OUT (PicoBlaze IN)
    reg_int_en_in       : IN  STD_LOGIC_VECTOR(8 DOWNTO 1);   -- Enable status reg OUT (PicoBlaze IN)
    reg_int_sense_in    : IN  STD_LOGIC_VECTOR(8 DOWNTO 1);   -- Edge status reg OUT (PicoBlaze IN)
    proc_irq            : OUT STD_LOGIC;
    proc_irq_ack        : IN  STD_LOGIC);
END INT_controler_8;
-----------------------------------------------------------------------
ARCHITECTURE Behavioral OF INT_controler_8 IS
-----------------------------------------------------------------------
  SIGNAL int_buf            : STD_LOGIC_VECTOR(8 DOWNTO 1) := (OTHERS => '0');      -- request buffer for edge detection
  SIGNAL int_i              : STD_LOGIC_VECTOR(8 DOWNTO 1) := (OTHERS => '0');      -- active interrupt requests (both edge and level)

  SIGNAL reg_int_status_i   : STD_LOGIC_VECTOR(3 DOWNTO 0):= X"0";                  -- Active interrupt number (internal)

-----------------------------------------------------------------------
BEGIN
-----------------------------------------------------------------------

  int_proc: PROCESS(clk) BEGIN
  ---------------------------------------------------------------------
    IF rising_edge(clk) THEN
    -------------------------------------------------------------------
      IF rst = '1' THEN
        int_i <= (OTHERS => '0');
      -----------------------------------------------------------------
      ELSE
      -----------------------------------------------------------------
        int_buf <= int_in;      -- buffering interrupt signals for edge detection
        ---------------------------------------------------------------

        ---------------------------------------------------------------
        -- Interrupt channel #1
        IF reg_int_en_in(1) = '0' THEN                  -- interrupt disabled
          int_i(1) <= '0';
        ELSE                                            -- interrupt enabled
          IF reg_int_sense_in(1) = '1' THEN             -- edge sensitive interrupt
            IF int_in(1) = '1' AND int_buf(1) = '0' THEN                    -- set interrupt request (edge detection)
              int_i(1) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"1" THEN       -- clear interrupt request
              int_i(1) <= '0';
            END IF;
          ELSE                                          -- level sensitive interrupt
            int_i(1) <= int_in(1);
          END IF;
        END IF;

        ---------------------------------------------------------------
        -- Interrupt channel #2
        IF reg_int_en_in(2) = '0' THEN
          int_i(2) <= '0';
        ELSE
          IF reg_int_sense_in(2) = '1' THEN
            IF int_in(2) = '1' AND int_buf(2) = '0' THEN
              int_i(2) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"2" THEN
              int_i(2) <= '0';
            END IF;
          ELSE
            int_i(2) <= int_in(2);
          END IF;
        END IF;

        ---------------------------------------------------------------
        -- Interrupt channel #3
        IF reg_int_en_in(3) = '0' THEN
          int_i(3) <= '0';
        ELSE
          IF reg_int_sense_in(3) = '1' THEN
            IF int_in(3) = '1' AND int_buf(3) = '0' THEN
              int_i(3) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"3" THEN
              int_i(3) <= '0';
            END IF;
          ELSE
            int_i(3) <= int_in(3);
          END IF;
        END IF;

        ---------------------------------------------------------------
        -- Interrupt channel #4
        IF reg_int_en_in(4) = '0' THEN
          int_i(4) <= '0';
        ELSE
          IF reg_int_sense_in(4) = '1' THEN
            IF int_in(4) = '1' AND int_buf(4) = '0' THEN
              int_i(4) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"4" THEN
              int_i(4) <= '0';
            END IF;
          ELSE
            int_i(4) <= int_in(4);
          END IF;
        END IF;

        ---------------------------------------------------------------
        -- Interrupt channel #5
        IF reg_int_en_in(5) = '0' THEN
          int_i(5) <= '0';
        ELSE
          IF reg_int_sense_in(5) = '1' THEN
            IF int_in(5) = '1' AND int_buf(5) = '0' THEN
              int_i(5) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"5" THEN
              int_i(5) <= '0';
            END IF;
          ELSE
            int_i(5) <= int_in(5);
          END IF;
        END IF;

        ---------------------------------------------------------------
        -- Interrupt channel #6
        IF reg_int_en_in(6) = '0' THEN
          int_i(6) <= '0';
        ELSE
          IF reg_int_sense_in(6) = '1' THEN
            IF int_in(6) = '1' AND int_buf(6) = '0' THEN
              int_i(6) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"6" THEN
              int_i(6) <= '0';
            END IF;
          ELSE
            int_i(6) <= int_in(6);
          END IF;
        END IF;

        ---------------------------------------------------------------
        -- Interrupt channel #7
        IF reg_int_en_in(7) = '0' THEN
          int_i(7) <= '0';
        ELSE
          IF reg_int_sense_in(7) = '1' THEN
            IF int_in(7) = '1' AND int_buf(7) = '0' THEN
              int_i(7) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"7" THEN
              int_i(7) <= '0';
            END IF;
          ELSE
            int_i(7) <= int_in(7);
          END IF;
        END IF;

        ---------------------------------------------------------------
        -- Interrupt channel #8
        IF reg_int_en_in(8) = '0' THEN
          int_i(8) <= '0';
        ELSE
          IF reg_int_sense_in(8) = '1' THEN
            IF int_in(8) = '1' AND int_buf(8) = '0' THEN
              int_i(8) <= '1';
            ELSIF proc_irq_ack = '1' AND reg_int_status_i = X"8" THEN
              int_i(8) <= '0';
            END IF;
          ELSE
            int_i(8) <= int_in(8);
          END IF;
        END IF;

        ---------------------------------------------------------------
        ---------------------------------------------------------------
        IF    int_i(1) = '1' THEN reg_int_status_i <= X"1";
        ELSIF int_i(2) = '1' THEN reg_int_status_i <= X"2";
        ELSIF int_i(3) = '1' THEN reg_int_status_i <= X"3";
        ELSIF int_i(4) = '1' THEN reg_int_status_i <= X"4";
        ELSIF int_i(5) = '1' THEN reg_int_status_i <= X"5";
        ELSIF int_i(6) = '1' THEN reg_int_status_i <= X"6";
        ELSIF int_i(7) = '1' THEN reg_int_status_i <= X"7";
        ELSIF int_i(8) = '1' THEN reg_int_status_i <= X"8";
                             ELSE reg_int_status_i <= X"F";
        END IF;
      END IF;
    -------------------------------------------------------------------
    END IF;
  END PROCESS int_proc;

-----------------------------------------------------------------------

  proc_irq <= int_i(1) OR int_i(2) OR int_i(3) OR int_i(4) OR int_i(5) OR int_i(6) OR int_i(7) OR int_i(8);

  reg_int_status <= X"0" & reg_int_status_i;

-----------------------------------------------------------------------
END Behavioral;
-----------------------------------------------------------------------
