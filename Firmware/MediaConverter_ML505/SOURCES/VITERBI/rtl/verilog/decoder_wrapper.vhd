-------------------------------------------------------------------------------
--! @file
--! @brief Wrapper for the convolutional viterbi decoder
--! 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoder_wrapper is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           srst : in  STD_LOGIC;
           symbol0 : in  STD_LOGIC;
           symbol1 : in  STD_LOGIC;
           bit_out : out  STD_LOGIC);
end decoder_wrapper;

architecture Behavioral of decoder_wrapper is

    component decoder IS
        PORT (
          mclk                    : IN STD_LOGIC;
          rst                     : IN STD_LOGIC;
          srst                    : IN STD_LOGIC;
          valid_in                : IN STD_LOGIC;
          symbol0                 : IN STD_LOGIC_VECTOR(3 - 1 DOWNTO 0);
          symbol1                 : IN STD_LOGIC_VECTOR(3 - 1 DOWNTO 0);
          pattern                 : IN STD_LOGIC_VECTOR(2 - 1 DOWNTO 0);
          tb_dir						      : IN STD_LOGIC;
          bit_out                 : OUT STD_LOGIC;
          valid_out               : OUT STD_LOGIC;
          tb_dir_o                : OUT STD_LOGIC;
          traceback_error         : OUT STD_LOGIC;
          filo_error              : OUT STD_LOGIC;
          tbdir_mod_err           : OUT STD_LOGIC);
    end component;
    
    SIGNAL symbol0_i : STD_LOGIC_VECTOR(2 DOWNTO 0) := (OTHERS => '0');
    SIGNAL symbol1_i : STD_LOGIC_VECTOR(2 DOWNTO 0) := (OTHERS => '0');
    SIGNAL bit_out_i : STD_LOGIC := '0';

begin

 -----------------------------------------------------------------------------
--! @brief Process to convert the soft decision input to hard decision
--! @vhdlflow
--!
--!
--! @param[in]   clk  Clock, used on rising edge
-----------------------------------------------------------------------------
 soft_to_hard: PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF symbol0 = '1' THEN
        symbol0_i <= "111";
      ELSE
        symbol0_i <= "000";
      END IF;
      
      IF symbol1 = '1' THEN
        symbol1_i <= "111";
      ELSE
        symbol1_i <= "000";
      END IF;
    END IF;
  END PROCESS soft_to_hard;  

  dec : decoder
  PORT MAP (
    mclk => clk,
    rst => rst,
    srst => srst,
    valid_in => '1',
    symbol0 => symbol0_i,
    symbol1 => symbol1_i,
    pattern => "11", --! no puncturing
    tb_dir => '0',
    
    bit_out => bit_out_i,
    valid_out => OPEN,

    traceback_error => OPEN, --! error data
    filo_error => OPEN,
    tbdir_mod_err => OPEN
  );

  bit_out <= bit_out_i;

end Behavioral;

