-------------------------------------------------------------------------------
--! @file
--! @brief Wrapper for the convolutional encoder
--! 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity encoder_wrapper is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           srst : in  STD_LOGIC;
           bit_in : in  STD_LOGIC;
           symbol0 : out  STD_LOGIC;
           symbol1 : out  STD_LOGIC);
end encoder_wrapper;

architecture Behavioral of encoder_wrapper is

  component encoder IS
  PORT (
    clock                   : IN std_logic;
    reset                   : IN std_logic;
    srst                    : IN std_logic;
    frm_end_i               : IN std_logic;
    bit_in                  : IN std_logic;
    valid_in                : IN std_logic;
    symbol0                 : OUT std_logic;
    symbol1                 : OUT std_logic;
    valid_out               : OUT std_logic;
    frm_end_o               : OUT std_logic);
  end component;
  
  SIGNAL symbol0_i : STD_LOGIC := '0';
  SIGNAL symbol1_i : STD_LOGIC := '0';

begin

  enc : encoder
    PORT MAP (
      clock => clk,
      reset => rst,
      srst => srst,
      frm_end_i => '1',
      bit_in => bit_in,
      valid_in => '1',
      symbol0 => symbol0_i,
      symbol1 => symbol1_i,
      valid_out => OPEN,
      frm_end_o => OPEN
    );
    
end Behavioral;

