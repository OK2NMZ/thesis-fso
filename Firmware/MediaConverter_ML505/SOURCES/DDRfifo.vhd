----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 18:49:54 02/04/2016
-- Design Name:
-- Module Name: DDRfifo - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
LIBRARY unisim; -- Required for Xilinx primitives
USE unisim.ALL;
ENTITY DDRfifo IS
	GENERIC (
		APPDATA_WIDTH : INTEGER := 128;
		-- # of usr read/write data bus bits.
		FIFO_SIZE_POW2 : INTEGER := 25
		-- length of the FIFO is 2^25 bytes
	);
	PORT (
		-- FIFO compatible interface, parsing SOF and EOF
		rst : IN STD_LOGIC := '0';
		wr_clk : IN STD_LOGIC := '0';
		rd_clk : IN STD_LOGIC := '0';
		din : IN STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		wr_en : IN STD_LOGIC := '0';
		rd_en : IN STD_LOGIC := '0';
		dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0) := (OTHERS => '0');
		full : OUT STD_LOGIC := '1';
		empty : OUT STD_LOGIC := '1';
		empty_prog : OUT STD_LOGIC := '1';
		-- DDR application interface
		sys_rst_n : OUT std_logic := '0';
		phy_init_done : IN std_logic := '0';
		rst0_tb : IN std_logic := '0';
		clk0_tb : IN std_logic := '0';
		app_wdf_afull : IN std_logic := '0';
		app_af_afull : IN std_logic := '0';
		rd_data_valid : IN std_logic := '0';
		app_wdf_wren : OUT std_logic := '0';
		app_af_wren : OUT std_logic := '0';
		app_af_addr : OUT std_logic_vector(30 DOWNTO 0) := (OTHERS => '0');
		app_af_cmd : OUT std_logic_vector(2 DOWNTO 0) := (OTHERS => '0');
		rd_data_fifo_out : IN std_logic_vector((APPDATA_WIDTH - 1) DOWNTO 0) := (OTHERS => '0');
		app_wdf_data : OUT std_logic_vector((APPDATA_WIDTH - 1) DOWNTO 0) := (OTHERS => '0');
		app_wdf_mask_data : OUT std_logic_vector((APPDATA_WIDTH/8 - 1) DOWNTO 0) := (OTHERS => '0')
		);
	END DDRfifo;
	ARCHITECTURE Behavioral OF DDRfifo IS
	
		SIGNAL app_wdf_wren_i : std_logic := '0';
		SIGNAL dout_i :  std_logic_vector(8 DOWNTO 0) := (OTHERS => '0');
	
		--- DDR controller application interface signals --
		SIGNAL phy_init_done_i : std_logic := '0';
		SIGNAL rst0_tb_i : std_logic := '0';
		SIGNAL ddr_write_addr : std_logic_vector(FIFO_SIZE_POW2 DOWNTO 0) := (OTHERS => '0');
		SIGNAL ddr_read_addr : std_logic_vector(FIFO_SIZE_POW2 DOWNTO 0) := (OTHERS => '0');
		CONSTANT ddr_max_addr : std_logic_vector(FIFO_SIZE_POW2 DOWNTO 0) := (OTHERS => '1');
		CONSTANT WRITE_CMD : std_logic_vector(2 DOWNTO 0) := "000";
		CONSTANT READ_CMD : std_logic_vector(2 DOWNTO 0) := "001";
		CONSTANT SOF : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FB"; -- K27.7
		CONSTANT EOF : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FD"; -- K29.7
		CONSTANT ERR : STD_LOGIC_VECTOR(7 DOWNTO 0) := X"FE";
		TYPE FIFOstate_t IS (IDLE, WRITE_CYCLE0, WRITE_CYCLE1, READ_CYCLE0);
		SIGNAL FIFOstate : FIFOstate_t := IDLE;
		SIGNAL empty_i : std_logic := '1';
		SIGNAL full_i : std_logic := '1';
		SIGNAL full_ii : std_logic := '1';
		SIGNAL full_ii_sync : std_logic := '1';
		SIGNAL full_iii_sync : std_logic := '1';
		SIGNAL full_from_user : std_logic := '0';
		SIGNAL full_from_user_assert : std_logic := '0';
		SIGNAL wr_en_from_user : std_logic := '0';
		SIGNAL din_from_user : std_logic_vector(127 DOWNTO 0);
		SIGNAL din_from_user_0 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_1 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_2 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_3 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_4 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_5 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_6 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_7 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_8 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_9 : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_a : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_b : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_c : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_d : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_e : std_logic_vector(8 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL din_from_user_state : std_logic_vector(3 DOWNTO 0) := (OTHERS => '0'); -- to form microframes
		SIGNAL din_from_user_count : std_logic_vector(3 DOWNTO 0) := (OTHERS => '0'); -- for MSB byte, microframe header
		SIGNAL din_from_user_relcntr : std_logic_vector(2 DOWNTO 0) := (OTHERS => '0'); -- for MSB byte, microframe header
		SIGNAL din_from_user_isEOF : std_logic := '0'; -- for MSB byte, microframe header
		SIGNAL din_from_user_align_cntr : std_logic := '0';
		SIGNAL din_from_user_align : std_logic := '0';
		SIGNAL din_from_user_inc : std_logic := '0';
		SIGNAL SOF_recved_from_user : std_logic := '0';
		SIGNAL rd_en_to_ddr : std_logic := '0';
		SIGNAL empty_to_ddr : std_logic := '0';
		SIGNAL almost_empty_to_ddr : std_logic := '0';
		SIGNAL dout_to_ddr : std_logic_vector(127 DOWNTO 0) := (OTHERS => '0');
		SIGNAL dout_to_user : std_logic_vector(127 DOWNTO 0) := (OTHERS => '0');
		ATTRIBUTE keep : BOOLEAN;
		ATTRIBUTE keep OF dout_to_user : SIGNAL IS true;
		SIGNAL dout_to_user_0 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_1 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_2 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_3 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_4 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_5 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_6 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_7 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_8 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_9 : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_a : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_b : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_c : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_d : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL dout_to_user_e : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0'); -- for combining
		SIGNAL data_dout_to_user_i : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0');
		SIGNAL data_dout_to_user_ii : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0');
		SIGNAL dout_to_user_state : std_logic_vector(3 DOWNTO 0) := X"0";
		SIGNAL dout_to_user_count : std_logic_vector(3 DOWNTO 0) := (OTHERS => '0'); -- for MSB byte, microframe header
		SIGNAL dout_to_user_relcntr : std_logic_vector(2 DOWNTO 0) := (OTHERS => '0'); -- for MSB byte, microframe header
		SIGNAL dout_to_user_this_relcntr : std_logic_vector(2 DOWNTO 0) := (OTHERS => '0'); -- for MSB byte, microframe header
		SIGNAL dout_to_user_isEOF : std_logic := '0'; -- for MSB byte, microframe header
		SIGNAL SOF_sent_to_user : std_logic_vector(1 DOWNTO 0) := (OTHERS => '0');
		SIGNAL dropping_to_user : std_logic := '0';
		SIGNAL rd_en_to_user : std_logic := '0';
		SIGNAL empty_to_user : std_logic := '0';
		SIGNAL empty_to_user_prog : std_logic := '0';
		SIGNAL wr_en_from_ddr : std_logic := '0';
		SIGNAL full_from_ddr : std_logic := '0';
		SIGNAL full_from_ddr_assert : std_logic := '0';
		SIGNAL din_from_ddr : std_logic_vector(127 DOWNTO 0) := (OTHERS => '0');
	BEGIN
		PROCESS (clk0_tb) BEGIN
		IF rising_edge(clk0_tb) THEN
			phy_init_done_i <= phy_init_done;
			rst0_tb_i <= rst0_tb;
		END IF;
	END PROCESS;
	FIFO_DDR_WRITE : ENTITY WORK.FIFO_DDR_WRITE
		PORT MAP(
			rst           => rst,
			wr_clk        => wr_clk,
			din           => din_from_user,
			wr_en         => wr_en_from_user,
			full          => full_from_user_assert,
			prog_full     => full_from_user,
			--TODO: full_from_user should be wire to prog_full and the prog level set to MTU!
			rd_clk        => clk0_tb,
			rd_en         => rd_en_to_ddr,
			dout          => dout_to_ddr,
			empty         => empty_to_ddr,
			almost_empty  => almost_empty_to_ddr
		);
	FIFO_DDR_READ : ENTITY WORK.FIFO_DDR_READ
		PORT MAP(
			rst         => rst,
			wr_clk      => clk0_tb,
			din         => din_from_ddr,
			wr_en       => wr_en_from_ddr,
			full        => full_from_ddr_assert,
			prog_full   => full_from_ddr,
			rd_clk      => rd_clk,
			rd_en       => rd_en_to_user,
			dout        => dout_to_user,
			empty       => empty_to_user,
			--almost_empty => empty_to_user_prog
			prog_empty  => empty_to_user_prog
		);
	-- FIFO 128bit to user 9bit
	dout_to_user_0 <= dout_to_user(7 DOWNTO 0);
	dout_to_user_1 <= dout_to_user(15 DOWNTO 8);
	dout_to_user_2 <= dout_to_user(23 DOWNTO 16);
	dout_to_user_3 <= dout_to_user(31 DOWNTO 24);
	dout_to_user_4 <= dout_to_user(39 DOWNTO 32);
	dout_to_user_5 <= dout_to_user(47 DOWNTO 40);
	dout_to_user_6 <= dout_to_user(55 DOWNTO 48);
	dout_to_user_7 <= dout_to_user(63 DOWNTO 56);
	dout_to_user_8 <= dout_to_user(71 DOWNTO 64);
	dout_to_user_9 <= dout_to_user(79 DOWNTO 72);
	dout_to_user_a <= dout_to_user(87 DOWNTO 80);
	dout_to_user_b <= dout_to_user(95 DOWNTO 88);
	dout_to_user_c <= dout_to_user(103 DOWNTO 96);
	dout_to_user_d <= dout_to_user(111 DOWNTO 104);
	dout_to_user_e <= dout_to_user(119 DOWNTO 112);
	dout_to_user_isEOF <= dout_to_user(127);
	dout_to_user_relcntr <= dout_to_user(126 DOWNTO 124);
	dout_to_user_count <= dout_to_user(123 DOWNTO 120);
	empty_prog <= empty_to_user_prog OR empty_i;
	empty <= empty_i;
	PROCESS (rd_clk) BEGIN
	IF rising_edge(rd_clk) THEN
		rd_en_to_user <= '0';
		IF rst = '1' THEN
			dout_to_user_state <= X"0";
			SOF_sent_to_user <= "10";
			empty_i <= '1';
		ELSE
			IF dropping_to_user = '1' THEN
				empty_i <= '1';
				dout_i <= '1' & SOF;
				IF empty_to_user = '0' THEN
					IF dout_to_user_isEOF = '1' THEN
						rd_en_to_user <= '0';
						SOF_sent_to_user <= "10";
						dropping_to_user <= '0';
						dout_to_user_state <= X"0";
						dout_to_user_this_relcntr <= dout_to_user_relcntr;
						empty_i <= empty_to_user_prog;
					ELSE
						rd_en_to_user <= '1';
					END IF;
				END IF;
			ELSIF dout_to_user_isEOF = '1' AND
				dout_to_user_this_relcntr /= dout_to_user_relcntr AND
				empty_to_user = '0' THEN
				--this handles internal 2x128bit alignment
				rd_en_to_user <= '1'; --fetch next 128bits
				dout_to_user_this_relcntr <= dout_to_user_relcntr; -- ensure this is really one shot rden!
				dout_to_user_state <= X"0";
				SOF_sent_to_user <= "00";
				empty_i <= empty_to_user_prog;
				dout_i <= '1' & SOF;
			ELSIF SOF_sent_to_user /= "00" AND empty_to_user = '0' THEN
				SOF_sent_to_user <= SOF_sent_to_user - "1";
				IF (SOF_sent_to_user - "1") = "00" THEN
					dout_to_user_this_relcntr <= dout_to_user_relcntr;
					dout_to_user_state <= X"0";
					empty_i <= empty_to_user_prog;
					dout_i <= '1' & SOF;
				ELSE
					empty_i <= '1';
				END IF;
			ELSIF rd_en = '1' AND SOF_sent_to_user = "00" THEN
				IF dout_to_user_relcntr = dout_to_user_this_relcntr THEN
					CASE dout_to_user_state IS
						WHEN X"0" =>
							dout_i <= '0' & dout_to_user_0;
						WHEN X"1" =>
							dout_i <= '0' & dout_to_user_1;
						WHEN X"2" =>
							dout_i <= '0' & dout_to_user_2;
						WHEN X"3" =>
							dout_i <= '0' & dout_to_user_3;
						WHEN X"4" =>
							dout_i <= '0' & dout_to_user_4;
						WHEN X"5" =>
							dout_i <= '0' & dout_to_user_5;
						WHEN X"6" =>
							dout_i <= '0' & dout_to_user_6;
						WHEN X"7" =>
							dout_i <= '0' & dout_to_user_7;
						WHEN X"8" =>
							dout_i <= '0' & dout_to_user_8;
						WHEN X"9" =>
							dout_i <= '0' & dout_to_user_9;
						WHEN X"a" =>
							dout_i <= '0' & dout_to_user_a;
						WHEN X"b" =>
							dout_i <= '0' & dout_to_user_b;
						WHEN X"c" =>
							dout_i <= '0' & dout_to_user_c;
						WHEN X"d" =>
							dout_i <= '0' & dout_to_user_d;
						WHEN OTHERS =>
							IF dout_to_user_isEOF = '0' THEN
								dout_i <= '0' & data_dout_to_user_i;
							ELSE
								dout_i <= '0' & dout_to_user_e;
							END IF;
					END CASE;
					IF dout_to_user_isEOF = '0' THEN
						IF ((dout_to_user_state + X"1") >= dout_to_user_count) THEN
								dout_to_user_state <= X"0";
						ELSE
							dout_to_user_state <= dout_to_user_state + X"1";
							IF ((dout_to_user_state + X"2") >= dout_to_user_count) THEN
									data_dout_to_user_i <= dout_to_user_e;
								rd_en_to_user <= '1'; --fetch next 128bits
							END IF;
						END IF;
					ELSE -- EOF microframe, keep the order of case vs. this if-else
						IF (dout_to_user_state >= dout_to_user_count) THEN
							SOF_sent_to_user <= "10";
							dout_to_user_state <= X"0";
							dout_i <= '1' & EOF;
							IF empty_to_user = '0' THEN
								rd_en_to_user <= '1'; --fetch next 128bits
							END IF;
							empty_i <= '1';
						ELSE
							dout_to_user_state <= dout_to_user_state + X"1";
						END IF;
					END IF; -- END dout_to_user_isEOF
				ELSE -- (dout_to_user_relcntr = dout_to_user_this_relcntr)
					dropping_to_user <= '1';
				END IF; -- END (dout_to_user_relcntr = dout_to_user_this_relcntr)
			ELSE --rd_en = '0';
				IF empty_to_user_prog = '1' THEN
					IF dout_to_user_state = X"0" THEN --! we are not in the middle of microframe?
							empty_i <= '1';
					END IF;
				ELSE
					empty_i <= '0';
				END IF;
			END IF; -- END rd_en
		END IF; -- NOT RESET
	END IF; -- END RISING EDGE
END PROCESS;
-- User 9bit to FIFO 128bit
din_from_user <= din_from_user_isEOF & din_from_user_relcntr & din_from_user_count &
	din_from_user_e(7 DOWNTO 0) & din_from_user_d(7 DOWNTO 0) & din_from_user_c(7 DOWNTO 0) & din_from_user_b(7 DOWNTO 0) &
	din_from_user_a(7 DOWNTO 0) & din_from_user_9(7 DOWNTO 0) & din_from_user_8(7 DOWNTO 0) & din_from_user_7(7 DOWNTO 0) &
	din_from_user_6(7 DOWNTO 0) & din_from_user_5(7 DOWNTO 0) & din_from_user_4(7 DOWNTO 0) & din_from_user_3(7 DOWNTO 0) &
	din_from_user_2(7 DOWNTO 0) & din_from_user_1(7 DOWNTO 0) & din_from_user_0(7 DOWNTO 0);
sync_full_ii_wr_clk : ENTITY WORK.SignalSynchronizer
	PORT MAP(sig_clk1(0) => full_ii, clk1 => clk0_tb, clk2 => wr_clk, sig_clk2(0) => full_ii_sync, arst => rst);
   -- for not overflowing on init
sync_full_iii_wr_clk : ENTITY WORK.SignalSynchronizer
	PORT MAP(sig_clk1(0) => phy_init_done_i, clk1 => clk0_tb, clk2 => wr_clk, sig_clk2(0) => full_iii_sync, arst => rst);
	-- combine all 'not ready' signals
	full <= full_i OR full_ii_sync OR (NOT full_iii_sync);

	PROCESS (wr_clk) BEGIN
	IF rising_edge(wr_clk) THEN
-- synopsys translate_off
-- pragma translate_off
-- synthesis translate_off
		assert not(full_from_user_assert = '1' AND wr_en_from_user = '1')
			report "Simulation Finished" severity failure;
-- synopsys translate_on
-- pragma translate_on
-- synthesis translate_on
		-- default assignment
		wr_en_from_user <= '0';
		din_from_user_isEOF <= '0';
		IF rst = '1' THEN
			din_from_user_state <= X"0";
			din_from_user_relcntr <= "000";
			din_from_user_align_cntr <= '0';
			din_from_user_align <= '0';
			SOF_recved_from_user <= '0';
			din_from_user_inc <= '0';
		ELSE
			IF din_from_user_inc = '1' THEN
				din_from_user_inc <= '0';
				din_from_user_relcntr <= din_from_user_relcntr + "001";
				IF din_from_user_align = '1' THEN
					-- send alignment microframe - see UG96 page 452
					-- every write to addresss FIFO with WRITE command
					-- has to be followed by 2 writes to data FIFO.
					-- Pad with dummy EOF microframe with frame counter
					-- incremented -> this will be dropped at the output
					-- as an invalid, padding microframe.
					din_from_user_align <= '0';
					din_from_user_isEOF <= '1';
					din_from_user_count <= (OTHERS => '0');
					wr_en_from_user <= '1';
					din_from_user_align_cntr <= NOT din_from_user_align_cntr;
					din_from_user_0 <= '0' & X"12";
					din_from_user_1 <= '0' & X"34";
					din_from_user_2 <= '0' & X"56";
					din_from_user_3 <= '0' & X"78";
					din_from_user_4 <= '0' & X"9A";
					din_from_user_5 <= '0' & X"BC";
					din_from_user_6 <= '0' & X"DE";
					din_from_user_7 <= '0' & X"F0";
					din_from_user_8 <= '0' & X"12";
					din_from_user_9 <= '0' & X"34";
					din_from_user_A <= '0' & X"56";
					din_from_user_B <= '0' & X"78";
					din_from_user_C <= '0' & X"9A";
					din_from_user_D <= '0' & X"BC";
					din_from_user_E <= '0' & X"DE";
				END IF;
			END IF;
			IF wr_en = '1' THEN
				IF SOF_recved_from_user = '0' THEN
					IF din = '1' & SOF THEN
						din_from_user_state <= X"0";
						SOF_recved_from_user <= '1';
					END IF;
				ELSE
					IF din(8) = '1' THEN --EOF or ERR
						SOF_recved_from_user <= '0';
						din_from_user_isEOF <= '1';
						din_from_user_inc <= '1';
						din_from_user_count <= din_from_user_state;
						wr_en_from_user <= '1';
						din_from_user_align_cntr <= NOT din_from_user_align_cntr;
						full_i <= full_from_user;
						IF din_from_user_align_cntr = '0' THEN
							din_from_user_align <= '1';
						END IF;
					ELSE
						din_from_user_state <= din_from_user_state + X"1";
						CASE din_from_user_state IS
							WHEN X"0" =>
								din_from_user_0 <= din;
							WHEN X"1" =>
								din_from_user_1 <= din;
							WHEN X"2" =>
								din_from_user_2 <= din;
							WHEN X"3" =>
								din_from_user_3 <= din;
							WHEN X"4" =>
								din_from_user_4 <= din;
							WHEN X"5" =>
								din_from_user_5 <= din;
							WHEN X"6" =>
								din_from_user_6 <= din;
							WHEN X"7" =>
								din_from_user_7 <= din;
							WHEN X"8" =>
								din_from_user_8 <= din;
							WHEN X"9" =>
								din_from_user_9 <= din;
							WHEN X"a" =>
								din_from_user_a <= din;
							WHEN X"b" =>
								din_from_user_b <= din;
							WHEN X"c" =>
								din_from_user_c <= din;
							WHEN X"d" =>
								din_from_user_d <= din;
							WHEN OTHERS =>
								din_from_user_e <= din;
								din_from_user_state <= X"0";
								din_from_user_count <= X"F";
								wr_en_from_user <= '1';
								din_from_user_align_cntr <= NOT din_from_user_align_cntr;
								full_i <= full_from_user;
						END CASE;
					END IF; --din(8) = '1'
				END IF; --SOF_recved_from_user = '0'
			ELSE --wr_en = '0'
				full_i <= full_from_user;
			END IF;
		END IF;
	END IF;
END PROCESS;
-- Read from input FIFO (128bit) and writing to the DDR
-- and reading from the DDR and writing to the output FIFO
PROCESS (clk0_tb)
VARIABLE try_read_ddr : INTEGER := 0;
VARIABLE try_write_ddr : INTEGER := 0;
BEGIN
	try_read_ddr := 0;
	try_write_ddr := 0;
	IF rising_edge(clk0_tb) THEN
		rd_en_to_ddr <= '0';
		app_wdf_wren_i <= '0';
		app_af_wren <= '0';
		app_af_addr <= (OTHERS => '0');
		IF rst0_tb_i = '1' THEN
			FIFOstate <= IDLE;
			ddr_write_addr <= (OTHERS => '0');
			ddr_read_addr <=  (OTHERS => '0');
		ELSE
			CASE FIFOstate IS
				WHEN IDLE =>
					IF phy_init_done_i = '1' AND app_wdf_afull = '0' AND app_af_afull = '0' THEN
						try_read_ddr := 1;
						try_write_ddr := 1;
					END IF;
				WHEN WRITE_CYCLE0 =>
					-- we can proceed to next write states
					app_af_cmd <= WRITE_CMD;
					app_af_addr(FIFO_SIZE_POW2 DOWNTO 0) <= ddr_write_addr;
					ddr_write_addr <= ddr_write_addr + X"8";
					app_af_wren <= '1';
					app_wdf_wren_i <= '1';
					rd_en_to_ddr <= '1';
					FIFOstate <= WRITE_CYCLE1;
				WHEN WRITE_CYCLE1 =>
					try_write_ddr := 1;
					try_read_ddr := 1;
					app_wdf_wren_i <= '1';
					FIFOstate <= IDLE;
				WHEN READ_CYCLE0 =>
					IF conv_integer(unsigned(ddr_write_addr)) = conv_integer(unsigned(ddr_read_addr + X"8")) THEN
							try_write_ddr := 1;
						FIFOstate <= IDLE;
					ELSE
						app_af_cmd <= READ_CMD;
						app_af_addr(FIFO_SIZE_POW2 DOWNTO 0) <= ddr_read_addr + X"8";
						app_af_wren <= '1';
						ddr_read_addr <= ddr_read_addr + X"8";
						try_write_ddr := 1;
						try_read_ddr := 1;
						FIFOstate <= IDLE;
					END IF;
				WHEN OTHERS =>
					FIFOstate <= IDLE;
			END CASE;
			-- Set FULL flag if there is less than 16384 bytes of space left, this number must be
			-- greater than the length of the FIFO_DDR_WRITE FIFO, since after the FULL flag is set,
			-- this FIFO continues to be emptied to the DDR circular buffer. Overflow must not occur.
			IF conv_integer(unsigned(ddr_write_addr)) > conv_integer(unsigned(ddr_read_addr)) THEN
				IF (conv_integer(unsigned(ddr_max_addr)) - 
				   (conv_integer(unsigned(ddr_write_addr)) - conv_integer(unsigned(ddr_read_addr))) ) < 16384 THEN
					full_ii <= '1';
				ELSE
					full_ii <= '0';
				END IF;
			ELSIF conv_integer(unsigned(ddr_write_addr)) < conv_integer(unsigned(ddr_read_addr)) THEN
				IF (conv_integer(unsigned(ddr_read_addr)) - conv_integer(unsigned(ddr_write_addr))) < 16384 THEN
					full_ii <= '1';
				ELSE
					full_ii <= '0';
				END IF;
			ELSE -- we are empty, ddr_write_addr == ddr_read_addr
				full_ii <= '0';
			END IF;
			IF phy_init_done_i = '1' THEN
				IF try_write_ddr = 1 THEN
					IF almost_empty_to_ddr = '0' AND app_wdf_afull = '0' AND app_af_afull = '0' AND full_ii = '0' THEN
						try_read_ddr := 0;
						rd_en_to_ddr <= '1';
						FIFOstate <= WRITE_CYCLE0;
					ELSE
						try_read_ddr := 1;
						FIFOstate <= IDLE;
					END IF;
				END IF;
				IF try_read_ddr = 1 THEN
					IF full_from_ddr = '0' AND app_af_afull = '0' THEN
						IF conv_integer(unsigned(ddr_write_addr)) /= conv_integer(unsigned(ddr_read_addr)) THEN
							--we have something to read
							try_write_ddr := 0;
							FIFOstate <= READ_CYCLE0;
						ELSE
							try_write_ddr := 1;
							FIFOstate <= IDLE;
						END IF;
					ELSE
						try_write_ddr := 1;
						FIFOstate <= IDLE;
					END IF;
				END IF;
			END IF; -- phy_init_done_i
		END IF; -- if reset
	END IF; -- if rising endge
END PROCESS;
PROCESS (clk0_tb) BEGIN
IF rising_edge(clk0_tb) THEN
	IF rd_data_valid = '1' THEN
		din_from_ddr <= rd_data_fifo_out;
		wr_en_from_ddr <= '1';
	ELSE
		wr_en_from_ddr <= '0';
	END IF;
END IF;
END PROCESS;
sys_rst_n <= NOT rst; --assign asynchronously - clk0_tb is not available yet!
app_wdf_data <= dout_to_ddr;
app_wdf_mask_data <= (OTHERS => '0');
app_wdf_wren <= app_wdf_wren_i;
dout <= dout_i;

--mmmmmmm                 m           mmmmm                       #     
--   #     mmm    mmm   mm#mm         #    #  mmm   m mm    mmm   # mm  
--   #    #"  #  #   "    #           #mmmm" #"  #  #"  #  #"  "  #"  # 
--   #    #""""   """m    #           #    # #""""  #   #  #      #   # 
--   #    "#mm"  "mmm"    "mm         #mmmm" "#mm"  #   #  "#mm"  #   # 



-- synopsys translate_off
-- pragma translate_off
-- synthesis translate_off
PROCESS (rd_clk)
variable state_ddr_out : integer := 0;
variable last_fifo_out : std_logic_vector(8 downto 0) := (OTHERS => '0');
BEGIN
	IF rising_edge(rd_clk) THEN
		CASE(state_ddr_out) IS
			WHEN 0 =>
				IF rd_en = '1' THEN
					IF dout_i /= '1' & SOF THEN
						assert false report "Error FIFO out, SOF expected" severity failure;
					END IF;
					last_fifo_out := dout_i;
					state_ddr_out := 1;
				END IF;
				
			WHEN 1 =>
				IF rd_en = '1' THEN
					IF last_fifo_out = '1' & SOF THEN
						IF dout_i = '1' & SOF THEN
							assert false report "Error FIFO out, expected SOF" severity failure;
						END IF;
					ELSIF last_fifo_out(8) = '0' THEN
						IF dout_i(8) = '1' AND
							(dout_i(7 downto 0) = EOF OR dout_i(7 downto 0) = ERR) THEN
							state_ddr_out := 0;
						ELSIF dout_i(8) = '1' THEN
							assert false report "Error FIFO out, expected EOF or ERR" severity failure;
						END IF;
					END IF;
					last_fifo_out := dout_i;
				END IF;			
			
			WHEN others => 
			state_ddr_out := 0;
		END CASE;
	
	END IF;
END PROCESS;


PROCESS (clk0_tb) 
-- for init, using singleton technique
variable state_in : integer := 0;
variable last_microframe_in : std_logic_vector((APPDATA_WIDTH - 1) DOWNTO 0) := (OTHERS => '0');
variable microframe_cnt_in : integer := 0;

variable state_out : integer := 0;
variable last_microframe_out : std_logic_vector((APPDATA_WIDTH - 1) DOWNTO 0) := (OTHERS => '0');
variable microframe_cnt_out : integer := 0;
BEGIN
	IF rising_edge(clk0_tb) THEN
		assert not(full_from_ddr_assert = '1' AND wr_en_from_ddr = '1')
			report "Simulation Finished" severity failure;
		
		CASE(state_in) IS
			WHEN 0 => --init
			IF app_wdf_wren_i = '1' THEN
				microframe_cnt_in := microframe_cnt_in + 1;
				last_microframe_in := dout_to_ddr;
				state_in := 1;
			END IF;
			
			WHEN 1 => --data
			IF app_wdf_wren_i = '1' THEN
				microframe_cnt_in := microframe_cnt_in + 1;
				
				IF dout_to_ddr(126 DOWNTO 124) = last_microframe_in(126 DOWNTO 124) THEN				
					IF dout_to_ddr(127) = '1' THEN -- is EOF?
						state_in := 2;
					END IF;
				ELSE
					assert false report "Error DDR in, no/bad EOF" severity failure;
				END IF;
				last_microframe_in := dout_to_ddr;
			END IF;
			
			WHEN 2 => --EOF
			IF app_wdf_wren_i = '1' THEN
				microframe_cnt_in := microframe_cnt_in + 1;
				
				IF (microframe_cnt_in mod 2) = 0 THEN --this is alignment?
					IF not (dout_to_ddr(127) = '1' AND
						dout_to_ddr(126 DOWNTO 124) /= last_microframe_in(126 DOWNTO 124)) THEN 
						assert false report "Error DDR in, alignment" severity failure;
					END IF;
				END IF;
				state_in := 1;
				last_microframe_in := dout_to_ddr;
			END IF;	
			
			WHEN others =>
			state_in := 0;
			
		END CASE;
		
		CASE(state_out) IS
			WHEN 0  => --init
			IF rd_data_valid = '1' THEN
				microframe_cnt_out := microframe_cnt_out + 1;
				last_microframe_out := rd_data_fifo_out;
				state_out := 1;
			END IF;
			
			WHEN 1 => --data
			IF rd_data_valid = '1' THEN
				microframe_cnt_out := microframe_cnt_out + 1;		
				IF rd_data_fifo_out(126 DOWNTO 124) = last_microframe_out(126 DOWNTO 124) THEN				
					IF rd_data_fifo_out(127) = '1' THEN -- is EOF?
						state_out := 2;
					END IF;
				ELSE
					assert false report "Error DDR out, no/bad EOF" severity failure;
				END IF;
				last_microframe_out := rd_data_fifo_out;
			END IF;
			
			WHEN 2 => --EOF
			IF rd_data_valid = '1' THEN
				microframe_cnt_out := microframe_cnt_out + 1;
				IF (microframe_cnt_out mod 2) = 0 THEN --this is alignment?
					IF not (rd_data_fifo_out(127) = '1' AND
						rd_data_fifo_out(126 DOWNTO 124) /= last_microframe_out(126 DOWNTO 124)) THEN 
						assert false report "Error DDR out, alignment" severity failure;
					END IF;
				END IF;
				state_out := 1;
				last_microframe_out := rd_data_fifo_out;
			END IF;	
			
			WHEN others =>
			state_out := 0;
			
		END CASE;
	END IF;
END PROCESS;
-- synopsys translate_on
-- pragma translate_on
-- synthesis translate_on






END Behavioral;
