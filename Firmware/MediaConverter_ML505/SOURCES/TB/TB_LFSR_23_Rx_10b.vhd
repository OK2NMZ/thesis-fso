--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
--USE ieee.numeric_std.ALL;
--------------------------------------------------------------------------------
ENTITY TB_LFSR_23_Rx_10b IS
END TB_LFSR_23_Rx_10b;
--------------------------------------------------------------------------------
ARCHITECTURE behavior OF TB_LFSR_23_Rx_10b IS
--------------------------------------------------------------------------------

  COMPONENT LFSR_23_Tx_10b
  PORT(
    clk                     : IN  STD_LOGIC;
    Data_out                : OUT STD_LOGIC_VECTOR( 9 DOWNTO 0));       -- transmit LSB first, MSB last
  END COMPONENT;

  ------------------------------------------------------------------------------

  COMPONENT LFSR_23_Rx_10b
  PORT(
    clk_Rx                  : IN  STD_LOGIC;                        -- Rx data clock
    data_Rx                 : IN  STD_LOGIC_VECTOR(9 DOWNTO 0);     -- Rx data vector; LSB is the first received (oldest) bit

    clk_sys                 : IN  STD_LOGIC;                        -- system (MCU) clock
    IRQ_1s                  : OUT STD_LOGIC;                        -- 1s interrupt request
    toggle_1s               : OUT STD_LOGIC;                        -- toggle each 1s (for data consistency check)
    Err_cnt_1s              : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);    -- number of errors in last 1s
    LFSR_locked             : OUT STD_LOGIC);                       -- PRBS is detected correctly (BER < 10%)
  END COMPONENT;

  ------------------------------------------------------------------------------

  CONSTANT clk_Rx_period    : TIME :=  8 ns;        -- 125 MHz
  CONSTANT clk_sys_period   : TIME := 10 ns;        -- 100 MHz

  ------------------------------------------------------------------------------

  SIGNAL clk_Rx             : STD_LOGIC := '0';
  SIGNAL data_Tx            : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
  SIGNAL data_Rx            : STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
  SIGNAL clk_sys            : STD_LOGIC := '0';
  SIGNAL IRQ_1s             : STD_LOGIC;                        -- 1s interrupt request
  SIGNAL toggle_1s          : STD_LOGIC;                        -- toggle each 1s (for data consistency check)
  SIGNAL Err_cnt_1s         : STD_LOGIC_VECTOR(31 DOWNTO 0);    -- number of errors in last 1s
  SIGNAL LFSR_locked        : STD_LOGIC := '0';

  ------------------------------------------------------------------------------

  SIGNAL MUX_data           : STD_LOGIC_VECTOR(3 DOWNTO 0) := X"0";

--------------------------------------------------------------------------------
BEGIN
--------------------------------------------------------------------------------

  ------------------------------------------------------------------------------
  -- Instantiate UUT
  ------------------------------------------------------------------------------

  LFSR_23_Tx_10b_i : LFSR_23_Tx_10b
  PORT MAP(
    clk                     => clk_Rx,
    Data_out                => data_Tx);


  LFSR_23_Rx_10b_i : LFSR_23_Rx_10b
  PORT MAP(
    clk_Rx                  => clk_Rx,
    data_Rx                 => data_Rx,
    clk_sys                 => clk_sys,
    IRQ_1s                  => IRQ_1s,
    toggle_1s               => toggle_1s,
    Err_cnt_1s              => Err_cnt_1s,
    LFSR_locked             => LFSR_locked);

  ------------------------------------------------------------------------------
  -- Data MUX
  ------------------------------------------------------------------------------

  PROCESS(data_Tx, MUX_data) BEGIN
    CASE MUX_data IS
      WHEN X"0"     => data_Rx <= data_Tx;
      WHEN X"1"     => data_Rx <= NOT data_Tx;
      WHEN X"2"     => data_Rx <= "0000000000";
      WHEN X"3"     => data_Rx <= "1111111111";
      WHEN X"4"     => data_Rx(9 DOWNTO 1) <= data_Tx(9 DOWNTO 1); data_Rx(0 DOWNTO 0) <= NOT data_Tx(0 DOWNTO 0); 
      WHEN X"5"     => data_Rx(9 DOWNTO 2) <= data_Tx(9 DOWNTO 2); data_Rx(1 DOWNTO 0) <= NOT data_Tx(1 DOWNTO 0); 
      WHEN OTHERS   => data_Rx <= data_Tx;
    END CASE;
  END PROCESS;

  ------------------------------------------------------------------------------
   -- Clock process definitions
  ------------------------------------------------------------------------------

  clk_Rx_process : PROCESS BEGIN
    clk_Rx <= '0'; WAIT FOR clk_Rx_period/2;
    clk_Rx <= '1'; WAIT FOR clk_Rx_period/2;
  END PROCESS;

  clk_sys_process : PROCESS BEGIN
    clk_sys <= '0'; WAIT FOR clk_sys_period/2;
    clk_sys <= '1'; WAIT FOR clk_sys_period/2;
  END PROCESS;

  ------------------------------------------------------------------------------
   -- Stimulus process
  ------------------------------------------------------------------------------

  stim_proc: PROCESS BEGIN
    ----------------------------------------------------------------------------
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"1"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"2"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"3"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"4"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"5"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*100;

    MUX_data <= X"1"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*8;
  --MUX_data <= X"5"; WAIT FOR clk_Rx_period*1;
    MUX_data <= X"4"; WAIT FOR clk_Rx_period*100;
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*100;
    
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*10000;
    MUX_data <= X"4"; WAIT FOR clk_Rx_period*10000;
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*10000;
    MUX_data <= X"5"; WAIT FOR clk_Rx_period*10000;
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*10000;
    MUX_data <= X"1"; WAIT FOR clk_Rx_period*10000;
    MUX_data <= X"0"; WAIT FOR clk_Rx_period*10000;
    
    ----------------------------------------------------------------------------
    REPORT "===========================================================" SEVERITY NOTE;
    REPORT "=======================  Konec simulace  ========================" SEVERITY NOTE;
    REPORT "===========================================================" SEVERITY NOTE;
    REPORT "x" SEVERITY FAILURE;
    ----------------------------------------------------------------------------
  END PROCESS;
  ------------------------------------------------------------------------------

--------------------------------------------------------------------------------
END;
--------------------------------------------------------------------------------
