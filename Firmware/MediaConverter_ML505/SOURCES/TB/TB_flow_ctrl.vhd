LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_textio.all;
use std.textio.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

ENTITY TB_flow_ctrl IS
END TB_flow_ctrl;

ARCHITECTURE behavior OF TB_flow_ctrl IS

    -- Component Declaration for the Unit Under Test (UUT)

    COMPONENT FlowController
    PORT(
         ETH_DATA_IN : IN  std_logic_vector(7 downto 0);
         ETH_DV_IN : IN  std_logic;
         ETH_GF_IN : IN  std_logic;
         ETH_BF_IN : IN  std_logic;
         ETH_CLK_IN : IN  std_logic;
         ETH_DATA_OUT : OUT  std_logic_vector(7 downto 0);
         ETH_DV_OUT : OUT  std_logic;
         ETH_ACK_OUT : IN  std_logic;
         ETH_CLK_OUT : IN  std_logic;
         SFP_DATA_IN : IN  std_logic_vector(7 downto 0);
         SFP_DV_IN : IN  std_logic;
         SFP_GF_IN : IN  std_logic;
         SFP_BF_IN : IN  std_logic;
         SFP_CLK_IN : IN  std_logic;
         SFP_DATA_OUT : OUT  std_logic_vector(7 downto 0);
         SFP_DV_OUT : OUT  std_logic;
         SFP_ACK_OUT : IN  std_logic;
         SFP_CLK_OUT : IN  std_logic;
         UBLAZE_DATA_IN  : in  STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
         UBLAZE_DV_IN    : in  STD_LOGIC := '0';
         UBLAZE_GF_IN    : in STD_LOGIC := '0';
         UBLAZE_BF_IN    : in STD_LOGIC := '0';
         UBLAZE_CLK_IN   : in  STD_LOGIC := '0';
         UBLAZE_DATA_OUT : out  STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
         UBLAZE_DV_OUT   : out  STD_LOGIC := '0';
         UBLAZE_ACK_OUT  : in  STD_LOGIC := '0';
         UBLAZE_CLK_OUT  : in  STD_LOGIC := '0';
         SYS_CLK : IN  std_logic;
         SYS_RST : IN  std_logic
        );
    END COMPONENT;


   --Inputs
   signal ETH_DATA_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal ETH_DV_IN : std_logic := '0';
   signal ETH_GF_IN : std_logic := '0';
   signal ETH_BF_IN : std_logic := '0';
   signal ETH_CLK_IN : std_logic := '0';
   signal ETH_ACK_OUT : std_logic := '0';
   signal ETH_CLK_OUT : std_logic := '0';
   signal SFP_DATA_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal SFP_DV_IN : std_logic := '0';
   signal SFP_GF_IN : std_logic := '0';
   signal SFP_BF_IN : std_logic := '0';
   signal SFP_CLK_IN : std_logic := '0';
   signal SFP_ACK_OUT : std_logic := '0';
   signal SFP_CLK_OUT : std_logic := '0';
   signal SYS_CLK : std_logic := '0';
   signal SYS_RST : std_logic := '0';

   signal UBLAZE_DATA_IN  : STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
   signal UBLAZE_DV_IN    : STD_LOGIC := '0';
   signal UBLAZE_GF_IN    : STD_LOGIC := '0';
   signal UBLAZE_BF_IN    : STD_LOGIC := '0';
   signal UBLAZE_CLK_IN   : STD_LOGIC := '0';

   signal UBLAZE_DATA_OUT : STD_LOGIC_VECTOR (7 downto 0) := (OTHERS => '0');
   signal UBLAZE_DV_OUT   : STD_LOGIC := '0';
   signal UBLAZE_ACK_OUT  : STD_LOGIC := '0';
   signal UBLAZE_CLK_OUT  : STD_LOGIC := '0';



 	--Outputs
   signal ETH_DATA_OUT : std_logic_vector(7 downto 0);
   signal ETH_DV_OUT : std_logic;
   signal SFP_DATA_OUT : std_logic_vector(7 downto 0);
   signal SFP_DV_OUT : std_logic;

   -- Clock period definitions
   constant SYS_CLK_period : time := 20 ns;

   file packet_file   : text open read_mode is "SOURCES/TB/sample_ping.frame";
   file packet_file_2 : text open read_mode is "SOURCES/TB/sample_ping_reply.frame";
   file packet_file_3 : text open read_mode is "SOURCES/TB/sample_vpn_2.frame";
   file packet_file_4 : text open read_mode is "SOURCES/TB/sample_vpn_1.frame";
   file packet_file_5 : text open read_mode is "SOURCES/TB/sample_ub_to_eth.frame";



BEGIN

	-- Instantiate the Unit Under Test (UUT)
   uut: FlowController PORT MAP (
          ETH_DATA_IN => ETH_DATA_IN,
          ETH_DV_IN => ETH_DV_IN,
          ETH_GF_IN => ETH_GF_IN,
          ETH_BF_IN => ETH_BF_IN,
          ETH_CLK_IN => ETH_CLK_IN,
          ETH_DATA_OUT => ETH_DATA_OUT,
          ETH_DV_OUT => ETH_DV_OUT,
          ETH_ACK_OUT => ETH_ACK_OUT,
          ETH_CLK_OUT => ETH_CLK_OUT,
          SFP_DATA_IN => SFP_DATA_IN,
          SFP_DV_IN => SFP_DV_IN,
          SFP_GF_IN => SFP_GF_IN,
          SFP_BF_IN => SFP_BF_IN,
          SFP_CLK_IN   => SFP_CLK_IN,
          SFP_DATA_OUT => SFP_DATA_OUT,
          SFP_DV_OUT   => SFP_DV_OUT,
          SFP_ACK_OUT  => SFP_ACK_OUT,
          SFP_CLK_OUT  => SFP_CLK_OUT,
          UBLAZE_DATA_IN => UBLAZE_DATA_IN,
          UBLAZE_DV_IN => UBLAZE_DV_IN,
          UBLAZE_GF_IN => UBLAZE_GF_IN,
          UBLAZE_BF_IN => UBLAZE_BF_IN,
          UBLAZE_CLK_IN => UBLAZE_CLK_IN,
          UBLAZE_DATA_OUT => UBLAZE_DATA_OUT,
          UBLAZE_DV_OUT   => UBLAZE_DV_OUT,
          UBLAZE_ACK_OUT  => UBLAZE_ACK_OUT,
          UBLAZE_CLK_OUT  => UBLAZE_CLK_OUT,
          SYS_CLK => SYS_CLK,
          SYS_RST => SYS_RST
        );

   -- Clock process definitions
   SYS_CLK_process :process
   begin
		SYS_CLK <= '1';
		wait for SYS_CLK_period/2;
		SYS_CLK <= '0';
		wait for SYS_CLK_period/2;
   end process;

   ETH_CLK_IN     <= SYS_CLK;
   ETH_CLK_OUT    <= SYS_CLK;
   SFP_CLK_IN     <= SYS_CLK;
   SFP_CLK_OUT    <= SYS_CLK;
   UBLAZE_CLK_OUT <= SYS_CLK;
   UBLAZE_CLK_IN  <= SYS_CLK;


   -- Stimulus process
   stim_proc: process

   variable read_line : line;
   variable hex_data  : std_logic_vector(7 downto 0);

   begin
      -- system reset
      SYS_RST <= '1';
      wait for 20 ns;
      SYS_RST <= '0';
      wait for 200 ns;

      --
      -- SEND FIRST FRAME (ETH -> SFP)
      --
      ETH_DV_IN   <= '1';
      -- read file byte by byte until EOF and write it to ETH_DATA_IN
      while not endfile(packet_file) loop
        readline(packet_file, read_line);
        hread(read_line, hex_data);
        ETH_DATA_IN <= hex_data;
        wait for SYS_CLK_period;
      end loop;

      ETH_DV_IN   <= '0';
      wait for SYS_CLK_period;
      ETH_GF_IN   <= '1';
      wait for SYS_CLK_period;
      ETH_GF_IN   <= '0';



      --
      -- SEND SECOND FRAME (ETH -> UBLAZE)
      --
     ETH_DV_IN   <= '1';
     while not endfile(packet_file_2) loop
      readline(packet_file_2, read_line);
      hread(read_line, hex_data);
      ETH_DATA_IN <= hex_data;
      wait for SYS_CLK_period;
     end loop;

     ETH_DV_IN   <= '0';
     wait for SYS_CLK_period;
     ETH_GF_IN   <= '1';
     wait for SYS_CLK_period;
     ETH_GF_IN   <= '0';

      wait;
   end process;

   send_sfp: process

   variable read_line_2 : line;
   variable hex_data_2  : std_logic_vector(7 downto 0);

   begin
     -- system reset
     SYS_RST <= '1';
     wait for 20 ns;
     SYS_RST <= '0';
     wait for 200 ns;

     --
     -- SEND THIRD FRAME (SFP -> ETH with BF = 0)
     --
     SFP_DV_IN   <= '1';
     while not endfile(packet_file_3) loop
       readline(packet_file_3, read_line_2);
       hread(read_line_2, hex_data_2);
       SFP_DATA_IN <= hex_data_2;
       wait for SYS_CLK_period;
     end loop;

     SFP_DV_IN   <= '0';
     wait for SYS_CLK_period;
     SFP_GF_IN   <= '1';
     wait for SYS_CLK_period;
     SFP_GF_IN   <= '0';

     ----
     ---- SEND FOURTH FRAME (ETH -> UBLAZE with BF = 1)
     ----
     --ETH_DV_IN   <= '1';
     --while not endfile(packet_file_4) loop
     --  readline(packet_file_4, read_line);
     --  hread(read_line, hex_data);
     --  ETH_DATA_IN <= hex_data;
     --  wait for SYS_CLK_period;
     --end loop;

     --ETH_DV_IN   <= '0';
     --wait for SYS_CLK_period;
     --ETH_BF_IN   <= '1';
     --wait for SYS_CLK_period;
     --ETH_BF_IN   <= '0';

   end process;

   ublaze_to_eth: process

   variable read_line : line;
   variable hex_data  : std_logic_vector(7 downto 0);

   begin

   wait for 220 ns;

      UBLAZE_DV_IN <= '1';
      while not endfile(packet_file_5) loop
        readline(packet_file_5, read_line);
        hread(read_line, hex_data);
        UBLAZE_DATA_IN <= hex_data;
        wait for SYS_CLK_period;
      end loop;
      UBLAZE_DV_IN <= '0';
      wait for SYS_CLK_period;
      UBLAZE_GF_IN   <= '1';
      wait for SYS_CLK_period;
      UBLAZE_GF_IN   <= '0';
      wait;

   end process;

   -- ACK first TX'ed byte (ETH -> SFP)
   ack_sfp: process
   begin
    if sfp_dv_out = '1' then
      sfp_ack_out <= '1';
      wait for SYS_CLK_period;
      sfp_ack_out <= '0';

      while sfp_dv_out = '1' loop
        wait for SYS_CLK_period;
      end loop;
    end if;
    wait for SYS_CLK_period;
   end process;

      -- ACK first TX'ed byte (SFP -> ETH)
   ack_eth: process
   begin
    if eth_dv_out = '1' then
      eth_ack_out <= '1';
      wait for SYS_CLK_period;
      eth_ack_out <= '0';

      while eth_dv_out = '1' loop
        wait for SYS_CLK_period;
      end loop;
    end if;
    wait for SYS_CLK_period;
   end process;

   -- ACK first TX'ed byte (ETH -> UB)
  ack_ub: process
  begin
   if ublaze_dv_out = '1' then
     ublaze_ack_out <= '1';
     wait for SYS_CLK_period;
     ublaze_ack_out <= '0';

     while ublaze_dv_out = '1' loop
       wait for SYS_CLK_period;
     end loop;
   end if;
   wait for SYS_CLK_period;
  end process;




END;
