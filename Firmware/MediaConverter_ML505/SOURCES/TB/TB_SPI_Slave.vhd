--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
--USE ieee.numeric_std.ALL;
--------------------------------------------------------------------------------
ENTITY TB_SPI_Slave IS
END TB_SPI_Slave;
--------------------------------------------------------------------------------
ARCHITECTURE behavior OF TB_SPI_Slave IS 
--------------------------------------------------------------------------------

  CONSTANT test_write           : BOOLEAN := FALSE;
  CONSTANT test_write_RO        : BOOLEAN := FALSE;
  CONSTANT test_write_burst     : BOOLEAN := FALSE;

  CONSTANT test_read            : BOOLEAN := FALSE;
  CONSTANT test_read_0x1F       : BOOLEAN := TRUE;
  CONSTANT test_read_wrong      : BOOLEAN := TRUE;
  CONSTANT test_read_burst      : BOOLEAN := TRUE;


  ------------------------------------------------------------------------------
  -- Component Declaration for the Unit Under Test (UUT)
  
  COMPONENT SPI_Slave
  PORT(
    clk                 : IN  STD_LOGIC;
    rst                 : IN  STD_LOGIC;
    SPI_SCK             : IN  STD_LOGIC;
    SPI_SDI             : IN  STD_LOGIC;
    SPI_SDO             : OUT STD_LOGIC;
    SPI_CSN             : IN  STD_LOGIC;
    SPI_Global_Status   : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    RAM_Address         : OUT STD_LOGIC_VECTOR( 5 DOWNTO 0);
    RAM_Data_IN         : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0);
    RAM_WR              : OUT STD_LOGIC;
    RAM_Data_OUT        : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0));
  END COMPONENT;

  ------------------------------------------------------------------------------
    
  COMPONENT RAM_SPI_Wrapper
  PORT(
    clk                 : IN  STD_LOGIC;
    RAM_A_Addr_in       : IN  STD_LOGIC_VECTOR( 5 DOWNTO 0);
    RAM_A_Data_in       : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    RAM_A_Wr_EN         : IN  STD_LOGIC;
    RAM_A_Data_out      : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0);
    RAM_B_Addr_in       : IN  STD_LOGIC_VECTOR( 5 DOWNTO 0);
    RAM_B_Data_in       : IN  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    RAM_B_Wr_EN         : IN  STD_LOGIC;
    RAM_B_Data_out      : OUT STD_LOGIC_VECTOR( 7 DOWNTO 0));
  END COMPONENT;

  ------------------------------------------------------------------------------

   --Inputs
   signal clk               : std_logic := '0';
   signal rst               : std_logic := '0';
   signal SPI_SCK           : std_logic := '0';
   signal SPI_SDI           : std_logic := '0';
   signal SPI_CSN           : std_logic := '0';
   signal spi_global_status : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0');

   --Outputs
   signal SPI_SDO           : std_logic;

   signal SPI_RAM_Address   : std_logic_vector(5 DOWNTO 0);
   signal SPI_RAM_Data_IN   : std_logic_vector(7 DOWNTO 0);
   signal SPI_RAM_WR        : std_logic;
   signal SPI_RAM_Data_OUT  : std_logic_vector(7 DOWNTO 0);

   signal Proc_RAM_Address  : std_logic_vector(5 DOWNTO 0) := (OTHERS => '0');
   signal Proc_RAM_Data_IN  : std_logic_vector(7 DOWNTO 0) := (OTHERS => '0');
   signal Proc_RAM_WR       : std_logic := '0';
   signal Proc_RAM_Data_OUT : std_logic_vector(7 DOWNTO 0);

   -- Clock period definitions
   constant clk_period      : time := 10 ns;
   constant spi_period      : time := 100 ns;


  SIGNAL SPI_SCK_i          : STD_LOGIC := '0';
  SIGNAL SPI_SCK_rising     : STD_LOGIC := '0';
  SIGNAL SPI_SCK_falling    : STD_LOGIC := '0';

  SIGNAL SPI_CSN_i          : STD_LOGIC := '0';
  SIGNAL SPI_CSN_rising     : STD_LOGIC := '0';
  SIGNAL SPI_CSN_falling    : STD_LOGIC := '0';


  SIGNAL SPI_check_din_i        : STD_LOGIC_VECTOR( 7 DOWNTO 0) := (OTHERS => '0');
  SIGNAL SPI_check_din_REG      : STD_LOGIC_VECTOR( 7 DOWNTO 0) := (OTHERS => '0');
  SIGNAL SPI_check_dout_i       : STD_LOGIC_VECTOR( 7 DOWNTO 0) := (OTHERS => '0');
  SIGNAL SPI_check_dout_REG     : STD_LOGIC_VECTOR( 7 DOWNTO 0) := (OTHERS => '0');
  TYPE t_st_deserialize IS (
    st_idle,
    st_bit_7,
    st_bit_6,
    st_bit_5,
    st_bit_4,
    st_bit_3,
    st_bit_2,
    st_bit_1,
    st_bit_0,
    st_dout);
  SIGNAL st_deserialize_in      : t_st_deserialize := st_idle;
  SIGNAL st_deserialize_out     : t_st_deserialize := st_idle;

  SIGNAL SPI_command            : STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
  SIGNAL SPI_data_wr            : STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";

--------------------------------------------------------------------------------
BEGIN
--------------------------------------------------------------------------------
 
  -- Instantiate the Unit Under Test (UUT)
  uut_1 : SPI_Slave
  PORT MAP(
    clk                 => clk,
    rst                 => rst,
    SPI_SCK             => SPI_SCK,
    SPI_SDI             => SPI_SDI,
    SPI_SDO             => SPI_SDO,
    SPI_CSN             => SPI_CSN,
    SPI_global_status   => SPI_global_status,
    RAM_Address         => SPI_RAM_Address,
    RAM_Data_IN         => SPI_RAM_Data_IN,
    RAM_WR              => SPI_RAM_WR,
    RAM_Data_OUT        => SPI_RAM_Data_OUT);

  ------------------------------------------------------------------------------

  uut_2 : RAM_SPI_Wrapper
  PORT MAP(
    clk                 => clk,
    RAM_A_Addr_in       => Proc_RAM_Address,
    RAM_A_Data_in       => Proc_RAM_Data_IN,
    RAM_A_Wr_EN         => Proc_RAM_WR,
    RAM_A_Data_out      => Proc_RAM_Data_OUT,
    RAM_B_Addr_in       => SPI_RAM_Address,
    RAM_B_Data_in       => SPI_RAM_Data_IN,
    RAM_B_Wr_EN         => SPI_RAM_WR,
    RAM_B_Data_out      => SPI_RAM_Data_OUT);

  ------------------------------------------------------------------------------

   -- Clock process definitions
  clk_process : PROCESS BEGIN
    clk <= '0'; WAIT FOR clk_period/2;
    clk <= '1'; WAIT FOR clk_period/2;
  END PROCESS;
 
  ------------------------------------------------------------------------------

   -- Stimulus process
  stim_proc: PROCESS BEGIN
  ------------------------------------------------------------------------------
    rst                 <= '1';
    SPI_SCK             <= '0';
    SPI_SDI             <= '0';
    SPI_CSN             <= '1';
    SPI_global_status   <= X"69";
    ----------------------------------------------------------------------------
    WAIT FOR clk_period/10;
    WAIT FOR clk_period*10;
    rst         <= '0';
    WAIT FOR clk_period*10;
    ----------------------------------------------------------------------------

    -- SPI read
    WAIT FOR spi_period * 10;
    IF test_read THEN
      SPI_global_status <= X"81"; 
      SPI_command       <= "01111111";      -- "01" read & "111111" address
      SPI_data_wr       <= "00000000";              -- data from SPI
      WAIT FOR spi_period;
      SPI_CSN <= '0'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '0';WAIT FOR spi_period;
      SPI_CSN <= '1';WAIT FOR spi_period;
      WAIT FOR spi_period * 20;
    END IF;

    ----------------------------------------------------------------------------

    -- SPI read 0x1F (DIPs)
    WAIT FOR spi_period * 10;
    IF test_read_0x1F THEN
      SPI_global_status <= X"81"; 
      SPI_command       <= "01011111";      -- "01" read & "011111" = 0x1F address
      SPI_data_wr       <= "00000000";              -- data from SPI
      WAIT FOR spi_period;
      SPI_CSN <= '0'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '0';WAIT FOR spi_period;
      SPI_CSN <= '1';WAIT FOR spi_period;
      WAIT FOR spi_period * 20;
    END IF;

    ----------------------------------------------------------------------------

    -- invalid frame
    WAIT FOR spi_period * 10;
    IF test_read_wrong THEN
      SPI_global_status <= X"18"; 
      SPI_command       <= "01000111";      -- "01" read & "000111" address
      SPI_data_wr       <= "00000000";              -- data from SPI
      WAIT FOR spi_period;
      SPI_CSN <= '0'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;


      SPI_SCK <= '0';WAIT FOR spi_period;
      SPI_CSN <= '1';WAIT FOR spi_period;
      WAIT FOR spi_period * 20;
    END IF;

    ----------------------------------------------------------------------------

    -- SPI burst read
    WAIT FOR spi_period * 10;
    IF test_read_burst THEN
      SPI_global_status <= X"AA"; 
      SPI_command       <= "01010000";      -- "01" read & "010000" address
      SPI_data_wr       <= "00000000";              -- data from SPI
      WAIT FOR spi_period;
      SPI_CSN <= '0'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      FOR i IN 0 TO 65 LOOP
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      END LOOP;
      SPI_SCK <= '0';WAIT FOR spi_period;
      SPI_CSN <= '1';WAIT FOR spi_period;
      WAIT FOR spi_period * 20;
    END IF;


    ----------------------------------------------------------------------------

    -- SPI write
    WAIT FOR spi_period * 10;
    IF test_write THEN
      SPI_command <= "00111111";                -- "00" write & "111111" address
      SPI_data_wr <= "11000011";                -- data to SPI (0xC3)
      WAIT FOR spi_period;
      SPI_CSN <= '0'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '0';WAIT FOR spi_period;
      SPI_CSN <= '1';WAIT FOR spi_period;
      WAIT FOR spi_period * 20;
    END IF;

    ----------------------------------------------------------------------------

    -- SPI write to read-only sector
    WAIT FOR spi_period * 10;
    IF test_write_RO THEN
      SPI_command <= "00011111";                -- "00" write & "011111" address
      SPI_data_wr <= "00000011";                -- data from SPI
      WAIT FOR spi_period;
      SPI_CSN <= '0'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
      SPI_SCK <= '0';WAIT FOR spi_period;
      SPI_CSN <= '1';WAIT FOR spi_period;
      WAIT FOR spi_period * 20;
    END IF;

    ----------------------------------------------------------------------------

    -- SPI write burst
    WAIT FOR spi_period * 10;
    IF test_write_burst THEN
      SPI_command <= "00010000";                -- "00" write & "010000" address
      SPI_data_wr <= "11000000";                -- data to SPI (0xC0)
      WAIT FOR spi_period;
      SPI_CSN <= '0'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(7); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(6); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(5); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(4); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(3); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(2); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(1); WAIT FOR spi_period;
      SPI_SCK <= '0'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      SPI_SCK <= '1'; SPI_SDI <= SPI_command(0); WAIT FOR spi_period;
      FOR i IN 0 TO 63 LOOP
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(7); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(6); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(5); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(4); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(3); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(2); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(1); WAIT FOR spi_period;
        SPI_SCK <= '0'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
        SPI_SCK <= '1'; SPI_SDI <= SPI_data_wr(0); WAIT FOR spi_period;
        SPI_data_wr <= SPI_data_wr + 1;
      END LOOP;
      SPI_SCK <= '0';WAIT FOR spi_period;
      SPI_CSN <= '1';WAIT FOR spi_period;
      WAIT FOR spi_period * 20;
    END IF;


    ----------------------------------------------------------------------------
    WAIT FOR spi_period * 10;
    REPORT "===========================================================================" SEVERITY NOTE;
    REPORT "================================ Konec simulace ================================" SEVERITY NOTE;
    REPORT "===========================================================================" SEVERITY NOTE;
    REPORT "x" SEVERITY FAILURE;
  END PROCESS;


  ------------------------------------------------------------------------------

  -- Frame data checker (deserialize SPI_SDO data)
  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF SPI_CSN_rising = '1' THEN
        st_deserialize_out  <= st_idle;
        SPI_check_dout_REG  <= SPI_check_dout_i;
      ELSE
        CASE st_deserialize_out IS
          WHEN st_idle      =>  IF SPI_CSN_falling = '1' THEN st_deserialize_out <= st_bit_7; END IF;
          WHEN st_bit_7     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_bit_6; SPI_check_dout_i(7) <= SPI_SDO; END IF;
          WHEN st_bit_6     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_bit_5; SPI_check_dout_i(6) <= SPI_SDO; END IF;
          WHEN st_bit_5     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_bit_4; SPI_check_dout_i(5) <= SPI_SDO; END IF;
          WHEN st_bit_4     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_bit_3; SPI_check_dout_i(4) <= SPI_SDO; END IF;
          WHEN st_bit_3     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_bit_2; SPI_check_dout_i(3) <= SPI_SDO; END IF;
          WHEN st_bit_2     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_bit_1; SPI_check_dout_i(2) <= SPI_SDO; END IF;
          WHEN st_bit_1     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_bit_0; SPI_check_dout_i(1) <= SPI_SDO; END IF;
          WHEN st_bit_0     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_out <= st_dout;  SPI_check_dout_i(0) <= SPI_SDO; END IF;
          WHEN st_dout      =>  SPI_check_dout_REG  <= SPI_check_dout_i;
                                st_deserialize_out  <= st_bit_7;
        END CASE;
      END IF;
    END IF;
  END PROCESS;

  -- Frame data checker (deserialize SPI_SDI data)
  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      IF SPI_CSN_rising = '1' THEN
        st_deserialize_in   <= st_idle;
        SPI_check_din_REG   <= SPI_check_din_i;
      ELSE
        CASE st_deserialize_in IS
          WHEN st_idle      =>  IF SPI_CSN_falling = '1' THEN st_deserialize_in <= st_bit_7; END IF;
          WHEN st_bit_7     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_bit_6; SPI_check_din_i(7) <= SPI_SDI; END IF;
          WHEN st_bit_6     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_bit_5; SPI_check_din_i(6) <= SPI_SDI; END IF;
          WHEN st_bit_5     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_bit_4; SPI_check_din_i(5) <= SPI_SDI; END IF;
          WHEN st_bit_4     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_bit_3; SPI_check_din_i(4) <= SPI_SDI; END IF;
          WHEN st_bit_3     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_bit_2; SPI_check_din_i(3) <= SPI_SDI; END IF;
          WHEN st_bit_2     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_bit_1; SPI_check_din_i(2) <= SPI_SDI; END IF;
          WHEN st_bit_1     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_bit_0; SPI_check_din_i(1) <= SPI_SDI; END IF;
          WHEN st_bit_0     =>  IF SPI_SCK_rising  = '1' THEN st_deserialize_in <= st_dout;  SPI_check_din_i(0) <= SPI_SDI; END IF;
          WHEN st_dout      =>  SPI_check_din_REG   <= SPI_check_din_i;
                                st_deserialize_in   <= st_bit_7;
        END CASE;
      END IF;
    END IF;
  END PROCESS;

  -- SPI_SCK edge detection
  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      SPI_SCK_rising  <= '0';       -- default assignment
      SPI_SCK_falling <= '0';       -- default assignment
      SPI_SCK_i       <= SPI_SCK;
      IF SPI_SCK = '1' AND SPI_SCK_i = '0' THEN SPI_SCK_rising  <= '1'; END IF;
      IF SPI_SCK = '0' AND SPI_SCK_i = '1' THEN SPI_SCK_falling <= '1'; END IF;
    END IF;
  END PROCESS;


  -- SPI_CSN edge detection
  PROCESS(clk) BEGIN
    IF rising_edge(clk) THEN
      SPI_CSN_rising  <= '0';       -- default assignment
      SPI_CSN_falling <= '0';       -- default assignment
      SPI_CSN_i       <= SPI_CSN;
      IF SPI_CSN = '1' AND SPI_CSN_i = '0' THEN SPI_CSN_rising  <= '1'; END IF;
      IF SPI_CSN = '0' AND SPI_CSN_i = '1' THEN SPI_CSN_falling <= '1'; END IF;
    END IF;
  END PROCESS;

--------------------------------------------------------------------------------
END;
--------------------------------------------------------------------------------
